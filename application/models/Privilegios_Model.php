<?php

use phpDocumentor\Reflection\Types\Null_;

class Privilegios_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  /**
   * Devuelve listado de modulos  
   *
   * @access public   
   * @return array [id,nombre]
   */
  public function get_modulos()
  {
    $sql = "SELECT id_modulo as id ,nombre 
            FROM modulo";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function get_idPrivilegio_Byname($name)
  {
    $sql = "SELECT id_privilegios
            FROM privilegios
          Where nombre='$name'";
    $resultados = $this->db->query($sql);
    $dataset = $resultados->result();
    return $dataset[0];
  }

  /**
   * Devuelve listado de tabla crud
   *   
   * este metodo devulve listado de laas insttruccion de un crud   
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_crud()
  {
    $sql = "SELECT id_crud as id ,nombre 
            FROM crud";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * inserta un privilegio
   *      
   * Este metodo inserta un nuevo privilegio
   * 
   * @access public
   * @param string $nombre del privilegio a insertar
   * @return bolean
   */
  public function insert_privilegio($nombre)
  {
    $data = array(
      'id_privilegios' => 'null',
      'nombre' => $nombre,
    );
    $this->db->insert('privilegios', $data);
    return ($this->db->affected_rows() != 1) ? false : true;
  }

  /**
   * Devuelve listado de tabla crud
   *   
   * este metodo devulve listado de los privilegios   
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_privilegio($QuitaAdmin = false)
  {
    $filtro = '';
    if ($QuitaAdmin) {
      $filtro = "Where nombre <> 'Desarrollo' $filtro and nombre <> 'administrador'";
    }
    $sql = "SELECT id_privilegios as id ,nombre 
            FROM privilegios $filtro";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * inserta un privilegio
   *      
   * Este metodo inserta un nuevo privilegio
   * 
   * @access public
   * @param int $id_privilegios id del privilegio de la bd
   * @param int $id_modulo id del modulo de la bd
   * @param int $id_crud id del crud de la bd
   * @return bolean
   */
  public function insert_acceso($id_privilegios, $id_modulo, $id_crud)
  {
    $data = array(
      'id_privilegios' => $id_privilegios,
      'id_modulo' => $id_modulo,
      'id_crud' => $id_crud,

    );
    $this->db->insert('accesos', $data);
    return ($this->db->affected_rows() != 1) ? false : true;
  }

  /**
   * detalle_privilegio
   * 
   * esto regresa un listado con el detalle del privilegio
   *
   * ejemplo privilegio=> administrador
   *
   * @param  mixed $id_privilegio
   * @return Array
   */
  public function detalle_privilegio($id_privilegio)
  {

    $sql = "SELECT x.privilegio,x.modulo,crud.nombre,crud.id_crud,x.id_modulo,x.id_privilegios
    FROM crud INNER JOIN(
          SELECT p.nombre as privilegio,m.nombre as modulo,a.id_crud,a.id_modulo,a.id_privilegios
                  from privilegios as p inner join accesos as a 
              on p.id_privilegios=a.id_privilegios INNER JOIN modulo as m 
              on a.id_modulo = m.id_modulo
        	  WHERE p.id_privilegios=$id_privilegio
     ) as x
      WHERE crud.id_crud=x.id_crud";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * deleted_acceso
   *
   * @param  Int $id_crud
   * @param  Int $id_modulo
   * @param  Int $id_privilegios
   * @return Bolean
   */
  public function deleted_acceso($id_crud, $id_modulo, $id_privilegios)
  {
    $sql = "DELETE FROM `accesos` 
    WHERE `accesos`.`id_privilegios` = $id_privilegios AND 
    `accesos`.`id_modulo` = $id_modulo AND `accesos`.`id_crud` = $id_crud";
    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true;
  }


  /**
   * get_lista_privilegios
   * 
   * Esta funcion regresa la lista de permisos dependiendo el id 
   *
   * @param  Int $id
   * @return Array
   */
  public function get_lista_privilegios($id = 0)
  {
    $sql = "SELECT pri.nombre as privilegio,mo.nombre as modulo,crud.nombre as 'crud'
            FROM personal as p INNER JOIN usuario as us
              ON p.id_personal =us.personal INNER JOIN privilegios as pri 
              ON us.privilegio=pri.id_privilegios INNER JOIN accesos as acc
              ON pri.id_privilegios=acc.id_privilegios INNER JOIN modulo as mo
              ON acc.id_modulo=mo.id_modulo INNER JOIN crud 
              ON acc.id_crud=crud.id_crud
            WHERE p.id_personal=$id
          order by mo.nombre";
    $resultados = $this->db->query($sql);

    return $resultados->result();
  }

  /**
   * get_modulosBy_id
   * 
   * Esto regresa el nombre de todos los modulos que tiene el usuario
   *
   * @param  INT $id
   * @return Array
   */
  public function get_modulosBy_id($id)
  {
    $sql = "SELECT DISTINCT  m.nombre as 'modulo'
        from usuario as u inner JOIN privilegios as p 
          on u.privilegio=p.id_privilegios INNER JOIN accesos as ac
          on p.id_privilegios=ac.id_privilegios INNER JOIN modulo as m
          on ac.id_modulo=m.id_modulo 
        WHERE u.personal=$id";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
}
