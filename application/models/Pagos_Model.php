

<?php

use phpDocumentor\Reflection\Types\Null_;

class Pagos_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  /**
   * Get_cedulas
   * 
   * Esto regresa una lista con lo que debería ganar cada medico radiologo dependiendo cuanto cobre
   *
   * @param  Int $year_inicio año inicio para buscar
   * @param  Int $mes_inicio  mes inicio para buscar
   * @param  Int $dia_inicio dia inicio para buscar
   * @param  Int $year_fin año fin para buscar
   * @param  Int $mes_fin mes fin para buscar
   * @param  Int $dia_fin dia fin para buscar
   * @return Array
   */
  public function Get_cedulas($year_inicio, $mes_inicio, $dia_inicio, $year_fin, $mes_fin, $dia_fin)
  {
    $sql = "SELECT x.radiologo,(p.cobro_estudio*x.tomas)as 'pago_toma',(p.cobro_interpretacion* x.interpretaciones) as 'pago_interpretaciones',x.tomas as 'tomas_totales', x.interpretaciones as 'totales_interpretaciones',x.udn
            from personal as p inner JOIN (
                          SELECT COUNT(p.id_personal)as 'tomas',sum(p.num_interpretaciones) as 'interpretaciones' , p.radiologo,p.id_personal,p.udn
                          from pagos p
                          WHERE  (YEAR(p.created)=$year_inicio and  MONTH(p.created)>=$mes_inicio and DAY(p.created)>=$dia_inicio ) and  (DAY(p.created)<=$dia_fin and MONTH(p.created)<=$mes_fin and YEAR(p.created)=$year_fin) 
                          GROUP BY p.id_personal) as x
          on p.id_personal=x.id_personal;";
    // echo $sql;
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function consentrado($medico,$fecha_inicio,$fecha_fin)
  {
    // $sql = "SELECT x.created,x.radiologo,x.nim,x.id_personal,x.nim_sass,es.nombre as 'estudio',es.id_estudio,(select count(*)  from dicom_rutas where ruta like concat('%',x.nim,'_',es.id_estudios_sass,'%') ) as placas,es.honorario,
    // es.honorario * (select count(*)  from dicom_rutas where ruta like concat('%',x.nim,'_',es.id_estudios_sass,'%') ) as total
    //       from  contenido_consulta as c  inner join estudio as es 
    //         on c.id_estudio=es.id_estudio inner join (
    //           SELECT p.created,p.radiologo, REPLACE(nim_sass,'/','-') as 'nim',p.id_personal,p.nim_sass,p.id_toma_muestra
    //           FROM pasgos as p
    //           WHERE  (YEAR(p.created)>=$year_inicio and  MONTH(p.created)>=$mes_inicio and DAY(p.created)>=$dia_inicio and YEAR(p.created)<=$year_fin) and  (MONTH(p.created)<=$mes_fin and DAY(p.created)<=$dia_fin) and p.id_personal=$id_personal
    //           ) as x 
    //       on c.id_toma_muestra=x.id_toma_muestra";
    $sql = "SELECT
            *,(
              SELECT
                COUNT(*) as placas_subidas
              FROM
                dicom_rutas
              WHERE
                ruta like concat('%', x.nomenclatura, '%')
            ) as placas_subidas
        from
          (
            SELECT
              a.created as 'Fecha_asignacion',
              t.id_toma_muestra,
              t.nim_sass,
              personal.id_empleado as 'tecnico_id',
              a.id_personal as 'medico',
              es.nombre,
              es.placas as 'placas',
              es.honorario,
              concat(
                REPLACE(t.nim_sass, '/', '-'),
                '_',
                es.id_estudios_sass
              ) as 'nomenclatura'
            from
              toma_muestra t
              inner JOIN contenido_consulta c on t.id_toma_muestra = c.id_toma_muestra
              inner JOIN interpretacion i on c.id_contenido_consulta = i.id_contenido_consulta
              inner JOIN estudio as es on c.id_estudio = es.id_estudio
              inner JOIN asignacion_interpretacion a on t.id_toma_muestra = a.id_toma_muestras
              inner JOIN estatus on a.id_estatus = estatus.id_estatus
              inner JOIN personal on t.id_personal=personal.id_personal
            WHERE
              estatus.nombre = 'TERMINADO'
              and a.id_personal = $medico
              and a.created BETWEEN '$fecha_inicio 00:00:00'
              AND '$fecha_fin 23:59:59'
          ) as x
      order by
        x.Fecha_asignacion
    ";
    $resultados = $this->db->query($sql);
    // echo $sql;
    return $resultados->result();
  }

  
  public function consentradoV2($id_personal,$fecha_inicio,$fecha_fin){
        $sql="SELECT
                *,(
                  SELECT
                    COUNT(DISTINCT ruta) as placas_subidas
                  FROM
                    dicom_rutas
                  WHERE
                    ruta like concat('%', x.nomenclatura, '%')
                ) as placas_subidas
              from
                (
                  SELECT
                    a.created as 'created',
                    a.created as 'Fecha_asignacion',
                    t.id_toma_muestra,
                    t.nim_sass,
                    t.id_personal as 'tecnico',
                    a.id_personal as 'radiologo',
                    a.id_personal as 'medico',
                    es.nombre as 'estudio',
                    es.placas as 'placas',
                    es.honorario,
                    es.id_estudio,
                    per.id_empleado as 'id_htds_medico',
                    people.id_empleado as 'id_htds_tecnico',
                    concat(
                        REPLACE(t.nim_sass, '/', '-'),
                        '_',
                        es.id_estudios_sass
                    ) as 'nomenclatura'
                  from
                    toma_muestra t
                    inner JOIN contenido_consulta c on t.id_toma_muestra = c.id_toma_muestra
                    inner JOIN personal as people on people.id_personal=t.id_personal 
                    inner JOIN interpretacion i on c.id_contenido_consulta = i.id_contenido_consulta
                    inner JOIN estudio as es on c.id_estudio = es.id_estudio
                    inner JOIN asignacion_interpretacion a on t.id_toma_muestra = a.id_toma_muestras
                    inner JOIN estatus on a.id_estatus = estatus.id_estatus
                    inner JOIN personal per on a.id_personal=per.id_personal
                  WHERE
                    estatus.nombre = 'TERMINADO'
                    and a.id_personal = $id_personal
                    and a.created BETWEEN '$fecha_inicio 00:00:00'
                    AND '$fecha_fin 23:59:59'
                  ) as x
                    order by
              x.Fecha_asignacion
    ";
    $resultados = $this->db->query($sql);

    return $resultados->result();
  }

  public function consentrado_SinInterpretar($id_personal,$fecha_inicio,$fecha_fin){
    $sql="SELECT
            *,(
              SELECT
                COUNT( DISTINCT ruta ) as placas_subidas
              FROM
                dicom_rutas
              WHERE
                ruta like concat('%', x.nomenclatura, '%')
            ) as placas_subidas
          from
            (
              SELECT
                a.created as 'created',
                a.created as 'Fecha_asignacion',
                t.id_toma_muestra,
                t.nim_sass,
                t.id_personal as 'tecnico',
                a.id_personal as 'radiologo',
                a.id_personal as 'medico',
                es.nombre as 'estudio',
                es.placas as 'placas',
                es.honorario,
                es.id_estudio,
                per.id_empleado as 'id_htds_medico',
                people.id_empleado as 'id_htds_tecnico',
                concat(
                  REPLACE(t.nim_sass, '/', '-'),
                  '_',
                  es.id_estudios_sass
                ) as 'nomenclatura'
              from
                toma_muestra t
                inner JOIN contenido_consulta c on t.id_toma_muestra = c.id_toma_muestra
                inner JOIN personal as people on people.id_personal=t.id_personal                     
                inner JOIN estudio as es on c.id_estudio = es.id_estudio
                inner JOIN asignacion_interpretacion a on t.id_toma_muestra = a.id_toma_muestras
                inner JOIN estatus on a.id_estatus = estatus.id_estatus
                inner JOIN personal per on a.id_personal=per.id_personal
              WHERE
                estatus.nombre <> 'TERMINADO'
                and a.id_personal = $id_personal
                and a.created BETWEEN '$fecha_inicio 00:00:00'
                AND '$fecha_fin 23:59:59'
              ) as x
                order by
          x.Fecha_asignacion
    ";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }


  /**
   * regresa array con todas las tomas donde se ve si faltan las imágenes en el servidor 
   *
   * @return Array
   */
  public function validacion()
  {
    $id = $_SESSION['usuario']->id_personal;
    $sql = "SELECT x.realizo,x.fecha,x.sass,x.nombre,x.id_estudios_sass,x.honorario,x.placas, (select count(*) from dicom_rutas where ruta like concat('%',x.sass,'_',x.id_estudios_sass,'%') ) as 'placas_servidor'
          from ( 
            SELECT t.fecha,replace(t.nim_sass,'/','-') sass ,e.nombre,e.id_estudios_sass,e.honorario,e.placas,t.realizo
            FROM toma_muestra t INNER JOIN contenido_consulta c 
            on t.id_toma_muestra =c.id_toma_muestra INNER JOIN estudio e 
            on c.id_estudio=e.id_estudio) as x
          WHERE x.realizo=$id";

    $resultados = $this->db->query($sql);
    //echo $sql;
    return $resultados->result();
  }
  /**
   * regresa array con todas las tomas donde se ve si faltan las imágenes en el servidor 
   *
   * @return Array
   */
  public function validacion_Date($mes, $dia, $years)
  {
    $id = $_SESSION['usuario']->id_personal;
    $sql = "SELECT x.realizo,x.fecha,x.sass,x.nombre,x.id_estudios_sass,x.honorario,x.placas, (select count(*) from dicom_rutas where ruta like concat('%',x.sass,'_',x.id_estudios_sass,'%') ) as 'placas_servidor'
            from ( SELECT t.fecha,replace(t.nim_sass,'/','-') sass ,e.nombre,e.id_estudios_sass,e.honorario,e.placas ,t.realizo
              FROM toma_muestra t INNER JOIN contenido_consulta c 
              on t.id_toma_muestra =c.id_toma_muestra INNER JOIN estudio e 
              on c.id_estudio=e.id_estudio) as x 
            WHERE DAY(x.fecha)=$dia and MONTH(x.fecha)=$mes and YEAR(x.fecha)=$years and x.realizo=$id";

    $resultados = $this->db->query($sql);
    //echo $sql;
    return $resultados->result();
  }
}
