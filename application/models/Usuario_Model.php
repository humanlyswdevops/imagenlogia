<?php

use phpDocumentor\Reflection\Types\Null_;

class Usuario_Model extends CI_Model{
  public function __construct(){
    $this->load->database();    
  }    

  /**
   * Inserta un usuario 
   *   
   * este metodo inserta un usuario y greda un true si se inserto de forma correcta
   * 
   * @access public
   * @param String $ussername nombre de usuario
   * @param String $passwod contraseña
   * @param int $privilegio id del privilegio
   * @param int $personal id del personal registrado
   * @return bolean
   */
  public function insert($ussername,$passwod,$privilegio,$personal){
    $data=array(      
      'id_usuario'=>'null',
      'username'=>$ussername,     
      'pass'=>$passwod,     
      'privilegio'=>$privilegio,
      'personal'=>$personal        
    );
    $this->db->insert('usuario',$data);
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }

  public function existe($name){
    $sql="SELECT COUNT(*) as existe 
            FROM usuario 
          WHERE username='$name'";
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();
    return $dataset[0];
  }

  public function Buscar_id_ByUssername($name){
    $sql="SELECT COUNT(*) as existe 
            FROM usuario 
          WHERE username='$name'";
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();
    return $dataset[0];
  }
  
  /**
   * get
   * 
   * Estolo regresa la informacion del usuario que se visualiza en la tabla
   *
   * @return Array
   */
  public function get(){
    $sql="SELECT per.id_personal,pri.nombre as privilegio,CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as personal,u.username,per.id_empleado
            FROM usuario as u INNER JOIN privilegios as pri
              on u.privilegio=pri.id_privilegios INNER JOIN personal as per
              on u.personal=per.id_personal";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }  
  /**
   * get_todo
   * 
   * Esto regresa toda la informacion de un personal incluyendo usuario por id
   *
   * @param  Int  $id
   * @return Array
   */
  public function get_todo($id=0){
    $sql="SELECT u.id_usuario,u.username,u.privilegio,per.id_personal,per.nombre,per.apellido_paterno,per.apellido_materno,per.no_cedula,per.id_empleado,per.udn 
            from usuario as u INNER JOIN personal as per 
              on u.personal=per.id_personal
            WHERE per.id_personal=$id";
            
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }


  public function existe_user($usser){
    $sql="SELECT COUNT(*) as existe 
            FROM usuario 
          WHERE username='$usser'";    
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();
    return $dataset[0];
  }

}