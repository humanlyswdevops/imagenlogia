<?php
use phpDocumentor\Reflection\Types\Null_;

class Asignacion_Interpretacion_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

     
  /**
   * insert
   *
   * @param  int $id_personal 
   * @param  int $id_toma_muestras   
   * @return int
   */
  public function insert($id_personal,$id_toma_muestras){
    $data=array(      
      'id_personal'=>$id_personal,
      'id_toma_muestras'=>$id_toma_muestras,
      'id_estatus'=>1 //1 significa en proceso (filtro)
    );

    $this->db->insert('asignacion_interpretacion',$data);
    $id = $this->db->insert_id();
    
    return $id;
  }     
  
  /**
   * vencer_toma
   *
   * deespues de que el medico termina la interpetracion esto cambia su estado 
   * para saber que ya finalizo
   * 
   * @param  int  $id_toma_muestra id de la toma de muestra de la bd
   * @return bolean
   */
  public function Cambiar_status($id_toma_muestra,$estado){
    $this->db->set('id_estatus',$estado);
    $this->db->where('id_toma_muestras', $id_toma_muestra);
    $this->db->update('asignacion_interpretacion'); 
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }  

  public function Cambiar_estatus_asignacion($id_toma_muestra){
    $this->db->set('id_estatus',2);
    $this->db->where('');
    
  }

  
  /**
   * get_pendientes
   * 
   * Esto regresa los pendiente dependiendo el privilegio regresa todos 
   * los pendiente o uq solo filtre por su id si no es administrador
   *
   * @param  Int $id
   * @param  String $privilegio
   * @return Array []
   */
  public function get_pendientes($id=0,$privilegio=''){    
    $condicion="";
    if ($privilegio!='administrador') {
      $condicion="where x.id_personal=$id";  
    }
    //,' ',pa.apellido_materno
    $sql="SELECT t.id_toma_muestra,x.medico,x.id_personal,CONCAT(pa.nombre,' ',pa.apellido_paterno) as paciente,x.fecha,x.estatus
            FROM toma_muestra as t INNER JOIN (
                SELECT p.id_personal,CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as medico,a.id_toma_muestras,a.created as fecha,es.nombre as estatus
                  from asignacion_interpretacion as a INNER join personal as p    
                    on a.id_personal=p.id_personal INNER JOIN estatus as es
                    on a.id_estatus=es.id_estatus
                WHERE es.nombre='EN PROCESO'
            ) as x
          on t.id_toma_muestra= x.id_toma_muestras INNER JOIN paciente as pa 
          on t.id_paciente=pa.id_paciente
          $condicion";
    //nuevo

    $sql ="SELECT
              t.nim_sass,
              t.id_toma_muestra,
              x.medico,
              x.id_personal,
              CONCAT(pa.nombre, ' ', pa.apellido_paterno) as paciente,
              x.fecha,
              x.estatus,
              REPLACE(udn.nombre, '%20', ' ') as 'udn'
          FROM
              toma_muestra as t
              INNER JOIN (
                  SELECT
                      p.id_personal,
                      CONCAT(
                          p.nombre,
                          ' ',
                          p.apellido_paterno,
                          ' ',
                          p.apellido_materno
                      ) as medico,
                      a.id_toma_muestras,
                      
                      es.nombre as estatus,
                      DATE_FORMAT(a.created ,'%a %d %M %h:%i %p')as fecha
                  from
                      asignacion_interpretacion as a
                      INNER join personal as p on a.id_personal = p.id_personal
                      INNER JOIN estatus as es on a.id_estatus = es.id_estatus
                  WHERE
                      es.nombre = 'EN PROCESO'
              ) as x on t.id_toma_muestra = x.id_toma_muestras
              INNER JOIN paciente as pa on t.id_paciente = pa.id_paciente
              INNER JOIN personal tecnico on t.id_personal = tecnico.id_personal
              INNER JOIN udn on tecnico.udn = udn.id_udn       
              $condicion       
              order by
                  x.fecha DESC
              ";
    
    $this->db->query("SET lc_time_names = 'es_ES'");
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }



  public function get_pendiente_agregadoDespues($id=0,$privilegio=''){
    $condicion="";
    if ($privilegio!='administrador') {
      $condicion="and a.id_personal=$id";  
    }
    $sql="SELECT
            t.nim_sass,
            (SELECT 'Paciente Con Nuevos Estudios') estatus,
            c.id_contenido_consulta,
            t.id_toma_muestra,
            concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as medico,
            per.id_personal,
            concat(pa.nombre, ' ', pa.apellido_paterno) as paciente,            
            udn.nombre as 'udn',
            DATE_FORMAT(a.created ,'%a %d %M %h:%i %p')as 'created'
          from
            toma_muestra t
            INNER JOIN contenido_consulta c on t.id_toma_muestra = c.id_toma_muestra
            INNER JOIN asignacion_interpretacion a on t.id_toma_muestra = a.id_toma_muestras
            INNER JOIN personal as per on a.id_personal = per.id_personal
            INNER JOIN paciente pa on t.id_paciente = pa.id_paciente
            INNER JOIN personal people  on  t.id_personal=people.id_personal
            INNER JOIN udn on people.udn=udn.id_udn
          WHERE
            c.agregado_despues = 1 and c.agregado_despues_terminar = 0 $condicion
          GROUP by t.nim_sass";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function Get_pospone($id,$privilegio)
  {
    $condicion="";
    if ($privilegio!='administrador') {
      $condicion="and a.id_personal=$id";  
    }
    $sql="SELECT
              t.id_toma_muestra,
              t.nim_sass,
              concat(doctor.nombre, ' ', doctor.apellido_paterno) as 'radiologo',
              concat(paciente.nombre, ' ', paciente.apellido_paterno) as 'paciente',
              DATE_FORMAT(a.created ,'%a %d %M %h:%i %p')as 'created',
              (
                select 'Estudio propuesto'
              ) as estatus,
              udn.nombre as udn
          FROM
              pendiente p
              INNER JOIN status_pendiente sp on p.id_status_pendiente = sp.id_status_pendiente
              INNER JOIN contenido_consulta c on c.id_contenido_consulta = p.id_contenido_consulta
              INNER JOIN toma_muestra t on t.id_toma_muestra = c.id_toma_muestra
              INNER JOIN paciente on t.id_paciente = paciente.id_paciente
              inner JOIN personal tecnico on t.id_personal = tecnico.id_personal
              inner JOIN udn on tecnico.udn = udn.id_udn
              INNER JOIN asignacion_interpretacion a on t.id_toma_muestra = a.id_toma_muestras
              INNER JOIN personal doctor on a.id_personal = doctor.id_personal
          WHERE
              sp.nombre = 'EN PROCESO' $condicion
          group by
              t.id_toma_muestra;";
    
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  
  /**
   * get_pendientes_BYDate
   * 
   * Esta funcion regresa los pendiente dependiendo en rango de fechas que se le pase
   *
   * @param  Int $id_medico
   * @param  String $formato_inicio
   * @param  String $formato_fin
   * @return Array 
   */
  public function get_pendientes_BYDate($id_medico,$formato_inicio,$formato_fin){
    
    $condicion="";
    $privilegio=$_SESSION['usuario']->privilegio;
    if ($privilegio!='administrador') {
      $condicion="and x.id_personal=$id_medico";  
    }

    $sql ="SELECT
            t.id_toma_muestra,
            t.nim_sass,
            x.medico,
            x.id_personal,
            CONCAT(pa.nombre, ' ', pa.apellido_paterno) as paciente,
            x.fecha,
            x.estatus,
            REPLACE(udn.nombre, '%20', ' ') as 'udn',
            DAY(x.fecha) as 'dia',
            MONTH(x.fecha) as 'mes',
            YEAR(x.fecha) as 'years'
          FROM
            toma_muestra as t
            INNER JOIN (
                SELECT
                  p.id_personal,
                  CONCAT(
                      p.nombre,
                      ' ',
                      p.apellido_paterno,
                      ' ',
                      p.apellido_materno
                  ) as medico,
                  a.id_toma_muestras,
                  a.created as fecha,
                  es.nombre as estatus
                from
                  asignacion_interpretacion as a
                  INNER join personal as p on a.id_personal = p.id_personal
                  INNER JOIN estatus as es on a.id_estatus = es.id_estatus
                WHERE
                  es.nombre = 'EN PROCESO'
            ) as x on t.id_toma_muestra = x.id_toma_muestras
            INNER JOIN paciente as pa on t.id_paciente = pa.id_paciente
            INNER JOIN personal tecnico on t.id_personal = tecnico.id_personal
            INNER JOIN udn on tecnico.udn = udn.id_udn
          WHERE
              x.fecha BETWEEN '$formato_inicio 00:00:00' AND '$formato_fin 23:59:59' $condicion ";
    $resultados = $this->db->query($sql);
    return $resultados->result();
    
  }
  
  /**
   * get_historico
   * 
   * Esto regresa un historial de todas las interpretaciones
   *
   * @return Array
   */
  public function get_historico($privilegio=0,$id=0){

    $condicion="";
    if ($privilegio!='administrador' /*&& $privilegio!= 'Gefe Radiologo'*/) {
      
    }else{
      
      //$condicion="where x.realizo=$id or x.id_personal=$id "; 
    } 

    if ($privilegio=='Jefe Radiologo' || $privilegio=='técnico radiólogo') {
      $condicion="WHERE t.id_personal=$id"; 
    }else{
      if($condicion=='administrador'){
        $condicion=""; 
      }else{
        $condicion="WHERE a.id_personal=$id"; 
      }
    }
    //echo $privilegio;        
    /*if($privilegio=='administrador'/*&&$privilegio== 'Gefe Radiologo'){
      $condicion='';
    }else{
      if($privilegio=='técnico radiólogo' || $privilegio== 'Gefe Radiologo'){
        $condicion="where x.realizo=$id"; 
      }else{
        $condicion="where x.id_personal=$id";  
      }
    }*/
  

    $sql="SELECT t.id_toma_muestra,x.medico,x.id_personal,CONCAT(pa.nombre,' ',pa.apellido_paterno) as paciente,x.fecha,x.estatus,x.realizo
            FROM toma_muestra as t INNER JOIN (
                SELECT p.id_personal,CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as medico,a.id_toma_muestras,a.created as fecha,es.nombre as estatus,t.realizo
                  from toma_muestra as t INNER JOIN asignacion_interpretacion as a  
                  on t.id_toma_muestra=a.id_toma_muestras	INNER join personal as p    
                    on a.id_personal=p.id_personal INNER JOIN estatus as es
                    on a.id_estatus=es.id_estatus                
            ) as x
          on t.id_toma_muestra= x.id_toma_muestras INNER JOIN paciente as pa 
          on t.id_paciente=pa.id_paciente
          $condicion
          ";    
    #consulta V2
    $sql="SELECT DATE_FORMAT(t.fecha ,'%a %d %M %h:%i %p')as fecha ,a.id_estatus,a.modificado,concat(p.nombre,' ',p.apellido_paterno) as 'paciente',concat(medico.nombre,' ',medico.apellido_paterno,' ',medico.apellido_materno) as 'medico',es.nombre as 'estatus',t.id_personal as 'id_tecnico',a.id_personal as 'id_medico',t.id_toma_muestra,t.id_paciente,t.nim_sass,t.vigente,t.realizo,a.id_personal,a.created,a.id_toma_muestras
          from toma_muestra t INNER JOIN asignacion_interpretacion a
            on t.id_toma_muestra=a.id_toma_muestras INNER JOIN paciente p
            on t.id_paciente=p.id_paciente INNER JOIN personal as medico
            on a.id_personal=medico.id_personal INNER JOIN estatus es
            on a.id_estatus=es.id_estatus
            $condicion
           ;
      ";
    //nuevo
    if($_SESSION['usuario']->privilegio=='administrador'){
      $condicion='';
    }

    $sql="SELECT t.nim_sass, DATE_FORMAT(t.fecha ,'%a %d %M %h:%i %p') as fecha,a.id_estatus,a.modificado,concat(p.nombre,' ',p.apellido_paterno) as 'paciente',concat(medico.nombre,' ',medico.apellido_paterno,' ',medico.apellido_materno) as 'medico',es.nombre as 'estatus',t.id_personal as 'id_tecnico',a.id_personal as 'id_medico',t.id_toma_muestra,t.id_paciente,t.nim_sass,t.vigente,t.realizo,a.id_personal,a.created,a.id_toma_muestras, REPLACE(udn.nombre, '%20', ' ') as 'udn'
            from toma_muestra t INNER JOIN asignacion_interpretacion a
              on t.id_toma_muestra=a.id_toma_muestras INNER JOIN paciente p
              on t.id_paciente=p.id_paciente INNER JOIN personal as medico
              on a.id_personal=medico.id_personal INNER JOIN estatus es
              on a.id_estatus=es.id_estatus INNER JOIN personal per 
              on t.id_personal=per.id_personal INNER JOIN udn 
              on per.udn= udn.id_udn
              $condicion
      ";
    
    $this->db->query("SET lc_time_names = 'es_ES'");
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * get_asignaciones
   *
   * Esto regresa un listado de todas las asignaciones que se le han echo aun medico radiologo
   * el filtro es por dia
   * 
   * @return Array
   */
  public function get_asignaciones()
  {
    date_default_timezone_set('America/Mexico_City');
    $mes = date('m');
    $dia = date('d');
    $year=date('Y');
    $id= $_SESSION['usuario']->id_personal;
    
    $sql="SELECT a.id_toma_muestras,a.created, concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as radiologo,e.nombre as estado,t.nim_sass as sass,t.fecha,concat(pa.nombre,' ',pa.apellido_paterno) as paciente
    from personal as p inner join asignacion_interpretacion as a
      on p.id_personal=a.id_personal inner join estatus as e
      on a.id_estatus=e.id_estatus inner join toma_muestra as t
      on a.id_toma_muestras=t.id_toma_muestra inner join paciente as pa 
        on t.id_paciente= pa.id_paciente
    where e.nombre<>'TERMINADO' and (day(t.fecha)=$dia and month(t.fecha)=$mes and YEAR(t.fecha)=$year) AND    (t.realizo=$id or t.id_personal=$id)";
    // quitar filtro por dia
    //and (day(t.fecha)=$dia and month(t.fecha)=$mes)    
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  public function Prueba($id){
         
    $mes = date('m');
    $dia=date('d');

    $sql="UPDATE asignacion_interpretacion
            SET id_personal = $id
          WHERE day(created)=$dia and month(created)=$mes";

    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true;     
  }
  
  /**
   * list_asignacion
   * esto regresa una lista para con los id_toma_muestras por le dia 
   * para poder evitar errores de duplicidad de llaves primarias
   *
   * @return void
   */
  public function list_asignacion()
  {
    date_default_timezone_set('America/Mexico_City');
    $mes = date('m');
    $dia=date('d');

    $sql="SELECT DISTINCT a.id_toma_muestras 
          FROM asignacion_interpretacion as a  INNER JOIN estatus as es 
            on a.id_estatus=es.id_estatus
          WHERE es.nombre<>'TERMINADO' and (day(a.created)=$dia and month(a.created)=$mes)";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * Cambia_asignacion
   * 
   * esto cambia la asignación del personal por otro radiologo
   *
   * @param  mixed $id id del personal
   * @param  mixed $id_toma_muestras id de la toma de muestra
   * @return Boolean
   */
  public function Cambia_asignacion($id,$id_toma_muestras){
    $sql="UPDATE asignacion_interpretacion
          SET id_personal = '$id' 
          WHERE id_toma_muestras = $id_toma_muestras";          
    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true;
  }
  
  /**
   * existe_asignacion
   *
   * Esta funcion regresa 0 si no existe o 1 si existe lo ocupo para la asignación de la interpretacion
   * 
   * @param  mixed $id_personal
   * @param  mixed $id_toma_muestras
   * @return void
   */
  public function existe_asignacion($id_personal,$id_toma_muestras)
  {
    $sql="SELECT COUNT(*) as existe
            FROM asignacion_interpretacion as a 
          WHERE  a.id_personal = $id_personal AND a.id_toma_muestras =$id_toma_muestras";
    $resultados = $this->db->query($sql);
    $respuesta= $resultados->result();
    return $respuesta[0];
  }

    
  /**
   * limpia_asignacion
   * 
   * Esto elimina todas las asignaciones 
   * por la toma y que el id_personal sea diferente al id
   * 
   * Lo ocupo para aginar todas las tomas a un radiologo
   * 
   *
   * @param  Int $id_personal id del medico radiologo
   * @param  Int $id_toma_muestra id de la toma de muestra
   * @return void
   */
  public function limpia_asignacion($id_personal,$id_toma_muestra){
    $sql="DELETE  FROM asignacion_interpretacion 
          WHERE id_toma_muestras = $id_toma_muestra and id_personal<> $id_personal";

    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true; 
  }
  
  /**
   * eliminar_asignacion
   * 
   * Esto elimina la todas las asignaciones por el id de la toma de muestra
   *
   * @param  Int $id_toma_muestra
   * @return Boolean
   */
  public function eliminar_asignacion($id_toma_muestra){
    $sql="DELETE  FROM asignacion_interpretacion 
          WHERE id_toma_muestras = $id_toma_muestra";

    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true; 
  }

  public function ModificaAsignacion($id_toma_muestra,$medico){
    $sql="UPDATE `asignacion_interpretacion` SET `id_personal` = '$medico' 
            WHERE id_toma_muestras = $id_toma_muestra;";

    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true; 
  }


  /**
   * INSERT INTO `` (`id`, ``, ``, `created`, ``) VALUES (NULL, '1', '1', CURRENT_TIMESTAMP, '1');
   */

  public function ModificaAsignacion_Log($id_tomaMuestra,$Id_medico){
    $data=array(      
      'id_tomaMuestra'=>$id_tomaMuestra,
      'Id_medico'=>$Id_medico,
      'movido_por'=>$_SESSION['usuario']->id_personal //1 significa en proceso (filtro)
    );

    $this->db->insert('logasignacion',$data);        
    return ($this->db->affected_rows() != 1) ? false : true; 
  }

  public function ContenidoConsulta_estatus($id_contenido_consulta){
    $this->db->set('agregado_despues_terminar',1);
    $this->db->where('id_contenido_consulta', $id_contenido_consulta);
    $this->db->update('contenido_consulta'); 
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }  

}