<?php
use phpDocumentor\Reflection\Types\Null_;
class Interpretacion_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }
  
  /**
   * Insert
   * Esto inserta una interpretación (lo que el radiologo escribe)
   *
   * @param  Int $id_contenido_consulta
   * @param  String $titulo
   * @param  String $texto
   * @return Boolean
   */
  public function Insert($id_contenido_consulta,$titulo,$texto){
    $data=array(      
      'id_interpretacion'=>'null',
      'id_contenido_consulta'=>$id_contenido_consulta,     
      'titulo'=>$titulo,     
      'texto'=>$texto
    );
    
    $this->db->insert('interpretacion',$data);
    return ($this->db->affected_rows() != 1) ? false : true;  
  }

  public function Update($id,$interpretacion){
    //UPDATE interpretacion SET texto = '' WHERE id_contenido_consulta = 11;
    $this->db->set('texto',$interpretacion);
    $this->db->where('id_contenido_consulta', $id);
    $this->db->update('interpretacion');
    return ($this->db->affected_rows() != 1) ? false : true;    
  }


  /**
   * Esto regresa todo lo que tiene una consulta 
   *   
   * este metodo regresa listado de los estudios
   * 
   * @access public   
   * @return array [fecha,dicom,estudio,paciente]
   */
  public function get_consulta($id){
    $sql = "SELECT t.datos_clinicos,t.fecha ,di.nombre as 'dicom',es.nombre as 'estudio',CONCAT(pa.nombre,' ',pa.apellido_paterno,' ',pa.apellido_materno) as 'paciente',con.id_contenido_consulta
              from paciente as pa INNER JOIN toma_muestra as t on pa.id_paciente=t.id_paciente INNER JOIN contenido_consulta as con
                on t.id_toma_muestra=con.id_toma_muestra INNER JOIN dicom  as di
                on con.id_contenido_consulta =di.id_contenido_consulta INNER JOIN estudio as es
                on con.id_estudio=es.id_estudio
            WHERE t.id_toma_muestra=$id
            ORDER by t.fecha,es.nombre";
            return $sql;
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  //
  public function Get_datosClinicos($id){
    $sql="SELECT datos_clinicos 
    FROM toma_muestra 
    WHERE id_toma_muestra=$id";
    
    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }
  
  /**
   * get_interpretaciones
   * 
   * Esto nos regresa las interpretaciones dependiendo el id de la toma de muestra
   * 
   * @param  Int $id Id de la toma de muestra como viene de la BD
   * @return Array
   */
  public function get_interpretaciones($id=0){
    $sql="SELECT concat('Interpretación del estudio: ',es.nombre) as estudio,i.titulo,i.texto as 'interpretacion',con.id_contenido_consulta,i.id_interpretacion
          FROM toma_muestra as t INNER JOIN contenido_consulta as con 
            on t.id_toma_muestra=con.id_toma_muestra INNER JOIN estudio as es
            on con.id_estudio= es.id_estudio INNER JOIN interpretacion as i
            on con.id_contenido_consulta = i.id_contenido_consulta
          WHERE t.id_toma_muestra=$id";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  } 
  
  /**
   * Get_interpretacions_respuestaHas
   *
   * @param  mixed $id_tomaMuestra id de la toma de muestra
   * @return Array [id_estudios_sass,estudio,interpretacion ]
   */
  public function Get_interpretacions_respuestaHas($id_tomaMuestra){
    $sql="SELECT es.id_estudios_sass,concat('Interpretación del estudio: ',es.nombre) as estudio,TO_BASE64(i.texto) as 'interpretacion'
          FROM toma_muestra as t INNER JOIN contenido_consulta as con 
            on t.id_toma_muestra=con.id_toma_muestra INNER JOIN estudio as es
            on con.id_estudio= es.id_estudio INNER JOIN interpretacion as i
            on con.id_contenido_consulta = i.id_contenido_consulta
          WHERE t.id_toma_muestra=$id_tomaMuestra";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
    
  /**
   * Get_pacientes_has
   *
   * @param  mixed $id_tomaMuestra
   * @return Array [nim_sass,paciente,curp]
   */
  public function Get_pacientes_has($id_tomaMuestra)
  {
    $sql="SELECT t.nim_sass,concat(p.nombre,' ',p.apellido_paterno) as 'paciente',p.curp
            from toma_muestra as t INNER JOIN paciente as p 
              on t.id_paciente=p.id_paciente
          WHERE t.id_toma_muestra=$id_tomaMuestra ";
    
    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }
    
  /**
   * get_interpretacion
   *
   * Esto regresa solo una interpretacion por su id
   * 
   * @param  Int $id Id de la toma de muestra como viene de la BD
   * @return Array
   */
  public function get_interpretacion($id){
    $sql="SELECT titulo,texto 
          FROM interpretacion 
          WHERE id_interpretacion=$id";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function curp_paciente_byfolio($toma){
    $sql="SELECT paciente.curp from paciente inner join (
      SELECT t.id_paciente as paciente
      from toma_muestra as t INNER JOIN contenido_consulta as c 
        on t.id_toma_muestra=c.id_toma_muestra INNER JOIN interpretacion as i 
        on c.id_contenido_consulta=i.id_contenido_consulta INNER JOIN estudio as es
        on c.id_estudio=es.id_estudio
      WHERE t.id_toma_muestra=$toma) as tem on tem.paciente=paciente.id_paciente";
    $resultados = $this->db->query($sql);
    $pacinte =$resultados->result();
    return $pacinte[0];
  }

  
  /**
   * getInterpretacion_Id
   *
   * Esto regresa un arreglo con la informacion de la interpretacion depende el id que le pases 
   * 
   * @param  INT $id
   * @return Array
   */
  public function getInterpretacion_Id($id)
  {
    $sql="SELECT * 
           FROM interpretacion 
          WHERE id_interpretacion=$id";
    $resultados = $this->db->query($sql);
    $interpretacion =$resultados->result();
    return $interpretacion[0];
  }
  
    
  /**
   * Cambiar_status
   * 
   * Esto actualiza la interpretacion
   *   
   * @param  mixed $id_interpretacion
   * @return void
   */
  public function Actualizar_Interpretacion($id_interpretacion,$contenido){
    $this->db->set('texto',$contenido);
    $this->db->where('id_interpretacion', $id_interpretacion);
    $this->db->update('interpretacion'); 
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }  
  
  /**
   * Get_nomenclatura
   * 
   * Esta función sirve para regresar la nomenclatura de una dicom dependiendo el id del atabla contenido_consulta 
   *
   * @param  Int $id_contenido_consulta
   * @return String
   */
  public function Get_nomenclatura($id_contenido_consulta){
    $sql="SELECT concat( REPLACE(t.nim_sass,'/','-'),'_',e.id_estudios_sass) as nomemclatura 
            from toma_muestra t 
              INNER JOIN contenido_consulta c on t.id_toma_muestra=c.id_toma_muestra 
              INNER JOIN estudio e on c.id_estudio=e.id_estudio
          WHERE c.id_contenido_consulta='$id_contenido_consulta'";
    $resultados = $this->db->query($sql);
    $interpretacion =$resultados->result();
    return $interpretacion[0];
  }
  
  /**
   * Get_dicom_by_nomenclatura
   *
   * esta función regresa todas las dicom que existen por la nomenclatura
   * 
   * @param  String $nomenclatura
   * @return Array
  */
  public function Get_dicom_by_nomenclatura($nomenclatura){
    $sql="SELECT ruta 
            FROM `dicom_rutas` 
          WHERE `ruta` LIKE '%$nomenclatura%'";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }


  public function Get_estatus_pendientes($status){
    $sql="SELECT * FROM `status_pendiente` WHERE `nombre` LIKE '$status'";

    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }

  public function New_pendiente($id_contenido_consulta,$id_status_pendiente){

    $data=array(
      'id_contenido_consulta'=>$id_contenido_consulta,     
      'id_status_pendiente'=>$id_status_pendiente,     
    );
    
    $this->db->insert('pendiente',$data);
    return ($this->db->affected_rows() != 1) ? false : true;  
  }
  
  public function Is_pendiente($id_contenido_consulta){
    $sql="SELECT COUNT(*) existe FROM pendiente p 
            INNER JOIN status_pendiente s 
            on p.id_status_pendiente=s.id_status_pendiente
          WHERE s.nombre='EN PROCESO' and p.id_contenido_consulta=$id_contenido_consulta";
    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];  
  }

  public function Get_estatus_pendiente($estatus='FINALIZADO'){
    $sql="SELECT * 
            FROM `status_pendiente` 
          WHERE nombre='$estatus'";
    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];  
  }

  public function Update_pendientes($id_contenido_consulta,$id_status_pendiente){
    //UPDATE interpretacion SET texto = '' WHERE id_contenido_consulta = 11;
    $this->db->set('id_status_pendiente',$id_status_pendiente);
    $this->db->where('id_contenido_consulta', $id_contenido_consulta,);
    $this->db->update('pendiente');
    return ($this->db->affected_rows() != 1) ? false : true;    
  }

}
