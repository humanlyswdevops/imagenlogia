<?php

use phpDocumentor\Reflection\Types\Null_;

class Has_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  
  public function insert($nombre,$id_contenido_consulta){
    $data=array(      
      'id_dicom'=>'null',
      'nombre'=>$nombre,
      'id_contenido_consulta'=>$id_contenido_consulta
    );

    $this->db->insert('dicom',$data);
    $id = $this->db->insert_id();
    return $id;
  }

  public function Existe_personal($id){
    $sql="SELECT COUNT(*) as existe 
            FROM personal 
          WHERE id_empleado='$id'";

    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();    
    return $dataset[0];
  }

  public function ContenidoConsulta ($nim_sass){
    /*$sql="SELECT c.id_contenido_consulta,c.id_estudio
            from toma_muestra t INNER JOIN contenido_consulta c 
              on t.id_toma_muestra=c.id_toma_muestra
            WHERE t.nim_sass='$nim_sass'";*/

    $sql="SELECT c.id_contenido_consulta,c.id_estudio,e.id_estudios_sass
          from toma_muestra t INNER JOIN contenido_consulta c 
            on t.id_toma_muestra=c.id_toma_muestra INNER JOIN estudio e 
            on c.id_estudio=e.id_estudio
          WHERE t.nim_sass='$nim_sass'";
    
    

    $resultados = $this->db->query($sql);
    return $resultados->result();  
  }

  public function get_dicoms_byContenidoConsulta($id_contenidoConsulta){
    $sql="SELECT d.nombre
          FROM contenido_consulta c INNER JOIN dicom d 
            on c.id_contenido_consulta=d.id_contenido_consulta 
          WHERE c.id_contenido_consulta='$id_contenidoConsulta'";
    $resultados = $this->db->query($sql);
    return $resultados->result();  
  }

  public function get_estudio_byContenidoConsulta($id_contenidoConsulta){
    $sql="SELECT  e.nombre as 'estudio',e.id_estudios_sass as 'id_sass'
            from contenido_consulta c INNER JOIN estudio e 
              on e.id_estudio=c.id_estudio
            WHERE c.id_contenido_consulta='$id_contenidoConsulta'";
    $resultados = $this->db->query($sql);
    $data=$resultados->result(); 
    return $data[0];
  }
  
  /**
   * BuscaDicom
   * 
   * Pasando el nombre de la dicom 
   * regresa el nombre 
   *
   * @param  String $formato
   * @return Array
   */
  public function BuscaDicom($formato){    
    $sql="SELECT * 
            FROM dicom_rutas 
          WHERE ruta LIKE '%$formato%'";

    $resultados = $this->db->query($sql);
    return $resultados->result(); 
  }

  public function Busca_interpretacion($id_contenido_consulta=0)
  {
    $sql="SELECT * from interpretacion WHERE id_contenido_consulta=$id_contenido_consulta";
    $resultados = $this->db->query($sql);    
    $data=$resultados->result(); 
    return $data[0];
  }
}