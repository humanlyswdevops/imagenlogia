<?php
use phpDocumentor\Reflection\Types\Null_;

class Paciente_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  /**
   * Devuelve listado de tabla pacientes
   *   
   * este metodo regresa listado de los pacientes
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_pacientes(){
    $sql = "SELECT id_paciente as id, CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre 
              FROM paciente";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  /**
   * Devuelve un paciente por su id
   *   
   * este metodo regresa un paciente
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_paciente_byId($id=null){
    $sql = "SELECT id_paciente as id, CONCAT(nombre,' ',apellido_paterno,' ',apellido_materno) as nombre 
              FROM paciente
              WHERE id_paciente=$id";
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();
    return $dataset[0];
  }

    /**
   * inserta un estudio
   *      
   * Este método inserta un nuevo paciente
   * 
   * @access public
   * @param string $nombre nombre del paciente
   * @param string $apellido_p apellido paterno del paciente
   * @param string $apellido_m apellido materno del paciente
   * @param string $curp curp del paciente
   * @param string $id_laboratorio id del paciente en caso de tenerlo 
   * @return int id con el que se registro 
   */
  public function insert($nombre,$apellido_p,$apellido_m,$curp,$id_laboratorio='null'){
    $data=array(
      'id_paciente'=>'null',
      'nombre'=>$nombre,      
      'apellido_paterno'=>$apellido_p,
      'apellido_materno'=>$apellido_m,
      'curp'=>$curp,
      'id_laboratorio'=>$id_laboratorio
    );
    $this->db->insert('paciente',$data);
    return $this->db->insert_id();
  }
  
  /**
   * get_paciente_by_curp
   * 
   * con este valido si existe el paciente pesándole el curp 
   *
   * @param  String $curp curp del paciente
   * @return void
   */
  public function get_paciente_by_curp($curp){
    $sql="SELECT COUNT(*) as existe,p.id_paciente
            from paciente as p
          WHERE p.curp='$curp'";

    $resultados = $this->db->query($sql);
    $dataSet= $resultados->result();
    return $dataSet[0];
  }


  public function Get_name_By_NumSass($id_sass){
    $sql="SELECT concat(p.nombre,' ',p.apellido_paterno) as full_name
            FROM toma_muestra t INNER JOIN paciente p 
              on t.id_paciente= p.id_paciente
          WHERE t.nim_sass='$id_sass'";

    $resultados = $this->db->query($sql);
    $dataSet= $resultados->result();
    return $dataSet[0];

  }
}
