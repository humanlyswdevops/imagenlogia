<?php

use phpDocumentor\Reflection\Types\Null_;

class Dicom_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  /**
   * guardar informacion de la bd de la imagen dicom
   *   
   * este metodo inserta la informacion de la imagen dicom
   * 
   * @access public  
   * @param String $nombre nombre o url de la dicom
   * @param String $id_contenido_consulta id dela toma y estudio a la que pertenece la dicom
   *
   * @return int
   */
  public function insert($nombre,$id_contenido_consulta){
    $data=array(      
      'id_dicom'=>'null',
      'nombre'=>$nombre,
      'id_contenido_consulta'=>$id_contenido_consulta
    );

    $this->db->insert('dicom',$data);
    $id = $this->db->insert_id();
    return $id;
  }

  public function Get_By_ContenidoConsulta($id){
    $sql="SELECT * 
            FROM dicom 
          WHERE id_contenido_consulta=$id";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * Encuentra_dicoms
   * 
   * esto solo sirve para los laboratorio que la dicoms de suben en automático 
   * busca las imágenes por su patrón que es curp/fecha/nim del sass/id del estudio/
   * busca el inicio del patrón para después 
   *
   * @param  String $curp  curp del paciente
   * @param  String $nim_sass  
   * @param  String $id_estudio
   * @return void
  */
  public function Encuentra_dicoms($curp='',$fecha='',$nim_sass='',$id_estudio=''){
    
    $cadena="$curp/$fecha/$nim_sass/$id_estudio";  
    $sql="SELECT r.id_ruta,r.ruta 
            FROM dicom_rutas r 
            WHERE r.ruta LIKE '$cadena%'";    
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * Busca_dicom
   * 
   * esto solo sirve para los laboratorio que la dicoms de suben en automático 
   * esto sirve para encontrar la dicom 
   * regresa la información que ce necesitá para localizar la imagen dicom
   *
   * @param  Int $id_toma_muestra  id de la tabla de Toma de muestra
   * @return Array [curp,nim_sass,id_estudio,fecha]
   */
  public function Busca_dicom($id_toma_muestra){
    $sql="SELECT p.curp,t.nim_sass,e.id_estudios_sass,e.id_estudio,CONCAT(DAY(t.fecha),'-',MONTH(t.fecha),'-',YEAR(t.fecha)) as fecha,e.nombre as nombre_estudio,con.id_contenido_consulta,t.id_toma_muestra,t.datos_clinicos
            from paciente as p INNER JOIN toma_muestra as t 
              on p.id_paciente=t.id_paciente INNER JOIN contenido_consulta as con 
              on t.id_toma_muestra=con.id_toma_muestra INNER JOIN estudio as e
              on con.id_estudio=e.id_estudio
            WHERE t.id_toma_muestra=$id_toma_muestra
          GROUP by e.id_estudio";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }  

  public function Find_new_studys($id_toma_muestra){

    $sql="SELECT
            p.curp,
            t.nim_sass,
            e.id_estudios_sass,
            e.id_estudio,
            CONCAT(
                DAY(t.fecha),
                '-',
                MONTH(t.fecha),
                '-',
                YEAR(t.fecha)
            ) as fecha,
            e.nombre as nombre_estudio,
            con.id_contenido_consulta,
            t.id_toma_muestra,
            t.datos_clinicos
        from
            paciente as p
            INNER JOIN toma_muestra as t on p.id_paciente = t.id_paciente
            INNER JOIN contenido_consulta as con on t.id_toma_muestra = con.id_toma_muestra
            INNER JOIN estudio as e on con.id_estudio = e.id_estudio
        WHERE
            con.agregado_despues = true
            and t.id_toma_muestra = $id_toma_muestra
        GROUP by
            e.id_estudio";
    $resultados = $this->db->query($sql);
    return $resultados->result();

  }

  public function Find_propuestos($id_toma_muestra){
    $sql="SELECT p.curp,t.nim_sass,e.id_estudios_sass,e.id_estudio,CONCAT(DAY(t.fecha),'-',MONTH(t.fecha),'-',YEAR(t.fecha)) as fecha,e.nombre as nombre_estudio,con.id_contenido_consulta,t.id_toma_muestra,t.datos_clinicos,status_pendiente.nombre
          from
              paciente as p    
              INNER JOIN toma_muestra as t on p.id_paciente = t.id_paciente
              INNER JOIN contenido_consulta as con on t.id_toma_muestra = con.id_toma_muestra
              INNER JOIN estudio as e on con.id_estudio = e.id_estudio
              INNER JOIN pendiente pendi on pendi.id_contenido_consulta = con.id_contenido_consulta 
              INNER JOIN status_pendiente on status_pendiente.id_status_pendiente=pendi.id_status_pendiente
          WHERE
              t.id_toma_muestra = $id_toma_muestra and status_pendiente.nombre='EN PROCESO'
          GROUP by
              e.id_estudio;";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * Encuentra_dicoms
   * 
   * esto solo sirve para los laboratorio que la dicoms de suben en automático 
   * busca las imágenes por su patrón que es curp/fecha/nim del sass/id del estudio/
   * busca el inicio del patrón para después 
   *
   * @param  String $curp  curp del paciente
   * @param  String $nim_sass  
   * @param  String $id_estudio
   * @return void
  */
  public function Busca_dicoms($nim_sass='',$id_estudio='',$udn=''){

    $nim_sass = str_replace('/', '-', $nim_sass);
    
    $ruta="C:\\\\\\\\wamp\\\\\\\\www\\\\\\\\Imagenologia2\\\\\\\\$udn\\\\\\\\$nim_sass"."_"."$id_estudio".'_';
    $ruta2="$nim_sass"."_"."$id_estudio";
    $sql="SELECT r.id_ruta
            FROM dicom_rutas r 
            WHERE r.ruta LIKE '%$ruta2%'";    
    $resultados = $this->db->query($sql);
    
    return $resultados->result();
  }


  /**
   * Encuentra_dicoms
   * 
   * esto solo sirve para los laboratorio que la dicoms de suben en automático 
   * busca las imágenes por su patrón que es curp/fecha/nim del sass/id del estudio/
   * busca el inicio del patrón para después 
   *
   * @param  String $curp  curp del paciente
   * @param  String $nim_sass  
   * @param  String $id_estudio
   * @return void
  */
  public function Busca_dicomsV2($nim_sass='',$id_estudio=''){

    $nim_sass = str_replace('/', '-', $nim_sass);
    
    $ruta="$nim_sass"."_"."$id_estudio";
    
    $sql="SELECT r.id_ruta,r.ruta
            FROM dicom_rutas r 
            WHERE r.ruta LIKE '%$ruta%'";
    //echo $sql;
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  
  /**
   * Nueva_ruta
   *
   * esto solo se usa cuando la subida de las dicom son en automático
   * 
   * @param  String $ruta ruta
   * @return void
   */
  public function Nueva_ruta($ruta){
    $data=array(      
      'id_ruta'=>'null',
      'ruta'=>$ruta
    );

    $this->db->insert('dicom_rutas',$data);
    $this->db->insert_id();
    return ($this->db->affected_rows() != 1) ? false : true;
  }



  
  public function insertImagen($id_contenido_consulta,$imagen){
    $data=array(      
      'imagen'=>$imagen,
      'id_contenido_consulta'=>$id_contenido_consulta,
    );

    $this->db->insert('imagenes',$data);
    return ($this->db->affected_rows() != 1) ? false : true;
  }

  public function Get_imagenes  ($id=0){
    $sql="SELECT * 
            FROM `imagenes` 
          WHERE id_contenido_consulta=$id";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function Get_interpretaciones($id_contenido_consulta)
  {
    $sql="SELECT texto as 'interpretacion'  
            FROM `interpretacion` 
          WHERE `id_contenido_consulta` = $id_contenido_consulta";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }





}