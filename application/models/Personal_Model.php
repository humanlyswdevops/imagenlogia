<?php

use phpDocumentor\Reflection\Types\Null_;

class Personal_Model extends CI_Model{
  public function __construct(){
    $this->load->database();    
  }    
  
  /**
   * Inserta un personal
   *   
   * este metodo inserta un personal y regresa el id con el que se inserto
   * 
   * @access public
   * @param String $nombre nombre de personal
   * @param String $apellido_paterno apellido paterno de personal
   * @param String $apellido_materno apellido materno de personal
   * @param String $no_cedula max:20  numero de cedula de personal
   * @param String $id_empleado numero id HTDS
   * @param int $udn id de la udn
   *
   * @return int
   */
  public function insert($nombre,$apellido_paterno,$apellido_materno,$id_empleado,$udn){
    $data=array(      
      'id_personal'=>'null',
      'nombre'=>$nombre,     
      'apellido_paterno'=>$apellido_paterno,     
      'apellido_materno'=>$apellido_materno,      
      'id_empleado'=>$id_empleado,
      'udn'=>$udn
    );
    
    $this->db->insert('personal',$data);
    $id = $this->db->insert_id();
    return $id;
  }

  public function get_By_idHtds($id_Htds){
    $sql="SELECT id_personal 
            FROM personal 
          WHERE id_empleado ='$id_Htds'";

    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();    
    return $dataset[0];
  }
 
    
  /**
   * get_personal_ByPrivilegio
   *
   * Esto regresa una lista de personal por privilegios 
   * ejemplo: 'medico radiologo'
   * 
   * @param  String $privilegio nombre del privilegio
   * @return Array
   */
  public function get_personal_ByPrivilegio($privilegio){
    $sql="SELECT p.id_personal as id,CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as nombre 
          FROM privilegios as pri inner JOIN usuario as u 
            on pri.id_privilegios=u.privilegio INNER JOIN personal as p 
            on u.personal=p.id_personal
          WHERE pri.nombre='$privilegio'";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * Insert_cedula
   * 
   * Esto inserta una cedula pasando el id del empleado 
   *
   * @param  Int $id_personal
   * @param  String $cedula
   * @return void
   */
  public function Insert_cedula ($id_personal,$cedula){
    $data=array(      
      'id_cedulas'=>'null',
      'nombre'=>$cedula,     
      'id_personal'=>$id_personal
    );
    
    $this->db->insert('cedulas',$data);
    $id = $this->db->insert_id();
    return $id;
  }
  
  /**
   * existe_cedula 
   * 
   * 0 no existe 1 si existe
   *
   * @param  Int $no_cedula
   * @return String
   */
  public function existe_cedula($no_cedula){
    $sql="SELECT COUNT(*) as existe 
            FROM `cedulas` 
            WHERE nombre='$no_cedula'";
    $resultados = $this->db->query($sql);
    $datos=$resultados->result();
    return $datos[0];
  }
  
  /**
   * Get
   * 
   * Regresa lista de personal
   *
   * @return Array [nombre,id_empleado,udn,privilegio,id_personal]
   */
  public function Get(){
    $sql="SELECT CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as nombre,p.id_empleado,u.nombre as udn,pr.nombre as privilegio,pr.id_privilegios,p.id_personal, us.username
            FROM personal as p INNER JOIN udn as u
            on p.udn=u.id_udn INNER JOIN usuario as us
            on p.id_personal=us.personal INNER JOIN privilegios as pr 
            on us.privilegio=pr.id_privilegios";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * Get_todo
   * 
   * Regresa todas la información del personal
   *
   * @param  Int $id_personal
   * @return Array [id_empleado,id_personal,nombre,apellido_paterno,apellido_materno,udn,privilegio,foto,curp,acta_nacimiento,cv]
   */
  public function Get_todo($id_personal){
    $sql="SELECT p.id_personal,p.nombre,p.apellido_paterno,p.apellido_materno,p.id_empleado,p.udn,u.privilegio,p.foto,p.curp,p.acta_nacimiento,p.cv,p.cobro_estudio,p.cobro_interpretacion
            FROM personal as p INNER JOIN usuario as u 
            on p.id_personal=u.personal 
          WHERE id_personal='$id_personal'";

    $resultados = $this->db->query($sql);
    $personal =$resultados->result();
    return $personal[0];
  }  
  /**
   * Get_cedulas
   *
   * Regresa arreglo se las celulas dependiendo el id del personal
   * 
   * @param  INt $id_personal
   * @param Bolean $is_txt True => regresa texto | False => regresa ruta
   * @return Array [nombre,id_cedulas]
   */
  public function Get_cedulas($id_personal,$is_txt=true){
    if ($is_txt) {
      $sql="SELECT nombre,id_cedulas 
            FROM `cedulas` 
          WHERE id_personal='$id_personal' and is_texto=1 ";
    }else{
      $sql="SELECT nombre,id_cedulas 
        FROM `cedulas` 
      WHERE id_personal='$id_personal' and is_texto=0 ";
    }
    
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
    
  /**
   * Nueva_Cedula
   * 
   * Esto inserta una cedula por el id
   *
   * @param  String $nombre
   * @param  Int $id_personal
   * @return Int
   */
  public function Nueva_Cedula($nombre,$id_personal,$is_texto=''){    
    if ($is_texto=='') {
      $data=array(      
        'id_cedulas'=>'null',
        'nombre'=>$nombre,
        'id_personal'=>$id_personal        
      );  
    }else{
      $data=array(      
        'id_cedulas'=>'null',
        'nombre'=>$nombre,
        'id_personal'=>$id_personal,
        'is_texto'=>$is_texto
      );
    }

   // var_dump($data);
    
    $this->db->insert('cedulas',$data);
    return $this->db->insert_id();
  }
    
  /**
   * deleted_Cedula
   * 
   * esto elimina una cedula por el id de la cedula
   *
   * @param  Int $id_cedula
   * @return Boolean
   */
  public function deleted_Cedula($id_cedula){
    $sql="DELETE FROM `cedulas` 
          WHERE `cedulas`.`id_cedulas` = $id_cedula";
    $this->db->query($sql);
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }

  /**
   * cancelar_toma
   * 
   * Esto cambia es estado que cancela la toma
   *
   * @param  mixed $id_toma_muestra
   * @return void
   */
  public function update_user($id_personal,$nombre,$apellido_paterno,$apellido_materno,$id_empleado,$cobro_interpretacion,$cobro_estudio){
    $this->db->set('nombre',$nombre);
    $this->db->set('apellido_paterno',$apellido_paterno);
    $this->db->set('apellido_materno',$apellido_materno);
    $this->db->set('id_empleado',$id_empleado);    
    $this->db->set('cobro_interpretacion',$cobro_interpretacion);
    $this->db->set('cobro_estudio',$cobro_estudio);

    
    $this->db->where('id_personal', $id_personal);
    $this->db->update('personal'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2 
    return ($this->db->affected_rows() != 1) ? false : true;
                                     
  }

  /**
   * cancelar_toma
   * 
   * Esto cambia es estado que cancela la toma
   *
   * @param  mixed $id_toma_muestra
   * @return void
   */
  public function update_firma($id_personal,$firma){
    $this->db->set('firma',$firma);

    
    $this->db->where('id_personal', $id_personal);
    $this->db->update('personal'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2 
    return ($this->db->affected_rows() != 1) ? false : true;
                                     
  }
    
  /**
   * update_userFor_usser
   * 
   * esto actualiza a un personal por un id
   *
   * @param  Int $id_personal Id del personal
   * @param  String $nombre nombre 
   * @param  String $apellido_paterno apellido paterno
   * @param  String $apellido_materno apellido materno
   * @param  String $foto foto
   * @return Boolean
   */
  public function update_userFor_usser($id_personal,$nombre,$apellido_paterno,$apellido_materno,$foto){
    $this->db->set('nombre',$nombre);
    $this->db->set('apellido_paterno',$apellido_paterno);
    $this->db->set('apellido_materno',$apellido_materno);
    if ($foto!='') {
      $this->db->set('foto',$foto);  
    }
    
    
    $this->db->where('id_personal', $id_personal);
    $this->db->update('personal'); 
    return ($this->db->affected_rows() != 1) ? false : true;
  }
  
  /**
   * update_documentos
   * 
   * esto actualiza la curp y acta de nacimiento de un personal por id
   *
   * @param  Int $id_personal id del personal
   * @param  String $curp curp
   * @param  String $acta_nacimiento acta nacimiento
   * @return Boolean
   */
  public function update_documentos($id_personal,$curp,$acta_nacimiento){
    if($curp!=''){
      $this->db->set('curp',$curp);
    }
    if ($acta_nacimiento!='') {
      $this->db->set('acta_nacimiento',$acta_nacimiento);        
    }       
    
    $this->db->where('id_personal', $id_personal);
    $this->db->update('personal'); 
    return ($this->db->affected_rows() != 1) ? false : true;
  }
    
  /**
   * update_cv
   * 
   * Esto actualiza el cv de un personal por el cv
   *
   * @param  Int $id_personal
   * @param  Strin $cv
   * @return Boolean
   */
  public function update_cv($id_personal,$cv){    
    $this->db->set('cv',$cv);                   
    
    $this->db->where('id_personal', $id_personal);
    $this->db->update('personal'); 
    return ($this->db->affected_rows() != 1) ? false : true;
  }

  public function get_und_byUDN($id_personal){
    $sql="SELECT u.nombre as udn
            from personal p INNER JOIN udn u on p.udn=u.id_udn 
          WHERE p.id_personal='$id_personal'";
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();
    
    return $dataset[0];
  }
  
  /**
   * listaRadiologos
   * 
   * Esto regresa un listado de los medicos radiologo y sus udn
   *
   * @return Array
   */
  public function listaRadiologos(){
        
    $sql="SELECT concat(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as 'radiologo' ,pri.nombre as privilegio,udn.nombre as 'udn',per.id_personal
    from usuario as usser inner join personal  as per 
      on usser.personal=per.id_personal inner join privilegios as pri 
      on usser.privilegio=pri.id_privilegios inner join udn 
      on per.udn=udn.id_udn
    where pri.nombre='medico radiólogo' and (per.id_empleado= 'T904' or per.id_empleado='T906' or per.id_empleado='T903' or per.id_empleado='A130' or per.id_empleado='T692')";
    $resultados = $this->db->query($sql);
    return $resultados->result();

  }
  
}