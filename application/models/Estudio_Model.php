<?php

use phpDocumentor\Reflection\Types\Null_;

class Estudio_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  /**
   * Devuelve listado de tabla udn
   *   
   * este metodo regresa listado de los estudios
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_estudios(){
    $sql = "SELECT id_estudio as id ,nombre 
            FROM estudio";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

    /**
   * inserta un estudio
   *      
   * Este metodo inserta un nuevo estudio
   * 
   * @access public
   * @param string $estudio del privilegio a insertar
   * @return bolean
   */
  public function insert($nombre,$id_estudios_sass){
    $data=array(
      'id_estudio'=>'null',
      'nombre'=>$nombre,      
      'id_estudios_sass'=>$id_estudios_sass
    );
    $this->db->insert('estudio',$data);
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }

  /**
   * get_paciente_by_curp
   * 
   * con este valido si existe el paciente pesándole el curp 
   *
   * @param  String $curp curp del paciente
   * @return void
   */
  public function get_id_estudio_by_nombre($nombre){
    $sql="SELECT id_estudio 
            FROM estudio 
          WHERE nombre='$nombre'";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function get_id_estudioBy_nimSass($nim_sass=''){
    $sql="SELECT id_estudio from estudio 
            WHERE id_estudios_sass='$nim_sass'";
    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }
  



  public function get_todo(){
    $sql="SELECT * from estudio";            
    $resultados = $this->db->query($sql);
    return $resultados->result();    
  }

  public function Updated($id_estudio,$nombre,$honorario,$placas){

    $this->db->set('nombre',$nombre);
    $this->db->set('honorario',$honorario);
    $this->db->set('placas',$placas);

    $this->db->where('id_estudio', $id_estudio);
    $this->db->update('estudio'); 
    return ($this->db->affected_rows() != 1) ? false : true;
  }

  
  public function get_name_By_nimSass($nim_sass=''){
    $sql="SELECT nombre 
           FROM estudio 
          WHERE id_estudio='$nim_sass'";

    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }
  
  /**
   * get_ID_By_nimSass
   *
   * @param  mixed $nim_sass
   * @return void
   */
  public function get_ID_By_nimSass($nim_sass=''){
    $sql="SELECT id_estudio
           FROM estudio
          WHERE id_estudio='$nim_sass'";

    $resultados = $this->db->query($sql);
    $dataset=$resultados->result();
    return $dataset[0];
  }

}
