<?php

use phpDocumentor\Reflection\Types\Null_;

class Toma_Muestra_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  /**
   * Inserta un toma de muestra
   * 
   * este metodo inserta una toma de muestra en la bd
   * 
   * @access public  
   * @param String $id_personal id del personal (id de la session del personal)
   * @param String $id_paciente id  del paciente de la bd
   * @param String $nim_sass nim del sass (opcional)
   * @param String $datos_clinicos datos clinicos del paciente 
   *
   * @return int
   */
  public function insert($id_personal,$id_paciente,$nim_sass='',$datos_clinicos=''){
    $data=array(      
      'id_toma_muestra'=>'null',
      'id_personal'=>$id_personal,
      'id_paciente'=>$id_paciente,
      'nim_sass'=>$nim_sass,
      'realizo'=>$_SESSION['usuario']->privilegio,
      'datos_clinicos'=>$datos_clinicos
    );

    $this->db->insert('toma_muestra',$data);
    $id = $this->db->insert_id();
    return $id;
  }   
  
  /**
   * get_detalle
   *
   * Esta funcion regresa listado de las tomas pendientes por medicos radiologos
   * 
   * @param  mixed $id
   * @return void
   */
  public function get_detalle($id){
    $sql="SELECT p.id_paciente,CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as 'paciente',e.nombre as estudio,e.id_estudio,CONCAT(per.nombre,per.apellido_paterno,per.apellido_materno) as 'medico',per.id_personal as 'id_medico',c.id_contenido_consulta
            from toma_muestra as t INNER JOIN contenido_consulta as c
              ON t.id_toma_muestra=c.id_toma_muestra INNER JOIN estudio as e
              ON c.id_estudio=e.id_estudio INNER JOIN paciente as p
              ON t.id_paciente = p.id_paciente INNER JOIN personal as per
              ON t.id_personal=per.id_personal
            WHERE t.id_toma_muestra=$id";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }


    
  /**
   * get_tomas_vigentes
   *
   * Esto son los datos de tabla para asignar un medico 
   * 
   * @return void
   */
  public function get_tomas_vigentes(){
    $sql="SELECT t.id_toma_muestra,CONCAT(p.nombre,' ',p.apellido_paterno) as paciente,t.fecha,t.nim_sass
            FROM toma_muestra as t INNER JOIN paciente as p
            on t.id_paciente=p.id_paciente
          WHERE t.vigente=true";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
  
  /**
   * Existe_nimSass_inToma
   * 
   * regresa String 0 si no existe y 1 si existe
   *
   * @param  String $id_sass nim del sas
   * @return void
   */
  public function Existe_nimSass_inToma($id_sass){
    $sql="SELECT COUNT(*) as existe
            FROM `toma_muestra` 
          WHERE nim_sass='$id_sass'";
    $resultados = $this->db->query($sql);
    $dato=$resultados->result();
    return $dato[0];
  }
    
  /**
   * cancelar_toma
   * 
   * Esto cambia es estado que cancela la toma
   *
   * @param  mixed $id_toma_muestra
   * @return void
   */
  public function cancelar_toma($id_toma_muestra){
    $this->db->set('vigente',0);
    $this->db->where('id_toma_muestra', $id_toma_muestra);
    $this->db->update('toma_muestra'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2 
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }



  public function Get_estudios_Existentes($id_sass){
    $sql="SELECT c.id_contenido_consulta,t.id_toma_muestra,e.nombre,e.id_estudios_sass,e.id_estudio
            FROM toma_muestra t 
            INNER JOIN contenido_consulta c on t.id_toma_muestra=c.id_toma_muestra 
            INNER JOIN estudio e on c.id_estudio=e.id_estudio 
          WHERE t.nim_sass='$id_sass'";
    
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

}