<?php
//24/04/2021

use phpDocumentor\Reflection\Types\Null_;

class Asignacion_Automatica_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }

  /**
   * get_interpretaciones_terminadas
   * 
   * Esto regresa una lista con los id y cantidades de las interpretaciones realizadas por un medico radiologo
   *
   * @return Array [tomas,id_personal,]
   */
  public function get_interpretaciones_terminadas(){
    date_default_timezone_set('America/Mexico_City');
    $mes = date('m');
    $dia=date('d');  

    $sql = "SELECT  count(*) as tomas,p.id_personal,concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as radiologo
            from toma_muestra t INNER JOIN asignacion_interpretacion a 
              on t.id_toma_muestra=a.id_toma_muestras INNER JOIN estatus as e 
              on a.id_estatus=e.id_estatus INNER JOIN personal as p 
              on a.id_personal=p.id_personal
            where (e.nombre='TERMINADO' or e.nombre='EN PROCESO') and (day(t.fecha)=$dia and month(t.fecha)=$mes)
            group by p.id_personal";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }  
 

  /**
   * Get_lista_byRadiologos
   * 
   * esto trae un listado con el id y nombre completo de un medico radiologo
   *
   * @return Array [id,radiologo]
   */
  public function Get_lista_byRadiologos($condicion=false){
    $regla='';
    if ($condicion) {
      $regla="and per.interpreta=1";
    }
    $sql = "SELECT per.id_personal,CONCAT(per.nombre,' ',per.apellido_paterno,' ',per.apellido_materno) as radiologo
          from personal as per INNER JOIN usuario as us 
            on per.id_personal=us.personal INNER JOIN privilegios as priv 
            on us.privilegio=priv.id_privilegios
          WHERE priv.nombre='medico radiólogo' $regla ";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }


  
  /**
   * Get_lista_byRadiologos_por_UDN
   * 
   * Esto regresa una lista de los medico radiologos que atienden interpretan una udn 
   * dependiendo la udn que recibe 
   *
   * @param  int $udn id de la udn 
   * @return Array
   */
  public function Get_lista_byRadiologos_por_UDN($udn){
    $sql="SELECT u.id_udn,u.nombre, p.id_personal,CONCAT(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as radiologo
            from personal as p INNER JOIN filial_asignada as f 
            on p.id_personal=f.id_personal INNER JOIN udn as u 
            on f.id_udn=u.id_udn
          WHERE u.id_udn=$udn ";
    //echo $sql;
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  /**
   * BuscaUDN
   *
   * Esto se encarga de saber el id de la udn a la que pertenece el usuario 
   * (usuario activo $_SESSION) 
   * 
   * @param  int id de la session
   * @return ARRAY
   */
  public function BuscaUDN($id=0)
  {
    $sql="SELECT u.id_udn from personal as p INNER JOIN udn as u on p.udn=u.id_udn 
            WHERE p.id_personal=$id";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function Toma_sinEstudios(){
    $sql="SELECT p.id_personal,concat(p.nombre,' ',p.apellido_paterno,' ',p.apellido_materno) as 'radiologo' 
            FROM personal p 
          WHERE p.nombre='Estudio' and p.apellido_paterno='sin' and apellido_materno = 'interpretacion'";
    $resultados=$this->db->query($sql);
    $respuesta= $resultados->result();
    return $respuesta[0];
  }
}
