<?php
use phpDocumentor\Reflection\Types\Null_;

class Configuracion_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  public function existe_asignacion($id_interpretacion){
    $sql="SELECT * FROM `body_interpretacion` 
          WHERE `id_personal`=$id_interpretacion";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }  

    
  /**
   * Cambiar_status
   *
   * @param  mixed $id_personal
   * @return void
   */
  public function Cambiar_status($id_personal){
    $this->db->set('estatus',0);
    $this->db->where('id_personal', $id_personal);
    $this->db->update('body_interpretacion'); 
    return ($this->db->affected_rows() != 1) ? false : true;                                 
  }

  
  public function Activa_pre_interpretacion($id_body_interpretacion){
    
    $this->db->set('estatus','1');
    $this->db->where('id_body_interpretacion', $id_body_interpretacion);
    $this->db->update('body_interpretacion'); 
    return ($this->db->affected_rows() != 1) ? false : true;
                                    
  }
  public function Update_pre_interpretacion($body,$id_body_interpretacion,$titulo){

    $this->db->set('contenido',$body);
    $this->db->set('title',$titulo);
    $this->db->where('id_body_interpretacion', $id_body_interpretacion);
    $this->db->update('body_interpretacion'); 
    return ($this->db->affected_rows() != 1) ? false : true;
  
  }
  public function Insert_pre_interpretacion($id,$pre_interpretacion,$title=''){
    $data=array(      
      'id_body_interpretacion'=>'null',
      'contenido'=>$pre_interpretacion,
      'id_personal'=>$id,
      'title'=>$title
    );
    $this->db->insert('body_interpretacion',$data);
    return $this->db->insert_id();
  }

  public function Get_interpretacion_activa($id_personal){
    $sql="SELECT * 
            FROM body_interpretacion 
          WHERE id_personal=$id_personal and estatus=true";
    
    $resultados = $this->db->query($sql);
    $Dataset= $resultados->result();
    return $Dataset[0];
  }

  public function getFirma($id=0){
    $sql="SELECT p.firma 
            from personal as p 
          WHERE p.id_personal=$id";
    $resultados = $this->db->query($sql);
    $Dataset= $resultados->result();
    return $Dataset[0];
  }

  
  /**
   * GetMachotes
   *
   * @param  mixed $id
   * @return Array
   */
  public function GetMachotes($id=0){
    $sql="SELECT * FROM body_interpretacion i WHERE i.id_personal=$id";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
    
  /**
   * get_machoteById
   *
   * @param  mixed $id
   * @return Object
   */
  public function get_machoteById($id=0){
    $sql="SELECT * FROM body_interpretacion i WHERE i.id_body_interpretacion=$id";
    $resultados = $this->db->query($sql);
    $Dataset= $resultados->result();
    return $Dataset[0];
  }
  
}