<?php

use phpDocumentor\Reflection\Types\Null_;

/**
 * Admin_Model
 * 
 * Esta clase la uso para la parte de las gráficas 
 */
class Admin_Model extends CI_Model
{
  public function __construct()
  {
    $this->load->database();
  }
  
  /**
   * Count_noInterpretado
   *
   * Esto regresa los totales de tomas no interpretadas 
   * @return Int
   */
  public function Count_noInterpretado(){
    $sql="SELECT COUNT(*) as 'no_interpretada'
          from toma_muestra as t INNER JOIN asignacion_interpretacion as a 
            on t.id_toma_muestra=a.id_toma_muestras INNER JOIN estatus as es 
            on a.id_estatus=es.id_estatus
          WHERE es.nombre='EN PROCESO'";
    $resultados = $this->db->query($sql);
    $respues=$resultados->result();
    return $respues[0];
  }
  
  /**
   * Count_terminados
   * 
   *  Esto regresa los totales de las interpretación finalizadas
   * 
   * @return void
   */
  public function Count_terminados(){//
    $sql="SELECT COUNT(*) as 'terminado'
          from toma_muestra as t INNER JOIN asignacion_interpretacion as a 
            on t.id_toma_muestra=a.id_toma_muestras INNER JOIN estatus as es 
            on a.id_estatus=es.id_estatus
          WHERE es.nombre='TERMINADO'";
    $resultados = $this->db->query($sql);
    $respues=$resultados->result();
    return $respues[0];
  }
  
  /**
   * interpretabion_x_dia
   * 
   * Esto regresa una lista con las totales de interpretaciones mas el dia (lo ocupo para graficar) 
   *
   * @return Array [tomas,fecha]
   */
  public function interpretabion_x_dia(){
    $sql="SELECT COUNT(*) as tomas,tempo.fecha,tempo.dia,tempo.mes,tempo.years
            FROM(
                  SELECT DATE_FORMAT(i.created, '%d/%m/%Y') as fecha,day(i.created) as dia , month(i.created) as mes,year(i.created) as years
                  from toma_muestra as t INNER JOIN contenido_consulta as c 
                    on t.id_toma_muestra=c.id_toma_muestra INNER JOIN interpretacion as i 
                    on c.id_contenido_consulta=i.id_contenido_consulta INNER JOIN asignacion_interpretacion as a 
                    on t.id_toma_muestra=a.id_toma_muestras INNER JOIN estatus as es 
                    on a.id_estatus=es.id_estatus) as tempo
            GROUP BY tempo.fecha desc ";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
 
}
