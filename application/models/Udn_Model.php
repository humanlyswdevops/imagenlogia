<?php

use phpDocumentor\Reflection\Types\Null_;

class Udn_Model extends CI_Model{
  public function __construct(){
    $this->load->database();    
  }    

  /**
   * Devuelve listado de tabla udn
   *   
   * este metodo devulve listado de las udns
   * 
   * @access public   
   * @return array [id,nombre]
   */
  public function get_udns(){
    $sql="SELECT id_udn as id ,nombre 
            FROM udn";
    $resultados = $this->db->query($sql);
    return $resultados->result();
  }

  public function Existe($nombre){
    $sql="SELECT COUNT(*) as existe 
            FROM udn 
          WHERE nombre='$nombre'";

    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();    
    return $dataset[0];
  }

  public function Insert($nombre){
    $data=array(      
      'id_udn'=>'null',
      'nombre'=>$nombre      
    );

    $this->db->insert('udn',$data);
    $id = $this->db->insert_id();
    return $id;
  }

  public function Get_byName($nombre)
  {
    $sql="SELECT id_udn
            FROM udn 
          WHERE nombre='$nombre'";
    $resultados = $this->db->query($sql);
    $dataset= $resultados->result();    
    return $dataset[0];
  }
  
   
  
}