<?php

use phpDocumentor\Reflection\Types\Null_;

class Contenido_Consulta_Model extends CI_Model{
  public function __construct(){
    $this->load->database();
  }

  /**
   * crea un contenido de la consulta (maucho a muchos en la bd)
   *   
   * este metodo inserta el contenido de una toma de muesta
   * 
   * @access public  
   * @param String $id_toma_muestra id de la toma de muestra generada   
   * @param String $id_estudio id del estudio 
   *
   * @return int
   */
  public function insert($id_toma_muestra,$id_estudio,$agregado_despues=0){
    $data=array(      
      'id_contenido_consulta'=>'null',
      'id_toma_muestra'=>$id_toma_muestra,
      'id_estudio'=>$id_estudio,
      'agregado_despues'=>$agregado_despues
    );

    $this->db->insert('contenido_consulta',$data);
    $id = $this->db->insert_id();
    return $id;
  }

  
  /**
   * get_propuestos
   * 
   * Esta función regresa un conjunto de los estudios que se propusieron pesándole el id de la toma de muestra
   *
   * @param  mixed $id_toma_muestra
   * @return void
   */
  public function get_propuestos($id_toma_muestra){
    $sql = "SELECT c.id_contenido_consulta,es.nombre,s.nombre 
    FROM pendiente p 
    INNER JOIN contenido_consulta c on c.id_contenido_consulta=p.id_contenido_consulta 
    INNER JOIN estudio es on c.id_estudio=es.id_estudio 
    INNER JOIN status_pendiente s on p.id_status_pendiente=s.id_status_pendiente 
    WHERE s.nombre='EN PROCESO' and c.id_toma_muestra=$id_toma_muestra";

    $resultados = $this->db->query($sql);
    return $resultados->result();
  }
    
}