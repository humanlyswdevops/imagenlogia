<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sass extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
  }
  public function index()
  {

    $js='
    {
      "nombre": "ADELINA",
      "app": "RUIZ",
      "apm": "GONZÁLEZ",
      "curp":"ADRG",
      "estudios":[{
        "id":2,
        "nombre":"Rayos X",
        "precio":1000
      },{
        "id":3,
        "nombre":"Estudio 2",
        "precio":1200
      }]
    }
    ';

    echo json_encode($js);
  }
}
