<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Administracion extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Toma_Muestra_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Admin_Model');
    $this->load->model('Pagos_Model');
    $this->load->library('acceso');
  }

  public function index(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Administración";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $data = [
        'radiologos' => $this->Personal_Model->listaRadiologos(),
        'no_interpretadas'=>$this->Admin_Model->Count_noInterpretado(),
        'terminado'=>$this->Admin_Model->Count_terminados(),
        'graficadate'=>$this->Admin_Model->interpretabion_x_dia()
      ];

      $this->load->view('body/Body_admin', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  
  public function concentrado(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "CONSENTRADO";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $data = [
        'radiologos' => $this->Personal_Model->listaRadiologos(),
        'no_interpretadas' => $this->Admin_Model->Count_noInterpretado(),
        'terminado' => $this->Admin_Model->Count_terminados(),
        'graficadate' => $this->Admin_Model->interpretabion_x_dia()
      ];

      $this->load->view('body/Body_concentrado', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function get_pagos(){
    $year_inicio=$this->input->post('year_ini');
    $mes_inicio=$this->input->post('mes_ini');
    $dia_inicio=$this->input->post('dia_ini');

    $year_fin=$this->input->post('year_fin');
    $mes_fin=$this->input->post('mes_fin');
    $dia_fin=$this->input->post('dia_fin');

    $dataset=$this->Pagos_Model->Get_cedulas($year_inicio,$mes_inicio,$dia_inicio,$year_fin,$mes_fin,$dia_fin);
    echo json_encode($dataset);
  }

  public function consentrado(){  
    // $year_inicio=$this->input->post('year_ini');
    // $mes_inicio=$this->input->post('mes_ini');
    // $dia_inicio=$this->input->post('dia_ini');
    // $year_fin=$this->input->post('year_fin');
    // $mes_fin=$this->input->post('mes_fin');
    // $dia_fin=$this->input->post('dia_fin');

    $fecha_inicio=$this->input->post('fecha_uno');
    $fecha_fin=$this->input->post('fecha_fin');

    $id_personal=$this->input->post('id_radiologo');
    
    $dataset=$this->Pagos_Model->consentradoV2($id_personal,$fecha_inicio,$fecha_fin);
    // $dataset=$this->Pagos_Model->consentrado($year_inicio,$mes_inicio,$dia_inicio,$year_fin,$mes_fin,$dia_fin,$id_personal);
    echo json_encode($dataset);
  }


  public function consentrado_SinInterpretar(){
    $fecha_inicio=$this->input->post('fecha_uno');
    $fecha_fin=$this->input->post('fecha_fin');

    $id_personal=$this->input->post('id_radiologo');
    
    $dataset=$this->Pagos_Model->consentrado_SinInterpretar($id_personal,$fecha_inicio,$fecha_fin);
    // $dataset=$this->Pagos_Model->consentrado($year_inicio,$mes_inicio,$dia_inicio,$year_fin,$mes_fin,$dia_fin,$id_personal);
    echo json_encode($dataset);
  }
}
