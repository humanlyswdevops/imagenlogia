<?php
//24/04/2021
defined('BASEPATH') or exit('No direct script access allowed');

class Toma_Muestra extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Estudio_Model');
    $this->load->model('Paciente_Model');

    $this->load->model('Toma_Muestra_Model');
    $this->load->model('Contenido_Consulta_Model');
    $this->load->model('Dicom_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Asignacion_Automatica_Model');
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->library('acceso');

    $this->load->library('Elegir');    
  }

  public function index(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Toma de muestras";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        'lista_pacietes' => $this->Paciente_Model->get_pacientes(),
        'lista_estudios' => $this->Estudio_Model->get_estudios(),
        'modulo'=>$this->acceso->crud($privilegios,$modulo),        
        'accesos'=>$privilegios,
        'medicos' => $this->Personal_Model->get_personal_ByPrivilegio('medico radiologo')
      ];
      $this->load->view('body/body_tomaMuestra', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function Manual(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Toma de muestras";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        'lista_pacietes' => $this->Paciente_Model->get_pacientes(),
        'lista_estudios' => $this->Estudio_Model->get_estudios(),
        'modulo'=>$this->acceso->crud($privilegios,$modulo),        
        'accesos'=>$privilegios,
        'medicos' => $this->Personal_Model->get_personal_ByPrivilegio('medico radiologo')
      ];
      $this->load->view('body/body_tomaMuestra_manual', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  
  /**
   * Save
   *
   * Esto va a servir para los laboratorios que no tinen la subida de las dicom en automatico
   * 
   * @return void
   */
  public function Save(){
   
    $id_personal = $_SESSION['usuario']->id_personal; //este va a hacer el ide de la personal que va estar logeada 
    $paciente = $this->input->post('paciente');
    $sass = $this->input->post('nim_sass');
    $valida_paciente=$this->Paciente_Model->get_paciente_by_curp($paciente);
    if($valida_paciente->existe=='1'){
      $paciente=$valida_paciente->id_paciente;
      $folio = $this->Toma_Muestra_Model->insert($id_personal, $paciente, $sass);
      $estudios_identificador = $this->input->post('estudios_identificador');
      foreach ($estudios_identificador as $key => $estudio) {
        $datoEstudio = explode("-", $estudio);
        //esto viene asi estudio-1 nos intereza el numero ya que es el id del estudio
        $id_estudio = $datoEstudio[1];
        $validaEstudio=$this->Estudio_Model->get_id_estudioBy_nimSass($id_estudio);
        
        if ($validaEstudio->id_estudio!='') {          
          $id_estudioDataBase=$validaEstudio->id_estudio;
          $subFolio = $this->Contenido_Consulta_Model->insert($folio, $id_estudioDataBase);

          //la imagenes dicom vinen asi dicom_2 donde el numero es el id del estudio
          $dicomText = "dicom_$id_estudio";
          $imagenes = $this->input->post($dicomText);          
          foreach ($imagenes as $key => $dicom) {
            $this->Dicom_Model->insert($dicom, $subFolio);
          }
        }        
      }
      $respuesta=[];
      /**
       * Asignamos en automático un medico radiologo si se realizo la toma de muestra
       */
      if ($folio>0) {
        $lista_radiologo=$this->Asignacion_Automatica_Model->Get_lista_byRadiologos(true);
        $lista_Count=$this->Asignacion_Automatica_Model->get_interpretaciones_terminadas();
        $radiologo=$this->EligeRadiologo($lista_radiologo,$lista_Count);
        $id_radiologo=$radiologo['id'];
        $nombre=$radiologo['nombre'];
        //echo "Medico Elegido ". $radiologo['nombre']."<br/>";
        //echo "id del medico ",$radiologo['id'];
        /********************************************/
        /*        Realizamos la asignación          */
        /********************************************/
        $this->Asignacion_Interpretacion_Model->insert($id_radiologo, $folio);      
        $this->Toma_Muestra_Model->cancelar_toma($folio);
        $respuesta = [
          'status' => 'success',
          'msg' => "Toma creada correctamente, Se asigno toma al Medico Radiológo: $nombre",
          'medico' =>  $nombre,
          'id_created'=>$folio
        ];
      }else{
        $respuesta = [
          'status' => 'error',
          'msg' => "Error al crear la toma"        
        ];
      }
    }else{
      $respuesta = [
        'status' => 'error',
        'msg' => "Paciente no valido"        
      ];
    }
    
    echo json_encode($respuesta);
  }

  // esto solo es de prubas para simular la subida del dicom
  public function Save_dicom()
  {
    $this->load->helper('form');
    $dicom = $_FILES['dicom']['name'];
    $paciente_id = $this->input->post('paciente_id');
    $respuesta = [
      "name_dicom" => $dicom,
      "paciente_id" => $paciente_id
    ];
    echo json_encode($respuesta);
  }

  /**
   * Guarda_automatico
   * 
   * Notas: en estos casos no llega el curp por lo que su id del sass lo trataremos como el curp 
   * de igual manera nuestro apellidos no llega por separado por lo que vamos a ocupar el paterno como ambos apellidos
   *
   * Esto va a guardar para laboratorios que su guardado es automático
   *
   * @return void
   */
  
  public function Guarda_automatico(){
    $id_personal = $_SESSION['usuario']->id_personal; //este va a hacer el ide de la personal que va estar logeada 
    $curp=$this->input->post('curp');
    $comentario=$this->input->post('comentario');

    $nombre=$this->input->post('nombre');
    $apellido_paterno=$this->input->post('app');
    $apellido_materno=$this->input->post('apm');    
    $id_laboratorio=null;
    $estudios=$this->input->post('estudios');
    $sass=$this->input->post('sass');  
    $is_asignated=$this->input->post('is_asignated');//esta es la bandera que significa si asigna a un medico o no asigna    
    $valida=$this->Toma_Muestra_Model->Existe_nimSass_inToma($sass);    
    if($valida->existe!='1'){
      /**
       * esto es para validar si existe por su curp sacamos el id del paciente de lo contrario tenemos guardarlo y traer su id 
      */    
      $paciente=$this->Paciente_Model->get_paciente_by_curp($curp);
      // $paciente=$paciente[0];
      $id_paciente=0;
      if ($paciente->existe=='1') {      
        $id_paciente=$paciente->id_paciente;            
      } else {
        $id_paciente= $this->Paciente_Model->insert($nombre,$apellido_paterno,$apellido_materno,$curp,$id_laboratorio);            
      }            
      /**
       * creamos la consultas
       */
      $folio = $this->Toma_Muestra_Model->insert($id_personal, $id_paciente, $sass,$comentario);      
      /**
       * añadimos todos los estudios que se realizo
       */
      
      $JsonEstudios=json_decode($estudios);
      $succes=false;
      //////////////////////////////////////////////////////////////////////
      foreach ($JsonEstudios as $key => $value) {        
        $id_estudio=0;
        $nombreEstudio=strtoupper($value->nombre);      
        $nombreEstudio=trim($nombreEstudio);
        $id_estudio=$this->Estudio_Model->get_id_estudio_by_nombre($nombreEstudio);        
        $id_estudio=$id_estudio[0];
        $id_estudio=$id_estudio->id_estudio; 
        
        
        //Estudios mal 
        //RX TELE DE TORAX AP Y LAT
        if ($nombreEstudio== 'RX TELE DE TORAX PA Y LAT') {
          $id_estudio=50;
        }
        if($nombreEstudio=='RX COLUMNA LUMBAR APLAT Y OBLICUAS'){
          $id_estudio=85;
        }
        if ($nombreEstudio=='RX ABDOMEN SIMPLE DE PIE Y DE DECUBITO (2 POSICIONES)') {
          $id_estudio=63;
        }

        if ($id_estudio==0||$id_estudio<0) {
          if ($this->Estudio_Model->insert($nombreEstudio,$value->id)) {          
            $id_estudio=$this->Estudio_Model->get_id_estudio_by_nombre($nombreEstudio);                        
            $this->Contenido_Consulta_Model->insert($folio, $id_estudio);            
          }else{            
            $respuesta = [
              'status' => 'error',
              'msg' => "Error al guardar el estudio",
              'log'=>"SELECT id_estudio FROM estudio WHERE nombre='$nombreEstudio'"
            ];
          }
        }else{
          $this->Contenido_Consulta_Model->insert($folio, $id_estudio);
          // $respuesta = [
          //   'status' => 'success',
          //   'msg' => 'Toma creada correctamente',
          //   'url' => base_url('Asignar_Medico'),
          //   'id_created'=>$folio
          // ];
          $succes=true;
          
        }
      }      
      //////////////////////////////////////////////////////////////////////      
      //            Asignamos medico radiologo en automático
      //////////////////////////////////////////////////////////////////////
      if ($succes) {    
        $radiologo=$this->EligeRadiologoBy_udn($is_asignated);
        $id_radiologo=$radiologo->id_personal;
        $nombre=$radiologo->radiologo;

        //echo "Medico Elegido ". $radiologo['nombre']."<br/>";
        //echo "id del medico ",$radiologo['id'];
        /********************************************/
        /*        Realizamos la asignación          */
        /********************************************/
        $this->Asignacion_Interpretacion_Model->insert($id_radiologo, $folio);      
        $this->Toma_Muestra_Model->cancelar_toma($folio);
        /********************************************/
        $respuesta = [
          'status' => 'success',
          'msg' => "Toma creada correctamente, Se asigno toma al Medico Radiológo: $nombre",
          'medico' =>  $nombre,
          'id_created'=>$folio
        ];
      }
    }else{
      $respuesta = [
        'status' => 'error',
        'msg' => 'Este numero de Sass ya fue tomado'
      ];
    }
    echo json_encode($respuesta);
  }

  public function prueba(){
    //echo "Hola";        
    $lista_radiologo=$this->Asignacion_Automatica_Model->Get_lista_byRadiologos(true);
    $lista_Count=$this->Asignacion_Automatica_Model->get_interpretaciones_terminadas();

    $radiologo=$this->EligeRadiologo($lista_radiologo,$lista_Count);
    
    echo "Medico Elegido ". $radiologo['nombre']."<br/>";
    echo "id del medico ",$radiologo['id'];        
  }
  
  /**
   * EligeRadiologo
   * 
   * Este algoritmo  regresa un medico radiologo el cual se asigna en automático
   * se base en el arreglo lista_radiologo el cual trae una lista de todos los usuario 
   * con privilegio de medico radiologo.
   * y con el arreglo $lista_Count sabemos el id y nombre y cantidades de interpretaciones finalizadas 
   * el filtro es por dia actual 
   * 
   *
   * @param  Array $lista_radiologo lista de todos los médicos radiologo
   * @param  Array $lista_Count lista de las cantidades de interpretaciones echas 
   * @return void
   */
  public function EligeRadiologo($lista_radiologo,$lista_Count){
    $radiologos = [];
    $bandera = false;
    // este arreglo llevala los totales de INTERPRETACIÓN en general solo controlara el min y max 
    $MaxMin = [];
    //  este recorrido crea el arreglo de los radiologo junto a las interpretaciones que se le an asignado
    foreach ($lista_radiologo as $key => $radiologo) {
      foreach ($lista_Count as $key => $item) {
        if ($item->id_personal == $radiologo->id_personal) {
          array_push($MaxMin, intval($item->tomas));
          array_push($radiologos, [
            'num_tomas' => intval($item->tomas),
            'id' => $item->id_personal,
            'nombre' => $item->radiologo
          ]);
          $bandera = true;
        }
      }
      if ($bandera == false) {
        array_push($MaxMin, 0);
        array_push($radiologos, [
          'num_tomas' => 0,
          'id' => $radiologo->id_personal,
          'nombre' => $radiologo->radiologo
        ]);
      }
      $bandera = false;
    }

    // asta este punto se tiene arreglo con el radiologo mas el numero de sus interpretaciones echas
    $toma_minima = min($MaxMin);
    $toma_maxima = max($MaxMin);
    foreach ($radiologos as $key => $radio) {
      // buscamos el primer radiologo con la interpretación mínima 
      if ($radio['num_tomas'] == $toma_minima) {
        $respuesta=[
          'nombre'=>$radio['nombre'],
          'id'=>intval($radio['id'])
        ];
        return $respuesta;
      }
    }    
  }

  public function Correo(){
    echo "Hola";
  }

  public function Guarda_dicom(){
    date_default_timezone_set('MET');
    
    $paciente_id = $this->input->post('id_paciente');
    $id_estudio=$this->input->post('id_estudio');
    $idsass=$this->input->post('idsass');

    
    $day=date('d_m_Y');  
    $dicomName = strtr($_FILES['dicom']['name'], " ", "_");
    $ramdom1=rand(1,1000);   
    $dicomName=$idsass.'_'.$id_estudio.'_'.$paciente_id.'_'.$day.'_'.$ramdom1;

    $_FILES['dicom']['name']=$dicomName;    

    $mi_archivo = 'dicom';
    $config['upload_path'] = "uploads/dicoms/";    
    $config['allowed_types'] = "*";
    $this->load->library('upload', $config);    
    if (!$this->upload->do_upload($mi_archivo)) {
      //*** ocurrio un error
      $data['uploadError'] = $this->upload->display_errors();
      echo $this->upload->display_errors();
      return;
    }

    $data['uploadSuccess'] = $this->upload->data();
        
    $respuesta = [
      "name_dicom" => $dicomName,
      "paciente_id" => $paciente_id
    ];
    echo json_encode($respuesta);    
  }



  
  /**
   * EligeRadiologoBy_udn
   *
   * Este devuelve un objeto de medico radiologo dependiendo la udn que le corresponde 
   * 
   * @return Object
   */
  public function EligeRadiologoBy_udn($is_asignated='no'){    

    if ($is_asignated=='si') {
      $id_personal = $_SESSION['usuario']->id_personal; 
      $udn=$this->Asignacion_Automatica_Model->BuscaUDN($id_personal);
      
      $lista_radiologo=$this->Asignacion_Automatica_Model->Get_lista_byRadiologos_por_UDN($udn[0]->id_udn);     
      
      if (sizeof($lista_radiologo)>0) {
        $leng = sizeof($lista_radiologo);
        $elegir=rand ( 1 , $leng );
        //var_dump($lista_radiologo[$elegir-1]);      
        return $lista_radiologo[$elegir-1];
      }else{
        echo 'Error';
      }
    }else{
      $lista_radiologo=$this->Asignacion_Automatica_Model->Toma_sinEstudios();     
      return $lista_radiologo;      
      //id_personal
    }
  }
  
  /**
   * Validate_studys
   * 
   * Esta funcion regresa el total del estudio por el nim del sass
   *
   * @return void
   */
  public function Validate_studys(){

    $nim_sass = $this->input->post('nim_sass');

    $conjunto = $this->Toma_Muestra_Model->Get_estudios_Existentes($nim_sass);
    echo json_encode($conjunto);
  }

  public function Add_studys(){
    
    $estudios = $this->input->post('estudios'); 
    
    $json= json_decode($estudios);          

    foreach ($json as $key => $item) {
      $id_estudio_interno=$this->Estudio_Model->get_id_estudioBy_nimSass($item->id_examen);
      $id=$this->Contenido_Consulta_Model->insert(
                                            $item->id_toma_muestra,
                                            $id_estudio_interno->id_estudio,
                                            1
                                          );
                                          
    }
    if ($id) {
      $respuesta = [
        'status' => 'success',
        'msg' => "Estudio Agregado Correctamente."       
      ];
    }else{
      $respuesta = [
        'status' => 'error',
        'msg' => "Error no se pudo agregar Estudio al nim."
      ];
    }
    echo json_encode($respuesta);
                                    
  }

}
