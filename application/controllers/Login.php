<?php

class Login extends CI_Controller {
    
    
  public function __construct(){
      parent::__construct();  
    $this->load->model('Login_Model');    
  }

	public function index(){
    
    if (isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url('Home'), 'refresh');
    }
		$this->load->view('body/login');		
  }
  public function login2(){
		$this->load->view('body/login2');		
  }
  

	public function Ingresar(){
    $usser=$this->input->post('usser');
    $password=$this->input->post('password');
    $respuesta=$this->Login_Model->valida($usser,$password);
    $_SESSION['usuario']=$respuesta[0];
    echo json_encode($respuesta);
	}
	public function Salir(){
    session_destroy();
    echo base_url();
  }
  
}
