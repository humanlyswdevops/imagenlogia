<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Correo extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    //Cargamos la librería email
    $this->load->library('email');

    /*
      * Configuramos los parámetros para enviar el email,
      * las siguientes configuraciones es recomendable
      * hacerlas en el fichero email.php dentro del directorio config,
      * en este caso para hacer un ejemplo rápido lo hacemos 
      * en el propio controlador
    */
    $config['mailtype'] = 'html';
    //Indicamos el protocolo a utilizar
    $config['protocol'] = 'mail';
    //El servidor de correo que utilizaremos
    $config["smtp_host"] = 'host';
    //Nuestro usuario
    $config["smtp_user"] = 'gonzalez.c0d3@gmail.com';
    //Nuestra contraseña
    $config["smtp_pass"] = 'deadmau5A';
    //El puerto que utilizará el servidor smtp
    $config["smtp_port"] = 587;
    //El juego de caracteres a utilizar
    $config['charset'] = 'utf-8';
    //Permitimos que se puedan cortar palabras
    $config['wordwrap'] = TRUE;
    //El email debe ser valido  
    $config['validate'] = true;
    //Establecemos esta configuración
    $this->email->initialize($config);
    //Ponemos la dirección de correo que enviará el email y un nombre
    $this->email->from('l.enrique@humanly-sw.com', 'Acceso');
    /*
      * Ponemos el o los destinatarios para los que va el email
      * en este caso al ser un formulario de contacto te lo enviarás a ti
      * mismo
      * gonzalez.c0d3@gmail.com
    */
    $this->email->to($this->input->post('email'), 'Acceso');
    $code = $this->input->post('curp');
    //Definimos el asunto del mensaje
    $this->email->subject('Ingreso al sistema');
    $htmlContent = '<h1>Hola, el el siguiente link podrás ingresar al sistema para visualizar tu historial clínico</h1>';
    $htmlContent .= '<a href="https://humanly-sw.com/app_imagenologia/">Ingresar</a>';
    $htmlContent .= "<p>Código de ingreso $code</p>";

    //Definimos el mensaje a enviar
    $this->email->message(
      $htmlContent
    );

    //Enviamos el email y si se produce bien o mal que avise con una flasdata
    
    if ($this->email->send()) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Correo mandado'
      ];
      // echo "si";
      //   var_dump($this->email->print_debugger());
      echo json_decode($respuesta);
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al mandar correo'
      ];
      echo json_decode($respuesta);
      // echo "no";
      //   var_dump($this->email->print_debugger());
    }
    $respuesta = [
      'status' => 'error',
      'msg' => 'Error al mandar correo'
    ];
    echo json_decode($respuesta);    
  }
}
