<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Muestras_Pendientes extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
    $this->load->model('Contenido_Consulta_Model');
    $this->load->library('acceso');
  }

  public function index()
  {
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $privilegio=$_SESSION['usuario']->privilegio;

    $modulo = "Muestras pendientes";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        'tabla'  => $this->Asignacion_Interpretacion_Model->get_pendientes($id_personal,$privilegio),
        'Asignados_despues'=> $this->Asignacion_Interpretacion_Model->get_pendiente_agregadoDespues($id_personal,$privilegio),
        'estudios_pospone'=> $this->Asignacion_Interpretacion_Model->Get_pospone($id_personal,$privilegio)
      ];      
      $this->load->view('body/body_pendientes', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function Historial(){

    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $privilegio=$_SESSION['usuario']->privilegio;
    $modulo = "Historial";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        'tabla'  => $this->Asignacion_Interpretacion_Model->get_historico($privilegio,$id_personal)
      ];
      
      
      $this->load->view('body/body_historico', $data);
      $this->load->view('default/footer');            
      $this->load->view('default/scrips');
    }
  }

  public function datos_Pendientes_trataos(){
    $id_personal = $_SESSION['usuario']->id_personal;
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    $privilegio=$_SESSION['usuario']->privilegio;
    $dataset=$this->Asignacion_Interpretacion_Model->get_historico($privilegio,$id_personal);   
    
    $conjunto=[];
    foreach ($dataset as $key => $estudio) {      
      $existePendiente= $this->Contenido_Consulta_Model->get_propuestos($estudio->id_toma_muestras);    
      //var_dump($existePendiente);
      $conjunto[]=[
        'fecha'=> $estudio->fecha,
        'id_estatus'=>$estudio->id_estatus,
        'modificado'=>$estudio->modificado,
        'paciente'=>$estudio->paciente,
        'medico'=>$estudio->medico,
        'estatus'=>$estudio->estatus,
        'id_tecnico'=>$estudio->id_tecnico,
        'id_medico'=>$estudio->id_medico,
        'id_toma_muestra'=>$estudio->id_toma_muestra,
        'id_paciente'=>$estudio->id_paciente,
        'nim_sass'=>$estudio->nim_sass,
        'vigente'=>$estudio->vigente,
        'realizo'=>$estudio->realizo,
        'id_personal'=>$estudio->id_personal,
        'created'=>$estudio->created,
        'id_toma_muestras'=>$estudio->id_toma_muestras,
        'udn'=>$estudio->udn,
        'pospuestos'=>$existePendiente
      ];      
    }
    //echo json_encode($conjunto);
    //var_dump($conjunto);
  }

  public function Ver($id_toma){  
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }

    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Historial";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $data = [
        'interpretaciones' => $this->Interpretacion_Model->get_interpretaciones($id_toma)
      ];
      $this->load->view('body/body_ver_interpretaciones', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  
  /**
   * Earring_byDate 
   * 
   * Este metodo regresa los pendientes por un rango de fechas
   *
   * @return void
   */
  public function Earring_byDate(){
    $id_medico=$this->input->post('id_medico');
    $formato_inicio=$this->input->post('formato_inicio');
    $formato_fin=$this->input->post('formato_fin');
    
    $Earring=$this->Asignacion_Interpretacion_Model->get_pendientes_BYDate($id_medico,$formato_inicio,$formato_fin);
    
    echo json_encode($Earring);
  }

}
