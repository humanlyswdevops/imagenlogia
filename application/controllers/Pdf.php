<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pdf extends CI_Controller
{
  public function __construct()
  {    
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
  }
  public function index()
  {
    echo '';
  }

  public function GenerarPdf_Interpretacion($id)
  {
    $interpretacion = $this->Interpretacion_Model->get_interpretacion($id);

    //sdate_default_timezone_set('America/Los_Angeles'); //change zone as per need
    $viewdata = [
      'interpretacion' => $interpretacion,
      'membretada'=>true
    ];

    $html = $this->load->view('body/body_pruebas', $viewdata, TRUE);

    // Cargamos la librería
    $this->load->library('pdfgenerator');
    // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
    $filename = 'Estudios';
    // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
    $this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
  }
  
  public function ImprimirPdf_Interpretacion($id){
    $interpretacion = $this->Interpretacion_Model->get_interpretacion($id);

    //sdate_default_timezone_set('America/Los_Angeles'); //change zone as per need
    $viewdata = [
      'interpretacion' => $interpretacion,
      'membretada'=>false
    ];

    $html = $this->load->view('body/body_pruebas', $viewdata, TRUE);

    // Cargamos la librería
    $this->load->library('pdfgenerator');
    // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
    $filename = 'Estudios';
    // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
    $this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
  }

  public function GenerarPdf_ver($id)
  {
    $interpretacion = $this->Interpretacion_Model->get_interpretacion($id);

    //sdate_default_timezone_set('America/Los_Angeles'); //change zone as per need
    $viewdata = [
      'interpretacion' => $interpretacion
    ];

    $this->load->view('body/body_document', $viewdata);

    // Cargamos la librería
    //$this->load->library('pdfgenerator');
    // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
    //$filename = 'Estudios';
    // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
    //$this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
  }



  public function Recibo()
  {
    /*
       * Cuando cargamos una librería
       * es similar a hacer en PHP puro esto:
       * require_once("libreria.php");
       * $lib=new Libreria();
       */

    //Cargamos la librería email
    $this->load->library('email');

    /*
        * Configuramos los parámetros para enviar el email,
        * las siguientes configuraciones es recomendable
        * hacerlas en el fichero email.php dentro del directorio config,
        * en este caso para hacer un ejemplo rápido lo hacemos
        * en el propio controlador
        */

    //Indicamos el protocolo a utilizar
    $config['protocol'] = 'smtp';

    //El servidor de correo que utilizaremos
    $config["smtp_host"] = 'smtp.gmail.com';

    //Nuestro usuario
    $config["smtp_user"] = 'gonzalez.c0d3@gmail.com';

    //Nuestra contraseña
    $config["smtp_pass"] = 'deadmau5A';

    //El puerto que utilizará el servidor smtp
    $config["smtp_port"] = '587';

    //El juego de caracteres a utilizar
    $config['charset'] = 'utf-8';

    //Permitimos que se puedan cortar palabras
    $config['wordwrap'] = TRUE;

    //El email debe ser valido 
    $config['validate'] = true;


    //Establecemos esta configuración
    $this->email->initialize($config);

    //Ponemos la dirección de correo que enviará el email y un nombre
    $this->email->from('gonzalez.c0d3@gmail.com', 'Luis E');

    /*
       * Ponemos el o los destinatarios para los que va el email
       * en este caso al ser un formulario de contacto te lo enviarás a ti
       * mismo
       */
    $this->email->to('gonzalez.c0d3@gmail.com', 'Destino');

    //Definimos el asunto del mensaje
    $this->email->subject('receta');

    //Definimos el mensaje a enviar
    $this->email->message(
      "MEnsaje de pruebas"
    );

    //Enviamos el email y si se produce bien o mal que avise con una flasdata
    if ($this->email->send()) {
      $this->session->set_flashdata('envio', 'Email enviado correctamente');
    } else {
      $this->session->set_flashdata('envio', 'No se a enviado el email');
    }
    echo "*******************";
  }

  public function Save()
  {
    // $mi_archivo = 'dicom';
    // $config['upload_path'] = "http://187.188.166.50/imagenologia2/Acatzingo/";
    // $config['file_name'] = "nombre_archivo";
    // $config['allowed_types'] = "*";
    // $config['max_size'] = "100000";


    // $this->load->library('upload', $config);

    // if (!$this->upload->do_upload($mi_archivo)) {
    //   //*** ocurrio un error
    //   $data['uploadError'] = $this->upload->display_errors();
    //   echo $this->upload->display_errors();
    //   return;
    // }

    // $data['uploadSuccess'] = $this->upload->data();
    // echo 'succes';


    // Primero creamos un ID de conexión a nuestro servidor
    $cid = ftp_connect("192.168.100.118");
    // // Luego creamos un login al mismo con nuestro usuario y contraseña
    $resultado = ftp_login($cid, "root", "sudo");
    // // Comprobamos que se creo el Id de conexión y se pudo hacer el login
    if ((!$cid) || (!$resultado)) {
      echo "Fallo en la conexión";
      die;
    } else {
      echo "Conectado.";
    }
    // // Cambiamos a modo pasivo, esto es importante porque, de esta manera le decimos al 
    // //servidor que seremos nosotros quienes comenzaremos la transmisión de datos.
    // ftp_pasv ($cid, true) ;
    // echo "<br> Cambio a modo pasivo<br />";
    // // Nos cambiamos al directorio, donde queremos subir los archivos, si se van a subir a la raíz
    // // esta por demás decir que este paso no es necesario. En mi caso uso un directorio llamado boca
    // ftp_chdir($cid, "boca");
    // echo "Cambiado al directorio necesario";   
    // // Tomamos el nombre del archivo a transmitir, pero en lugar de usar $_POST, usamos $_FILES que le indica a PHP
    // // Que estamos transmitiendo un archivo, esto es en realidad un matriz, el segundo argumento de la matriz, indica
    // // el nombre del archivo
    // $local = $_FILES["archivo"]["name"];
    // // Este es el nombre temporal del archivo mientras dura la transmisión
    // $remoto = $_FILES["archivo"]["tmp_name"];
    // // El tamaño del archivo
    // $tama = $_FILES["archivo"]["size"];
    // echo "<br />$local<br />";
    // echo "$remoto<br />";
    // echo "subiendo el archivo...<br />";
    // // Juntamos la ruta del servidor con el nombre real del archivo
    // $ruta = "/srv/www/htdocs/boca/" . $local;
    // // Verificamos si no hemos excedido el tamaño del archivo
    // if (!$tama<=$_POST["MAX_FILE_SIZE"]){
    //   echo "Excede el tamaño del archivo...<br />";
    // } else {
    //   // Verificamos si ya se subio el archivo temporal
    //   if (is_uploaded_file($remoto)){
    //     // copiamos el archivo temporal, del directorio de temporales de nuestro servidor a la ruta que creamos
    //     copy($remoto, $ruta);		
    //   }
    //   // Sino se pudo subir el temporal
    //   else {
    //     echo "no se pudo subir el archivo " . $local;
    //   }
    // }
    // echo "Ruta: " . $ruta;
    // //cerramos la conexión FTP
    // ftp_close($cid);
  }

  public function ftp()
  {
    $cid = ftp_connect("ac-labs.com.mx");
    // // Luego creamos un login al mismo con nuestro usuario y contraseña
    $resultado = ftp_login($cid, "aclabscom", "*jLq)wouHhXb");
    // // Comprobamos que se creo el Id de conexión y se pudo hacer el login
    if ((!$cid) || (!$resultado)) {
      echo "Fallo en la conexión";
      die;
    } else {
      echo "Conectado.";
    }
    ftp_close($cid);
  }


  public function prueba1($id){
    $this->load->library('session');
    $this->load->library('Pdf');
    $interpretacion = $this->Interpretacion_Model->get_interpretacion($id);

    //sdate_default_timezone_set('America/Los_Angeles'); //change zone as per need
    /*$viewdata = [
      'interpretacion' => $interpretacion
    ];

    $Html=$this->load->view('body/body_document', $viewdata,true);
  */

    
    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
    
    $pdf->SetPrintHeader(false); 
    $pdf->SetPrintFooter(false);

    $pdf->AddPage();

    $pdf->writeHTML($interpretacion->texto, true, false, true, false, '');        

    $pdf->lastPage();      
    $pdf->Output('example_006.pdf', 'I');  
  }
}
