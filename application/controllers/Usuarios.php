<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Udn_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Usuario_Model');

    $this->load->library('acceso');
  }

  public function index()
  {
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }

    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Usuarios";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $dataBody = [
        'lista_udn' => $this->Udn_Model->get_udns(),
        'lista_privilegios' => $this->Privilegios_Model->get_privilegio(),
        'modulo' => $this->acceso->crud($privilegios, $modulo)
      ];
      $this->load->view('body/body_usuario', $dataBody);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  public function Ver()
  {
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Usuarios";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $dataBody = [
        'table' => $this->Usuario_Model->get()
      ];
      $this->load->view('body/body_view_personal', $dataBody);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function update()
  {

    $id_personal = $this->input->post('id_personal');
    $nombre = $this->input->post('nombre');
    $apellido_p = $this->input->post('apellido_p');
    $apellido_m = $this->input->post('apellido_m');
    $id_empleado = $this->input->post('id_empleado');
    $privilegio = $this->input->post('privilegio');


    $cobro_estudio = $this->input->post('cobro_estudio');
    $cobro_interpretacion = $this->input->post('cobro_interpretacion');

    $respuesta = [];
    if ($this->Personal_Model->update_user($id_personal, $nombre, $apellido_p, $apellido_m, $id_empleado, $cobro_interpretacion, $cobro_estudio)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Personal modificado'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'No se pudo modificar el personal'
      ];
    }
    echo json_encode($respuesta);
  }
  public function Save()
  {
    $respuesta = [];
    // $cedulas = $this->input->post('cedula');
    // $cuenta = 0;
    // if ($cedulas != null) { //validamos que venga aluna cedula
    //   foreach ($cedulas as $key => $cedula) {
    //     $cuenta += 1;
    //     //validación que no exista 
    //     $no_cedula = $this->Personal_Model->existe_cedula($cedula);
    //     if ($no_cedula->existe == '1') {
    //       $respuesta = [
    //         'status' => 'error',
    //         'msg' => 'Este numero de cedula ya se encuentra registrado'
    //       ];

    //       echo  json_encode($respuesta);
    //       return 0;
    //     }
    //   }
    // }
    // if ($cuenta != 0) {
    $nombre = $this->input->post('nombre');
    $nombre = strtoupper($nombre);
    $apellido_p = $this->input->post('apellido_p');
    $apellido_p = strtoupper($apellido_p);
    $apellido_m = $this->input->post('apellido_m');
    $apellido_m = strtoupper($apellido_m);
    $id_empleado = $this->input->post('id_empleado');
    $udn = $this->input->post('udn');
    $usser = $this->input->post('usser');
    $pass = $this->input->post('pass');
    $privilegio = $this->input->post('privilegio');

    $idUsser = $this->Personal_Model->insert($nombre, $apellido_p, $apellido_m, $id_empleado, $udn);
    if ($this->Usuario_Model->insert($usser, $pass, $privilegio, $idUsser)) {
      // foreach ($cedulas as $key => $noCedulas) {
      //   $this->Personal_Model->Insert_cedula($idUsser, $noCedulas);
      // }
      $respuesta = [
        'status' => 'success',
        'msg' => 'Usuario creado correctamente'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al crear usuario'
      ];
    }
    // } else {
    //   $respuesta = [
    //     'status' => 'error',
    //     'msg' => 'Error no Añadiste ninguna cedula'
    //   ];
    // }
    echo json_encode($respuesta);
  }
  public function ver_datos()
  {
    $id = $this->input->post('id');

    $respuesta = $this->Usuario_Model->get_todo($id);
    echo json_encode($respuesta[0]);
  }
  public function Update_Usuario()
  {
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Usuarios";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $dataBody = [
        'table' => $this->Personal_Model->Get(),
        'lista_udn' => $this->Udn_Model->get_udns(),
        'lista_privilegios' => $this->Privilegios_Model->get_privilegio(true),
        'modulo' => $this->acceso->crud($privilegios, $modulo)
      ];

      $this->load->view('body/body_update_personal', $dataBody);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function Get()
  {
    $id_personal = $this->input->post('id_personal');
    $respuesta = [
      'personal' => $this->Personal_Model->Get_todo($id_personal),
      'cedulas' => $this->Personal_Model->Get_cedulas($id_personal)
    ];
    echo json_encode($respuesta);
  }

  public function Elimina_Cedula()
  {
    $id_cedula = $this->input->post('id_cedula');
    $respuesta = [];
    if ($this->Personal_Model->deleted_Cedula($id_cedula)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Numero de cedula eliminado'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'No se pudo eliminar el numero de cedula'
      ];
    }

    echo json_encode($respuesta);
  }

  public function Nueva_Cedula()
  {
    $cedula = $this->input->post('cedula');
    $id_personal = $this->input->post('id_personal');

    $no_cedula = $this->Personal_Model->existe_cedula($cedula);
    if ($no_cedula->existe == '1') {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Este numero de cedula ya se encuentra registrado'
      ];
    } else {
      $id = $this->Personal_Model->Nueva_Cedula($cedula, $id_personal, 1);
      $respuesta = [
        'status' => 'success',
        'msg' => 'Cedula agregada correctamente',
        'data' => [
          'id' => $id,
          'nombre' => $cedula
        ]
      ];
    }
    echo  json_encode($respuesta);
  }
  public function Cedula()
  {
    // var_dump($_FILES);   
    date_default_timezone_set('MET');
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'cedu';
    $config['upload_path'] = "uploads/documentos/actas_nacimiento";
    $day = date('d_m_Y');
    $ran = rand(0, 1000000);
    $config['file_name'] = "Acta_nacimiento_$id_personal-$day-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    $acta = '';
    if ($_FILES['cedu']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $cedula = $data['uploadSuccess']['file_name'];

      $id = $this->Personal_Model->Nueva_Cedula($cedula, $id_personal, 0);
      echo $id . $cedula;
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar acta de nacimiento',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }


  public function Firma()
  {
    $id_user_firma = $this->input->post('id');
    $mi_archivo = 'firma';
    $config['upload_path'] = "uploads/firma";
    $ran = rand(0, 1000000);
    $config['file_name'] = "firma_$id_user_firma-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    if ($_FILES['firma']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      //
      $firma = $data['uploadSuccess']['file_name'];
      if ($this->Personal_Model->update_firma($id_user_firma, $firma)) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Firma actualizado'
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al agregar Firma',
          'log' => 'no se pudo actualizar'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar Firma',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }
}
