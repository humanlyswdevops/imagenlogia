<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pruebas extends CI_Controller{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
    $this->load->model('Interpretacion_Model');
  }
  public function index(){
    $id=$this->input->post('id');    
    
    $interpretacion=$this->Interpretacion_Model->get_interpretacion($id);
    $interpretacion=$interpretacion[0];
    
    date_default_timezone_set('America/Los_Angeles');//change zone as per need
     $viewdata=[
       'interpretacion'=>$interpretacion
    ];
    
    $html = $this->load->view('body/body_pruebas', $viewdata, TRUE);

    // Cargamos la librería
    $this->load->library('pdfgenerator');
    // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
    $filename = 'Estudios';
    // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
    $this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
  }

  public function Save(){
    // $mi_archivo = 'dicom';
    // $config['upload_path'] = "http://187.188.166.50/imagenologia2/Acatzingo/";
    // $config['file_name'] = "nombre_archivo";
    // $config['allowed_types'] = "*";
    // $config['max_size'] = "100000";
    

    // $this->load->library('upload', $config);
    
    // if (!$this->upload->do_upload($mi_archivo)) {
    //   //*** ocurrio un error
    //   $data['uploadError'] = $this->upload->display_errors();
    //   echo $this->upload->display_errors();
    //   return;
    // }

    // $data['uploadSuccess'] = $this->upload->data();
    // echo 'succes';


    // Primero creamos un ID de conexión a nuestro servidor
    $cid = ftp_connect("192.168.100.118");
    // // Luego creamos un login al mismo con nuestro usuario y contraseña
    $resultado = ftp_login($cid, "root","sudo");
    // // Comprobamos que se creo el Id de conexión y se pudo hacer el login
    if ((!$cid) || (!$resultado)) {
      echo "Fallo en la conexión"; die;
    } else {
      echo "Conectado.";
    }
    // // Cambiamos a modo pasivo, esto es importante porque, de esta manera le decimos al 
    // //servidor que seremos nosotros quienes comenzaremos la transmisión de datos.
    // ftp_pasv ($cid, true) ;
    // echo "<br> Cambio a modo pasivo<br />";
    // // Nos cambiamos al directorio, donde queremos subir los archivos, si se van a subir a la raíz
    // // esta por demás decir que este paso no es necesario. En mi caso uso un directorio llamado boca
    // ftp_chdir($cid, "boca");
    // echo "Cambiado al directorio necesario";   
    // // Tomamos el nombre del archivo a transmitir, pero en lugar de usar $_POST, usamos $_FILES que le indica a PHP
    // // Que estamos transmitiendo un archivo, esto es en realidad un matriz, el segundo argumento de la matriz, indica
    // // el nombre del archivo
    // $local = $_FILES["archivo"]["name"];
    // // Este es el nombre temporal del archivo mientras dura la transmisión
    // $remoto = $_FILES["archivo"]["tmp_name"];
    // // El tamaño del archivo
    // $tama = $_FILES["archivo"]["size"];
    // echo "<br />$local<br />";
    // echo "$remoto<br />";
    // echo "subiendo el archivo...<br />";
    // // Juntamos la ruta del servidor con el nombre real del archivo
    // $ruta = "/srv/www/htdocs/boca/" . $local;
    // // Verificamos si no hemos excedido el tamaño del archivo
    // if (!$tama<=$_POST["MAX_FILE_SIZE"]){
    //   echo "Excede el tamaño del archivo...<br />";
    // } else {
    //   // Verificamos si ya se subio el archivo temporal
    //   if (is_uploaded_file($remoto)){
    //     // copiamos el archivo temporal, del directorio de temporales de nuestro servidor a la ruta que creamos
    //     copy($remoto, $ruta);		
    //   }
    //   // Sino se pudo subir el temporal
    //   else {
    //     echo "no se pudo subir el archivo " . $local;
    //   }
    // }
    // echo "Ruta: " . $ruta;
    // //cerramos la conexión FTP
    // ftp_close($cid);
  }

  public function ftp(){
    $cid = ftp_connect("ac-labs.com.mx");
    // // Luego creamos un login al mismo con nuestro usuario y contraseña
    $resultado = ftp_login($cid, "aclabscom","*jLq)wouHhXb");
    // // Comprobamos que se creo el Id de conexión y se pudo hacer el login
    if ((!$cid) || (!$resultado)) {
      echo "Fallo en la conexión"; die;
    } else {
      echo "Conectado.";
    }
    ftp_close($cid);
    
  }

  public function Recibo($id){    

    $interpretacion=$this->Interpretacion_Model->get_interpretacion($id);
    $interpretacion=$interpretacion[0];
    
    date_default_timezone_set('America/Los_Angeles');//change zone as per need
      $viewdata=[
       'interpretacion'=>$interpretacion
    ];
    
    $html = $this->load->view('body/body_pruebas', $viewdata, TRUE);

    // Cargamos la librería
    $this->load->library('pdfgenerator');
    // definamos un nombre para el archivo. No es necesario agregar la extension .pdf
    $filename = 'Estudios';
    // generamos el PDF. Pasemos por encima de la configuración general y definamos otro tipo de papel
    $this->pdfgenerator->generate($html, $filename, true, 'Letter', 'portrait');
  }

  public function mail()
  {
    echo "h";
  }

  public function Pdf($id)
  {
    $interpretacion = $this->Interpretacion_Model->get_interpretacion($id);
    $this->load->library('Pdf');
    $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
    
    ///
    $pdf->SetPrintHeader(false); 
    $pdf->SetPrintFooter(false);

    $pdf->SetMargins(25, 10,25, true);

    $pdf->SetFont('helvetica', '', 12);
    $pdf->AddPage();

    //var_dump($interpretacion);
    $html = $interpretacion[0]->texto;

    $html='
    <!DOCTYPE html>
      <html lang="en">

      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Interpretacion</title>
        <style type="text/css">   
          /**{
            font-family:Arial;
          }*/

          @page {
            margin: 80px 1000px;
          }    
          
          @font-face {
            font-family: "Arial";
            font-style: normal;
            font-weight: normal;      
          }
          


          html {
          }
          
          h3 {
            font-family: \'Arial\' ;     
          }
          
          
          font-family: \'Arial\';

          p {
          }
          p.ql-indent-1 {      
            padding: 0px 0px 0px 20px;
            font-family: \'Arial\' ;
          }
          strong{
          }
          
          
          font-family: \'Arial\' ;

          .panel-main  {
          }
          
          
          font-family: \'Arial\' ;
      
          .panel-main {
            padding: 1px;
            margin: 1px;
          }
          
          .p-j {
            text-align: justify;
          }

          .pie {
            text-decoration: overline;
            width: 100%;
            position: absolute;
            bottom: 0;      
          }
          
          
          .oculta{
            display: none;
          }
          /***
            Estilo para la pre visualización 
          */
          
          .ql-align-right {
            text-align: right;
            float:right;
          }
          .ql-align-center {
            text-align: center;
            float:right;
          }
          .ql-align-left {
            text-align: left;
            float:right;
          }
          .ql-align-justify{
            text-align: justify;
            float:right;
          }

          .panel-main table {
            border-collapse: collapse;
          }

          .panel-main td {
            border: 1px solid #000;
            padding: 2px 5px;
          }

          .panel-main table {
            table-layout: fixed;
            width: 100%;
          }
          .panel-main table td {
            outline: 0;
          }
          h3.ql-align-center {
          }
          font-family: \'Arial\';
        </style>
      </head>
      <body>

        <div class="panel-main " style="width: 100%;">   
          '.$html.'
        </div>
        
      </body>

      </html>
    ';
    //echo $html;
    
    $pdf->writeHTML($html, true, false, true, false, '');        
    
    $pdf->lastPage();      
    $pdf->Output('example_006.pdf', 'I');    
  }

  public function fecha(){

    setlocale(LC_TIME, 'es_ES.UTF-8');
    $fecha=strftime("%d de %B de %Y");
    echo $fecha;
    //echo strftime("%A, %d de %B de %Y %H-%M-%S").'<br/>';
  }
}
