<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propuesto extends CI_Controller {

  public function __construct(){
    parent::__construct();      
    $this->load->library('acceso');  
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
    $this->load->model('Dicom_Model');    
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Configuracion_Model');
    $this->load->model('Estudio_Model');    
    $this->load->model('Paciente_Model');
  }
  
  public function index(){    
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }

    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Interpretación";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $machotes=$this->Configuracion_Model->GetMachotes($id_personal);
      $id_toma_muestra=$this->input->post('id_toma_muestra');
      $this->load->view('default/head');
      $this->load->view('default/cargarTabs');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data=[
        'consulta'=>$this->Interpretacion_Model->get_consulta($id_toma_muestra),
        'toma'=>$this->input->post('id_toma_muestra'),
        'machotes'=>$machotes,
        'nota'=>$this->Interpretacion_Model->Get_datosClinicos($id_toma_muestra)
      ]; 
      $dataScrips=[
        'js_machote'=>json_encode($machotes)
      ];
      $this->load->view('Propuesto/body_interpretacion',$data);
      $this->load->view('default/footer');
      $this->load->view('Propuesto/scrips_dicom',$dataScrips);
    }    
  }  
}