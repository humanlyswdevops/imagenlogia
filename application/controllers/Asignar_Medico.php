<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Asignar_Medico extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Toma_Muestra_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Privilegios_Model');
    $this->load->library('acceso');
  }

  public function index()
  {

    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Asignación de medico";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      $data = [
        'tabla' => $this->Asignacion_Interpretacion_Model->get_asignaciones(),
        'medicos' => $this->Personal_Model->listaRadiologos()
      ];

      $this->load->view('body/Body_Asignar_Medico', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function Asignar(){
    //var_dump($_POST);
    
    $medico = $this->input->post('medico[]');
    $id_toma_muestra = $this->input->post('id_toma_muestra');
    $respuesta = [];
    $existe = '';
    $bandera=true;
    foreach ($medico as $key => $medico) {
      

      if ($this->Asignacion_Interpretacion_Model->ModificaAsignacion($id_toma_muestra,$medico)) {
        // guardamos quien realizo este movimiento
        $this->Asignacion_Interpretacion_Model->ModificaAsignacion_Log($id_toma_muestra,$medico);
        $respuesta = [
          'status' => 'success',
          'msg' => 'Se asignaron tomas'
        ];
      }

      

      // if ($bandera) {
      //   $this->Asignacion_Interpretacion_Model->eliminar_asignacion($id_toma_muestra);
      //   $bandera=false;
      // }
      // $this->Asignacion_Interpretacion_Model->insert($medico, $id_toma_muestra);
      
    }
    # cambio de filtro por el estado terminado 
    // $this->Toma_Muestra_Model->cancelar_toma($id_toma_muestra)    
    echo json_encode($respuesta);
  }

  public function Un_medico()
  {
    $medico_select2 = $this->input->post('medico_select2');
    $folios = $this->Asignacion_Interpretacion_Model->list_asignacion();
    $respuesta = [];

    if ($medico_select2 != '') {
      foreach ($folios as $key => $tomas) {
        $folio = $tomas->id_toma_muestras;
        $existe = $this->Asignacion_Interpretacion_Model->existe_asignacion($medico_select2, $folio);
        $existe = $existe->existe;
        // var_dump($existe);
        if ($existe == '0') {
          $existe = $this->Asignacion_Interpretacion_Model->insert($medico_select2,$folio);
          $this->Asignacion_Interpretacion_Model->limpia_asignacion($medico_select2,$folio);
          $respuesta = [
            'status' => 'success',
            'msg' => 'Se asignaron todas las tomas al medico.'
          ];
        } else {
          $this->Asignacion_Interpretacion_Model->limpia_asignacion($medico_select2,$folio);
          $respuesta = [
            'status' => 'success',
            'msg' => 'Se asignaron todas las tomas al medico.'
          ];
        }
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'no hay datos del medico'
      ];
    }
    echo json_encode($respuesta);
  }
}
