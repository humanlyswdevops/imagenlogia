<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// include './application/libraries/Acceso.php';

class Home extends CI_Controller {

  public function __construct(){
    parent::__construct();       
    $this->load->model('Privilegios_Model');    
    $this->load->library('acceso');
    $this->load->library('pruebas');
  }
  
  
  public function index(){   
    // var_dump($this->pruebas);     
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;   
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    $id_personal = $_SESSION['usuario']->id_personal;
    $this->load->view('default/head');
    $this->load->view('default/nav');
    
    // var_dump($this->Privilegios_Model->get_modulosBy_id($id_personal));
    $dataMenu=[
      'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
      'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
    ];
   
    $this->load->view('default/menu',$dataMenu);
    $data=[
      'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal)
    ];
  
		$this->load->view('body/body_home',$data);
		$this->load->view('default/footer');
		$this->load->view('default/scrips');
  }  

}