<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Paciente extends CI_Controller
{

  public function __construct(){
    parent::__construct();
    $this->load->model('Paciente_Model');    
  }

  public function index(){
    
  }
  public function Save(){
    $nombre= $this->input->post('nombre');
    $apellido_paterno= $this->input->post('apellido_p');
    $apellido_materno= $this->input->post('apellido_m');
    $curp= $this->input->post('curp');
    $id_laboratorio= $this->input->post('id_laboratorio');    
    
    $id= $this->Paciente_Model->insert($nombre,$apellido_paterno,$apellido_materno,$curp,$id_laboratorio);
    $data=$this->Paciente_Model->get_paciente_byId($id);
    
    if($id>0){
      $respuesta=[
        'status'=>'success',
        'msg'=>'Paciente creado correctamente',
        'data'=>$data[0]
      ];      
    }else{
      $respuesta=[
        'status'=>'error',
        'msg'=>'Error al registrar paciente'        
      ];
    }

    echo json_encode($respuesta);
     

  }
}
