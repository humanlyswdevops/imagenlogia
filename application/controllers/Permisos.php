<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permisos extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->library('acceso');
  }
  public function index(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Permisos";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {            
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $data_menu=[
        'acceso'=>$privilegios,
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal)
      ];
      $this->load->view('default/menu',$dataMenu);
      $dataBody = [
        'modulos' => $this->Privilegios_Model->get_modulos(),
        'operaciones' => $this->Privilegios_Model->get_crud(),
        'privilegios' => $this->Privilegios_Model->get_privilegio(),
        'modulo'=>$this->acceso->crud($privilegios,$modulo)
      ];
      $this->load->view('body/body_permisos', $dataBody);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  public function get_detalleAcceso()
  {
    $id_privilegio = $this->input->post('id_privilegio');
    $respuest = $this->Privilegios_Model->detalle_privilegio($id_privilegio);
    echo json_encode($respuest);
  }
  public function dropAcceso()
  {
    $id_crud = $this->input->post('id_crud');
    $id_modulo = $this->input->post('id_modulo');
    $id_privilegios = $this->input->post('id_privilegios');
    if ($this->Privilegios_Model->deleted_acceso($id_crud, $id_modulo, $id_privilegios)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Acceso eliminado'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al eliminar acceso'
      ];
    }
    echo json_encode($respuesta);
  }
  public function insertAcceso()
  {
    $operacion = $this->input->post('operacion');
    $modulo = $this->input->post('modulo');
    $privilegio = $this->input->post('privilegio');
    if ($this->Privilegios_Model->insert_acceso($privilegio, $modulo, $operacion)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Acceso creado'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al crear acceso'
      ];
    }
    echo json_encode($respuesta);
  }

  public function insertPrivilegio()
  {
    $privilegio = $this->input->post('privilegio');
    if ($privilegio != '') {
      if ($this->Privilegios_Model->insert_privilegio($privilegio)) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Privilegio creado'
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al crear acceso'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al crear acceso'
      ];
    }
    echo json_encode($respuesta);
  }
}
