<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Interpreta extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->library('acceso');
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
    $this->load->model('Dicom_Model');
  }

  public function index(){

    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Interpretación";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {

      $this->load->view('default/head');
      $this->load->view('default/nav');
      $this->load->view('default/menu');
      $data = [];
      $this->load->view('body/body_interpreta',$data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips_dicom');
    }
  }

  /**
   * Get_datos_toma
   *
   * @return 0:
   * estudio:
   *  data:[
   *    contenido: "3",
   *    dicoms:[
   *            "dicom 1",
   *            "dicom 2"],
   *    nombre: "estudio prueba 1"
   *  ] 
   * 
   */
  public function Get_datos_toma(){
    $id_toma_muestra = $this->input->post('id_toma_muestra');
    $datos = $this->Interpretacion_Model->get_consulta($id_toma_muestra);

    $estudios = $this->BuscarEstudio($datos);
    $data_consulta = [];
    foreach ($estudios as $key => $estudio) {
      $dicoms = $this->Regresar_dicom($datos, $estudio);
      array_push($data_consulta, [
        'estudio' => [
          'nombre' => $estudio,
          'data' => $dicoms
        ]
      ]);
    }
    echo json_encode($data_consulta);
  }


  /**
   * BuscarEstudio
   * 
   * Esto recibe un arreglo y crear uno solo de estudios y quita estudios duplicados
   *
   * @param  Array $array arreglo comom viene de la BD
   * @return Array
   */
  public function BuscarEstudio($array){
    $estudios = [];
    foreach ($array as $key => $item) {
      array_push($estudios, $item->estudio);
    }
    $estudios = array_unique($estudios);
    return $estudios;
  }

  /*
   * esto regresa un arreglo con las rutas de las dicoms dependiendo el estudio que se le pase
   *
   * @param  Array $arreglos
   * @param  String $estudio
   * @return Array
   */
  public function Regresar_dicom($arreglos = [], $estudio){
    $dicoms = [];
    $id_contenido = 0;
    foreach ($arreglos as $key => $conjunto) {
      if ($conjunto->estudio == $estudio) {
        array_push($dicoms, $conjunto->dicom);
        $id_contenido = $conjunto->id_contenido_consulta;
      }
    }
    $respuesta = [
      'dicoms' => $dicoms,
      'contenido' => $id_contenido
    ];
    return $respuesta;
  }

  public function load_interpretacion(){
    $DataSet=$this->Dicom_Model->Busca_dicom(7);
    foreach ($DataSet as $key => $item) {
      var_dump($item);
      echo '<br><br><br>';
    }
  }
}
