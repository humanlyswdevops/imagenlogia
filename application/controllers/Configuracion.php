<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Configuracion extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Configuracion_Model');
    $this->load->model('Personal_Model');
    $this->load->library('acceso');    
  }

  public function index(){    
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;    
    $modulo = "Configuracion";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        
      ];

      $this->load->view('body/body_config', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  public function interpretacion(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;    
    $modulo = "Configuracion";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        
      ];

      $this->load->view('body/body_configuracion_Interpretacion', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function Get_interpretacionesBody(){
    $id_personal = $_SESSION['usuario']->id_personal;    
    // $id_personal=5;//solo para pruebas quitar !!!
    $dataset=$this->Configuracion_Model->existe_asignacion($id_personal);    

    echo json_encode($dataset);

  }

  public function Activar(){
    $interpretacion=$this->input->post('id');
    $personal=$this->input->post('personal');
    $this->Configuracion_Model->Cambiar_status($personal);
    $respuesta=[];
    if($this->Configuracion_Model->Activa_pre_interpretacion($interpretacion)){
      $respuesta=[
        'status'=>'success',
        'msg'=>'estatus de interpretación activado'        
      ]; 
    }else{
      $respuesta=[
        'status'=>'error',
        'msg'=>'No se pudo activar la interpretación'        
      ]; 
    }
    echo json_encode($respuesta);        
  }
  public function Modificar_pre_interpetacion(){    
    $interpretacion=$this->input->post('id');
    $personal=$this->input->post('personal');
    $HtmlEditor=$this->input->post('HtmlEditor');
    $titulo=$this->input->post('titulo');

    $respuesta=[];
    if($this->Configuracion_Model->Update_pre_interpretacion($HtmlEditor,$interpretacion,$titulo)){
      $respuesta=[
        'status'=>'success',
        'msg'=>'Se modifico la interpretación'        
      ]; 
    }else{
      $respuesta=[
        'status'=>'error',
        'msg'=>'No se pudo modificar la interpretación'        
      ]; 
    }
    echo json_encode($respuesta);    
  }

  public function Nueva(){
    $id_personal = $_SESSION['usuario']->id_personal;    
    // $id_personal=5; //solo para pruebas
    $HtmlEditor=$this->input->post('HtmlEditor');    
    $title=$this->input->post('title');    
    $respuesta=[];
    if ($this->Configuracion_Model->Insert_pre_interpretacion($id_personal,$HtmlEditor,$title)) {
      $respuesta=[
        'status'=>'success',
        'msg'=>'Nueva interpretacion creada'        
      ]; 
    } else {
      $respuesta=[
        'status'=>'error',
        'msg'=>'Error al crear interpretación'        
      ]; 
    }        
    echo json_encode($respuesta);
  }

  public function Pre_interpretaciones_Activas(){
    $id_personal = $_SESSION['usuario']->id_personal;
    $Dataset=$this->Configuracion_Model->Get_interpretacion_activa($id_personal);
    echo json_encode($Dataset);
  }

  public function Get_Firma(){
    $id_personal = $_SESSION['usuario']->id_personal;
    // $Dataset=
    
    $respuesta=[
      'firma'=>$this->Configuracion_Model->getFirma($id_personal),
      'cedulas' => $this->Personal_Model->Get_cedulas($id_personal)
      ];
    echo json_encode($respuesta);
  }

  public function get_machote(){
    $id=$this->input->post('id');    
    $respuesta=$this->Configuracion_Model->get_machoteById($id);

    echo $respuesta->contenido;

  }
}
