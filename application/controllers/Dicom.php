<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dicom extends CI_Controller
{
  public function __construct(){
    parent::__construct();  
    $this->load->model('Dicom_Model');  
  }

  public function index(){
    $dicom=$this->input->post('dicom');    
    
    if ($this->Dicom_Model->Nueva_ruta($dicom)) {
      echo 'success';
    }else{
      echo 'error';
    }
  }

  public function get_dicoma(){
    $id_contenidoConsulta=$this->input->post('id_contenido_consulta');
    $imagenes=$this->Dicom_Model->Get_By_ContenidoConsulta($id_contenidoConsulta);

    echo json_encode($imagenes);
  }

  public function get_jpg(){
    $id_contenidoConsulta= $this->input->post('id');
    $conjunto= $this->Dicom_Model->Get_imagenes($id_contenidoConsulta);
    
    echo json_encode($conjunto);
  }
}
