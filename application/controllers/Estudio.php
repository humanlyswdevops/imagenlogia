<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Estudio extends CI_Controller
{

  public function __construct(){
    parent::__construct();
    $this->load->model('Privilegios_Model');
    $this->load->model('Estudio_Model');
    $this->load->library('acceso');
    
    $this->load->model('Toma_Muestra_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Asignacion_Interpretacion_Model');       
    $this->load->model('Pagos_Model'); 
  }

  public function index(){    
    // if (!isset($_SESSION['usuario']->id_personal)) {
    //   redirect(base_url(), 'refresh');
    // }        

    $id_personal = $_SESSION['usuario']->id_personal;    
    $modulo = "Estudios";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data = [
        'lista_estudio' => $this->Estudio_Model->get_estudios(),
        'dataset'=>$this->Estudio_Model->get_todo()
      ];

      $this->load->view('body/body_estudio', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }
  public function Save()
  {
    $estudio = $this->input->post('estudio');
    $estudio = strtoupper($estudio);
    if ($this->Estudio_Model->insert($estudio)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Estudio creado correctamente'
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al crear estudio'
      ];
    }
    echo json_encode($respuesta);
  }

  public function edita(){
    $id_estudio=$this->input->post('id_estudio');
    $nombre=$this->input->post('nombre');    
    $honorario=$this->input->post('honorario');
    $placas=$this->input->post('placas');

    
    if ($this->Estudio_Model->Updated($id_estudio,$nombre,$honorario,$placas)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Estudio actualizado.'        
      ];  
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al actualizar estudio.',        
      ];
    }

    echo json_encode($respuesta);
    
    
  }

  public function Todos()
  {
    $estudios= $this->Estudio_Model->get_todo();
    echo json_encode($estudios);

  }
  
  public function Valida(){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "validacion";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $this->load->view('default/head');
      $this->load->view('default/nav');
      $dataMenu = [
        'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu', $dataMenu);
      
      $data = [
        'tabla' => $this->Pagos_Model->validacion(),
        'medicos' => $this->Personal_Model->get_personal_ByPrivilegio('medico radiologo')
      ];

      $this->load->view('body/Body_valida_dicoms', $data);
      $this->load->view('default/footer');
      $this->load->view('default/scrips');
    }
  }

  public function datos_valida(){
    $filtro=$this->input->post('filtro');
    if ($filtro=='1') {
      $years=$this->input->post('years');
      $dia=$this->input->post('dia');
      $mes=$this->input->post('mes');

      $dataset=$this->Pagos_Model->validacion_Date($mes,$dia,$years);
      echo json_encode($dataset);
    }else{
      $dataset=$this->Pagos_Model->validacion();
      echo json_encode($dataset);
    }
  }
}
