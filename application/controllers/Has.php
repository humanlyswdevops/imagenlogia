<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Has extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Has_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Udn_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Usuario_Model');
    $this->load->model('Login_Model');
  }

  public function index()
  {
    var_dump($_POST);
  }

  public function Ingresa($user_name, $id_empleado, $sucursal, $privilegio, $id_empresa){

    //https://humanly-sw.com/imagenologia/Has/Ingresa/Luis%20enrique%20gonzalez%20Arellano/A035/Matriz/medico_radiologo/1
    //https://humanly-sw.com/imagenologia/Has/Ingresa/Luis%20enrique%20gonzalez%20Arellano/A035/Matriz/tecnico_radiologo/1
    //https://humanly-sw.com/imagenologia/Has/Ingresa/Luis%20enrique%20gonzalez%20Arellano/A035/Matriz/administrador/1
    //
    //buscamos el id del privilegio
    if ($privilegio == 'medico_radiologo') {
      $privi = $this->Privilegios_Model->get_idPrivilegio_Byname('medico radiólogo');
    } else {
      if ($privilegio == 'tecnico_radiologo') {
        $privi = $this->Privilegios_Model->get_idPrivilegio_Byname('técnico radiólogo');
      } else {
        if ($privilegio == 'administrador') {
          $privi = $this->Privilegios_Model->get_idPrivilegio_Byname('administrador');
        }
      }
    }
    $privilegio = $privi->id_privilegios;

    //sirve para validar si existe el personal
    $people = $this->Has_Model->Existe_personal($id_empleado);

    if ($people->existe == '1') { //si exite el personal
      $personal = $this->Personal_Model->get_By_idHtds($id_empleado); //sacamos el id del personal por su id del htds
      $id_personal = $personal->id_personal;

      $usuario_id = $this->Usuario_Model->existe($id_empleado); //validamos si existe el usuario
      if ($usuario_id->existe == '0') {
        if ($this->Usuario_Model->insert($id_empleado, $id_empleado, $privilegio, $id_personal)) {
          $session = $this->Login_Model->valida_username($id_empleado);

          $_SESSION['usuario'] = $session[0];
          $_SESSION['empresa'] = $id_empresa;
          redirect(base_url('Home'));
        }
      } else {
        if ($usuario_id->existe == '1') {
          // echo 'existe usuario';    
          $session = $this->Login_Model->valida_username($id_empleado);

          $_SESSION['usuario'] = $session[0];
          $_SESSION['empresa'] = $id_empresa;
          redirect(base_url('Home'));
        }
      }
    } else {
      if ($people->existe == '0') { //no existe       
        $udn = $this->Valida_sucursal($sucursal);
        $personal = $this->Get_nombre($user_name);
        //insertamos el personal
        $id_personal = $this->Personal_Model->insert(strtoupper($personal['nombre']), strtoupper($personal['app']), strtoupper($personal['apm']), $id_empleado, $udn);
        //creamos el usuario
        if ($this->Usuario_Model->insert($id_empleado, $id_empleado, $privilegio, $id_personal)) {
          // echo 'usuario creado correctamente '; 
          $session = $this->Login_Model->valida_username($id_empleado);

          $_SESSION['usuario'] = $session[0];
          $_SESSION['empresa'] = $id_empresa;
          redirect(base_url('Home'));
        }
      }
    }
  }

  public function Get_nombre($nombre)
  {
    $separa = explode('%20', $nombre);
    $name = '';
    $app = '';
    $apm = '';
    foreach ($separa  as $key => $item) {
      if ($key == 0) {
        $name = $item;
      } else {
        if ($key == 1) {
          $app = $item;
        } else {
          if ($key > 1) {
            $apm .= $item . ' ';
          }
        }
      }
    }
    return [
      'nombre' => trim($name),
      'app' => trim($app),
      'apm' => trim($apm)
    ];
  }

  public function Valida_sucursal($sucursal)
  {
    $sucursal = trim($sucursal);
    $udn = $this->Udn_Model->Existe($sucursal);
    if ($udn->existe == '0') {
      $udn_id = $this->Udn_Model->Insert($sucursal);
      $udn = $udn_id;
    } else {
      $udn_id = $this->Udn_Model->Get_byName($sucursal);
      $udn = $udn_id->id_udn;
    }
    return $udn;
  }

  public function GetDicoms()
  {
    $sass = $this->input->post('sass');
    $dataset = $this->Has_Model->ContenidoConsulta($sass);
    $respuesta = [];
    foreach ($dataset as $key => $subFolio) {

      $estudio = $this->Has_Model->get_estudio_byContenidoConsulta($subFolio->id_contenido_consulta);

      $dicoms = $this->Has_Model->get_dicoms_byContenidoConsulta($subFolio->id_contenido_consulta);
      $dicomsArray = [];
      foreach ($dicoms as $key => $dicom) {
        array_push($dicomsArray, base_url("uploads/dicoms/$dicom->nombre"));
      }
      array_push($respuesta, [
        'nombre' => $estudio->estudio,
        'id_sass' => $estudio->id_sass,
        'dicoms' => $dicomsArray
      ]);
    }
    echo json_encode($respuesta);
  }
}
