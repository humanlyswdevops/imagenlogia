<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perfil extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Toma_Muestra_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Admin_Model');
    $this->load->library('acceso');
  }

  public function index()
  {
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);

    $this->load->view('default/head');
    $this->load->view('default/nav');
    $dataMenu = [
      'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
      'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
    ];
    $this->load->view('default/menu', $dataMenu);
    $data = [];

    $this->load->view('body/body_perfil', $data);
    $this->load->view('default/footer');
    $this->load->view('default/scrips');
  }

  public function Update()
  {
    $mi_archivo = 'foto';
    $config['upload_path'] = "uploads/fotos_perfil";
    $config['file_name'] = "nombre_archivo";
    $config['allowed_types'] = "*";
    $config['max_size'] = "50000";
    $config['max_width'] = "2000";
    $config['max_height'] = "2000";
    $img = '';
    if ($_FILES['foto']['name'] != '') {
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $img = $data['uploadSuccess']['file_name'];
    }


    $id_personal = $_SESSION['usuario']->id_personal;
    $nombre = $this->input->post('nombre');
    $apellido_paterno = $this->input->post('apellido_paterno');
    $apellido_materno = $this->input->post('apellido_materno');
    $respuesta = [];
    if ($this->Personal_Model->update_userFor_usser($id_personal, $nombre, $apellido_paterno, $apellido_materno, $img)) {
      $respuesta = [
        'status' => 'success',
        'msg' => 'Perfil modificado',
        'personal' => $this->Personal_Model->Get_todo($id_personal)
      ];
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al modificar perfil'
      ];
    }
    echo json_encode($respuesta);
  }

  public function Get()
  {
    $id_personal = $_SESSION['usuario']->id_personal;
    $respuesta = [
      'personal' => $this->Personal_Model->Get_todo($id_personal)
    ];
    echo json_encode($respuesta);
  }

  public function GetCelulas()
  {
    $id_personal = $_SESSION['usuario']->id_personal;
    $data = $this->Personal_Model->Get_cedulas($id_personal,false);
    echo json_encode($data);
  }

  public function Update_documentos()
  {
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'curp';
    $config['upload_path'] = "uploads/documentos";
    $config['file_name'] = "curp_$id_personal-";
    $config['allowed_types'] = "*";
    $config['max_size'] = "50000";
    $config['max_width'] = "2000";
    $config['max_height'] = "2000";
    $curp = '';
    $acta = '';
    if ($_FILES['curp']['name'] != '') {
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $curp = $data['uploadSuccess']['file_name'];
    }
    if ($_FILES['acta_nacimiento']['name'] != '') {
      echo 'entre';
      $mi_archivo = 'acta_nacimiento';
      $config['file_name'] = "acta_nacimiento_$id_personal-";
      $this->load->library('upload', $config);

      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $acta = $data['uploadSuccess']['file_name'];
    }
    $res = ['acta' => $acta, 'curp' => $curp];
    // var_dump($_FILES);
    echo json_encode($res);
  }

  public function Update_acta_nacimiento()
  {
    date_default_timezone_set('MET');
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'acta_nacimiento';
    $config['upload_path'] = "uploads/documentos/actas_nacimiento";
    $day = date('d_m_Y');
    $ran = rand(0, 1000000);
    $config['file_name'] = "Acta_nacimiento_$id_personal-$day-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    $acta = '';
    if ($_FILES['acta_nacimiento']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $acta_nacimiento = $data['uploadSuccess']['file_name'];
      if ($this->Personal_Model->update_documentos($id_personal, '', $acta_nacimiento)) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Acta de nacimiento actualizado'
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al agregar acta de nacimiento',
          'log' => 'no se pudo actualizar'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar acta de nacimiento',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }
  public function Update_curp()
  {
    date_default_timezone_set('MET');
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'curp';
    $config['upload_path'] = "uploads/documentos/curps";
    $day = date('d_m_Y');
    $ran = rand(0, 1000000);
    $config['file_name'] = "curp_$id_personal-$day-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    $acta = '';
    if ($_FILES['curp']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $curp = $data['uploadSuccess']['file_name'];
      if ($this->Personal_Model->update_documentos($id_personal, $curp, '')) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Curp actualizado'
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al agregar Curp',
          'log' => 'no se pudo actualizar'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar Curp',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }
  public function Cedula()
  {
    // var_dump($_FILES);   
    date_default_timezone_set('MET');
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'cedu';
    $config['upload_path'] = "uploads/documentos/cedulas";
    $day = date('d_m_Y');
    $ran = rand(0, 1000000);
    $config['file_name'] = "Cedula_ID$id_personal-$day-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    $acta = '';
    if ($_FILES['cedu']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $cedula = $data['uploadSuccess']['file_name'];
      //var_dump($cedula, $id_personal);
      $id = $this->Personal_Model->Nueva_Cedula($cedula, $id_personal);
      if ($id > 0) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Cedula agregada',
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al agregar cedula',
          'log' => 'no se inserto'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar cedula',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }

  public function Cv()
  {
    date_default_timezone_set('MET');
    $id_personal = $_SESSION['usuario']->id_personal;
    $mi_archivo = 'cv';
    $config['upload_path'] = "uploads/documentos/cv";
    $day = date('d_m_Y');
    $ran = rand(0, 1000000);
    $config['file_name'] = "cv_$id_personal-$day-$ran";
    $config['allowed_types'] = "*";
    $respuesta = [];
    $acta = '';
    if ($_FILES['cv']['name'] != '') {
      $this->load->library('upload', $config);
      if (!$this->upload->do_upload($mi_archivo)) {
        $data['uploadError'] = $this->upload->display_errors();
        echo $this->upload->display_errors();
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $cv = $data['uploadSuccess']['file_name'];
      if ($this->Personal_Model->update_cv($id_personal, $cv)) {
        $respuesta = [
          'status' => 'success',
          'msg' => 'Curriculum vitae actualizado'
        ];
      } else {
        $respuesta = [
          'status' => 'error',
          'msg' => 'Error al agregar Curriculum vitae',
          'log' => 'no se pudo actualizar'
        ];
      }
    } else {
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al agregar Curriculum vitae',
        'log' => 'no esta llegando la imagen'
      ];
    }
    echo json_encode($respuesta);
  }

  public function detalle($id)
  {
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);

    $this->load->view('default/head');
    $this->load->view('default/nav');
    $dataMenu = [
      'modulos' => $this->Privilegios_Model->get_modulosBy_id($id_personal),
      'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
    ];
    $this->load->view('default/menu', $dataMenu);
    $data = [
      'personal' => $this->Personal_Model->Get_todo($id),
      'cedulas' => $this->Personal_Model->Get_cedulas($id)
    ];

    $this->load->view('body/body_perfil_detalle', $data);
    $this->load->view('default/footer');
    $this->load->view('default/scrips');
  }
}
