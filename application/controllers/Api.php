<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Has_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Udn_Model');
    $this->load->model('Privilegios_Model');
    $this->load->model('Usuario_Model');
    $this->load->model('Login_Model');
  }

  public function index(){
    echo 'Accedo denegado';
  }

  public function V1_regresainterpretacion(){
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");

    $sass = $this->input->post('sass');
  
    $sass2 = str_replace ( '/' , '-' , $sass);    

    $dataset = $this->Has_Model->ContenidoConsulta($sass);
    
    
    //var_dump($dataset);
    
    $respuesta = [];
    foreach ($dataset as $key => $subFolio) {
      //id_estudio
      
      $estudio = $this->Has_Model->get_estudio_byContenidoConsulta($subFolio->id_contenido_consulta);
      
      //var_dump($estudio);
      //
      //$subFolio->id_contenido_consulta; 
    
      ##["estudio"]=>string(12) "RX HOMBRO AP"
      ##["id_sass"]=>string(5) "80027"

      $interpretacion=$this->Has_Model->Busca_interpretacion($subFolio->id_contenido_consulta);
      
      
      
      $id="$sass2".'_'."$subFolio->id_estudios_sass";
      
      $ConjuntoDicom =  $this->Has_Model->BuscaDicom($id);

      $dicomsArray = [];
      foreach ($ConjuntoDicom as $key => $dicom) {
        $CleaUrl=explode('Imagenologia2\\',$dicom->ruta);                
        array_push($dicomsArray,$CleaUrl[1]);
      }

      array_push($respuesta, [
        'nombre' => $estudio->estudio,
        'id_sass' => $sass,
        'dicoms' => $dicomsArray,
        'interpretacion'=>base64_encode($interpretacion->texto),
        'id_estudio'=>$subFolio->id_estudios_sass
      ]);
    }
    echo json_encode($respuesta);
  }
}
