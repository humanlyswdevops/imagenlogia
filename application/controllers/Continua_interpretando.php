<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Continua_interpretando extends CI_Controller {

  public function __construct(){
    parent::__construct();      
    $this->load->library('acceso');  
    $this->load->model('Privilegios_Model');
    $this->load->model('Interpretacion_Model');
    $this->load->model('Dicom_Model');    
    $this->load->model('Asignacion_Interpretacion_Model');
    $this->load->model('Personal_Model');
    $this->load->model('Configuracion_Model');
    $this->load->model('Estudio_Model');    
    $this->load->model('Paciente_Model');
  }
  
  public function index(){    
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
        
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Interpretación";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {
      $machotes=$this->Configuracion_Model->GetMachotes($id_personal);
      $id_toma_muestra=$this->input->post('id_toma_muestra');
      $this->load->view('default/head');
      $this->load->view('default/cargarTabs');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data=[
        'consulta'=>$this->Interpretacion_Model->get_consulta($id_toma_muestra),
        'toma'=>$this->input->post('id_toma_muestra'),
        'machotes'=>$machotes,
        'nota'=>$this->Interpretacion_Model->Get_datosClinicos($id_toma_muestra)
      ]; 
      $dataScrips=[
        'js_machote'=>json_encode($machotes)
      ];
      $this->load->view('Continua_interpretando/body_interpretacion',$data);
      $this->load->view('default_Continua_interpretando/footer');
      $this->load->view('default_Continua_interpretando/scrips_dicom',$dataScrips);
    }
  }

  public function Save(){
    
    $id_toma_muestra=$this->input->post('id_toma_muestra');  

    $Is_interpretacion_Pdf=false; //bandera para saber si es un PDF
    $nombre_interpretacion='';//maneja nombre del pdf
    if($_FILES['interpretacion']['name']!=''){
      $Is_interpretacion_Pdf=true;

      $nombre_interpretacion = "$id_toma_muestra-". $_FILES['interpretacion']['name'];           
      $_FILES['interpretacion']['name']=$nombre_interpretacion;        
      $mi_archivo = 'interpretacion';
      $config['upload_path'] = "uploads/interpretacionpdf/";    
      $config['allowed_types'] = "*";
      $this->load->library('upload', $config);    
      if (!$this->upload->do_upload($mi_archivo)) {
        //*** ocurrio un error
        $data['uploadError'] = $this->upload->display_errors();
        $respuesta = [
          'status' => 'error',
          'msg' => 'Erro no se pudo cargar PDF',
          'log'=>  $this->upload->display_errors()       
        ];
        echo json_encode($respuesta);
        return;
      }
      $data['uploadSuccess'] = $this->upload->data();
      $nombre_interpretacion=$data['uploadSuccess']['file_name'];
    }else{      
      $Is_interpretacion_Pdf=false; //bandera para saber si es un PDF
      $nombre_interpretacion='';//maneja nombre del pdf
    }

    $bandera=$this->input->post('bandera');
    $Conjunto_estudios=$this->input->post('JsonEstudios_Str');
    $status=$this->input->post('status');
    //con esto cambiamos el estatus de la asignacion de la interpretación 
    $this->Asignacion_Interpretacion_Model->Cambiar_status($id_toma_muestra,$status);
    $interpretacion = $this->input->post('interpretacion');
    $id_contenido = $this->input->post('id_contenido');
    $json=json_decode($interpretacion);
    $log=false;
    
    //Guardo todas las interpretaciones
    foreach ($json as $key => $interpreta) {   
      if ($Is_interpretacion_Pdf) {//si se subió un PDF                
        if(!$this->Interpretacion_Model->Update($id_contenido,$nombre_interpretacion)){
          $log=true;
        }            
      }else{//Si es interpretacion de
        if(!$this->Interpretacion_Model->Update($id_contenido,$interpreta->interpretacion)){
          $log=true;
        }
      }
    }
    //cambiamos el estatus de la toma    
    if($log){
      // esto controla los errores       
      $respuesta = [
        'status' => 'error',
        'msg' => 'Error al guardar interpretación'
      ];
    }else{
      // esto termina el proceso de la interpretacion 
      if ($bandera=="0") {      
        ////////////////////////////////////////////////////////////////////////
        //  Notificación a has de que ya termino la interpretacion
        ////////////////////////////////////////////////////////////////////////
          $estudios=$this->Interpretacion_Model->Get_interpretacions_respuestaHas($id_toma_muestra);
          $paciente=$this->Interpretacion_Model->Get_pacientes_has($id_toma_muestra);
          $notifica_has=[
            'id_sass'=> $paciente->nim_sass,
            'id_paciente'=>$paciente->curp,
            'nombre'=>$paciente->paciente,
            'estudios'=>$estudios,
            'empresa'=>$_SESSION['empresa']
          ];
          
        ////////////////////////////////////////////////////////////////////////
        //  Fin de Notificación a has de que ya termino la interpretacion
        ////////////////////////////////////////////////////////////////////////

        $phpVersion = substr(phpversion(), 0, 3)*1;

        if($phpVersion >= 5.4) {
          $notifica_has = json_encode($notifica_has, JSON_UNESCAPED_UNICODE);
        } else {
          $notifica_has = preg_replace('/\\\\u([a-f0-9]{4})/e', "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($notifica_has));
        }
        //$respuesta_del_has=$this->notifica_has('https://humanly-sw.com/imagenologia/Has',$notifica_has);

        $respuesta = [
          'status' => 'success',
          'msg' => 'Interpretación guardada',
          'url'=>base_url("Interpretacion/Ver/$id_toma_muestra"),  
          'log'=>$notifica_has,
          // 'respuesta'=>$respuesta_del_has
        ];
      }else {
        // esto va a continuar mandando la url con los estudios por las url hasta que se vacié en json 
        $respuesta = [
          'status' => 'success',
          'msg' => 'Interpretación guardada',
          'url'=>base_url("Continua_interpretando/Estudio/$Conjunto_estudios")          
        ];
      }      
    }
    echo json_encode($respuesta);
  }  

  public function Estudio($id){
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }
    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Interpretación";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {      
      $machotes=$this->Configuracion_Model->GetMachotes($id_personal);
      $this->load->view('default/head');
      $this->load->view('default/cargarTabs');
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Usuarios')
      ];
      $this->load->view('default/menu',$dataMenu);
      $data=[
        'estudios'=>$id,
        'machotes'=>$machotes
      ];
      $this->load->view('Continua_interpretando/body_estudio_interpretacion',$data);
      $this->load->view('default_Continua_interpretando/footer');
      $this->load->view('default_Continua_interpretando/scrips_dicom_seguimiento');
    }    
  }

  public function Ver($id_toma){
    
    if (!isset($_SESSION['usuario']->id_personal)) {
      redirect(base_url(), 'refresh');
    }

    $id_personal = $_SESSION['usuario']->id_personal;
    $modulo = "Interpretación";
    $privilegios = $this->Privilegios_Model->get_lista_privilegios($id_personal);
    if ($this->acceso->Es_valido($privilegios, $modulo)) {      
      $this->load->view('default/head');      
      $this->load->view('default/nav');
      $dataMenu=[
        'modulos'=>$this->Privilegios_Model->get_modulosBy_id($id_personal),
        'crud_usuarios' => $this->acceso->crud($privilegios, 'Interpretación')
      ];
      $this->load->view('default/menu',$dataMenu);

      $curp=$this->Interpretacion_Model->curp_paciente_byfolio($id_toma);
      $conjunto=$this->Interpretacion_Model->get_interpretaciones($id_toma);
      $interpretacioneTodo=[];
      foreach ($conjunto as $key => $estudio) {   
        #pido nomenclatura de la dicom
        $nomemclatura=$this->Interpretacion_Model->Get_nomenclatura($estudio->id_contenido_consulta);
        #regresa conjunto de dicom por la
        $dicomList=$this->Interpretacion_Model->Get_dicom_by_nomenclatura($nomemclatura->nomemclatura);

        $ruta=[];
        foreach ($dicomList as $key => $dicomItem) {
          $ruta[]=$dicomItem->ruta;
        }
        
        $interpretacioneTodo[]=[
          'dicoms'=>$ruta,
          'estudio'=>$estudio->estudio,
          'interpretacion'=>$estudio->interpretacion,
          'id_contenido_consulta'=>$estudio->id_contenido_consulta,
          'id_interpretacion'=>$estudio->id_interpretacion
        ];
      }
      
      $data=[
        'userCode'=>$curp,
        'interpretaciones'=>$interpretacioneTodo,
        'crud'=>$this->acceso->crud($privilegios, 'Interpretación')
      ];
      $this->load->view('body/body_ver_interpretaciones',$data);
      $this->load->view('default/footer');
      $this->load->view('default/libreriasDicom');
      $this->load->view('default/scrips');
    }
  }

  public function get_interpretacion(){
    $id_contenido =$_POST['id_contenido'];
    $dataSet=$this->Dicom_Model->Get_interpretaciones($id_contenido);
    echo json_encode($dataSet);
  }

  
  /**
   * Get_datos_toma
   *
   * @return 0:
   * estudio:
   *  data:[
   *    contenido: "3",
   *    dicoms:[
   *            "dicom 1",
   *            "dicom 2"],
   *    nombre: "estudio prueba 1"
   *  ] 
   * 
   */
  public function Get_datos_toma(){
    $id_toma_muestra=$this->input->post('id_toma_muestra');    
    $datos=$this->Interpretacion_Model->get_consulta($id_toma_muestra);
    
    $estudios=$this->BuscarEstudio($datos);
    $data_consulta=[];
    foreach ($estudios as $key => $estudio) {
      $dicoms=$this->Regresar_dicom($datos,$estudio);
      array_push($data_consulta,[
        'estudio'=>[
          'nombre'=>$estudio,
          'data'=>$dicoms]
      ]);
    }
    echo json_encode($data_consulta);
  }
    
  /**
   * Get_Estudios_dicom
   *
   * (Esto solo sirve cuando las dicoms se suben sola)
   * esto recibe por POST el id del estudio y después 
   * regresa los estudios junto a las rutas de las dicom que le pertenece 
   * 
   * @return void
   */
  public function Get_Estudios_dicom(){
    $id_toma_muestra = $this->input->post('id_toma_muestra');
    $dataSet=$this->Dicom_Model->Busca_dicom($id_toma_muestra);
    $respuesta=[];

    $sucursal=$this->Personal_Model->get_und_byUDN($_SESSION['usuario']->id_personal);

    

    foreach ($dataSet as $key => $estudio) {
      $id_estudio= $estudio->id_estudios_sass;
      $nim_sass=$estudio->nim_sass;
      $dicoms=$this->Dicom_Model->Busca_dicoms($nim_sass,$id_estudio,$sucursal->udn);
      
      $nombrePaciente=$this->Paciente_Model->Get_name_By_NumSass($nim_sass);
      $nombre =$nombrePaciente->full_name;

      array_push($respuesta,array(
        'estudio'=>$estudio,
        'dicoms'=>$dicoms,
        'paciente'=>$nombre,
        //'nota'=>datos_clinicos
        
      ));
    }
    echo json_encode($respuesta);
  }

  public function RegresaDicom(){
    $id_estudios_sass = $this->input->post('id_estudios_sass');
    
    $nim_sass=$this->input->post('nim_sass');
    
    //echo "$id_estudios_sass - $nim_sass---";
    $dataSet=$this->Dicom_Model->Busca_dicomsV2($nim_sass,$id_estudios_sass);
    echo json_encode($dataSet);
  }
    
  /**
   * BuscarEstudio
   * 
   * Esto recibe un arreglo y crear uno solo de estudios y quita estudios duplicados
   *
   * @param  Array $array arreglo comom viene de la BD
   * @return Array
   */
  public function BuscarEstudio($array){
    $estudios=[];
    foreach ($array as $key => $item) {
      array_push($estudios,$item->estudio);      
    }
    $estudios=array_unique($estudios);
    return $estudios;
  }

  /**
   * esto regresa un arreglo con las rutas de las dicoms dependiendo el estudio que se le pase
   *
   * @param  Array $arreglos
   * @param  String $estudio
   * @return Array
   */
  public function Regresar_dicom($arreglos=[],$estudio){
    $dicoms=[];
    $id_contenido=0;
    foreach ($arreglos as $key => $conjunto) {
      if ($conjunto->estudio==$estudio) {
        array_push($dicoms,$conjunto->dicom);
        $id_contenido=$conjunto->id_contenido_consulta;
      }      
    }
    $respuesta=[
      'dicoms'=>$dicoms,
     'contenido'=>$id_contenido
    ];
    return $respuesta;
  }

  public function GetNameEstudio(){
    $id=$this->input->post('id_estudio');
    $respuesta=$this->Estudio_Model->get_name_By_nimSass($id);
    echo json_encode($respuesta);
  }
}