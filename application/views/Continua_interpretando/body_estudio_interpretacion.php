<?php 
setlocale(LC_TIME, 'es_ES.UTF-8');
$fecha=strftime("%d de %B de %Y");
//echo $fecha;
?>

<style>
  /*div#salto {    
    height: 65px;
    width: 100%;
  }*/
  canvas {
    width: 100% !important;
  }
  div#dicomImage {
    width: 90% !important;
  }

  .text-aling{
    margin: 0px;
  }

  .full{
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    margin: 0;
    overflow: scroll;
    z-index: 999;
  }
  span.select2-container.select2-container--default.select2-container--open {
    z-index: 9999999999999;
  }
</style>

<br><br>
<input type="hidden" value="<?= $toma ?>" id="toma" />
<input type="hidden" name="id_contenido" id="id_contenido" />
<div id="main">
  <div class="container card row">
    <br>
    <div class="card-alert card cyan">
    <div class="row">
      <div class="col s12 m12 l8 xl8" id="full" style="margin-top: 44px;">
        <span class="card-title white-text darken-1">
          Estudios : <span id="estudioName"></span>
        </span>
        <p id="notas" class="s8" style="text-align: justify;"></p>
      </div>

      <div class="col s12 m12 l4 xl4" >
        
        <a class="btn tooltipped" data-position="top" data-tooltip="Aquí puedes subir la interpretación en formato PDF o puedes escribir en la parte de abajo la interpretación">            
          <img style="margin-top: 2px;" src="https://img.icons8.com/pastel-glyph/30/000000/info--v1.png"/>
        </a>
        <div class="file-field input-field">
          <div class="btn">
            <span>Interpretación</span>
            <input type="file" id="interpretacion" name="interpretacion" accept="application/pdf">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
      </div>

    </div>
      <!-- agregar después para poder hacer mas de una interpretación  -->

      <!--
      <div class="card-action cyan darken-1">
        <button class="btn secundario" onclick="add_newEditor()">
          Agregar otra interpretación
        </button>
        <br />
      </div> 
      -->
    </div>
    <div class="card " id="hoja">
      <!-- no quitar !!!! -->
      <input type="checkbox" id="toggleModalityLUT" />
      <br>
      <div class="row">
        <div class="col s12 m12 l6 xl6" id="full">
          <div class="col s12" id="herramientas">
            <p class="escalas">
              <label>
                <input type="checkbox" id="toggleVOILUT" />
                <span class="check-escalas">Escala de grises</span>
              </label>
              <button onclick="gira_dicom(90)" id="noventaGrados" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/90-degrees.png" />
              </button>
              <button onclick="gira_dicom(180)" id="ciento_ochenta" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/180-degrees.png" />
              </button>
              <button onclick="gira_dicom(270)" id="docientos_setenta" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/270-degrees.png" />
              </button>
              <button onclick="gira_dicom(360)" id="trecientos" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/color/30/000000/360-view.png" />
              </button>

              <!--  -->
              <!-- Botones para la funcionalidad de hacer Zoom -->
              <!-- <button onclick="Zomm()" id="mas" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/plus-math.png" />
              </button>

              <button onclick="QuitarZoom()" id="menos" class="d-none" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/office/30/000000/minus-math.png" />
              </button> -->

              <!-- Botones para la funcionalidad de hacer Zoom -->
              <button onclick="expandir();"  class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">                
                <img src="https://img.icons8.com/flat-round/30/000000/resize-diagonal.png"/>                
              </button>

              <!-- <button onclick="descargarImagen('<?= base_url('Interpretacion/subeimagen') ?>');"  class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">                
                <img src="https://img.icons8.com/office/30/000000/screenshot.png"/>
              </button>               -->

            </p>
          </div>
          <br>

          <!-- aquí va el div donde se va a pintar la dicom  -->
          <div style="width:100%;height:650px;position:relative;color: white;display:inline-block;overflow:hidden;" oncontextmenu="return false" class='disable-selection noIbar ' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
            <div id="dicomImage" style="width:100%;height:100%;top:0px;left:0px; position:absolute;border: 1px dashed #000;">
            </div>
          </div>

          <!-- aquí va el listado de las dicoms  -->
          <div class="row" id="linkDicoms">
          </div>

          <div class="row" id="linkDicoms_descarga">
          </div>
        </div>

        <!-- aquí va el editor de texto  -->
        <div class="col s12 m12 l6 xl6 center" id="panel_editores">
          <div class="">
            <label style="color: red;">Buscar machote</label>

            <div class="input-field">            
              <select class="select2 browser-default" id="select_machote">    
                <option >Seleccionar machote</option>                           
                <?php                   
                  foreach ($machotes as $key => $value) {                    
                    echo "<option value=\"$value->id_body_interpretacion\">$value->title</option>";
                  }
                ?>
              </select>
            </div>
          </div>
          <div class="" id="carData"></div>
          <div class="input-field col s12">
            <input id="titulo-1" name="titulo-1" type="hidden" class="validate">
            <label class="d-none" for="titulo-1">Titulo de la interpretación.</label>
          </div>
          <!-- editor-1  -->
          <div id="encabezado"  class="editor">            
            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Paciente: <span id="paciente_name"></span>
            </p>

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Sexo: <span id="sexo_paciente"></span>
            </p>

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Código: <span id="codigo_paciente"></span>
            </p>      

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Estudio: <span id="estudio_names"></span>
            </p> 

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              <span > <?php echo $fecha; ?></span>
            </p>  


                                    
            <div id="salto">            
            </div>                                          
          </div>

          <div class="editor" id="editor-1">

          </div>
          <div class="" id="pie"></div>
          <br><br>
        </div>
      </div>

      <div class="center mt-3">
        <div class="terminar_interpretacion">
          <button class="btn primario" onclick="terminar_interpretacion(0,'<?= base_url('Interpretacion') ?>')">
            Terminar interpretación
            <span class="material-icons">assignment_turned_in</span>
          </button>
        </div>
        <div class="siguiente_estudio">
          <button class="btn secundario" onclick="terminar_interpretacion(0,'<?= base_url('Interpretacion') ?>')">
            Interpretar Siguiente Estudio
            <span class="material-icons">queue_play_next</span>
          </button>
        </div>
      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</div>

<!-- modal cargando  -->

<div id="load" class="modal">
  <div class="modal-content">
    <h6 class="center" id="text_load"></h6>
    <div class="center">
      <img src="<?= base_url('template/img/barra2.gif') ?>" alt="progreso">
    </div>
  </div>
</div>





<div id="modal" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Crear tabla</h4>
    <hr>
    <div class="row">      
        <div class="row">

          <div class="input-field col s6">
            <input id="filas" type="number" class="validate">
            <label class="" for="filas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g id="original-icon" fill="#e67e22"><path d="M14.25,21.375c-7.85887,0 -14.25,6.39112 -14.25,14.25v42.75c0,7.85887 6.39113,14.25 14.25,14.25v-57h128.25c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM42.75,49.875c-7.85887,0 -14.25,6.39113 -14.25,14.25v42.75c0,7.85888 6.39113,14.25 14.25,14.25h7.125c0,-4.95188 0.75469,-9.73275 2.10132,-14.25h-9.22632v-42.75h114v42.75h-9.22632c1.34662,4.51725 2.10132,9.29812 2.10132,14.25h7.125c7.85888,0 14.25,-6.39112 14.25,-14.25v-42.75c0,-7.85887 -6.39112,-14.25 -14.25,-14.25zM99.75,85.5c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM92.625,99.75h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <input id="columnas" type="text" class="validate">
            <label class="" for="columnas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g fill="#e67e22"><path d="M64.125,14.25c-7.85887,0 -14.25,6.39112 -14.25,14.25v114c0,7.85888 6.39113,14.25 14.25,14.25h42.75c7.85888,0 14.25,-6.39112 14.25,-14.25v-7.125c-4.95188,0 -9.73275,-0.75469 -14.25,-2.10132v9.22632h-42.75v-114h42.75v9.22632c4.51725,-1.34662 9.29812,-2.10132 14.25,-2.10132v-7.125c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM121.125,49.875c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM114,64.125h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <button class="btn primario" id="insert-table" >
              Crear tabla
            </button>            
          </div>
                                 
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>