<?php 
setlocale(LC_TIME, 'es_ES.UTF-8');
$fecha=strftime("%d de %B de %Y");
//echo $fecha;
?>
<style>
  /*div#salto {    
    height: 65px;

    width: 100%;
  }*/

  .modal-info {
      max-width: 100% !important;
      max-height: 100% !important; 
      overflow-y: auto;
    } 
  canvas {
    width: 100% !important;
  }
  div#dicomImage {
    width: 90% !important;
  }

  .text-aling{
    margin: 0px;
  }

  .full{
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    margin: 0;
    overflow: scroll;
    z-index: 999;
  }
  span.select2-container.select2-container--default.select2-container--open {
    z-index: 9999999999999;
  }
</style>
<br><br>
<input type="hidden" value="<?= $toma ?>" id="toma" />
<input type="hidden" name="id_contenido" id="id_contenido" />
<input type="hidden" value="<?= base_url('Interpretacion/Get_capturas') ?>" id="ruta_sabeImg" />
<div id="main">  

  <div class="container card row">            
    <br>
    <div class="card-alert card cyan">
      <div class="card-content white-text">
        <div class="row">
          <div class="col s12 m12 l8 xl8" id="full" style="margin-top: 44px;">
            <span class="card-title white-text darken-1">
              Estudios : <span id="estudioName"></span>
            </span>
            <p style="text-align: justify;">
              <?php echo $nota->datos_clinicos; ?>
            </p>
            
          </div>

          <div class="col s12 m12 l4 xl4" >
            
            <a class="btn tooltipped" data-position="top" data-tooltip="Aquí puedes subir la interpretación en formato PDF o puedes escribir en la parte de abajo la interpretación">            
              <img style="margin-top: 2px;" src="https://img.icons8.com/pastel-glyph/30/000000/info--v1.png"/>
            </a>

            <a onclick="prosponer_interpretacion('<?= base_url('Interpretacion') ?>')" class="btn tooltipped right" data-position="top" data-tooltip="¿Quieres Interpreta después este estudio?">
              <img src="https://img.icons8.com/fluent/30/000000/no-reminders.png"/>
            </a>

            <div class="file-field input-field">
              <div class="btn">
                <span>Interpretación</span>
                <input type="file" id="interpretacion" name="interpretacion" accept="application/pdf">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
          </div>

        </div>                                  
      </div>
      <!-- agregar después para poder hacer mas de una interpretación  -->

      <!--
      <div class="card-action cyan darken-1">
        <button class="btn secundario" onclick="add_newEditor()">
          Agregar otra interpretación
        </button>
        <br />
      </div> 
      -->
    </div>
    
    <div class="card " id="hoja">
      <!-- no quitar !!!! -->
      <input type="checkbox" id="toggleModalityLUT" />
      <br>
      <div class="row">
        <!-- empieza parte de la dicom para verla -->
        <div class="col s12 m12 l6 xl6" id="full">
          <div class="col s12" id="herramientas" style="z-index: 10px;">
            <p class="escalas">
              <label>
                <input type="checkbox" id="toggleVOILUT" />
                <span class="check-escalas">Escala de grises</span>
              </label>
              <button onclick="gira_dicom(90)" id="noventaGrados" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/90-degrees.png" />
              </button>
              <button onclick="gira_dicom(180)" id="ciento_ochenta" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/180-degrees.png" />
              </button>
              <button onclick="gira_dicom(270)" id="docientos_setenta" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/ultraviolet/30/000000/270-degrees.png" />
              </button>
              <button onclick="gira_dicom(360)" id="trecientos" class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">
                <img src="https://img.icons8.com/color/30/000000/360-view.png" />
              </button>

       

              <!-- Botones para la funcionalidad de hacer Zoom -->
              <button onclick="expandir();"  class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">                
                <img src="https://img.icons8.com/flat-round/30/000000/resize-diagonal.png"/>
              </button>

              <button onclick="descargarImagen('<?= base_url('Interpretacion/subeimagen') ?>');"  class="btn ml-1 mr-1" style="background-color: #ffffff;padding:0px">                
                <img src="https://img.icons8.com/office/30/000000/screenshot.png"/>
              </button>
              
            </p>
          </div>
          <br>

          <!-- aquí va el div donde se va a pintar la dicom  -->
          <div style="width:100%;height:650px;position:relative;color: white;display:inline-block;overflow:hidden;" oncontextmenu="return false" class='disable-selection noIbar ' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
            <div id="dicomImage" style="width:100%;height:100%;top:0px;left:0px; position:absolute;border: 1px dashed #000;">
            </div>
          </div>

          <!-- aquí va el listado de las dicoms  -->
          <div class="row" id="linkDicoms">
          </div>

          <div class="row" id="linkDicoms_descarga">
          </div>

          <div class="carousel" id="carousel">
            
          </div>          
        </div>
        <!-- <img class="materialboxed" width="650" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgVFhYYGBgaHCMeHRwcHB0eHB4hHBkcHBkcHB4cIS4lHB4sIxoaJjgmKy8xNTU1GiQ9QDs0Py40NTEBDAwMEA8QHhIRGjEnJSM0MTQ0NDE6NDQ9NDE2NDQ2MTE0NDE9NDQxNDE2PzQ0NDQ0NDQ0NDQxNDE0QDQ0NDQxMf/AABEIALEBHAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAABAMFAgYHAQj/xABBEAABAwIDAwkFBwMDBAMAAAABAAIRAwQSITEFQVEGEyIyUmFxgZEUobHB0QcVI0JykvAz4fFUYoIWg5OyJDRD/8QAGAEBAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAjEQEBAQEBAAIABgMAAAAAAAAAARFRAhIhAwQyQVKREyIx/9oADAMBAAIRAxEAPwDRqdu9wcWtc4NALoBMAmATG6UOt3iQWPEay1wjxyy1Hqptn7RqUHF1N2EmN06GWmOIKZr7drvDQXCGNwtAaOiAQQB6IK80XxOF0cYMaT8Fg4EZEQeB1TjNqVQSQ4AmZIAnOJ9YE+CVrVXPcXuMucZJ4koMWiTA3rOtRLTBg94WLQJzyCyqwc8WI+EeCDynTLshHmsSNykpQM8Zad8BYvA3GfKEHrqRw4vDdxmM9+ixa0kwNSpGkAGHnMZiFEO9BJcUCw5794WLGF0xuEr2s6TOIu7yI8EU3agkgH5aKxKwIjIrJrJBMgeO/wAFiNczkpSGnV5yyHROQ4aqKiWVNmIxpl4rErKmYOseUoPa1PCYmfKN8IZSkTMfwfUIrOk6z5R7lmxzRo8jj0UEKzfRcBJGSxcBORkcdF65rYycT5f3QYoXi9QCEIQCEIQCEIQCEIQCEIQCEIQCELxBWsuBALqjwcvyiPVQ1rh89Fzi3cSInLPdxlSU60NH4r29HTCT6Hh9VhVrgj+q6dYLd51z8ygjNzWGUukd39lH7dU7Z9ye9rZl+M/Li0H5JG4azLAS6ZmRHDT3oD26p2j7vovfb6naPu+iVTlrYOf3D+aIBl5VcYDs/JYuvKgMYzl4K9p8nWxniPnChu+TjsJdTMkatOp/Sd/ggp/bqnbPuWdO9qEgYjr3fRJkLOl1h4oGDfVJ6x930TNO6BwzUfJGfRGRygDiq12pQ0kGRqEFlVuoJio45b2gZ55aeCzo15w9NxdiAILQBBjf6pWneOcQ1z3BpyO//KmpluJsVC4lwyLYkBwjP3pCpxdCBL3g/oHDjKXqXdSYEkTAJbBPksxWhv8AVeOjphOvCeCwq1mkD8VxzmMPE5mfMq6mMDe1c+7XJY/eVTiPQJo3Tchzr8uLR6aaKurtYD0CSO8Jpif7yqcR6I+8qnEegSSYsrR9V4ZTY573GA1oJJ8gmmJ6d9UcYBE+AWP3lU4j0C3ij9lVw2ia1y/mtIp02GtUz4taQB+4xvSN1yLo03Oa+4uaZaAS59lUwZiYLmPcfOE0xqv3lU4j0CPvKpxHoFY7d5LXNq1r6jQ6k+CyrTOKm6RIg5EHucAVQpph37yqcR6BZ0r55IxGGzmQ0GFXqajcPb1SRvTTFi68y6L3EyfyjTODpkpKNxP5icz+WBAmM4GaRpXRc7pvcBhiYBPcPBMUKgkNFRzhmYLYAyOev8lWX7LPo2HuOklZHHrDvRZWRcH9HFMfk1TVUXLgWnGWncfL6D0SePV/5EvvzL90jjPFGM8VL7DU7DvRHsNTsO9Ff8fv+N/pPn4/lP7RYzxRjPFe1aLmZOaRPFRrNln1WpZfuJGucdJKbs5LZ70kx5GYJHgm7Pq+f0UVVU60NA5yo2BkMMjTceChq02EFxqOxbpac40z3aBOUWODB+I9vR0IAGnFxGS8ezow6uYOZEEzOpBAKJs6RqspBvRc4u4EQEmVYutqI/8A0d+w/NFSnbjqveT+kAIIbGiHOk6D56LarCmBC1y3u6bSQGEg8XR7gMj5q+s9oM3M8NT8SibeNnZRyCjfT4KawqF7Q4ZDiRAncB6rK4fLZGqH+3Gh7esCXh7WnpdYAbxqfNVlK0diEgDPe5o+JVjynumuqYGGWs38SdSqegekEX7ScwN72DzJ+AKDSYPzk+DT8yEuV4hhv8L/AHn9oCktXsxNAYZxDMu7+EJBT2fXZ+ofFIYfbVyA5yoMtMM7lBWYwyXPdik5FuueRndkpxXhv9V46OmHfGk8F494Iwmo+DqCziZJ+aKVrspAdFzid0iB/NUonqlCmNHOOY1bC9qW9MCQXnjLYG7L1keiBEDcu8cmrCjsu1b0Q68qtDnuMHACJDZ3AcN51XP/ALLOTvtV8xzmzSo9N0jIkf02nxMHwHet65V1qb7l4ZUL+kAcJMAwBhy1AQL3O0ajnYnPc8nLM6b8gofv25ADQ9zcEAEPPSkkw4cBOm9JPkkQCI36yV65o6UAzjznuAmOIyQbhs7bVF7TTuabHNqNw1XkQHACAXNGW85jNcd5ecmvYrp1NsupOAfTcd7XaCd5GnpxW5c63E0F4ZBBjLOQ6Nfkrj7TNim42ZQugQ59Bok9pjuifQ4T5FBw5CChAJvZ3X8ilE1s/r+RViVf7LrtY8OcYEHdPwV7S2zRaTJDst7X+ui1C6eWtJGRy+KSbcVCYBJJ3Af2Xb8P8x68TJI4/ifl/Pu7bXQPv237LdB+V+7f4qOptigdIb4B/wAwtEq1arTDpaeBEH4KSlcOLXEnMRGQ4hdZ+b98jnfynjtX217lj3NLDIAjQjf3qvVfbvrPeGMBc9xgNa0Ek9wAT20LC7otDqjC1pMYhhcAeBLJDT3HNeb36vr1fV/d6fHmePM8z9mSds+r5/RVFnWc7FiMxG4d/BW9n1fP6LDSopXLQ0DnXiBEYQQO4dyHXLRkKjy2OAB1Ee74JBpbnIJMZZ6H5qOUDNW9e6QXEg/VLIQgArXZ9Nxc0ggAZ66ZqqVps6o1oDnQAD5nxz+CDc9n1nNyc44cQiPy8IlQ7ZvnAFoJDfGAZ171RDa+J4w6SPjqmq9F9YhjGlxOcAE5cSg1i8ficSsbbrBWO1dh16PSexwbxgwPHgqym6CCgwQsi3gsUAp7Lrs/UPioE1buBqNgQMQyklCmm3LYA514yiMIPksTd5/1HkbsgCkQ4QREk6GdM81GgZffVDq4leOvHnIuJ/zKXQg7b9nVy212W+5e4B9w8tacyZaMDZ4RBPoqsYcT8nYiG4eBbLpPe6SFabGpNOxbQ5dZ89x5x7T4HJVYe0PYwuyIOnEAn4BA6XiYAGWX+VWcn67nU8b2kOL3QHcAYA8IAHksqlctJc5uBhIBc4jInKHCchO/vCyp3NMhwDxAbicWkHCCYbMaElBFt+iw0n4gYiQQJcDOoHj7luvI7BUtKtiSXE0jDSZ6LhhOunSPvWovM4QMxlBG+CDM8Cti5GtLbtrmsJcQQ98EDCRxOokA+SDhV1QdTe5jhDmuLSOBBgqFPbZ/+xWjP8R//uUigE1s/r+RSqcsox5ZCD8FYlM33UPl8Utsy65uo1+gGuU5Jm+6h8vioNk1abKzHVAXMaZcAJmNMj3worYeV+yLuGXD7d9Oi5oDXEazmMQGbSdwcAtbojoP8viF1HlX9pNpdWTrZrK2PoEOcGxLHtcSc+4rmDHS2oeP1CsStk5BsaPaKhHSYxoB7LXO6bhvBgAT/u71f3FJvMXLHEuBoufmAMOFuKkfDE2J16RWgbH2tUt6nOMgmC1wcJa5p1a4AiRpv3BXG2uWD61N1JlJlFjgMeEuc50bsTjk3IZAeaiqbZv5vL5q8ser5qj2b+by+aurPq+f0Vo1VCEKAQhCAQhZsaSQAJJyCC75I7ArXlw2lSyz6TyJa1u8nj4L6W5Pcn6FnTDKTc4Ac89ZxG9x+S0X7K9hG0pGtUIBf6mQIHuXS6NfFMbkEd3YsqNLXtBB4gLlHLn7NKLadStQ6D2tLsI6ro3RuJ+K7Eq/bNEOovnslB8mCk0SHOIdwifEHgmdq7P5nAJJxU2vPcXTIHEZa96YubCLmownqveJ9YTfKthw25Ovs9Ix4tKDX6tPDGerQfULKy67P1D4rK6/L+gfMLGy67P1D4pCoChBQgcr2obSpvDpL8UiOrhIAz3zKTUjqhIDScmzA4SZKjQdu+za89psH2pw4qcuYAMzn0hG8knF/wBxVd/bgOa7DBDpw79CDI4xI81qv2f7edbV24YnFiAJAx5YX05OQLm9WTGJrV1Tb9oyswXVuZpvPSgQ5rgYc1wPVIMyDoZQUVm5pZVbAwvpmQdOjDjrvAChpU8NIYA1gqHH0Y6QAhjnRu1jzTViHNrMc3UOxTGRw5kHiCAR5pS7rvJxwenM6CM5iN3gEFJsmu95LnYiWktlxmSHSTkIAGkLf9lVBQt6928wKbCGwSBjd0QO8yQPNVewNkuc4NGWI7t3HzVb9qW36baLbOg6WMJaSI6T2y15MahoLgf9zh2UHJrh+JzncST6letY3CXYgHAiGwZM6mdMlCUIBNbP6/kUqmtn9fyKsSnbojCZEjhMJDnGdg/uKdvuofL4o2Bsk3FTDMNaMTjvwggGO/NRSbqrCZLDn/uU1Gq0NcQ3LKROuaueVexGUgKtKRTJDMLnYiHYeOUiBOm9a/S6j/L4hBlzzOx7ys6l01xksk+J+Su+RVi1z31XtDhSDcLXCQXvdhaSN4aA4xxAWw7Tt21beqx7W4mte5jssQNJpe7MDqlrXiO4K7UyNJtKjTOFuHzlXNj1fNUOzdT5K8s+r5/RRWqoTAs6h0Y70P0XvsVTsP8A2n6K5U2FkJn2Kp2HftP0R7FU7Dv2n6K5eGwstm5C7J9ouWA9Vpk/L018gqL2Kp2H/tP0XRPsstXMFd7mwQ0wHZTDCcpUy8Njc6N86o8hphjDhYBpAynxMLf9mDoNMzIHwXO+Tti7mgAW4sOWIxnG9dA2bWa1gaXN6OWo8ky8NnVgoLtkscOLT8F77QztN9QvDcMjrN9QplNnXzNyzY6nfVsOXSn1EFV/KK6e7mA4yBb0wMhoGn6rYeXNs519WIa4jEIIBIPRHBUW37V5NCGuP/x6YMNOuEyD3q5eGzqoudGfoH/s5eWfXZ+ofFMVrOpDOg/q9k9p3cvLe1e17SWOADhmQQNe8K5eGwiULLAeB9F7zZ4H0KyrBCz5s8D6FHNngfQoMAt05G8uqtk5wc3nKbyMbSdQBBOh6WHKd8AHu07mzwPoVkyi45Brie4FB9CbE27smu7nW12scQQadQhpGIQcjrrrKgdabOpy99/TLMWIDE2QIgDI5nyXA2U8+kHR3DNR4DwPog61yk+1Cm1jrfZ7CxmhqnJzuOAat8TnnkBquWXV26o7E45xAA0aBoGjcP8AOqh5s8D6FHNngfQoMELPmzwPoUc2eB9CgwTWz+v5FQc2eB9CmbBpD9DoVYlM33UPl8V5sDazreqHjTRw4hS16eJuHRKtsDOZBHDMKKtuVXKIXOFrGYGt7gJzkafzRUVPqP8AL4hM1rIE9HIRoTPmvW2hDXNkZx7iga5NbWbRe9tQE0qrcD8PWb2XN4kZ5bwStg2ttu1ZSeKDzVe9hYIa5rWNIIc44gJdBIyG/MrUPYD2h71JVsZPRMDgZPigw2bqfJXln1fP6KqtbctmSDKtbPq+f0QVdO5ECa1RuQyw5eWeigq3VSei95buOYnLP5qelXho/Gc3o6YSc40BjRR1awInnTOsFu865xlqVdvWcnEJu6wMYnz5rH2+r23eqf8Aam5fjOMcW/OJI1VfchgjA4umZkR4bvFNvVyce+31e271U1HbNwwENrVGg6w4ifRV6E2nxnFxQ27eSA24qidIeR80f9TXn+prf+R31XuyuTN5ciaFvUeO0Gw39xgK0rfZztRok2rz4OYT6B0pt6ZOKr/qa8/1Nb/yO+qP+prz/U1v3u+qQvbKpSdhq03sdwc0tPvSybTJw/U2tXcSXVXknUlxlSNvnkEvq1JAGEZnLz0CrUyy9qN0cR/bIJt6fGcOPujLYrVCN5g5fzNDa5IH4jiZHRIMepCVoXRPRc8tYdYE8TpHFTmqCA0VS4Tk0tjwzTb1PjOJRdZCazwcvy9yXqXlXF0XuI3GInyUorw0RWcOjphOsaTwWNWq0gfjOMGYwnfqZ8zuTb1cnEJvq2fSdlr3e5efeNXtn3Jz2luX4zjHFv8AbRVtw1gPRcXDvEJt6ZOJvvGr2z7l63adYZh7h4JJXnJzkvdXr8NCmXAGHOJwsb4uPwEnuTb1cnCNO9rOMB5k+Cx+8avbK3vaPIKnZYRc+03DyJwW1M4AJ0dUcNcjoJyVdc2ezQcL7e/t2yBzjw12H9TCBl4GU29TJxqv3jV7ZR941e2fcrvavJQtpur21Vl1btIDn05DmTpzlM9JvjotZhNvVycN/eNXtn3KSlfVCRie4NnMgTCr1NRuHt6pI3pt6ZOLA3Zg4ariQeG7cdF625cYGNzpMQRkRB3xqlqV0XO6byBhImAd2Q0UtOoJa0VC4TOHCQNCm3qZDSEIUUIQhAIQhAJ2z6vn9Ekn7Hq+aCopV4aBzj2wNMMjy7lDWYwyTUdi3S05xp4KWlXhoHOuGWmGfKeCHVZEGo7pZkYeOsFAtUZSDei9xdwIyShTr6NMaPd+1ZOt6eUF87+j3f5QILf+R/JykyiL26bjDiRb0XSBULdXuj8gOUb48FqGztnuq1W02gw46nsjMn0C6rynZgqMogAMo0mMa0aAYQ53nJ9wQQ3m3bipANQtaOqxnQY0cA1vzULdr3Ia1or1Q1skdI5EnjqfNInIqSsRDYOozy3z70Fz97tuW8xftbVpuyFTC0VKZ0Dw4DMcVzLlVsF9lcOoPzAzY4aPYc2uHlr3ytxCm5aUDcbMpVtX2zzTJ34C3E0Hw+SDlqEIQClt+sFEpbfrBBYitAA514yGRbO5QVWMMuL3Yp0Ldc4B4aKUV4bHOvHR0Ld8ceCKjwRhNR8E5gs4mSfmgVrMpAdFzie8QlU6+hT3Pcc+yRI4rJ1vTygvPHo9w+p9yBFoO5fRt9yVru2fb21lW9kAa01BBBdiAxS5uYdJJPFcl+zHYLbraDGuBNOnNRwOU4eoD4uw+QK6lyv5Ytx+z21V7ajJLy1vRjQQ4jMgg92SCXlPtp1t7PbB731GMBe8GJgAAuzyJgnNUQ5SPqMdRrUjXpvkPa5wcRukOcOiRqBxVNSZjcXvLnPJJLnEmTvzOqzs8mkiIxH6R80G7cjtn7Msqbm06gc9/Re6oCHu1IbhIjCM9BHFcf8AtK2C20vqjKbMFJ4D6Y1EEDEGngHYstwhbBdXVJj2Go57IMtecREzmCdDImQVtH2kbOp3eyadzRcKhoBrg4ZksgNqAnuycf0oOEoQUIBTWnXH83KFTWnXH83ILdCEIBCEIBCEIBO2fV8/okk7Z9Xz+iCppXAAaOdeIERhBjuHFBuQDAqvLYy6IByIjXu+CgZbCDJYSRkcQEKP2M9tn7gia9ffPMguMHuH0WR2jV7ZWPsZ7TP3BHsZ7bP3BD5RvH2W2wfcOr1nDm6LS9xOmUQPM4R7lZ3d06o99R5GJ5kzrmco8Bkk+RFAssroyCXPpglpkYZJgndmAmCySMwPNE+U6xPBYMfPlkFK6lvBB89FiyjrEcdQh8p14WTH+Per/YFmLi0vLYZvLW1G9+GQY+HmqEM4kR4hXfI+vzd0xxzD2uZkc82zoNc2hD5TrjNVkEg7iR6FRqz2lbE1ahxNze45uE5uOqW9jPaZ+4IvyhVS2/WCl9jPaZ+4KRtvDgQ5scMQJ0Q1Oy5bAHPPGURgB8gvDeZ/1HERlkAfeFXtcIIIkmIPDio0Uy+9qHIuPuWTr+odXn/GiUQg61yFZ7Ps2rdhzm1rlxpNec8LGiC5u4GSQJ3gJVrBjDz0jHW3mdSfj5q+2VSDdj2WE4hheTwl1RxII3xJHkqCvVYHsaYDXS3umJj1QMYnF2EGBqXATEjOZyH90tsRhZSGLCZc6YggZwDIUVO4fSfgeA1jj0XyIE6NduHipW31MDAx2IRJDBOETAJjRBlt1rXUKgw45bIA1yggtjeFuPIe7pOD7B0A1KRcGxl1Q13mZn/iVqb3h0AZjcNNCI8pV5yXrhlzTIZieXYZiMnZEz4IOL7QtjSq1KZ1Y9zT/wAXEfJLK75aYfb7vD1effH7yqRAKa064/m5Qpm3IxiNP7ILRCEIBCEIBCEIBO2fV8/okk7Z9Xz+iDVZQhCCenSlr3TGEDLjLgPLeVAmaVUCm9p1cWx/xJn4+5LIOifZbctfz9k6Bz7egeFRgxN9cJ9E5cUXMcWvbDmmCDlmCtA2NfGjWY9pILXAg8CHAtPkQPKV2DalZt/SbeUm9NrQ2vTbm5pEw8cWETnwhBr7dTkBlEdyxDoBjQ69+9esiRPVnM743x5Lyo4EnCIbJgd27PigjxHTgrjYtUUm17pwGClSdme28YGNBP5iXKusbV9R7WMGJ7jAH17kt9oe26TKTbGgQW03HnHDR9QDC5w4tb0mjvJ4IOdVXySeJJXppHCHbiSO/IT81EpMZjDuBmO9BGpbfrDxUSlt+sPFBEhCEDd3btY2mQ8OL2YiB+U4nNwnPWGz5pREoQdn+zW8F1sypaQOctiXsAzJa9zn6ccRcPRI3NGC0lufAj5btVpXInlA6yuWVm5gHpt7TDk9vjkHDvaF13btqytTF5bPDqVTPo7nTmDwMzluzQawWtc08CDkdDxHioqFNmFpYA0HQNyncCUxzTpIbGKRmRImd6iuXaGDOInWd5nwGaCj2M976z3Fpa5gwOLnF2J+KS7PTTQZBdA5OW7GF95UECgxzy4kxIaRHDRU2yNnOe/C0dJxmAOP+VF9qO3WULduzKJBcSHXDgd4ghniSATwDRxQcova5fUe86vc537iT81AgoQCmtOuP5uUKmtOuP5uQW6EIQCEIQCEIQCfser5pBO2fV8/og1VCk5l3Zd6FHMu7LvQoI0KTmXdl3oUcy7su9CgjVxsHbta1qNqUnlpbpHDeO9p3g5fFVfMu7LvQo5l3Zd6FB2Tk9y72a9xfcUjQqFpDnNk03YgcRwjNrs+BGeqLp+x29IXz8J0Y1uJ3hMfFcep0TIxNdG+AZWPMO7LvQoOgbZ+0JoY6hY0zRYcnPJmq8aHE4dUHg31C59UqFxk/wA4AcAjmXdl3oUcy7su9CgjQpPZ39l3oUcy7su9CgjUtv1h4rx1Jw1aR5Fe2/WHigiQpOZd2XehRzLuy70KCNCk5l3Zd6FHMu7LvQoI1uHIblrUsXFjm87bvM1KZ10ALmz+aAPGBpqtT5l3Zd6FZMtnkwGOJ4BpKD6B2fV2ZckVaN01gmXMeQ1w1MQ4iPeipsezYcRvaOEcXMkDycvn+nRM9Jro3wDKw5l3Zd6FB1/lB9o1vbMdQ2c3G89au4dEbjgBzcfQDvXIa1VznFziS5xJJOpJMkniZXnMu7LvQo5l3Zd6FBGhScy7su9CjmXdl3oUEamtOuP5uWPMu7LvQqa2pOD2ktI8QeCCzQhCAQhCAQhCATtn1fP6JJP2HV8/kEFWzQeAWaEIBCEIBCEIBCEIBYoQiM3aN8/ivEIVpENx1SlKHWHihCin2rJCEAhCEAprLrjz+C8Qtef1M+/0oSvUIWa0EIQgEIQgFi75oQglK8QhAIQhAIQhAJ+x6vmhCD//2Q=="> -->
        <!-- termina parte de la dicom para verla -->

        <!-- aquí va el editor de texto  -->
        <div class="col s12 m12 l6 xl6 center" id="panel_editores">
          
          <div class="">
            <label style="color: red;">Buscar machote</label>

            <div class="input-field">            
              <select class="select2 browser-default" id="select_machote">    
                <option >Seleccionar machote</option>                           
                <?php                   
                  foreach ($machotes as $key => $value) {                    
                    echo "<option value=\"$value->id_body_interpretacion\">$value->title</option>";
                  }
                ?>
              </select>
            </div>
          </div>
          
          <div class="" id="carData"></div>
          <div class="input-field col s12 d-none">
            <input id="titulo-1" name="titulo-1" type="hidden" class="validate">
            <label class="d-none" for="titulo-1">Titulo de la interpretación.</label>
          </div>
          <!-- editor-1  -->
          <div id="encabezado"  class="editor">            
            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Paciente: <span id="paciente_name"></span>
            </p>

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Sexo: <span id="sexo_paciente"></span>
            </p>

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Código: <span id="codigo_paciente"></span>
            </p> 

            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              Estudio: <span id="estudio_names"></span>
            </p>    
            
            <p class="ql-align-right" style="padding: 0px;margin:0px" >
              <!-- <span >  <?php echo $fecha; ?> </span> -->
            </p>    
            <br>
                                    
            <div id="salto">            
            </div>                                          
          </div>
          <div class="editor" id="editor-1">
            
          </div>
          <div class="" id="pie"></div>
          <br><br>
        </div>
      </div>

      <div class="center mt-3">
        <div class="terminar_interpretacion">
          <button class="btn primario" onclick="terminar_interpretacion(0,'<?= base_url('Continua_interpretando') ?>')">
            Terminar interpretación
            <span class="material-icons">assignment_turned_in</span>
          </button>
        </div>
        <div class="siguiente_estudio">
          <button class="btn secundario" onclick="terminar_interpretacion(0,'<?= base_url('Continua_interpretando') ?>')">
            Interpretar Siguiente Estudio
            <span class="material-icons">queue_play_next</span>
          </button>
        </div>


      </div>
      <br>
      <br>
      <br>
    </div>
  </div>
</div>

<!-- modal cargando  -->

<div id="load" class="modal">
  <div class="modal-content">
    <h6 class="center" id="text_load"></h6>
    <div class="center">
      <img src="<?= base_url('template/img/barra2.gif') ?>" alt="progreso">
    </div>
  </div>
</div>



<div id="modal" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Crear tabla</h4>
    <hr>
    <div class="row">      
        <div class="row">

          <div class="input-field col s6">
            <input id="filas" type="number" class="validate">
            <label class="" for="filas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g id="original-icon" fill="#e67e22"><path d="M14.25,21.375c-7.85887,0 -14.25,6.39112 -14.25,14.25v42.75c0,7.85887 6.39113,14.25 14.25,14.25v-57h128.25c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM42.75,49.875c-7.85887,0 -14.25,6.39113 -14.25,14.25v42.75c0,7.85888 6.39113,14.25 14.25,14.25h7.125c0,-4.95188 0.75469,-9.73275 2.10132,-14.25h-9.22632v-42.75h114v42.75h-9.22632c1.34662,4.51725 2.10132,9.29812 2.10132,14.25h7.125c7.85888,0 14.25,-6.39112 14.25,-14.25v-42.75c0,-7.85887 -6.39112,-14.25 -14.25,-14.25zM99.75,85.5c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM92.625,99.75h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <input id="columnas" type="text" class="validate">
            <label class="" for="columnas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g fill="#e67e22"><path d="M64.125,14.25c-7.85887,0 -14.25,6.39112 -14.25,14.25v114c0,7.85888 6.39113,14.25 14.25,14.25h42.75c7.85888,0 14.25,-6.39112 14.25,-14.25v-7.125c-4.95188,0 -9.73275,-0.75469 -14.25,-2.10132v9.22632h-42.75v-114h42.75v9.22632c4.51725,-1.34662 9.29812,-2.10132 14.25,-2.10132v-7.125c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM121.125,49.875c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM114,64.125h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <button class="btn primario" id="insert-table" >
              Crear tabla
            </button>            
          </div>
                                 
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>


<div id="modal-galeria" class="modal ">
  <div class="modal-content">
    <h4>ver captura de imagen</h4>
    <hr>
    <div class="row">      
      <div class="row">
        <div id="imagen_panel"></div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>