<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Interpretacion</title>
  <style type="text/css">
    /**{
      font-family:Arial;
    }*/

    @font-face {
      font-family: "Arial";
      font-style: normal;
      font-weight: normal;
      src: url(<?= base_url('template/fuentes/arial.ttf') ?>) format('truetype');
    }

    @page {
      margin: 80px 100px;
      font-size: 14px;
      font-family: 'Arial';
      /*margin*/
    }

    p {

      margin: 0px 0px 1px 0px;
      font-family: 'Arial';
    }

    html {
      font-family: 'Arial';
    }

    h3 {
      font-family: 'Arial';
    }

    p {
      font-family: 'Arial';
      margin-bottom: 8px;
    }

    p.ql-indent-1 {
      padding: 0px 0px 0px 20px;
    }

    strong {
      font-family: 'Arial';
    }

    .panel-main {
      font-family: 'Arial';
    }

    .panel-main {
      padding: 1px;
      margin: 1px;
    }

    .p-j {
      text-align: justify;
    }

    .pie {
      text-decoration: overline;
      width: 100%;
      position: absolute;
      bottom: 0;
    }

    /*
    img {
      white-space: nowrap;
      display:block;      
      
      position: absolute;
      bottom: 0;
    }
    */


    .oculta {
      display: none;
    }

    /***
      Estilo para la pre visualización 
    */

    .ql-align-right {
      text-align: right;
    }

    .ql-align-center {
      text-align: center;
    }

    .ql-align-left {
      text-align: left;
    }

    .ql-align-justify {
      text-align: justify;
    }

    .panel-main table {
      border-collapse: collapse;
    }

    .panel-main td {
      border: 1px solid #000;
      padding: 2px 5px;
    }

    .panel-main table {
      table-layout: fixed;
      width: 100%;
    }

    .panel-main table td {
      outline: 0;
    }

    h3.ql-align-center {
      font-family: 'Arial';
    }
  </style>
</head>

<body>
  <?php
  if ($membretada) {
  ?>

    <img style="position: fixed;
                  top: -60px;
                  left: -50px;
                  right: 0px;
                  height: 50px;
                  width: 250px;
                  height:110px
                " src="<?= base_url('template/img/cahitoredondo.svg') ?>">

    <img style="position: fixed;                
                bottom:-60px;
                left: -80px;
                height: 50px;
                width: 150px;
                height:70px" src="<?= base_url('template/img/logo_30.svg') ?>">

  <?php
  }
  ?>

  <div class="panel-main " style="width: 100%;">

    <?php
    $porciones = explode('<p></p>', $interpretacion[0]->texto);
    //var_dump($porciones);
    foreach ($porciones as $key => $item) {
      //var_dump($item);
      echo $item;
      echo '<p style="height: 20px;"></p>';
    }
    //<p class="ql-align-justify"><br></p>
    //<p class="ql-align-right"><br></p>
    //<p class="ql-align-left"><br></p>
    //<p class="ql-align-center"><br></p>      
    //<p><br></p>






    /*
      $exp_regular = '/<p></p>/';

      $resultado2 = str_replace($exp_regular, '<p style="height: 300px;">------------------------------------------</p>', $interpretacion[0]->texto);

      echo $resultado2;

      */

    //echo $porciones[0]; // porción1
    //echo $porciones[1]; // porción2


    //$coun=count($pp);
    //echo $interpretacion[0]->texto;
    // echo $coun;
    // foreach ($pp as $key => $value) {
    //var_dump($pp);
    // echo 0;
    // }
    ?>

    <!-- <?= var_dump($interpretacion[0]->texto);  ?> -->

  </div>



</body>

</html>