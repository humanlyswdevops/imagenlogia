<!DOCTYPE HTML>
<html lang="es" xml:lang="es">

<head>
  <meta charset="UTF-8">
  
  <title>Estudios</title>
  <style>
    body {
      background-color: #eee;
      font-family: Arial, Helvetica, sans-serif;     
    }
    h4{
      color:#fff;
    }
    h5 {
      font-size: 30px;
      color: RGB(0,116,200) !important;
      margin-top:0px;
      margin-bottom: 0px;
      text-align:center;
    }
    .second {
      background-color: RGB(0,116,200);
      width: 60%;
      border-radius: 10px 10px 0px 0px;
      padding-left: 40px;
      padding-right: 40px;
      text-align: center;
      font-size: 40px;
      color: #fff;
      padding-top: 1px;
      padding-bottom: 1px;
      margin:auto;
      display:block;
    }
    .tree {
      background-color: #cdcdcd;
      height: 400px;
      width: 60%;
      margin-top: -30px;
      z-index: 9;
      padding-left: 40px;
      padding-right: 40px;
      margin:auto;
      display:block;
      border-radius: 0px 0px 10px 10px;

    }
    .link {
      background-color: RGB(0,116,200);
      padding-left: 40px;
      padding-right: 40px;
      width: 90%;
      margin: auto;
      display: block;
      border-radius: 0px 0px 5px 5px;
      margin:auto;
      display:block;
    }
    .dates {
      background-color: #fff;
      width: 90%;
      margin: auto;
      display: block;
      padding-left: 10px;
      padding-right: 10px;
      margin-top: -30px;
      z-index: 10;
      height: 350px;
      border-radius: 0px 0px 5px 5px;
    }
    button {
      background-color: RGB(12,129,204);
      border-radius: 50px;
      width: 208px;
      height: 37px;
      overflow: hidden;
      font-size: 15px;
      font-weight: 500;
      color: #ffffff;
      border: none;
      outline: none;
      margin-top: 25px;
      margin-bottom: 30px;
      margin:auto;
      display: block;          
    }
    a{
      text-decoration: none;
    }
    button:hover {
      border-bottom: 2px solid RGBA(12,129,204,2);
      box-shadow: 0px 2px 0 RGBA(12,129,204,2);
    }
    .Firma{
      height: 300px;
      border-radius:10px;
    }
    .Firma p {
      text-align: center;
      color: #122D72 !important;
      font-size: 12px !important; 
    }
    .Firma b {
      text-align: center;
      color: #122D72 !important;
      line-height: 3px;
      font-size: 12px !important;
    }
    p{
      color:#566078 !important
    }
    .linea{
      background-color: RGBA(12,129,204,.5) !important;
      height: 2px;
      border: none;
    }
    b{
      color:#566078;
    }
    .user{
      font-weight: bold;
    }
    .password{
      font-weight: bold;
    }  
  </style>
</head>

<body>

  <div class="col-md-12 second">
    <h4>Agenda virtual</h4>
  </div>
  <div class="col-md-12 tree">
    <div class="col-md-12 dates">
      <br>
      <h5>Bienvenido!</h5>
      <hr class="linea">     

      <p  style="text-align: justify;">
        Ponemos a sus disposición la interpretación de la placa realizada
      </p> 
      <p style="text-align: justify;">
        Para abrir el informe es necesario que ingreses a este LINK y posterior ingreses el código único de reconocimiento.
      </p>

      <center>
        <p class="user">
          <b>Codigo:</b> 
          <?= $codigo ?>
        </p>
      </center>
                             
      <p>Da clic en el siguiente enlace para continuar</p></br>
      <a href="<?= $link ?>" target="blank_"><button>Clic Aquí</button></a></br></br>
    </div>
    
  </div>
  <br>
  <div class="col-md-12 Firma">
    <P>Este es un mensaje enviado automaticamente por Agenda Virtual,porfavor no responda</P>
    <p><b>Creador por Humanly Software</b></b></p>
  </div>

</body>
</html>