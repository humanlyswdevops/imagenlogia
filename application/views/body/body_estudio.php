<!-- BEGIN: Page Main-->
<div id="main">
  <br>
  <br>
  <br>
  <div class="row animate__animated animate__fadeInUpBig">
    <div class="col s12">
      <div class="row">
        <div class="card">
          <div class="card-content min-form">
          <!--
            <button class="btn primario" onclick="crear_estudio()" >
              Nuevo estudio
            </button>
          -->                    
            <div class="card-content" >
              <h4 class="card-title">Listado de Estudio</h4>
              <div class="row">
                <div class="col s12">
                  <table id="page-length-option" class="display">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Id Sass</th>
                        <th>Honorario</th>
                        <th>Conteo Placas</th>
                        <th>Editar</th>
                      </tr>
                    </thead>
                    
                    <tbody>
                      <?php
                      foreach ($dataset as $key => $estudio) {
                        //var_dump($estudio);
                        echo "<tr>
                                <td>$estudio->nombre</td>
                                <td>$estudio->id_estudios_sass</td>
                                <td>$estudio->honorario</td>
                                <td>$estudio->placas</td>
                                <td>
                                  <button class=\" btn primario\" onclick=\"Editarestudio($estudio->id_estudio,'$estudio->nombre','$estudio->id_estudios_sass','$estudio->honorario',$estudio->placas)\">
                                    editar
                                  </button>
                                </td>
                              </tr>
                              ";
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


</div>

<!-- modal para editar el usuario -->
<div id="modalEditar_estudio" class="modal modal-fixed-footer">
  <div class="modal-content">
    
    <!------>
    <div class="card-alert card barra">
      <div class="card-content white-text">
        <p class="center" id="nombre_estudio_card"></p>
      </div>
    </div>
    <!-------->    
    
    <h6 class="center">Edición de estudio</h6>
    <br>
    <div class="row">
      <form id="EditaEstudio" method="POST">
        <input type="hidden" id="id_estudio" name="id_estudio">        

        <div class="input-field col s12 m6 l6">
          <input id="nombre" name="nombre" type="text" class="validate">
          <label for="nombre">Nombre</label>
        </div>    

        <div class="input-field col s6">
          <input name="nim_sass" id="nim_sass" type="text" class="validate">
          <label for="nim_sass">Nim Sass</label>
        </div>

        <div class="input-field col s12 m6 l6">
          <input id="honorario" name="honorario" type="text" class="validate">
          <label for="honorario">Honorario</label>
        </div>

        <div class="input-field col s12 m6 l6">
          <input id="placas" name="placas" type="text" class="validate">
          <label for="placas">Conteo de placas</label>
        </div>

        <br>
        <div class="col s12">
          <button class="waves-effect waves-light  btn primario left" type="submit">
            Finalizar edición
          </button>
        </div>


      </form>
    </div>
  </div>
  <div class="modal-footer">    
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario right">Cerrar</a>
  </div>
</div>