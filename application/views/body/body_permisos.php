<?php
$elimina='';
if(in_array('ELIMINAR', $modulo, true)){
  $elimina='ELIMINAR';
}
$ver='';
if(in_array('VISUALIZAR', $modulo, true)){
  $ver='VISUALIZAR';
}
?>
<!-- BEGIN: Page Main-->
<br>
<br>


<div id="main">
  <div class="row animate__animated animate__fadeInUpBig">

  

  <?php
    if(in_array('INSERTAR', $modulo, true)){
  ?>
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <!-- Modal Trigger -->
          <a class="waves-effect waves-light btn primario modal-trigger" href="#" onclick="nuevo_privilegios()">Nuevo privilegio</a>
          <!--boton modal para crear nuevo tipo acceso -->
          <a class="waves-effect waves-light btn primario modal-trigger" href="#privilegioM">Nuevo acceso</a>
          <!-- Modal Structure -->   
        </div>
      </div>
      <div id="modal1" class="modal modal-fixed-footer">
        <div class="modal-content">
          <h4>Nuevo Privilegio</h4>
          <div class="row">
            <form class="col s12">

              <div class="input-field col s6">
                <span class="material-icons prefix">admin_panel_settings</span>
                <input id="privilegio2" type="text" class="validate">
                <label for="privilegio2">Nuevo privilegio</label>
              </div>

              <button class="waves-effect waves-light  btn primario" type="submit">
                Guardar
              </button>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
        </div>
      </div>
    </div>
  <?php
  }
  ?>
    <!-- Inicio de modal de nueva regla para el usuario-->
    <div id="privilegioM" class="modal modal-fixed-footer">
      <div class="modal-content">
        <h4>Nuevo acceso</h4>
        <div class="row">
          <form class="col s12" method="POST" onsubmit="Nuevo_acceso(event,'<?=$elimina?>')">

            <div class="col col s12 m6 l6">
              <label for="">
                <h6>Privilegio</h6>
              </label>
              <div class="input-field">
                <select id="privilegio" name="privilegio" class="browser-default" required>
                  <option selected disabled>Seleccionar</option>
                  <?php
                  foreach ($privilegios as $key => $privilegio) {
                    echo "<option value='$privilegio->id'>$privilegio->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="col s12 m6 l6">
              <label for="">
                <h6>Seleccionar modulo</h6>
              </label>
              <div class="input-field">
                <select id="modulo" name="modulo" class="browser-default" required>
                  <option selected disabled>Seleccionar</option>
                  <?php
                  foreach ($modulos as $key => $modulo) {
                    echo "<option value='$modulo->id'>$modulo->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="col s12 m6 l6">
              <label for="">
                <h6>Seleccionar tipo de operación</h6>
              </label>
              <div class="input-field">
                <select id="operacion" name="operacion" class=" browser-default" required>
                  <option selected disabled>Seleccionar</option>
                  <?php
                  foreach ($operaciones as $key => $operacion) {
                    echo "<option value='$operacion->id'>$operacion->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="col s12">
              <button class="waves-effect waves-light  btn primario" type="submit">
                Crear nuevo acceso
              </button>
            </div>

          </form>
        </div>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
      </div>
    </div>

    <!--Tabla de los permisos-->
    <div class="row s12">
      <div class="col s12 m6 l6">
        <div class="card">
          <div class="card-content">
            <h4 class="card-title">Listado de privilegios</h4>
            <div class="row">
              <div class="col s12">
                <table id="page-length-option" class="display">
                  <thead>
                    <tr>
                      <th>Privilegios</th>
                      <th>Acceso</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($privilegios as $key => $privilegio) {
                      echo "  <tr>
                              <td>$privilegio->nombre</td>
                              <td>
                                <button class=\"btn primario\"  onclick=\"detalle_privilegio($privilegio->id,'$elimina')\">
                                  Ver 
                                </button>
                              </td>
                            </tr>
                            ";
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <?php
        if($ver=='VISUALIZAR'){
      ?>
        <div class="col s12 m6 l6">
          <div class="card">
            <div class="card-content">

              <table  id="scroll-vert-hor" class="display nowrap">
                <thead>
                  <tr>
                    <th>Privilegio</th>
                    <th>Módulo</th>
                    <th>Tipo de operación</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="body_tabla_privilegio">

                </tbody>
              </table>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>



  </div>
</div>