<!-- BEGIN: Page Main-->
<style>
  p {
    color: black !important;
  }

  div#cedulas {
    display: grid;
    margin-top: -64px;
  }

  .ql-tooltip.ql-hidden {
    display: none;
  }

  div#cedulas {
    margin-top: 13px;
  }
</style>

<div id="main">

  <div class="row animate__animated animate__fadeInUpBig">

    <div class="col s12 m12 l6">

      <ul id="projects-collection" class="collection z-depth-1">
        <li class="collection-item avatar">
          <i class="material-icons cyan circle">speaker_notes</i>
          <h6 class="collection-header m-0">Interpretaciones</h6>
        </li>

        <?php
        //var_dump($_SESSION['usuario']->privilegio);
        $puedeEditar = false;
        //var_dump($_SESSION['usuario']->privilegio);      
        if ($_SESSION['usuario']->privilegio == 'medico radiólogo') {
          $puedeEditar = true;
        }

        $ruta = base_url('Interpretacion/Traer_interpretacion');
        $rutabase = base_url();

        foreach ($interpretaciones as $key => $interpretacion) {
          $interpretacion = (object)$interpretacion;
          //var_dump($interpretacion);
          $atributos = '';
          $NamePdf = '';
          if (strlen($interpretacion->interpretacion) <= 300) {
            $ruta_descarga = base_url("uploads/interpretacionpdf/$interpretacion->interpretacion");
            $atributos = "download=\"interpretacion.pdf\"";
            $NamePdf = $interpretacion->interpretacion;
          } else {
            $ruta_descarga = base_url("Pdf/GenerarPdf_Interpretacion/$interpretacion->id_interpretacion");
          }

          $ruta_imprmir = base_url("Pdf/ImprimirPdf_Interpretacion/$interpretacion->id_interpretacion");


          $Iconocorreo = base_url('uploads/assets/forward_to_inbox.svg');
          $icono2 = base_url('uploads/assets/cloud_download.svg');
          $icono3 = base_url('uploads/assets/remove_red_eye.svg');

          $Edita = '';
          if ($puedeEditar) {
            $Edita = '
              <a href="' . base_url("Interpretacion/Editar/$interpretacion->id_interpretacion") . '" class="btn primario mt-2">
                Actualizar
              </a>
            ';
          }

          $dicoms_imagenes = "";

          foreach ($interpretacion->dicoms as $key => $value) {
            $separa = explode('Imagenologia2', $value);
            $dicomView = $separa[1];

            $quitaDiagonal = explode('\\', $dicomView);
            $nueva_ruta = "";
            foreach ($quitaDiagonal as $key2 => $value3) {
              $nueva_ruta .= $value3 . '/';
            }
            $num = $key + 1;
            $dicoms_imagenes .= "
              <div class=\"chip btn primario bg-primario\" onclick=\"handle_viewDicom('$nueva_ruta')\">              
                <img src=\"https://img.icons8.com/color/48/000000/xray.png\" alt=\"Imagen dicom\">
                Imagen $num
              </div>";
          }

          echo "
          <li class=\"collection-item\">
          <div class=\"row\">
            <div class=\"col s6\">
              <p class=\"collections-content mt-1\">
                $interpretacion->estudio
              </p>
            </div>
            <div class=\"col s6\">              
              <a href=\"#\" class=\"btn task-cat secundario mt-1 \"  onclick=traer_interpretacion($interpretacion->id_interpretacion,'$ruta','$rutabase',`$NamePdf`) \">
                Ver <img src=\"$icono3\" alt=\"icono de ojo\">
              </a>

              <a href=\"$ruta_descarga\" $atributos class=\"btn task-cat primario ml-2 mt-1 \">
                Descargar <img src=\"$icono2\" alt=\"icono de nube\">
              </a>

              <a href=\"$ruta_imprmir\" $atributos class=\"btn task-cat primario ml-2 mt-1 \">
                imprimir
              </a>

              <button class=\"btn task-cat primario ml-2 mt-1 modal-trigger\" href=\"#modal12\">
                <img src=\"$Iconocorreo\" alt=\"icono de correo\">
              </button>
              $Edita                            
            </div>
            <div class=\"col s12 mt-2\">
              $dicoms_imagenes
              <hr>
            </div>
          </div>
        </li>
          ";
        }
        ?>

      </ul>
    </div>


    <div class="col s12 m12 l6">
      <div class="card">
        <div class="card-content" style="margin-top: -8px;">
          <div class="row">
            <div class="col s12">
              <!-- <img src="<?= base_url('template/img/head4.svg') ?>" alt="Encabezado del pdf" width="100%"> -->
              <!-- aqui se monta la parte de ver la consulta  -->
              <!-- class="ql-editor" --->
              <div id="consulta"></div>

              <div id="lienzo_dicom">
                <input type="checkbox" id="toggleVOILUT" />
                <!-- no quitar !!!! -->
                <input type="checkbox" id="toggleModalityLUT" />
                <!-- aquí va el div donde se va a pintar la dicom  -->
                <div style="width:100%;height:650px;position:relative;color: white;display:inline-block;overflow:hidden;" oncontextmenu="return false" class='disable-selection noIbar ' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
                  <div id="dicomImage" style="width:100%;height:100%;top:0px;left:0px; position:absolute;border: 1px dashed #000;">
                  </div>
                </div>

                <!-- aquí va el listado de las dicoms  -->
                <div class="row" id="linkDicoms">
                </div>

                <div class="row" id="linkDicoms_descarga">
                </div>
              </div>
              <!-- <img src="<?= base_url('template/img/feet4.svg') ?>" alt="pie del pdf" width="100%"> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal12" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Enviar Acceso</h4>
    <hr>
    <div class="row">
      <form id="frm_manda_acceso" method="POST">
        <div class="col s12">
          <input type="hidden" name="curp" id="curp" value="<?= $userCode->curp ?>">
          <div class="input-field col s9" id="panel_paciente">
            <h6 class="card-title">Ingresar correo </h6>
            <div class="input-field">
              <input type="email" name="email" id="email" required placeholder="Escribir correo">
            </div>
          </div>
        </div>
        <button class="waves-effect waves-light  btn primario" type="submit">
          Enviar
        </button>
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>

<!-- esta es la modal de cargando -->
<div id="load" class="modal">
  <div class="modal-content">
    <h6 class="center" id="text_load"></h6>
    <div class="center">
      <img src="<?= base_url('template/img/barra2.gif') ?>" alt="progreso">
    </div>
  </div>
</div>

<div id="dicomView" class="modal">
  <div class="modal-content">
    <h6 class="center" id="text_load"></h6>

  </div>
</div>