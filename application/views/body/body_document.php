<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Interpretacion</title>
  <style>    

    @page {
      margin: 20px 60px;
    }
        
    .panel-main {
      padding: 1px;
      margin: 1px;
    }
    
    .p-j {
      text-align: justify;
    }
    
    .pie {
      text-decoration: overline;
      width: 100%;
      position: absolute;
      bottom: 0;
    }
    
    img {
      display:block;
      margin:20px;
      margin-left: 80px;
      position: absolute;
      bottom: 0;  
    }

    
    .oculta{
      display: none;
    }
    /***
      Estilo para la pre visualización 
    */
    
    .ql-align-right {
      text-align: right;
    }
    .ql-align-center {
      text-align: center;
    }
    .ql-align-left {
      text-align: left;
    }
    .ql-align-justify{
      text-align: justify;
    }

    .panel-main table {
      border-collapse: collapse;
    }

    .panel-main td {
      border: 1px solid #000;
      padding: 2px 5px;
    }

    .panel-main table {
      table-layout: fixed;
      width: 100%;
    }
    .panel-main table td {
      outline: 0;
    }
  </style>
</head>

<body>

  <div class="panel-main " style="width: 100%;">   
    <?= $interpretacion[0]->texto ?>
  </div>
  
</body>

</html>
