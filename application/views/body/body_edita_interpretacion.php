<!-- BEGIN: Page Main-->
<style>
  p {
    color: black !important;
  }

  div#cedulas {
    display: grid;
    margin-top: -64px;
  }
</style>

<div id="main">
  <div class="d-none" id="interpretacion">
    <?= $interpretacion->texto ?>
  </div>
  
  <div class="row animate__animated animate__fadeInUpBig">
    <div class="col s12 m12 l9">
      <div class="card  ">
        <div class="card-content" style="margin-top: -8px;">
          <div class="row">
            <div class="col s12">              
              <input type="hidden" name="bandera" id="bandera">

              <div id="files_panel"></div>

              <div class="header_interpretacion d-none" id="header_interpretacion">
                <?= $interpretacion->texto ?>
              </div>

              <div id="encabezado">

              </div>
              
              <div id="consulta1" class="ql-editor">

              </div>

              <div id="pie" class="ql-editor">

              </div>
              
              <button class="btn primario mt-5 mb-5 right" onclick="ActualizaInterpretacion(<?= $id ?>)">
                Terminar de editar
              </button>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modal" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Crear tabla</h4>
    <hr>
    <div class="row">      
        <div class="row">

          <div class="input-field col s6">
            <input id="filas" type="number" class="validate">
            <label class="" for="filas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g id="original-icon" fill="#e67e22"><path d="M14.25,21.375c-7.85887,0 -14.25,6.39112 -14.25,14.25v42.75c0,7.85887 6.39113,14.25 14.25,14.25v-57h128.25c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM42.75,49.875c-7.85887,0 -14.25,6.39113 -14.25,14.25v42.75c0,7.85888 6.39113,14.25 14.25,14.25h7.125c0,-4.95188 0.75469,-9.73275 2.10132,-14.25h-9.22632v-42.75h114v42.75h-9.22632c1.34662,4.51725 2.10132,9.29812 2.10132,14.25h7.125c7.85888,0 14.25,-6.39112 14.25,-14.25v-42.75c0,-7.85887 -6.39112,-14.25 -14.25,-14.25zM99.75,85.5c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM92.625,99.75h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <input id="columnas" type="text" class="validate">
            <label class="" for="columnas">Numero de Filas 
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="30" height="30" viewBox="0 0 171 171" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g fill="#e67e22"><path d="M64.125,14.25c-7.85887,0 -14.25,6.39112 -14.25,14.25v114c0,7.85888 6.39113,14.25 14.25,14.25h42.75c7.85888,0 14.25,-6.39112 14.25,-14.25v-7.125c-4.95188,0 -9.73275,-0.75469 -14.25,-2.10132v9.22632h-42.75v-114h42.75v9.22632c4.51725,-1.34662 9.29812,-2.10132 14.25,-2.10132v-7.125c0,-7.85888 -6.39112,-14.25 -14.25,-14.25zM121.125,49.875c-19.67212,0 -35.625,15.95288 -35.625,35.625c0,19.67212 15.95288,35.625 35.625,35.625c19.67212,0 35.625,-15.95288 35.625,-35.625c0,-19.67212 -15.95288,-35.625 -35.625,-35.625zM114,64.125h14.25v14.25h14.25v14.25h-14.25v14.25h-14.25v-14.25h-14.25v-14.25h14.25z"></path></g></g></svg>
          </div>

          <div class="input-field col s6">
            <button class="btn primario" id="insert-table" >
              Crear tabla
            </button>            
          </div>
                                 
        </div>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>

<script>
  window.onload = () => {
    try {
      let body  = document.getElementById('header_interpretacion').innerHTML;

      body = body.split('id="salto"');
      
      if (body.length===2) {
        document.getElementById('bandera').value='Istexto';
        document.getElementById('encabezado').innerHTML = body[0];      
        let body2 =body[1].split('src="');
        document.getElementById('consulta1').innerHTML = '';
        let cuerpo=body2[0].substring(1);
        cuerpo=cuerpo.trim();
        document.getElementById('consulta1').innerHTML = cuerpo;
        
        document.getElementById('pie').innerHTML =`<p class="ql-align-center pie"> <img src="${body2[1]}`;
        habilita_editor('#consulta1');
      }else{
        if (body.length===1) {
          document.getElementById('bandera').value='Isfile';
          
          document.getElementById('files_panel').innerHTML=`
          <div class="file-field input-field">
            <div class="btn">
              <span>Actualizar interpretación</span>
              <input type="file" id="interpretacionf" name="interpretacionf" accept="application/pdf">
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
            </div>
          </div>
          `;
        }
      }
      
    } catch (err) {
      
    }

    

  };
</script>