<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 01:57:40 GMT -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
  <meta name="author" content="ThemeSelect">
  <title>Login</title>
  <link rel="apple-touch-icon" href="<?=base_url('template/config/icon.png')?>">
  <link rel="shortcut icon" type="image/x-icon" href="<?=base_url('template/config/icon.png')?>">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/vendors/vendors.min.css')?>">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/css/themes/horizontal-menu-template/materialize.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/css/themes/horizontal-menu-template/style.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/css/layouts/style-horizontal.min.css')?>">
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/css/pages/login.css')?>">
  <!-- END: Page Level CSS-->
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="<?=base_url('template/app-assets/css/custom/custom.css')?>">
  <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="horizontal-layout page-header-light horizontal-menu preload-transitions 1-column login-bg   blank-page blank-page" data-open="click" data-menu="horizontal-menu" data-col="1-column">
  <div class="row">
    <div class="col s12">
      <div class="container">
        <div id="login-page" class="row">          
          <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
            
            <form class="login-form" onsubmit="login(event,'<?=base_url('Home')?>')" method="post"> 
              <div class="row">
                <div class="input-field col s12">
                  <h5 class="ml-4">Ingreso</h5>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">person_outline</i>
                  <input id="username" type="text">
                  <label for="username" class="center-align">Nombre de usuario</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">lock_outline</i>
                  <input id="password" type="password">
                  <label for="password">Contraseña</label>
                </div>
              </div>
              <!-- <div class="row">
                <div class="col s12 m12 l12 ml-2 mt-1">
                  <p>
                    <label>
                      <input type="checkbox" />
                      <span>Remember Me</span>
                    </label>
                  </p>
                </div>
              </div> -->
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">
                    Ingresar
                  </button>                
                </div>
              </div>
              <!-- <div class="row">                
                <div class="input-field col s6 m6 l6">
                  <p class="margin right-align medium-small"><a href="user-forgot-password.html">Olvidaste tu contraseña ?</a></p>
                </div>
              </div> -->
            </form>
          </div>
        </div>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>

  <!-- BEGIN VENDOR JS-->
  <script src="<?=base_url('template/app-assets/js/vendors.min.js')?>"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN THEME  JS-->
  <script src="<?=base_url('template/app-assets/js/plugins.min.js')?>"></script>
  <script src="<?=base_url('template/app-assets/js/search.min.js')?>"></script>
  <script src="<?=base_url('template/app-assets/js/custom/custom-script.min.js')?>"></script>
  <script src="<?= base_url('template/peticiones/Login.js'); ?>"></script>
  <script src="<?= base_url('template/app-assets/js/scripts/extra-components-sweetalert.min.js'); ?>"></script>
  <script src="<?= base_url('template/app-assets/vendors/sweetalert/sweetalert.min.js'); ?>"></script>
  <!-- END THEME  JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->
</body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 01:57:40 GMT -->

</html>