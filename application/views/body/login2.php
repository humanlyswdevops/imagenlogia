<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 01:57:40 GMT -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
  <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
  <meta name="author" content="ThemeSelect">
  <title>Login</title>
  <link rel="apple-touch-icon" href="<?= base_url('template/config/icon.png') ?>">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('template/config/icon.png') ?>">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/vendors.min.css') ?>">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/themes/horizontal-menu-template/materialize.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/themes/horizontal-menu-template/style.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/layouts/style-horizontal.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/pages/login.css') ?>">
  <!-- END: Page Level CSS-->
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/custom/custom.css') ?>">
  <!-- END: Custom CSS-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.js" integrity="sha512-E378bwaeZf1nwXeJGIwTB58A5gPt5jFU3u6aTGja4ZdRFJeo/N/REKnBgNZOZlH6JdnOPO98vg2AnSGaNfCMFQ==" crossorigin="anonymous"></script>
  <style>
    html,
    body {
      background-color: #F6F4F2;
      color: #252423;
    }

    /* body {
      display: flex;
      justify-content: center;
      align-items: center;
      position: absolute;
      width: 100%;
      height: 100vh;
    } */

    .animation-wrapper {
      width: 80%;
      padding-bottom: 40%;
    }

    .stagger-visualizer {
      position: absolute;
      width: 1100px;
      height: 550px;
      transform-origin: left top;
    }

    .stagger-visualizer .dots-wrapper {
      transform: translateZ(0);
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      align-items: center;
    }

    .stagger-visualizer .dot {
      position: relative;
      z-index: 1;
      width: 23px;
      height: 23px;
      margin: 16px;
      background-color: currentColor;
      border-radius: 50%;
    }

    @media (min-width: 740px) {
      .stagger-visualizer .dot {
        background-color: transparent;
        background-image: linear-gradient(180deg, #FFFFFF 8%, #D3CDC6 100%);
      }
    }

    .stagger-visualizer .cursor {
      position: absolute;
      top: 0px;
      left: 0px;
      width: 37px;
      height: 37px;
      margin: 9px;
      background-color: currentColor;
      border-radius: 50%;
    }

    .animation-wrapper {
    margin-top: -479px;
    margin-left: 109px;
      }

      div#divLogin {
          z-index: 2;
          background: rgba(255,255,255,.5);
      }
  </style>
</head>
<!-- END: Head-->

<body class="horizontal-layout page-header-light horizontal-menu preload-transitions 1-column login-bg   blank-page blank-page" data-open="click" data-menu="horizontal-menu" data-col="1-column">
<div class="row">
    <div class="col s12">
      <div class="container">
        <div id="login-page" class="row">          
          <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
            
            <form class="login-form" onsubmit="login(event,'<?=base_url('Home')?>')" method="post"> 
              <div class="row">
                <div class="input-field col s12">
                  <h5 class="ml-4">Ingreso</h5>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">person_outline</i>
                  <input id="username" type="text">
                  <label for="username" class="center-align">Nombre de usuario</label>
                </div>
              </div>
              <div class="row margin">
                <div class="input-field col s12">
                  <i class="material-icons prefix pt-2">lock_outline</i>
                  <input id="password" type="password">
                  <label for="password">Contraseña</label>
                </div>
              </div>
              <!-- <div class="row">
                <div class="col s12 m12 l12 ml-2 mt-1">
                  <p>
                    <label>
                      <input type="checkbox" />
                      <span>Remember Me</span>
                    </label>
                  </p>
                </div>
              </div> -->
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">
                    Ingresar
                  </button>                
                </div>
              </div>
              <!-- <div class="row">                
                <div class="input-field col s6 m6 l6">
                  <p class="margin right-align medium-small"><a href="user-forgot-password.html">Olvidaste tu contraseña ?</a></p>
                </div>
              </div> -->
            </form>
          </div>
        </div>
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
  <div class="animation-wrapper">
    <div class="stagger-visualizer">
      <div class="cursor color-red"></div>
      <div class="dots-wrapper"></div>
    </div>
  </div>

  <!-- BEGIN VENDOR JS-->
  <script src="<?= base_url('template/app-assets/js/vendors.min.js') ?>"></script>
  <!-- BEGIN VENDOR JS-->
  <!-- BEGIN PAGE VENDOR JS-->
  <!-- END PAGE VENDOR JS-->
  <!-- BEGIN THEME  JS-->
  <script src="<?= base_url('template/app-assets/js/plugins.min.js') ?>"></script>
  <script src="<?= base_url('template/app-assets/js/search.min.js') ?>"></script>
  <script src="<?= base_url('template/app-assets/js/custom/custom-script.min.js') ?>"></script>
  <script src="<?= base_url('template/peticiones/Login.js'); ?>"></script>
  <script src="<?= base_url('template/app-assets/js/scripts/extra-components-sweetalert.min.js'); ?>"></script>
  <script src="<?= base_url('template/app-assets/vendors/sweetalert/sweetalert.min.js'); ?>"></script>
  <!-- END THEME  JS-->
  <!-- BEGIN PAGE LEVEL JS-->
  <!-- END PAGE LEVEL JS-->
  <script>
    function fitElementToParent(el, padding) {
      var timeout = null;

      function resize() {
        if (timeout) clearTimeout(timeout);
        anime.set(el, {
          scale: 1
        });
        var pad = padding || 0;
        var parentEl = el.parentNode;
        var elOffsetWidth = el.offsetWidth - pad;
        var parentOffsetWidth = parentEl.offsetWidth;
        var ratio = parentOffsetWidth / elOffsetWidth;
        timeout = setTimeout(anime.set(el, {
          scale: ratio
        }), 10);
      }
      resize();
      window.addEventListener('resize', resize);
    }

    var advancedStaggeringAnimation = (function() {

      var staggerVisualizerEl = document.querySelector('.stagger-visualizer');
      var dotsWrapperEl = staggerVisualizerEl.querySelector('.dots-wrapper');
      var dotsFragment = document.createDocumentFragment();
      var grid = [20, 10];
      var cell = 55;
      var numberOfElements = grid[0] * grid[1];
      var animation;
      var paused = true;

      fitElementToParent(staggerVisualizerEl, 0);

      for (var i = 0; i < numberOfElements; i++) {
        var dotEl = document.createElement('div');
        dotEl.classList.add('dot');
        dotsFragment.appendChild(dotEl);
      }

      dotsWrapperEl.appendChild(dotsFragment);

      var index = anime.random(0, numberOfElements - 1);
      var nextIndex = 0;

      anime.set('.stagger-visualizer .cursor', {
        translateX: anime.stagger(-cell, {
          grid: grid,
          from: index,
          axis: 'x'
        }),
        translateY: anime.stagger(-cell, {
          grid: grid,
          from: index,
          axis: 'y'
        }),
        translateZ: 0,
        scale: 1.5,
      });

      function play() {

        paused = false;
        if (animation) animation.pause();

        nextIndex = anime.random(0, numberOfElements - 1);

        animation = anime.timeline({
            easing: 'easeInOutQuad',
            complete: play
          })
          .add({
            targets: '.stagger-visualizer .cursor',
            keyframes: [{
                scale: .75,
                duration: 120
              },
              {
                scale: 2.5,
                duration: 220
              },
              {
                scale: 1.5,
                duration: 450
              },
            ],
            duration: 300
          })
          .add({
            targets: '.stagger-visualizer .dot',
            keyframes: [{
              translateX: anime.stagger('-2px', {
                grid: grid,
                from: index,
                axis: 'x'
              }),
              translateY: anime.stagger('-2px', {
                grid: grid,
                from: index,
                axis: 'y'
              }),
              duration: 100
            }, {
              translateX: anime.stagger('4px', {
                grid: grid,
                from: index,
                axis: 'x'
              }),
              translateY: anime.stagger('4px', {
                grid: grid,
                from: index,
                axis: 'y'
              }),
              scale: anime.stagger([2.6, 1], {
                grid: grid,
                from: index
              }),
              duration: 225
            }, {
              translateX: 0,
              translateY: 0,
              scale: 1,
              duration: 1200,
            }],
            delay: anime.stagger(80, {
              grid: grid,
              from: index
            })
          }, 30)
          .add({
            targets: '.stagger-visualizer .cursor',
            translateX: {
              value: anime.stagger(-cell, {
                grid: grid,
                from: nextIndex,
                axis: 'x'
              })
            },
            translateY: {
              value: anime.stagger(-cell, {
                grid: grid,
                from: nextIndex,
                axis: 'y'
              })
            },
            scale: 1.5,
            easing: 'cubicBezier(.075, .2, .165, 1)'
          }, '-=800')

        index = nextIndex;

      }

      play();

    })();
  </script>
</body>

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/user-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 01:57:40 GMT -->

</html>