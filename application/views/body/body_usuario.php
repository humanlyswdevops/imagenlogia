<?php
$insert = '';
if (in_array('INSERTAR', $modulo, true)) {
  $insert = 'INSERTAR';
}
?>
<br><br>
<div id="main ">
  <div class="row animate__animated animate__fadeInUpBig">
    <div class="col s12">

      <div class="row">
        <div class="col s12">
          <ul class="tabs">
            <li class="tab col m3"><a class="iconos active" href="#test1">Creación de Personal</a></li>
            <li class="tab col m3"><a class="iconos " href="#test2">Creación de acceso</a></li>
          </ul>
        </div>

        <form id="frm_usuarios" method="post">
          <div id="test1" class="col s12">
            <div class="card" style="height: 400px">
              <div class="card-content min-form ">
                <div class="input-field col s6">
                  <input id="nombre" name="nombre" type="text" class="validate">
                  <label for="nombre">Nombre</label>
                </div>
                <div class="input-field col s6">
                  <input id="apellido_p" name="apellido_p" type="text" class="validate">
                  <label for="apellido_p">Apellido paterno</label>
                </div>
                <div class="input-field col s6">
                  <input id="apellido_m" name="apellido_m" type="text" class="validate">
                  <label for="apellido_m">Apellido materno</label>
                </div>
                <div class="input-field col s6">
                  <input id="id_empleado" name="id_empleado" type="text" class="validate">
                  <label for="id_empleado">Id empleado</label>
                </div>
                <div class="input-field col s6">
                  <select name="udn" id="udn">
                    <?php
                    $seleted = 'seleted';
                    foreach ($lista_udn as $key => $udn) {
                      echo "<option value=\"$udn->id\" $seleted>$udn->nombre</option>";
                      $seleted = '';
                    }
                    ?>
                  </select>
                  <label for="udn">Sucursal</label>
                </div>

                <!-- <div class="input-field col s6">

                  <div class="input-field col s8">
                    <input id="cedula" name="cedula" type="text" class="validate">
                  <label for="cedula">No. cedula</label> 
                    <div class="col s12">

                      <ul class="collection" id="lista_cedulas">
                      </ul>
                    </div>
                  </div>
                  <div class="input-field col s4">
                    <button class="btn primario" onclick="nuevaCedula(event)">
                      <p>Cedula </p>
                    </button>
                  </div>

                </div> -->



              </div>
            </div>
          </div>

          <div id="test2" class="col s12">
            <div class="card" style="height: 400px">
              <div class="card-content min-form">

                <div class="input-field col s6">
                  <i class="material-icons prefix">face</i>
                  <input id="usser" name="usser" type="text" class="validate">
                  <label for="usser">Usuario</label>
                </div>

                <div class="input-field col s6">
                  <i class="material-icons prefix">vpn_key</i>
                  <input id="pass" name="pass" type="text" class="validate">
                  <label for="pass">Contraseña</label>
                </div>

                <div class="input-field col s6">
                  <select name="privilegio" id="privilegio">
                    <option selected disabled>Seleccionar privilegio</option>
                    <?php
                    foreach ($lista_privilegios as $key => $privilegio) {
                      echo "<option value=\"$privilegio->id\" >$privilegio->nombre</option>";
                    }
                    ?>
                  </select>
                  <label for="privilegio">Privilegio</label>
                </div>

                <?php
                if ($insert == 'INSERTAR') {
                ?>
                  <div class="input-field col s8">
                    <button id="boton" type="submit" class="btn primario">
                      Guardar
                    </button>
                  </div>
                <?php
                }
                ?>
              </div>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</div>