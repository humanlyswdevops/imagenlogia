<?php
$modulo_paciente = false;
foreach ($accesos as $key => $acceso) {
  if ($acceso->modulo == 'Pacientes' && $acceso->crud == 'INSERTAR') {
    $modulo_paciente = true;
  }
}
?>
<br><br>
<div id="main">
  <div class="container card row animate__animated animate__fadeInUpBig">
    <br>
    <input type="hidden" id="peticion" />

    <div class="row center">
      <div class="input-field col s6">
        <span class="material-icons prefix" onclick="borra_nim()" style="cursor: pointer;">qr_code_scanner</span>
        <input id="sass" type="text" name="sass" class="validate">
        <label for="sass">Nim de sass</label>
      </div>
      <div class="col s6">
        <br>
        <button class="btn secundario" onclick="busca_sass()" id="btn_busca">
          Buscar
        </button>
        <!-- <button class="btn primario" onclick="guardarToma()">
          hacer petición
        </button> -->

      </div>
    </div>
    <div class="row">

      <div id="panel_tarjeta" class="section">
        <div id="alerta">
        </div>
        <div id="cards">
        </div>


      </div>


      <!-- ***************************************************************************  -->
      <!-- quitar la clase "d-none" para clínicas que no suben las dicoms e automatico  -->
      <!-- ***************************************************************************  -->
      <div class="col s12 d-none">
        <div class="card-content md-form">
          <form action="Toma_Muestra/Save" id="form_muestras" method="post" enctype="multipart/form-data">
            <!-- este contenedor se ocupa para ocultar cuando ya se selecciono un paciente o se creo (despues de esa accion se oculta) -->
            <div id="panel_cliente">
              <div class="input-field col s9" id="panel_paciente">
                <h6 class="card-title">Seleccionar paciente</h6>
                <div class="input-field">
                  <select class="select2 browser-default" class="paciente" id="paciente">
                    <option value="" disabled selected>Seleccionar</option>
                    <?php
                    foreach ($lista_pacietes as $key => $paciente) {
                      echo "<option value=\"$paciente->id\">$paciente->nombre</option>";
                    }
                    ?>
                  </select>
                </div>
              </div>
              <?php
              if ($modulo_paciente) {
              ?>
                <div class="input-field col s3">
                  <a class="waves-effect waves-light btn primario modal-trigger " href="#Modal_cliente">
                    Nuevo Paciente <i class="material-icons ">group_add</i>
                  </a>
                </div>
              <?php } ?>
            </div>
            <input type="hidden" id="paciente_id" name="paciente_id" />

            <div class="input-field col s9">
              <h6 class="card-title">Seleccionar estudio</h6>
              <div class="input-field">
                <select class="select2 browser-default" id="estudio" class="estudio" disabled>
                  <option value="" disabled selected>Seleccionar</option>
                  <?php
                  foreach ($lista_estudios as $key => $estudio) {
                    echo "<option value=\"$estudio->id\">$estudio->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>

            <div class="panel col s12">
              <div class="col s1"></div>
              <div class="col s10 oculta" id="template">
                <!-- Aqui se va ir montando los estudios -->
                <div id="panel">
                  <div class="titulos">
                    <h6>
                      <span class="material-icons">face</span> Estudios del paciente : <samp id="paciente_text"></samp>
                    </h6>
                  </div>
                  <br><br>
                </div>
                <br><br>
                <div>
                  <div class="row">
                    <div class="input-field col s12">
                      <span class="material-icons prefix">fiber_pin</span>
                      <input id="icon_prefix" type="text" id="nim_sass" name="nim_sass" class="validate">
                      <label for="icon_prefix">Nim del sass</label>
                    </div>
                  </div>


                  <br /><br />
                  
                  


                  <button type="submit" class="btn secundario" id="btn_save" disabled>
                    Guardar Tomas
                  </button>
                  <br /><br /><br />
                </div>
              </div>
              <div class="col s1"></div>
            </div>

          </form>
        </div>
      </div>
    </div>

    <!-- aquitar todo esto para los que no se suben chido  -->
    <div class="col s12">
      <div id="notificacion">
      </div>

      <div class="center" id="btn_asignar">
        
        <p class="left" style="margin-left: 28px;">
          <label>
            <input type="checkbox" id="is_asignar" name="is_asignar" />
            <span>Este estudio no lleva interpretación</span>
          </label>
        </p>

        <div style="overflow: auto;"></div>

        
        <div class="row">
          <div class="row ">
            <div class="input-field col s6 right">
              <textarea id="datos_clinicos" name="datos_clinicos"  class="materialize-textarea"></textarea>
              <label for="textarea1">Datos clínicos del paciente</label>
            </div>
          </div>
        </div>

        <button style="margin-right: 259px" class="btn btn-small center primario mb-3" onclick="guardarToma()">
          Asignar al medico <span class="material-icons">send</span>
        </button>

      </div>

    </div>

    <br>
    <br>
  </div>
  <br><br><br><br><br>

  <!--Modal para crear un paciente-->
  <div id="Modal_cliente" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Registro de paciente</h4>
      <div class="row">
        <form class="col s12" id="form_paciente" onsubmit="Nuevo_paciente(event)">

          <div class="input-field col s6">
            <input id="nombre" name="nombre" type="text" class="validate">
            <label for="nombre">Nombre</label>
          </div>

          <div class="input-field col s6">
            <input id="apellido_p" name="apellido_p" type="text" class="validate">
            <label for="apellido_p">Apellido paterno</label>
          </div>

          <div class="input-field col s6">
            <input id="apellido_m" name="apellido_m" type="text" class="validate">
            <label for="apellido_m">Apellido materno</label>
          </div>

          <div class="input-field col s6">
            <input id="curp" name="curp" type="text" class="validate">
            <label for="curp">CURP</label>
          </div>

          <div class="input-field col s6">
            <input id="id_laboratorio" name="id_laboratorio" type="text" class="validate">
            <label for="id_laboratorio">ID laboratorio</label>
          </div>

          <div class="col s12">
            <button class="waves-effect waves-light  btn primario" type="submit" id="boton">
              Crear nuevo paciente
            </button>
          </div>

        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>


  <!-- Modal para registrar una modal -->
  <div id="Modal_dicom" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Subir imagen Dicom</h4>
      <div class="row">
        <div id="panel_upload">
        </div>

        <form id="form_save_dicom" onsubmit="Save_dicom(event,'<?= base_url() ?>')" enctype="multipart/form-data" class="col s12">

          <div class="file-field input-field">
            <div class="btn primario">
              <span>Seleccionar</span>
              <input type="file" name="dicom" id="dicom" />
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="Cargar imagenes dicom">
            </div>
            <input type="hidden" name="id_estudio" id="id_estudio" />
          </div>
          <div class="input-field col s12">
            <input id="url" type="text" class="validate">
            <label for="url">Escribir una url</label>
          </div>

          <div class="col s12">
            <button class="waves-effect waves-light  btn secundario" id="btn_dicom" type="submit">
              Guardar imagen Dicom
            </button>
          </div>

        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>


  <!-- modal para agregar todos los medicos a un estudio -->
  <div id="addMedico" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Asignar médicos</h4>
      <div class="row">
        <form id="frm-radiologo" method="POST">
          <input type="hidden" name="id_toma_muestra" id="id_toma_muestra">
          <div class="col s12">
            <div class="input-field col s9" id="panel_paciente">
              <h6 class="card-title">Seleccionar paciente </h6>
              <div class="input-field">
                <select class="medico" name="medico_select" id="medico_select">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php
                  foreach ($medicos as $key => $medico) {
                    echo "<option value=\"$medico->id\">$medico->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col s12">
            <!-- lista de los medicos que seleccione -->
            <ul class="collection" id="lista_radiologos">
            </ul>
          </div>
          <button class="waves-effect waves-light  btn primario" type="submit">
            Asignar médicos a esta toma
          </button>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>