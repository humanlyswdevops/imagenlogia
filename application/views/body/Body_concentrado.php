<div id="main">
  <div class="row ">
    <div class="col s12 " style=" position: relative; ">
      <div class="col s12 " style="margin: auto;">
        <div class="card">
          <div class="card-content">
            <div class=" card-title">Seleccionar rango de fechas</div>
            <div class="row">
              <div class="col s6">
                <label for="date_uno">Inicio</label>
                <input type="text" class="datepicker" id="fecha_uno" name="fecha_uno" />
              </div>
              <div class="col s6">
                <label for="date_fin">fin</label>
                <input type="text" class="datepicker" id="fecha_fin" name="fecha_fin" />
              </div>
            </div>
            <div class="mt-1 mb-1">
              <button class="btn primario " onclick="handleGetConcentrado(<?= $_SESSION['usuario']->id_personal ?>)">
                Quiero ver mi concentrado
              </button>
            </div>
          </div>
        </div>
      </div>

      <div class="col s12 m12 l12">
        <div id="button-trigger" class="card card card-default scrollspy">
          <div class="card-content">
            <h4 class="card-title">Tabla de concentrados</h4>
            <div class="row">
              <div class="col s12">
                <table id="tabla_Consentrados" class="display">
                  <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Estudio</th>
                      <th>Nim</th>
                      <th>Placas subidas</th>
                      <th>Placas por estudio</th>
                      <th>Honorario</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>

              <div class="col s12 m12 barra mt-1">
                <h5 style="color:#ffffff">
                  Monto total: <span id="total"></span>
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>