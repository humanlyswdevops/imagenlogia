<!-- BEGIN: Page Main-->
<div id="main">
  <div class="row animate__animated animate__fadeInUpBig">
    <!--Tabla de los permisos-->
    <div class="row s12">
      <div class="col s12">
        <div class="card">
          <div class="card-content">
            <h4 class="card-title">Listado de Estudios</h4>  
            
            <div class="row">
              <div class="col s5">
                <label for="date_uno">Buscar por fecha</label>
                <input type="text" class="datepicker" id="fecha" name="fecha" />
                <button class="btn primario" onclick="handle_valida_imagenes(1)">
                  Filtra búsqueda                  
                </button>
              </div>

              <div class="col s5 right" style="margin-top: 20px;">                                
                <button class="btn primario" onclick="handle_valida_imagenes(0)">
                  Buscar todo
                </button>
              </div>
            </div>      

            <div class="row">
              <div class="col s12">                
                <table id="tabla_valida" class="display">
                  <thead>
                    <tr>
                      <th>Fecha</th>                      
                      <th>NIM SASS</th>
                      <th>Estudio</th>
                      <th>Código Estudio</th>
                      <th>Placas en servidor</th>
                      <th>Placas por estudio</th>
                      <th>Estatus</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    //var_dump($tabla);
                    foreach ($tabla as $key => $item) {

                      /*echo "<tr>
                            <td>$item->radiologo</td>
                            <td>$item->paciente</td>
                            <td>$item->sass</td>
                            <td>
                              <a class=\"waves-effect waves-light btn primario modal-trigger\" href=\"#addMedico\" onclick=\"Asigna_id_deToma($item->id_toma_muestras)\" \">
                                Cambiar médico 
                              </a>                              
                            </td>
                          </tr>
                          ";*/
                    }
                    ?>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>


  <!-- modal para agregar todos los medicos a un estudio -->
  <div id="addMedico" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Asignar médicos</h4>
      <hr>
      <div class="row">
        <form id="frm-radiologo" method="POST">
          <input type="hidden" name="id_toma_muestra" id="id_toma_muestra">
          <div class="col s12">
            <div class="input-field col s9" id="panel_paciente">
              <h6 class="card-title">Seleccionar paciente </h6>
              <div class="input-field">
                <select class="medico" name="medico_select" id="medico_select" required>
                  <option value="" disabled selected>Seleccionar</option>
                  <?php
                  foreach ($medicos as $key => $medico) {
                    echo "<option value=\"$medico->id\">$medico->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col s12">
            <!-- lista de los medicos que seleccione -->
            <ul class="collection" id="lista_radiologos">
            </ul>
          </div>
          <button class="waves-effect waves-light  btn primario" type="submit">
            Asignar médicos a esta toma
          </button>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>

  <!-- esta modal es para asignar todo a un radiologo -->
  <div  id="modal12" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Asignar todas la tomas </h4>
      <hr>
      <div class="row">
        <form id="frm_radiologo_asignatodo" method="POST">
          
          <div class="col s12">
            <div class="input-field col s9" id="panel_paciente">
              <h6 class="card-title">Seleccionar Medico </h6>
              <div class="input-field">
                <select class="medico" name="medico_select2" id="medico_select2" required>
                  <option value="" disabled selected>Seleccionar</option>
                  <?php
                  foreach ($medicos as $key => $medico) {
                    echo "<option value=\"$medico->id\">$medico->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col s12">
            <!-- lista de los medicos que seleccione -->
            <ul class="collection" id="lista_radiologos">
            </ul>
          </div>
          <button class="waves-effect waves-light  btn primario" type="submit">
            Asignar médicos
          </button>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>

  