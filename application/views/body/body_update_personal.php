<br><br>
<div id="main">
  <div class="container  row animate__animated animate__fadeInUpBig">
    <div class="col s12" id="div_view_personal">
      <div class="card">
        <div class="card-content">
          <h4 class="card-title center">Listado de usuarios</h4>
          <div class="row">
            <div class="col s12">
              <table id="page-length-option" class="display">
                <input type="hidden" name="id_usuario" id="id_usuario">
                <!-- <input type="hidden" name="id_empleado" id="id_empleado"> -->
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Privilegio</th>
                    <th>Udn</th>
                    <th>Id empleado</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  //[nombre,id_empleado,udn,privilegio,id_personal]
                  foreach ($table as $key => $tupla) {
                    echo "  <tr>
                              <td>$tupla->nombre</td>
                              <td>$tupla->privilegio</td>
                              <td>$tupla->udn</td>
                              <td>$tupla->id_empleado</td>    
                              <td>
                                <button class=\"btn primario\" onclick=\"editar_usuario($tupla->id_personal)\">
                                  Modificar
                                </button>                                
                              </td>      

                              <td>
                                <button class=\"btn primario\" onclick=\"id_firma_pesonal($tupla->id_personal)\">
                                  Subir firma
                                </button>                                
                              </td>       
                            </tr>
                            ";
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<br>


<!-- modal para agregar todos los medicos a un estudio -->
<div id="modal_edita_usser" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Edición de usuario</h4>
    <div class="row">
      <form id="frm_update_usuario" method="POST">
        <input type="hidden" id="id_personal" name="id_personal">
        <div class="input-field col s12 m6 l6">
          <input id="nombre" name="nombre" type="text" class="validate">
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6 l6">
          <input id="apellido_p" name="apellido_p" type="text" class="validate">
          <label for="apellido_p">Apellido paterno</label>
        </div>
        <div class="input-field col s12 m6 l6">
          <input id="apellido_m" name="apellido_m" type="text" class="validate">
          <label for="apellido_m">Apellido materno</label>
        </div>

        <div class="input-field col s12 m6 l6">
          <span>Id empleado</span>
          <input id="id_empleado" name="id_empleado" type="text" class="validate" disabled>
          <!--<label for="id_empleado">Id empleado</label>-->
        </div>

        <div class="input-field col s12 m6 l6">
          <select name="privilegio" id="privilegio">
            <option selected disabled>Seleccionar privilegio</option>
            <?php
            foreach ($lista_privilegios as $key => $privilegio) {
              echo "<option value=\"$privilegio->id\" >$privilegio->nombre</option>";
            }
            ?>
          </select>
          <label for="privilegio">Privilegio</label>
        </div>

        <div class="input-field col s12 m6 l6">
          <input id="cobro_interpretacion" name="cobro_interpretacion" type="text" class="validate">
          <label for="cobro_interpretacion">Pago x interpretación</label>
        </div>

        <div class="input-field col s12 m6 l6">
          <input id="cobro_estudio" name="cobro_estudio" type="text" class="validate">
          <label for="cobro_estudio">Pago x estudio</label>
        </div>


        <div class="col s12">
          <div class="input-field col s12 m6 l6">
            <input id="text_cedula" name="text_cedula" type="text" class="validate">
            <label for="text_cedula">Numero de certificado</label>
          </div>

          <button class="btn primario " onclick="nueva_cedula(event)">
            Añadir cedula
          </button>

        </div>

        <div class="col s12">
          <ul class="collection" id="lista_cedulas">
            <li class="collection-item center iconos">
              Numero de cedulas:
            </li>
          </ul>
        </div>



        <div class="col s12">
          <button class="waves-effect waves-light  btn primario left" type="submit">
            Finalizar edición de usuario
          </button>
        </div>


      </form>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn primario left" onclick="detalle_personal(event,'<?= base_url('Perfil/detalle') ?>')">
      Ver Documentación
    </button>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario right">Cerrar</a>
  </div>
</div>


<div id="modal_sube_firma" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Subir firma</h4>
    <div class="row">
      <br><br>
      <form id="firma_frm" method="post" enctype="multipart/form-data">
        <div class="file-field input-field">
          <div class="btn btn-small primario">
            <span>Seleccionar firma</span>
            <input type="file" name="firma" id="firma" accept="image/*" />
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
        <input type="hidden" id="id_user_firma">
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn primario left" onclick="detalle_personal(event,'<?= base_url('Perfil/detalle') ?>')">
      Ver Documentación
    </button>
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario right">Cerrar</a>
  </div>
</div>