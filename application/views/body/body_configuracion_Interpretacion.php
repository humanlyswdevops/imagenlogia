<br><br>
<div id="main">
  <div class="container  row animate__animated animate__fadeInUpBig">
    <div class="col s12" id="div_view_personal">
      <div class="card">
        <div class="card-content">
          <h4 class="card-title center">Lista de cedulas</h4>
          <br><br>
          <div class="row">
            <div class="col s12">
              <!--
              <div class="chip ">
                item1
                <i class="close material-icons">close</i>
              </div>
              -->
              <input type="hidden" id="id_personal" name="id_personal" value="<?= $_SESSION['usuario']->id_personal ?>">
              <div class="col s12">
                <div class="input-field col s8 ">
                  <input id="text_cedula" name="text_cedula" type="text" class="validate">
                  <label for="text_cedula">Numero de certificado</label>
                </div>

                <div class="input-field col s4 ">
                  <button class="btn primario " onclick="nueva_cedula(event)">
                    Añadir cedula
                  </button>
                </div>



              </div>

              <div class="col s12">
                <ul class="collection" id="lista_cedulas">
                  <li class="collection-item center iconos">
                    Numero de cedulas:
                  </li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col s12" id="div_view_personal">
      <div class="card">
        <div class="card-content">
          <h4 class="card-title center">Firma que se mostrara en la interpretación</h4>
          <br>
          <div class="row">
            <div class="col s12">



              <div class="input-field col s8 ">
                <div class="card-alert  barra">
                  <div class="card-content center white-text ">
                    <p>visualización de Firma</p>
                  </div>
                </div>
                <div id="firmaver"></div>
              </div>

              <div class="input-field col s4 ">

                <div class="card-alert  barra">
                  <div class="card-content center white-text ">
                    <p>Actualizar firma</p>
                  </div>
                </div>

                <div class="center">
                  <br><br>
                  <button class="btn primario " onclick="id_firma_pesonal(<?=$_SESSION['usuario']->id_personal?>)">
                    Actualizar
                  </button>
                </div>

                
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
<br>
<br>

<div id="modal_sube_firma" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Subir firma</h4>
    <div class="row">
      <br><br>
      <form id="firma_frm" method="post" enctype="multipart/form-data">
        <div class="file-field input-field">
          <div class="btn btn-small primario">
            <span>Seleccionar firma</span>
            <input type="file" name="firma" id="firma" accept="image/*" />
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text">
          </div>
        </div>
        <input type="hidden" id="id_user_firma" >
      </form>
    </div>
  </div>
  <div class="modal-footer">    
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario right">Cerrar</a>
  </div>
</div>

<div id="modalCrea_tabla" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Crear tabla</h4>
    <hr>
    <div class="row">
      <form id="frm-radiologo" method="POST">
        <input type="hidden" name="id_toma_muestra" id="id_toma_muestra">
        <div class="col s12">
          <div class="input-field col s9" id="panel_paciente">
            <h6 class="card-title">Seleccionar paciente </h6>
            <div class="input-field">
              <select class="medico" name="medico_select" id="medico_select" required>
                <option value="" disabled selected>Seleccionar</option>
               
              </select>
            </div>
          </div>
        </div>
        <div class="col s12">
          <!-- lista de los medicos que seleccione -->
          <ul class="collection" id="lista_radiologos">
          </ul>
        </div>
        <button class="waves-effect waves-light  btn primario" type="submit">
          Asignar médicos a esta toma
        </button>
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
  </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  $(document).ready(function() {
    get_Cedulas(<?= $_SESSION['usuario']->id_personal ?>);

    $.ajax({
      url: '../Configuracion/Get_Firma',
      type: 'POST',
      dataType: 'json',
      success: function(json) {
        let {
          firma
        } = json;
        let rutaa = '<?= base_url('uploads/firma/') ?>';
        let foto_imagen = `
            <p class="ql-align-center">
              <img src='${rutaa+firma.firma}' width="120px" height="120px"  />
            </p>
            `;
        $('#firmaver').html(foto_imagen)

      },
      error: function(xhr, status) {
        alert('Disculpe, existió un problema');
      }
    });
  });
</script>