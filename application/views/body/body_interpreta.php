<br><br>
<div id="main">
  <div class="container card row">
    <div class="">
      <!-- aqui enpiieza el acordeon  -->
      <div class="col s12">
        <ul class="collapsible collapsible-accordion">
          <li>
            <div class="collapsible-header light-blue light-blue-text text-lighten-5">
              <i class="material-icons">local_library</i> Ver información de la imagen dicom
            </div>
            <div class="collapsible-body light-blue lighten-5">
              <div class="col s">
                <div class="container card">
                  <div class="card-content">
                    <h4 class="card-title">imformacion de la dicom</h4>
                    <div class="row">
                      <div class="col s12">
                        <ul id="lista" class="collection with-header">
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <!-- aqui termina el acordeon -->
      <div class="container">
        <div id="loadProgress">Carga de la imagen dicom:</div>
        <!-- Borrar Pruebas  -->
        <div class="row">
          <form id="form" class="form-horizontal">
            <div class="form-group">
              <label class="control-label col-sm-1" for="wadoURL">URL</label>
              <div class="col-sm-8">
                <input class="form-control" type="text" id="wadoURL" placeholder="Enter WADO URL" value="./../1581454359abel ibarra cruz">
              </div>
              <div class="col-sm-3">
                <button class="form-control" type="button" id="downloadAndView" class="btn btn-primary">
                  Ver dicom
                </button>
              </div>
            </div>
          </form>
        </div>
        <!--/ Borrar Pruebas  -->

        <!-- habilitar la escala de grises  -->
        <p>
          <label>
            <input type="checkbox" id="toggleVOILUT" />
            <span>Escala de grises</span>
          </label>
        </p>
        <!-- no quitar !!!! -->
        <input type="checkbox" id="toggleModalityLUT" />
        <br>
        <div class="row">
          <div class="col s12 m12 l6 xl6">
            <!-- aqui va el div donde se va a pintar la dicom  -->
            <div style="width:100%;height:650px;position:relative;color: white;display:inline-block;border-style:solid;border-color:black;" oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
              <div id="dicomImage" style="width:100%;height:600px;top:0px;left:0px; position:absolute">
              </div>
            </div>
            <!-- aqui va el listado de las dicoms  -->
            <div class="row">
              <div onclick="Ver_dicom('dicom/1581454359abel ibarra cruz')" class="col s4 m2 l2 bg-primario dicom-item"><span class="material-icons cast">cast</span> 1</div>
              <div onclick="Ver_dicom('dicom/alicia lopez calete')" class="col s4 m2 l2 bg-primario dicom-item"><span class="material-icons cast">cast</span> 2</div>
              <div onclick="Ver_dicom('dicom/emilio martinez reyes')" class="col s4 m2 l2 bg-primario dicom-item"><span class="material-icons cast">cast</span> 3</div>
            </div>
          </div>

          <!-- aqui va el editor de texto  -->
          <div class="col s12 m12 l6 xl6">
            <div id="editor">
            </div>
            <br><br>
            <input type="hidden" value="<?= $toma ?>" id="toma">
            <button class="btn primario right">
              Terminar interpretación
            </button>
            <br><br>
            <!-- espacio de pruebas   -->
            <!-- <div class="col s12">
              <ul class="tabs" id="tabs">

              </ul>
              <div id="panel_editores">
              </div>
            </div> -->




          </div>

        </div>
      </div>
    </div>
  </div>
</div>


<!-- modal cargando  -->

<div id="load" class="modal">
  <div class="modal-content">
    <h6 class="center" id="text_load"></h6>
    <div class="center">
      <img src="<?= base_url('template/img/barra2.gif') ?>" alt="progreso">
    </div>
  </div>
</div>