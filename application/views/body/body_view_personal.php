<br><br>
<div id="main">
  <div class="container  row animate__animated animate__fadeInUpBig">
    <div class="col s12">
      <div class="card">
        <div class="card-content">
          <h4 class="card-title">Listado de usuarios</h4>
          <div class="row">
            <div class="col s12">
              <table id="page-length-option" class="display">
                <thead>
                  <tr>                    
                    <th>Privilegio</th>
                    <th>Personal</th>
                    <th>Nombre de usuario</th>
                    <th>Id laboratorio</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($table as $key => $tupla) {
                    echo "  <tr>
                              <td>$tupla->privilegio</td>
                              <td>$tupla->personal</td>
                              <td>$tupla->username</td>
                              <td>$tupla->id_empleado</td>                              
                            </tr>
                            ";
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>