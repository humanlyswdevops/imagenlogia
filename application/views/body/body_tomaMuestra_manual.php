<?php
$modulo_paciente = false;
foreach ($accesos as $key => $acceso) {
  if ($acceso->modulo == 'Pacientes' && $acceso->crud == 'INSERTAR') {
    $modulo_paciente = true;
  }
}
?>
<br><br>
<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>">
<div id="main">
  <div id="verjs"></div>
  <div class="container card row animate__animated animate__fadeInUpBig">
    <br>
    <input type="hidden" id="peticion" />
    <form id="formulario_toma_muestras">
      <input type="hidden" name="paciente" id="paciente">
      <input type="hidden" name="nim_sass" id="nim_sass">
      <div class="row center">
        <div class="input-field col s6">
          <span class="material-icons prefix">qr_code_scanner</span>
          <input id="sass" type="text" name="sass" class="validate">
          <label for="sass">Nim de sass</label>
        </div>
        <div class="col s6">
          <br>
          <button class="btn secundario" onclick="busca_sass()" id="btn_busca" type="button">
            Buscar
          </button>
          <!-- 
            <button class="btn primario" onclick="guardarToma()">
              hacer petición
            </button> 
          -->
        </div>
      </div>

      <div class="row">
        <div id="panel_tarjeta" class="section">
          <div id="alerta">
          </div>
        </div>
      </div>

      <div id="panel" class="col s12 mb-5">
      </div>
      <div class="col s12">
        <div class="center" id="btn_asignar">
          <button class="btn btn-small center primario" type="submit">
            Asignar al medico <span class="material-icons">send</span>
          </button>
          <br><br><br><br><br><br><br>
        </div>
      </div>
    </form>
  </div>

  <!--Modal para crear un paciente-->
  <div id="Modal_cliente" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Registro de paciente</h4>
      <div class="row">
        <form class="col s12" id="form_paciente" onsubmit="Nuevo_paciente(event)">

          <div class="input-field col s6">
            <input id="nombre" name="nombre" type="text" class="validate">
            <label for="nombre">Nombre</label>
          </div>

          <div class="input-field col s6">
            <input id="apellido_p" name="apellido_p" type="text" class="validate">
            <label for="apellido_p">Apellido paterno</label>
          </div>

          <div class="input-field col s6">
            <input id="apellido_m" name="apellido_m" type="text" class="validate">
            <label for="apellido_m">Apellido materno</label>
          </div>

          <div class="input-field col s6">
            <input id="curp" name="curp" type="text" class="validate">
            <label for="curp">CURP</label>
          </div>

          <div class="input-field col s6">
            <input id="id_laboratorio" name="id_laboratorio" type="text" class="validate">
            <label for="id_laboratorio">ID laboratorio</label>
          </div>

          <div class="col s12">
            <button class="waves-effect waves-light  btn primario" type="submit" id="boton">
              Crear nuevo paciente
            </button>
          </div>

        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>
  <!-- Modal para registrar un dicom -->
  <div id="Modal_dicom" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Subir imagen Dicom</h4>
      <div class="row">
        <div id="panel_upload">
        </div>
        <form action="cargar_archivo" class="col s12" method="POST" id="form_save_dicom" enctype="multipart/form-data">
          <input type="hidden" id="paciente_id2" name="paciente_id" />
          <input type="hidden" name="id_paciente" id="id_paciente">
          <input type="hidden" name="idsass" id="idsass">

          <div class="file-field input-field">
            <div class="btn primario">
              <span>Seleccionar</span>
              <input type="file" name="dicom" id="dicom" />
            </div>
            <div class="file-path-wrapper">
              <input class="file-path validate" type="text" placeholder="Cargar imagenes dicom">
            </div>
            <input type="hidden" name="id_estudio" id="id_estudio" />
          </div>
          <div class="input-field col s12">

          </div>

          <div class="col s12">
            <button class="waves-effect waves-light  btn secundario" id="btn_dicom" type="submit">
              Guardar imagen Dicom
            </button>
          </div>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>
  <!-- modal para agregar todos los medicos a un estudio -->
  <div id="addMedico" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Asignar médicos</h4>
      <div class="row">
        <form id="frm-radiologo" method="POST">
          <input type="hidden" name="id_toma_muestra" id="id_toma_muestra">
          <div class="col s12">
            <div class="input-field col s9" id="panel_paciente">
              <h6 class="card-title">Seleccionar paciente </h6>
              <div class="input-field">
                <select class="medico" name="medico_select" id="medico_select">
                  <option value="" disabled selected>Seleccionar</option>
                  <?php
                  foreach ($medicos as $key => $medico) {
                    echo "<option value=\"$medico->id\">$medico->nombre</option>";
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col s12">
            <!-- lista de los medicos que seleccione -->
            <ul class="collection" id="lista_radiologos">
            </ul>
          </div>
          <button class="waves-effect waves-light  btn primario" type="submit">
            Asignar médicos a esta toma
          </button>
        </form>
      </div>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn secundario">Cerrar</a>
    </div>
  </div>