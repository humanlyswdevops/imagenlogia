<br><br>
<div id="main">
  <div class="container card row">
    <!--Tabla de los permisos-->
    <div class="row s12 animate__animated animate__fadeInUpBig">
      <div class="col s12 ">

        <div class="card">
          <div class="card-content">
            <div class=" card-title">Seleccionar rango de fechas
            </div>
            <div class="row">
              <div class="col s6">
                <label for="date_uno">Fecha de inicio</label>
                <input type="text" class="datepicker" id="fecha_uno" name="fecha_uno">
              </div>
              <div class="col s6">
                <label for="date_fin">Fecha de fin</label>
                <input type="text" class="datepicker" id="fecha_fin" name="fecha_fin">
              </div>
            </div>

            <div class="mt-1 mb-1">
              <button class="btn primario " onclick="handleGet_pendientes(<?= $_SESSION['usuario']->id_personal; ?>)">
                Filtrar pendientes por fechas
              </button>
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-content">
            <h4 class="card-title">Listado de pendientes </h4>

            <input type="hidden" id="ruta_pendientes" value="<?= base_url('Interpretacion') ?>">
            <div class="row">
              <div class="col s12">
                <div id="cargando"></div>
                <table id="tabla_pendientes" class="display">
                  <thead>
                    <tr>
                      <th>Nim</th>
                      <th>Medico</th>
                      <th>Paciente</th>
                      <th>fecha</th>
                      <th>estatus</th>
                      <th>filial</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $ruta = base_url('Interpretacion');

                    $bandera = false;
                    if ($_SESSION['usuario']->privilegio == 'medico radiólogo') {
                      $bandera = true;
                    }
                    foreach ($tabla as $key => $tupla) {

                      $btn = '';
                      if ($bandera) {
                        $btn = "
                            <form action=\"$ruta\" method=\"POST\">
                              <input type=\"hidden\" name=\"id_toma_muestra\" id=\"id_toma_muestra$key\" value=\"$tupla->id_toma_muestra\" />

                              <button type=\"submit\" class=\"btn primario\">
                                <span class=\"material-icons\">verified</span>
                              </button>
                            </form>
                        ";
                      }
                      // if ($tupla->estatus='SIN COMPLETAR') {

                      // }
                      echo "<tr>
                              <td>$tupla->nim_sass</td>
                              <td>$tupla->medico</td>                              
                              <td>$tupla->paciente</td>                              
                              <td>$tupla->fecha</td>                              
                              <td>$tupla->estatus</td> 
                              <td>$tupla->udn</td> 
                              <td>
                                $btn
                              </td>
                            </tr>
                            ";
                    }



                    $ruta_continue = base_url('New_Study');
                    foreach ($Asignados_despues as $key => $pendiente) {
                      $btn = '';
                      if ($bandera) {
                        $btn = "
                            <form action=\"$ruta_continue\" method=\"POST\">
                              <input type=\"hidden\" name=\"id_toma_muestra\" id=\"id_toma_muestra$key\" value=\"$pendiente->id_toma_muestra\" />

                              <button type=\"submit\" class=\"btn warning\">
                                <span class=\"material-icons\">verified</span>
                              </button>
                            </form>
                        ";
                      }

                      echo "<tr>
                              <td>$pendiente->nim_sass</td>
                              <td>$pendiente->medico</td>                              
                              <td>$pendiente->paciente</td>                              
                              <td>$pendiente->created</td>                              
                              <td>$pendiente->estatus</td> 
                              <td>$pendiente->udn</td> 
                              <td>
                                $btn
                              </td>
                            </tr>
                            ";
                    }

                    $ruta_pospone = base_url('Propuesto');
                    foreach ($estudios_pospone as $key => $pospuesto) {
                      $btn = '';
                      if ($bandera) {
                        $btn = "
                            <form action=\"$ruta_pospone\" method=\"POST\">
                              <input type=\"hidden\" name=\"id_toma_muestra\" id=\"id_toma_muestra$key\" value=\"$pospuesto->id_toma_muestra\" />

                              <button type=\"submit\" class=\"btn warning\">
                                <span class=\"material-icons\">verified</span>
                              </button>
                            </form>
                        ";
                      }

                      echo "<tr>
                              <td>$pospuesto->nim_sass</td>
                              <td>$pospuesto->radiologo</td>                              
                              <td>$pospuesto->paciente</td>                              
                              <td>$pospuesto->created</td>                              
                              <td>$pospuesto->estatus</td> 
                              <td>$pospuesto->udn</td> 
                              <td>
                                $btn
                              </td>
                            </tr>
                            ";
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

  <div id="load" class="modal">
    <div class="modal-content">
      <h6 class="center" id="text_load"></h6>
      <div class="center">
        <img src="<?= base_url('template/img/barra2.gif') ?>" alt="progreso">
      </div>
    </div>
  </div>