<!-- BEGIN: Page Main-->
<div id="main">
  <div class="row ">
    <div class="col s12">
      <div class="container">
        <!-- Account settings -->
        <section class="tabs-vertical mt-1 section">
          <div class="row">
            <div class="col l4 s12">
              <!-- tabs  -->
              <div class="card-panel">
                <ul class="tabs">
                  <li class="tab">
                    <a href="#general">
                      <i class="material-icons">face</i>
                      <span>Perfil</span>
                    </a>
                  </li>
                  <li class="tab">
                    <a href="#info">
                      <i class="material-icons">error_outline</i>
                      <span>Información</span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col l8 s12">
              <!-- tabs content -->
              <div id="general">
                <form id="actualiza_perfil" method="post" enctype="multipart/form-data">
                  <div class="card-panel">
                    <div class="display-flex">
                      <div class="media">
                        <?php
                        $foto = $_SESSION['usuario']->foto;
                        if ($foto != '' || $foto != null) {
                          $foto = base_url("uploads/fotos_perfil/$foto");
                        } else {
                          $foto = base_url('template/app-assets/images/avatar/avatar-7.png');
                        }
                        ?>
                        <img src="<?= $foto ?>" id="ver_foto" class="border-radius-4" alt="profile image" height="64" width="64">
                      </div>
                      <div class="media-body">
                        <div class="file-field input-field">
                        </div>
                      </div>
                    </div>
                    <div class="divider mb-1 mt-1"></div>
                    <div class="row">
                      <div class="col s12">
                        <div class="input-field">
                          <label for="nombre">Nombres</label>
                          <input type="text" id="nombre" name="nombre" value="<?= $personal->nombre ?>" disabled>
                          <small class="errorTxt1"></small>
                        </div>
                      </div>
                      <div class="col s12">
                        <div class="input-field">
                          <label for="apellido_paterno">Apellido paterno</label>
                          <input id="apellido_paterno" name="apellido_paterno" value="<?= $personal->apellido_paterno ?>" type="text" data-error=".errorTxt2" disabled>
                          <small class="errorTxt2"></small>
                        </div>
                      </div>
                      <div class="col s12">
                        <div class="input-field">
                          <label for="apellido_materno">Apellido materno</label>
                          <input id="apellido_materno" name="apellido_materno" type="text" data-error=".errorTxt2" value="<?= $personal->apellido_materno ?>" disabled>
                          <small class="errorTxt2"></small>
                        </div>
                      </div>

                    </div>
                  </div>
                </form>
              </div>

              <div id="info">
                <div class="card-panel">
                  <div class="card-alert card barra">
                    <div class="card-content center white-text ">
                      <p>Documentos</p>
                    </div>
                  </div>

                  <div class="row" id="tarjeta">
                    <?php
                    if ($personal->cv != '') {
                      $ruta = base_url("uploads/documentos/cv/$personal->cv");
                    ?>
                      <div class="col s6 m4  ">
                        <div class="card ">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light primario" onclick="abrir_modal('<?= $ruta ?>')">
                              <i class="material-icons">remove_red_eye</i>
                            </a>
                          </div>
                          <div class="card-content">
                            <p>
                              CV
                            </p>
                          </div>
                        </div>
                      </div>
                    <?php
                    }
                    ?>
                    <?php
                    if ($personal->curp != '') {
                      $ruta = base_url("uploads/documentos/curps/$personal->curp");
                    ?>
                      <div class="col s6 m4  ">
                        <div class="card ">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light primario" onclick="abrir_modal('<?= $ruta ?>')">
                              <i class="material-icons">remove_red_eye</i>
                            </a>
                          </div>
                          <div class="card-content">
                            <p>
                              Curp
                            </p>
                          </div>
                        </div>
                      </div>
                    <?php
                    }
                    ?>
                    <?php
                    if ($personal->acta_nacimiento != '') {
                      $ruta = base_url("uploads/documentos/actas_nacimiento/$personal->acta_nacimiento");
                    ?>
                      <div class="col s6 m4  ">
                        <div class="card ">
                          <div class="card-image">
                            <a class="btn-floating halfway-fab waves-effect waves-light primario" onclick="abrir_modal('<?= $ruta ?>')">
                              <i class="material-icons">remove_red_eye</i>
                            </a>
                          </div>
                          <div class="card-content">
                            <p>
                              Acta de nacimiento
                            </p>
                          </div>
                        </div>
                      </div>
                    <?php
                    }
                    ?>

                  </div>


                  <div class="card-alert card barra">
                    <div class="card-content center white-text ">
                      <p>Cedulas</p>
                    </div>
                  </div>

                  <div class="row" id="cedulas_card">
                    <?php
                    foreach ($cedulas as $key => $cedula) {
                      $ruta = base_url("uploads/documentos/cedulas/$cedula->nombre");
                      echo "<div class=\"col s6 m4  \">
                                <div class=\"card \">
                                  <div class=\"card-image\">
                                    <a class=\"btn-floating halfway-fab waves-effect waves-light primario\" onclick=\"abrir_modal('$ruta')\">
                                      <i class=\"material-icons\">remove_red_eye</i>
                                    </a>
                                  </div>
                                  <div class=\"card-content\">
                                    <p>
                                      Cedula N° $key
                                    </p>
                                  </div>
                                </div>
                              </div>";
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section><!-- START RIGHT SIDEBAR NAV -->

        <!-- END RIGHT SIDEBAR NAV -->
      </div>
      <div class="content-overlay"></div>
    </div>
  </div>
</div>

<div id="modal" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Vista del documento</h4>
    <div class="col s4" style="left: 0px; width: 100%; height: 0px; position: relative; padding-bottom: 141.4227%;" id="panel_documentos">

    </div>
  </div>
  <div class="modal-footer">

    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat primario ">Cerrar</a>
  </div>
</div>