<br><br>
<div id="main">
  <div class="container card row">
    <!--Tabla de los permisos-->
    <div class="row s12 animate__animated animate__fadeInUpBig">
      <div class="col s12 ">
        <div class="card">
          <div class="card-content">
            <h4 class="card-title">Listado de historial</h4>
            <div class="row">
              <div class="col s12">
              
                <table id="tabla_exporta" class="display">
                  <thead>
                    <tr>
                      <th>ID sass</th>
                      <th>Medico</th>
                      <th>Paciente</th>                      
                      <th>estatus</th>
                      <th>Sucursal</th>                      
                      <th>fecha</th>
                    </tr>
                  </thead>
                  <tbody>             

                    <?php
                    $ruta =base_url('Interpretacion/Ver');
                    $form_Interpretacion = base_url('Continua_interpretando');
                    
                    foreach ($tabla as $key => $tupla) {
                      $filtro=$tupla->estatus;
                      if ($tupla->estatus=='TERMINADO') {
                        
                        $filtro="<a href=\"$ruta/$tupla->id_toma_muestra\" class=\"btn-clear task-cat secundario col s12 \">
                                   Ver <span class=\"material-icons\">remove_red_eye</span>
                                 </a>";
                      }else if($tupla->estatus == 'SIN COMPLETAR'){
                          $filtro='SIN COMPLETAR';
                        /*$filtro="                                                    
                          <form action=\"$form_Interpretacion\" method=\"POST\">
                            <input type=\"hidden\" name=\"id_toma_muestra\" id=\"id_toma_muestra$key\" value=\"$tupla->id_toma_muestra\">

                            <button type=\"submit\" class=\"btn primario\">
                              Continuar interpretando
                            </button>
                          </form>
                          ";*/
                      }
                      
                      //SIN COMPLETAR
                      echo "<tr>
                              <td>$tupla->nim_sass</td>
                              <td>$tupla->medico</td>                              
                              <td>$tupla->paciente</td>                              
                              <td>$filtro</td>                              
                              <td>$tupla->udn</td>
                              <td>$tupla->fecha</td>                              
                            </tr>
                            ";
                    }
                    ?>
                    
                  </tbody>
                </table>
              </div>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>