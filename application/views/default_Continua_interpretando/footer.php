<footer class="page-footer footer footer-static  navbar-border navbar-shadow" >
  <div class="footer-copyright">
    <div class="container">
      <span>
        &copy; 2020 
        <a href="http://humanly-sw.com/" target="_blank">Humanly Software
        </a> All rights reserved.
      </span>
      <!-- <span class="right hide-on-small-only">Design and Developed by 
        <a href="https://pixinvent.com/">
        PIXINVENT
        </a>
      </span> -->
    </div>
  </div>
</footer>