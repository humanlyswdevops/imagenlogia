<script src="<?= base_url('template/app-assets/js/vendors.min.js'); ?>"></script>
<!-- tutotial -->
<script src="<?= base_url('template/app-assets/vendors/shepherd-js/shepherd.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/js/plugins.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/js/search.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/js/custom/custom-script.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/js/scripts/customizer.min.js'); ?>"></script>

<script src="<?= base_url('template/app-assets/vendors/select2/select2.full.min.js') ?>"></script>
<script src="<?= base_url('template/app-assets/js/scripts/form-select2.min.js') ?>"></script>

<script src="<?= base_url('template/app-assets/vendors/data-tables/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/vendors/data-tables/js/dataTables.select.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/js/scripts/data-tables.min.js'); ?>"></script>

<script src="<?= base_url('template/app-assets/js/scripts/extra-components-sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('template/app-assets/vendors/sweetalert/sweetalert.min.js'); ?>"></script>

<!-- estos las peticones ajax -->
<script src="<?= base_url('template/peticiones/Interpretacion.js'); ?>"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
  $(document).ready(() => {
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      inDuration: 300, // Transition in duration
      outDuration: 200, // Transition out duration
      startingTop: '4%', // Starting top style attribute
      endingTop: '10%', // Ending top style attribute
    });

    $(".select2").select2({
      width: '100%'
    });

    $('.js-example-basic-single').select2({
      placeholder: 'Select an option'
    });

    $(".salir").click(() => {
      $.ajax({
        url: '<?= base_url('Login/Salir') ?>',
        type: 'POST',
        success: function(ruta) {
          // console.log(ruta);
          window.location.href = 'https://asesores.ac-labs.com.mx/';
        }
      });
      window.reload();
    });
  });
</script>



<!--******************** estas son las librerias para ver la dicom *************************************-->
<script src="<?= base_url('template/librerias_dicom/cornerstone.min.js') ?>"></script>
<script src="<?= base_url('template/librerias_dicom/cornerstoneMath.min.js') ?>"></script>
<script src="<?= base_url('template/librerias_dicom/cornerstoneTools.min.js') ?>"></script>
<script src="<?= base_url('template/librerias_dicom/dicomParser.min.js') ?>"></script>


<!-- include the cornerstoneWADOImageLoader library -->
<script src="<?= base_url('template/librerias_dicom/cornerstoneWADOImageLoader.js') ?>"></script>
<script src="<?= base_url('template/librerias_dicom/uids.js') ?>"></script>

<!-- Lines ONLY required for this example to run without building the project -->
<script>
  window.cornerstoneWADOImageLoader || document.write(
    '<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')
</script>

<script src="<?= base_url('template/librerias_dicom/initializeWebWorkers.js') ?>"></script>

<!-- Include the Quill library -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script>
  
  var JsonEstudios = [];
  var JsonEstudios_Str = [];
  var TotalEditores = 1;
  var bandera = true;

  window.onload = () => {
    ///////////////////////////////////////////
    //  peticion para llenar
    ///////////////////////////////////////////
    $.ajax({
      url: '../../Configuracion/Pre_interpretaciones_Activas',
      type: 'POST',
      dataType: 'json',
      success: function(json) {
        console.log(json, '*************************************');

        let {
          contenido
        } = json;
        let body_interpretacion = document.getElementById('editor-1').children;
        console.log(body_interpretacion[0]);
        body_interpretacion[0].innerHTML = contenido;
      },
      error: function(xhr, status) {
        //alert('Disculpe, existió un problema');
      }
    });
    let data = atob('<?= $estudios ?>');
    data = JSON.parse(data);
    let bandera = 1;
    data.map((estudio) => {
      if (!bandera) {
        JsonEstudios.push(estudio);
      } else {
        let {
          dicoms,
          estudio: {
            nim_sass,
            curp,
            fecha,
            id_estudio,
            nombre_estudio,
            id_contenido_consulta,
            id_toma_muestra,
            id_estudios_sass
          }
        } = estudio;
        console.table(estudio);
        document.getElementById('estudioName').innerText = nombre_estudio;
        document.getElementById('id_contenido').value = id_contenido_consulta;
        document.getElementById('toma').value = id_toma_muestra;
        habilita_editor("#editor-1");
        cuenta = 1;

        
        
        $.ajax({
          url: '../../Interpretacion/RegresaDicom',
          data:{nim_sass,id_estudios_sass},
          type: 'POST',
          dataType: 'json',
          success: function(json) {
            console.log(json,'-------------');
            json.map((imagenD)=>{
              let {ruta}=imagenD;
            
              let corta = ruta.split('www\\')
              ruta = `https://imagenologia.dynpbx.mx/Imagenologia2/${corta[1]}`;
              console.log(ruta);

              crea_listado_dicom(id_estudio, cuenta, ruta);
              cuenta++;
            
            })
            
          },
          error: function(xhr, status) {
            alert('Disculpe, existió un problema');
          }
        });

        /////////////////////////////////////
        // Peticon para traer las imagenes solo es para las subidas manuales
        /*$.ajax({
          url: '../../Dicom/get_dicoma',
          data: {
            id_contenido_consulta
          },
          type: 'POST',
          dataType: 'json',
          success: (data) => {
            try {
              data.map(({
                nombre
              }) => {
                let ruta = '<?= base_url('uploads/dicoms') ?>';
                ruta = ruta + '/' + nombre;
                crea_listado_dicom(id_estudio, cuenta, ruta);
                if (cuenta == 1) {
                  primeraRuta = ruta;
                }
                cuenta++;
              })
            } catch (err) {
              console.error('ocurrio algun error al traer la imagenes dicom');
            }
          },
          error: (err) => {
            console.log('Errora al recoger la dicom')
          }
        })*/

        /***************************************
        dicoms.map((imagen) => {              
          let {
            ruta
          } = imagen;
          console.log('*************************************');
          let corta=ruta.split('www\\')              
          ruta=`https://imagenologia.dynpbx.mx/Imagenologia2/${corta[1]}`;
          console.log(ruta);
          console.log('*************************************');

          // quitar para pruebas chidas                     
          //ruta = '<?= base_url('dicom/1581454359abel ibarra cruz') ?>';
          crea_listado_dicom(id_estudio, cuenta, ruta);
          cuenta++;
        });**************************
        */


      }
      bandera = false;
    });
    JsonEstudios_Str = JSON.stringify(JsonEstudios);
    JsonEstudios_Str = btoa(JsonEstudios_Str);
    if (JsonEstudios.length == 0) {
      $('.terminar_interpretacion').show();
      $('.siguiente_estudio').hide();
    } else {
      $('.terminar_interpretacion').hide();
      $('.siguiente_estudio').show();
    }
  };

  cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
  cornerstoneWADOImageLoader.configure({
    beforeSend: function(xhr) {
      console.log(xhr, '------------------');
      // Add custom headers here (e.g. auth tokens)
      //xhr.setRequestHeader('APIKEY', 'my auth token');
    }
  });
  var loaded = false;

  function loadAndViewImage(imageId, lienzo) {
    var element = document.getElementById(lienzo);
    let templates = ``;
    try {
      let start = new Date().getTime();
      cornerstone.loadAndCacheImage(imageId).then(function(image) {
        console.log(image);
        var viewport = cornerstone.getDefaultViewportForImage(element, image);
        document.getElementById('toggleModalityLUT').checked = (viewport.modalityLUT !== undefined);
        document.getElementById('toggleVOILUT').checked = (viewport.voiLUT !== undefined);
        cornerstone.displayImage(element, image, viewport);
        if (loaded === false) {
          cornerstoneTools.mouseInput.enable(element);
          cornerstoneTools.mouseWheelInput.enable(element);
          cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
          cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
          cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
          cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel
          loaded = true;
        }

        function getSopClass() {
          const value = image.data.string('x00080016');
          return value + ' [' + uids[value] + ']';
        }

        function getPlanarConfiguration() {
          const value = image.data.uint16('x00280006');
          if (value === undefined) {
            return undefined;
          }
          return value + (value === 0 ? ' (pixel)' : ' (plane)');
        }
        // bandera = 1;
        if (bandera) {
          // bandera = 0;
          Formato_interpretacion();
          let fecha = moment().format("DD/MM/YYYY");
          templates += `<p class="ql-align-right">${fecha}</p>`;
          let nomPaciente = image.data.string('x00100010');
          nomPaciente = Get_nombre(nomPaciente);
          viewData('Nombre Paciente', nomPaciente);
          templates += `<p class="ql-align-left">Nombre Paciente: ${nomPaciente}</p>`;
          let sexo = image.data.string("x00100040");
          sexo = Get_sexo(sexo);
          templates += `<p class="ql-align-left">Sexo: ${sexo}</p>`;
          let $fecha = formato_fecha(image.data.string('x00080020'));
          // templates +=`<p>Fecha de toma: ${$fecha}</p>`;
          templates += `<p class="ql-align-left">Estudio: ${image.data.string('x00081030')}</p>`;
          // templates +=`<p>Referido por : DR(A) </p>`;     
          //////////////////////////////////////////////////////////
          // Peticion para traer la firma y cedulas
          /////////////////////////////////////////////////////////
          let foto_imagen = '';
          $.ajax({
            url: '../../Configuracion/Get_Firma',
            type: 'POST',
            dataType: 'json',
            success: function(json) {

              let {
                firma,
                cedulas
              } = json;
              //console.table(cedulas)

              let listaCedulas = '';
              cedulas.map(({
                nombre
              }) => {
                listaCedulas += `              
                <p>
                ${nombre}
                </p>              
              `;

              })
              let templateCedula = `              
              <p style="text-align: left;">
                Numero de cedula profesional:
              </p>              
              <div id="cedulas" style="text-align: left;">
                ${listaCedulas}
              </div>            
            `;
              $('#pie').append(templateCedula);


              let rutaa = '<?= base_url('uploads/firma/') ?>';
              foto_imagen = `
            <p class="ql-align-center">
              <img src='${rutaa+firma.firma}' width="40%"  />
            </p>
            `;
              $('#pie').append(foto_imagen);
              let foother = `
                          <p class="ql-align-center pie">
                            Firma del DR(A) <?= $_SESSION['usuario']->nombre ?>
                          </p>`;
              $('#pie').append(foother);
            },
            error: function(xhr, status) {
              //alert('Disculpe, existió un problema');
            }
          });


          //encabezado .ql-editor
          //para pasar encamezado
          $('#encabezado').append(templates);
        }

        bandera = false;
        viewData('Id paciente', image.data.string('x00100020'));
        viewData('Fecha de nacimiento', image.data.string('x00100030'));
        viewData('Sexo', image.data.string('x00100040'));
        viewData('Estudio', image.data.string('x00081030'));
        viewData('Id protocolo', image.data.string('x00181030'));
        viewData('Numero de Acceso', image.data.string('x00080050'));
        viewData('Id estudio', image.data.string('x00200010'));
        viewData('Fecha de Estudio', image.data.string('x00080020'));

        viewData('Hora', image.data.string('x00080030'));
        viewData('Serie', image.data.string('x0008103e'));
        viewData('Numero de serie', image.data.string('x00200011'));
        viewData('Modalidad', image.data.string('x00080060'));
        viewData('Parte de cuerpo', image.data.string('x00180015'));
        viewData('Tim serie', image.data.string('x00080031'));
        ////////////////////////////////////
        viewData('Fabricante', image.data.string('x00080070'));
        viewData('Modelo', image.data.string('x00081090'));
        viewData('Estacion', image.data.string('x00081010'));
        viewData('Institución', image.data.string('x00080080'));
        viewData('Software', image.data.string('x00181020'));
        viewData('Implementacion', image.data.string('x00020013'));
        viewData('Sop class', getSopClass());
        viewData('Muestras por píxel', image.data.uint16('x00280002'));
        viewData('interpretación fotométrica', image.data.string('x00280004'));
        viewData('Número de cuadros', image.data.string('x00280008'));
        viewData('Configuración plana', getPlanarConfiguration());
        viewData('Filas', image.data.uint16('x00280010'));
        viewData('Columnas', image.data.uint16('x00280011'));
        viewData('Espaciado de píxeles', image.data.string('x00280030'));
        viewData('Espaciado de píxeles de fila', image.rowPixelSpacing);
        viewData('Espaciado de píxeles de columna', image.columnPixelSpacing);
        viewData('Bits asignados', image.data.uint16('x00280100'));
        viewData('Bits almacenados', image.data.uint16('x00280101'));
        viewData('Centro de la ventana', image.data.string('x00281050'));
        viewData('Ancho de la ventana', image.data.string('x00281051'));
        viewData('Reescalar Intercepción', image.data.string('x00281052'));
        viewData('Valor mínimo de píxeles almacenados', image.minPixelValue);
        viewData('Valor máximo de píxeles almacenados', image.maxPixelValue);
        let tempo = image.data.elements.x7fe00010.basicOffsetTable ? image.data.elements.x7fe00010.basicOffsetTable.length : '';
        viewData('Tabla de compensación básica', tempo);
        tempo = image.data.elements.x7fe00010.fragments ? image.data.elements.x7fe00010.fragments.length : '';
        viewData('Fragmentos', tempo)
      }, function(err) {
        alert(err);
      });
    } catch (err) {
      alert(err);
    }
    $('#load').modal('close');
  }
  async function pruebaload(cierra = false) {
    $('#load').modal('open');
    if (cierra) {
      $('#load').modal('close');
    }
  }

  function Ver_dicom(direccion) {
    //  pruebaload();
    url = "wadouri:" + direccion;
    loadAndViewImage(url, 'dicomImage');
  }

  function pasarDatos(img, id = '') {
    document.getElementById(id).innerHTML = img;
  }

  function downloadAndView() {
    let url = document.getElementById('wadoURL').value;
    url = "wadouri:" + url;
    loadAndViewImage(url, 'dicomImage');
  }

  cornerstone.events.addEventListener('cornerstoneimageloadprogress', function(event) {
    const eventData = event.detail;
    const loadProgress = document.getElementById('loadProgress');
    if (eventData.percentComplete === 100) {
      pruebaload(true);
      console.info('termine')
    } else {
      pruebaload();
    }
    document.getElementById('text_load').innerText = `Cargando: ${eventData.percentComplete}%`;
  });

  function getUrlWithoutFrame() {
    var url = document.getElementById('wadoURL').value;
    var frameIndex = url.indexOf('frame=');
    if (frameIndex !== -1) {
      url = url.substr(0, frameIndex - 1);
    }
    return url;
  }

  var element = document.getElementById('dicomImage');
  cornerstone.enable(element);

  // document.getElementById('downloadAndView').addEventListener('click', function(e) {
  //   downloadAndView();
  // });

  // const form = document.getElementById('form');
  // form.addEventListener('submit', function() {
  //   downloadAndView();
  //   return false;
  // });

  document.getElementById('toggleModalityLUT').addEventListener('click', function() {
    var applyModalityLUT = document.getElementById('toggleModalityLUT').checked;
    console.log('applyModalityLUT=', applyModalityLUT);
    var image = cornerstone.getImage(element);
    var viewport = cornerstone.getViewport(element);
    if (applyModalityLUT) {
      viewport.modalityLUT = image.modalityLUT;
    } else {
      viewport.modalityLUT = undefined;
    }
    cornerstone.setViewport(element, viewport);
  });

  document.getElementById('toggleVOILUT').addEventListener('click', function() {
    var applyVOILUT = document.getElementById('toggleVOILUT').checked;
    console.log('applyVOILUT=', applyVOILUT);
    var image = cornerstone.getImage(element);
    var viewport = cornerstone.getViewport(element);
    if (applyVOILUT) {
      viewport.voiLUT = image.voiLUT;
    } else {
      viewport.voiLUT = undefined;
    }
    cornerstone.setViewport(element, viewport);
  });

  async function viewData(texto, valor = undefined, id = '', head = false) {
    if (valor === undefined || valor === '') {
      return;
    }
    if (head) {
      $(`#lista${id}`).append(`
                   <li class="collection-header">
                     <h4>${texto}: ${valor}</h4>
                   </li>
                   `);
    } else {
      $(`#lista${id}`).append(`
                   <li class="collection-item">
                     ${texto}: ${valor}
                   </li>
                   `);
    }
  }
</script>


</body>

</html>