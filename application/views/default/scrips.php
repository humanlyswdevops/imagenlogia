    <!-- END: Footer-->
    <!-- BEGIN VENDOR JS-->
    <script src="<?= base_url('template/app-assets/js/vendors.min.js'); ?>"></script>

    <!-- BEGIN VENDOR JS-->

    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN THEME  JS-->
    <script src="<?= base_url('template/app-assets/js/plugins.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/js/search.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/js/custom/custom-script.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/js/scripts/customizer.min.js'); ?>"></script>

    <script src="<?= base_url('template/app-assets/vendors/select2/select2.full.min.js') ?>"></script>
    <script src="<?= base_url('template/app-assets/js/scripts/form-select2.min.js') ?>"></script>

    <script src="<?= base_url('template/app-assets/vendors/data-tables/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/vendors/data-tables/js/dataTables.select.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/js/scripts/data-tables.min.js'); ?>"></script>

    <script src="<?= base_url('template/app-assets/js/scripts/extra-components-sweetalert.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/vendors/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- estos las peticones ajax -->
    <script src="<?= base_url('template/peticiones/privilegios.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/usuarios.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/estudio.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/toma_muestra.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/paciente.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/Save_dicom.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/Asignar_Medico.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/update_personal.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/ver_interpretacion.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/Admin.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/Perfil.js'); ?>"></script>
    <script src="<?= base_url('template/peticiones/configuracion.js'); ?>"></script>
    


    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!--- input file-->
    <!-- <script src="<?= base_url('template/app-assets/vendors/dropify/js/dropify.min.js'); ?>"></script>
    <script src="<?= base_url('template/app-assets/js/scripts/form-file-uploads.min.js'); ?>"></script>

    <script src="<?= base_url('template/app-assets/vendors/input-upload/input-file.min.js'); ?>"></script> -->

    <!-- END THEME  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?= base_url('template/app-assets/js/scripts/page-account-settings.min.js') ?>"></script>

    <!-- <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script> -->

    <!-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script> -->
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>            
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>            
    
    
    
    <script src="<?= base_url('template/peticiones/Pendientes.js'); ?>"></script>
    
    
    <script src="<?php base_url('template/peticiones/exporta-data.js') ?>"></script>
    <script>
      $(document).ready(function() {

        // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
        $('.modal').modal({
          dismissible: true, // Modal can be dismissed by clicking outside of the modal
          opacity: .5, // Opacity of modal background
          inDuration: 300, // Transition in duration
          outDuration: 200, // Transition out duration
          startingTop: '4%', // Starting top style attribute
          endingTop: '10%', // Ending top style attribute
        });

        $(".select2").select2({
          width: '100%'
        });

        $('.js-example-basic-single').select2({
          placeholder: 'Select an option'
        });

        $(".salir").click(() => {
          $.ajax({
            url: '<?= base_url('Login/Salir') ?>',
            type: 'POST',
            success: function(ruta) {
              console.log(ruta);
              location.href = 'https://asesores.ac-labs.com.mx/';
            }
          });
          location.reload();
        });        
      });
    </script>
    </body>

    </html>