      <!-- BEGIN: Horizontal nav start-->
      <nav class="white hide-on-med-and-down" id="horizontal-nav">
        <div class="nav-wrapper">
          <ul class="left hide-on-med-and-down" id="ul-horizontal-nav" data-menu="menu-navigation">

            <?php
            foreach ($modulos as $key => $value) {
              // echo "$value->modulo <br>";

              switch ($value->modulo) {
                case 'Administración': ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Administracion') ?>">
                      <i class="material-icons iconos">verified_user</i>
                      <span class="menu-title" data-i18n="Calendar">Administración</span>
                    </a>
                  </li>
                <?php
                  break;
                case 'Asignación de medico': ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Asignar_Medico') ?>">
                      <i class="material-icons iconos">fact_check</i>
                      <span class="menu-title" data-i18n="Calendar">Asignacion</span>
                    </a>
                  </li>
                <?php
                  break;
                case 'Interpretación':
                ?>
                <?php
                  break;
                case 'Muestras pendientes':
                ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Muestras_Pendientes') ?>">
                      <i class="material-icons iconos">table_chart</i>
                      <span class="menu-title" data-i18n="Calendar">Tomas pendientes</span>
                    </a>
                  </li>
                <?php
                  break;
                case 'Permisos':
                ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Permisos') ?>">
                      <i class="material-icons iconos">admin_panel_settings</i>
                      <span class="menu-title" data-i18n="Calendar">Accesos</span>
                    </a>
                  </li>
                <?php
                  break;
                case 'Toma de muestras':
                ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Toma_Muestra') ?>">
                      <i class="material-icons iconos">medical_services</i>
                      <span class="menu-title" data-i18n="Calendar">Toma Muestra</span>
                    </a>
                  </li>
                <?php
                  break;
                case 'validacion':                
                ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Estudio/Valida') ?>">
                      <i class="material-icons iconos">table_view</i>
                      <span class="menu-title" data-i18n="Calendar">Validacion</span>
                    </a>
                  </li>
                <?php
                  break;
                  case 'CONSENTRADO':
                    ?>
                    <li class="bold">
                      <a class="waves-effect waves-cyan " href="<?= base_url('concentrado') ?>">
                        <i class="material-icons iconos">table_view</i>
                        <span class="menu-title" data-i18n="Calendar">Concentrado</span>
                      </a>
                    </li>                     
                    <?php
                    break;
                  case 'Historial':
                ?>
                  <li class="bold">
                    <a class="waves-effect waves-cyan " href="<?= base_url('Muestras_Pendientes/Historial') ?>">
                      <i class="material-icons iconos">table_view</i>
                      <span class="menu-title" data-i18n="Calendar">Historial</span>
                    </a>
                  </li>
                <?php
                break;
                case 'Usuarios':
                ?>
                  <li>
                    <a class="dropdown-menu" href="Javascript:void(0)" data-target="TemplatesDropdown">
                      <i class="material-icons iconos">dvr</i><span>
                        <span class="dropdown-title" data-i18n="Templates">Usuarios</span>
                        <i class="material-icons right">keyboard_arrow_down</i>

                      </span>
                    </a>
                    <ul class="dropdown-content dropdown-horizontal-list" id="TemplatesDropdown" tabindex="0" style="">

                      <?php
                      foreach ($crud_usuarios as $key => $crud) {
                        if ($crud == 'INSERTAR') {
                          echo "
                            <li data-menu=\"\" tabindex=\"0\">
                              <a href=\"" . base_url('Usuarios') . "\">
                                <span data-i18n=\"Horizontal Menu\">
                                  $crud
                                </span>
                              </a>
                            </li> 
                          ";
                        } else {
                          if ($crud == 'ACTUALIZAR') {
                            echo "
                              <li data-menu=\"\" tabindex=\"0\">
                                <a href=\"" . base_url('Usuarios/Update_Usuario') . "\">
                                  <span data-i18n=\"Horizontal Menu\">
                                    $crud
                                  </span>
                                </a>
                              </li> 
                            ";
                          }
                        }
                      }
                      ?>
                    </ul>

                  </li>


                  <!-- <li >
                    <a class="waves-effect waves-cyan " href="<?= base_url('Usuarios') ?>">
                      
                      <span class="menu-title" data-i18n="Calendar">Usuarios</span>
                    </a>
                  </li> -->
            <?php
                  break;
                default:

                  break;
              }
            }
            ?>



          </ul>
        </div>
        <!-- END: Horizontal nav start-->
      </nav>
      </div>
      </header>
      <!-- END: Header-->


      <!-- BEGIN: menu de movill-->
      <aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-fixed hide-on-large-only">
        <div class="brand-sidebar sidenav-light"></div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed hide-on-large-only menu-shadow " id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">

          <?php
          foreach ($modulos as $key => $value) {
            // echo "$value->modulo <br>";
            switch ($value->modulo) {

              case 'Administración': ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Administracion') ?>">
                    <i class="material-icons iconos">fact_check</i>
                    <span class="menu-title" data-i18n="Calendar">Administración</span>
                  </a>
                </li>
              <?php
                break;

              case 'Asignación de medico': ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Asignar_Medico') ?>">
                    <i class="material-icons iconos">fact_check</i>
                    <span class="menu-title" data-i18n="Calendar">Asignación</span>
                  </a>
                </li>
              <?php
                break;
              case 'Interpretación':
              ?>
              
              <?php
                break;
              case 'Muestras pendientes':
              ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Muestras_Pendientes') ?>">
                    <i class="material-icons iconos">table_chart</i>
                    <span class="menu-title" data-i18n="Calendar">Tomas pendientes</span>
                  </a>
                </li>
              <?php
                break;
              case 'Permisos':
              ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Permisos') ?>">
                    <i class="material-icons iconos">admin_panel_settings</i>
                    <span class="menu-title" data-i18n="Calendar">Accesos</span>
                  </a>
                </li>
              <?php
                break;

              case 'Toma de muestras':
              ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Toma_Muestra') ?>">
                    <i class="material-icons iconos">medical_services</i>
                    <span class="menu-title" data-i18n="Calendar">Toma Muestra</span>
                  </a>
                </li>
              <?php
                break;
              case 'Historial':
              ?>
                <li class="bold">
                  <a class="waves-effect waves-cyan " href="<?= base_url('Muestras_Pendientes/Historial') ?>">
                    <i class="material-icons iconos">table_view</i>
                    <span class="menu-title" data-i18n="Calendar">Historial</span>
                  </a>
                </li>
              <?php
                break;
              case 'Usuarios':
              ?>
                <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan " href="Javascript:void(0)" tabindex="0">
                    <i class="material-icons iconos">people_alt</i>
                    <span class="menu-title" data-i18n="Cards">Usuarios</span>
                  </a>
                  <div class="collapsible-body" style="">
                    <ul class="collapsible collapsible-sub" data-collapsible="accordion">
                      <?php
                      //
                      foreach ($crud_usuarios as $key => $crud) {
                        if ($crud == 'INSERTAR') {
                          echo "                            
                            <li>
                              <a href=\"" . base_url('Usuarios') . "\">
                              <i class=\"material-icons iconos\">radio_button_unchecked</i>
                              <span data-i18n=\"Cards\">Crear usuario</span></a>
                            </li> 
                          ";
                        } else {
                          if ($crud == 'ACTUALIZAR') {
                            echo "              
                              <li>
                                <a href=\"" . base_url('Usuarios/Update_Usuario') . "\">
                                <i class=\"material-icons iconos\">radio_button_unchecked</i>
                                <span data-i18n=\"Cards\">Modificar usuario</span></a>
                              </li> 
                            ";
                          }
                        }
                      }
                      ?>
                    </ul>
                  </div>
                </li>
          <?php
                break;
              default:
                break;
            }
          }
          ?>
        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
      </aside>
      <!-- END: menu de movil -->