<body class="horizontal-layout page-header-light horizontal-menu preload-transitions 2-columns   " data-open="click" data-menu="horizontal-menu" data-col="2-columns">
  <!-- BEGIN: Header-->
  <header class="page-topbar" id="header">
    <div class="navbar navbar-fixed">
      <nav class="navbar-main barra navbar-color nav-collapsible sideNav-lock navbar-dark gradient-45deg-light-blue-cyan">
        <div class="nav-wrapper">
          <ul class="left">
            
              <h1 class="logo-wrapper">
                <a class="logo-top" href="<?=base_url('Home')?>" >
                  <img id="logo" src="<?= base_url('template/config/logo.png') ?>" alt="logo de laboratorio" width="120px" >
                  <!-- <span class="logo-text hide-on-med-and-down">Materialize</span> -->
                </a>
              </h1>
            
          </ul>
          <!-- <div class="header-search-wrapper hide-on-med-and-down"><i class="material-icons">search</i>
            <input class="header-search-input z-depth-2" type="text" name="Search" placeholder="Explore Materialize" data-search="template-list">
            <ul class="search-list collection display-none"></ul>
          </div> -->
          <ul class="navbar-list right">

            <li style="margin-top: 15px;" class="hide-on-med-and-down"><a class="waves-effect waves-block waves-light toggle-fullscreen" href="javascript:void(0);">
              <!-- <i class="material-icons ">settings_overscan</i> -->
              <img class="mt-5"  src="<?=base_url('uploads/assets/icono_expandir.svg')?>" alt="">
            </a></li>
            <li class="hide-on-large-only"><a class="waves-effect waves-block waves-light search-button" href="javascript:void(0);"><i class="material-icons">search </i></a></li>
            <li>
              <a class="waves-effect waves-block waves-light notification-button" href="javascript:void(0);" data-target="notifications-dropdown">
                <i class="material-icons">
                <img src="<?=base_url('uploads/assets/Icono_configguracion.svg')?>">
              <!-- esto nos da el numero de las notificaciones que tine
              <small class="notification-badge orange accent-3">5</small> -->
            
            </i></a></li>
            <li>
             
              <a class="waves-effect waves-block waves-light profile-button" href="javascript:void(0);" data-target="profile-dropdown">
                <span class="avatar-status avatar-online">
                  <?php
                  $foto =$_SESSION['usuario']->foto;
                  if ($foto!='' || $foto!=null) {
                    $foto=base_url("uploads/fotos_perfil/$foto");
                  }else{
                    $foto=base_url('template/app-assets/images/avatar/avatar-7.png');
                  }
                  ?>
                  <img src="<?= $foto; ?>" alt="avatar">
                  <i></i></span></a>
            </li>            
          </ul>
          <!-- translation-button-->

          <!-- notifications-dropdown-->
          <ul class="dropdown-content" id="notifications-dropdown">
            <li>
            <!-- <h6>NOTIFICATIONS<span class="new badge">5</span></h6> -->
              <h6>
                CONFIGURACIÓN
              </h6>
            </li>
            <li class="divider"></li>
            <li>
              <a class="black-text" href="<?=base_url('Perfil')?>">
                <span class="material-icons icon-bg-circle primario small">account_circle</span> 
                Perfil
              </a>                
            </li>
            <?php if($_SESSION['usuario']->privilegio=='medico radiólogo'){?>
            <li>              
              <a class="black-text" href="<?=base_url('Configuracion')?>">
                <span class="material-icons icon-bg-circle primario small">settings</span> 
                  Configuración plantillas
              </a>              
            </li>

            <li>
              <a class="black-text" href="<?=base_url('Configuracion/interpretacion')?>">
                <span class="small"><img src="<?=base_url('template/img/firma.svg')?>" alt="icono configuración de interpretación"></span> 
                  Configuración interpretación
              </a>
            </li>
            <?php } ?>
            <!-- <li><a class="black-text" href="#!"><span class="material-icons icon-bg-circle amber small">trending_up</span> Generate monthly report</a>
              <time class="media-meta grey-text darken-2" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
            </li> -->
          </ul>
          <!-- profile-dropdown-->
          <ul class="dropdown-content" id="profile-dropdown">
            <li>
              <a class="grey-text text-darken-1 salir" href="#">
                <!-- <i class="material-icons">exit_to_app</i> -->
                <img src="<?=base_url('uploads/assets/icono_salir.svg')?>" alt="">
                Salir</a>
            </li>
            <!-- <li class="divider"></li> -->
            <!-- <li><a class="grey-text text-darken-1" href="user-lock-screen.html"><i class="material-icons">lock_outline</i> Lock</a></li> -->

          </ul>
        </div>
        <nav class="display-none search-sm">
          <div class="nav-wrapper">
            <form id="navbarForm">
              <div class="input-field search-input-sm">
                <input class="search-box-sm" type="search" required="" id="search" placeholder="Explore Materialize" data-search="template-list">
                <label class="label-icon" for="search"><i class="material-icons search-sm-icon">search</i></label><i class="material-icons search-sm-close">close</i>
                <ul class="search-list collection search-list-sm display-none"></ul>
              </div>
            </form>
          </div>
        </nav>
      </nav>