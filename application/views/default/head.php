<!DOCTYPE html>

<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<!-- Mirrored from pixinvent.com/materialize-material-design-admin-template/html/ltr/horizontal-menu-template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Sep 2020 01:51:47 GMT -->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

  <title>Humanly - Imagenologia </title>
  <link rel="apple-touch-icon" href="<?= base_url('template/app-assets/images/favicon/apple-touch-icon-152x152.png'); ?>">
  <link rel="shortcut icon" type="image/x-icon" href="<?= base_url('template/app-assets/images/favicon/favicon-32x32.png') ?>">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!-- BEGIN: VENDOR CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/vendors.min.css') ?>">
  <!-- END: VENDOR CSS-->
  <!-- BEGIN: Page Level CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/themes/horizontal-menu-template/materialize.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/themes/horizontal-menu-template/style.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/layouts/style-horizontal.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/pages/dashboard.min.css') ?>">
  <!-- END: Page Level CSS-->
  <!-- BEGIN: Custom CSS-->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/custom/custom.css') ?>">
  <link rel="stylesheet" href="<?= base_url('template/app-assets/vendors/select2/select2.min.css'); ?>" type="text/css">
  <link rel="stylesheet" href="<?= base_url('template/app-assets/vendors/select2/select2-materialize.css'); ?>" type="text/css">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/css/pages/form-select2.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/data-tables/css/jquery.dataTables.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/data-tables/css/select.dataTables.min.css') ?>">

  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/sweetalert/sweetalert.css') ?>">
  <!-- libreria de  -->
  <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/dropify/css/dropify.min.css'); ?>">
  <!-- END: Custom CSS-->

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  <!-- <link rel="stylesheet" type="text/css" href="<?= base_url('template/app-assets/vendors/input-upload/input-file.css'); ?>"> -->

  <!-- Include stylesheet -->

  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/quill/2.0.0-dev.3/quill.snow.min.css">
  <link rel="stylesheet" href="https://unpkg.com/quill-table-ui@1.0.5/dist/index.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/quill/2.0.0-dev.3/quill.min.js"></script>
  <script src="https://unpkg.com/quill-table-ui@1.0.5/dist/umd/index.js"></script> -->


  <!--version anterior -->
  <!--<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">--->

  <!-- libreria de animate css  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

  <!-- C3 Js  -->
  <link rel="stylesheet" href="<?=base_url('template/c3/c3.css');?>">
  <script src="<?= base_url('template/c3/c3.js');?>"></script>
  <script src="<?= base_url('template/c3/d3.v5.min.js');?>" charset="utf-8"></script>
  

  <!-- Tour.js  -->
  <link rel="stylesheet" href="<?= base_url('template/app-assets/css/pages/extra-components-tour.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url('template/app-assets/vendors/shepherd-js/shepherd-theme-default.min.css') ?>">


  <!-- esta es la libreria del editor  -->
  <link rel="stylesheet" href="<?= base_url('template/quill-2-0/dist/css/quill.bubble.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('template/quill-2-0/dist/css/quill.snow.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('template/quill-2-0/dist/css/index.css'); ?>">

  <script src="<?= base_url('template/quill-2-0/dist/js/quill.min.js') ?>"></script>
  <script src="<?= base_url('template/quill-2-0/dist/js/index.js') ?>"></script>


  <!-- <script  src="./main_editor.js"></script> -->
  <!--version anterior -->
  <!--<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>-->
  <style>
    body {
      background-color: #D7D5D6;
    }
    .text-warning{
      color: #fff;
      font-weight: bold;
      font-size: 1rem;
    }

    .primario {

      background: rgb(241, 90, 0) !important;
      color: #fff;
    }

    .primario:hover {
      border-color: #d44f24 !important;
      box-shadow: 0 8px 25px -8px #f15a29;
      color: #fff;
    }

    .secundario {
      background: rgb(241, 90, 0) !important;
      color: #fff;
    }

    .secundario:hover {

      border-color: #d44f24 !important;
      box-shadow: 0 8px 25px -8px #f15a29;
      color: #fff;
    }

    .top5 {
      margin-top: 10px;
    }

    footer {
      background: rgb(3, 47, 72) !important;
      color: #fff !important;

    }

    .barra {
      background: rgb(3, 47, 72) !important;
      color: #fff !important;
    }

    .paginate_button>.current {
      background: rgb(3, 47, 72) !important;
    }

    .md-form {
      min-height: 1000px;
    }

    .panel {
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .bg-primario {
      background: rgb(3, 47, 72) !important;
    }

    .clear-fix {
      clear: fix;
    }

    .contenedor {
      display: flex;
      flex-wrap: wrap;
    }

    .inf__drop-area {
      width: 100%;
      background-color: rgb(199 199 199 / .3);
    }

    .titulos {
      border-radius: 5px;
      padding: .5px;
      border-bottom: 1px solid #58c3db;
      border-left: 4px solid #58c3db;
      border-right: 1px solid #58c3db;
      border-top: 4px solid #58c3db;
    }

    .titulos h6 {
      text-align: center;
      font-size: 2rem;
      color: #1e1e1e;
    }

    .oculta {
      display: none;
    }

    #barra-progreso {
      margin: auto;
      align-items: center;
      display: flex;
    }

    .d-none {
      display: none;
    }

    .fondo {
      background-color: #D7D5D6;
    }

    #editor {
      background-color: #ffffff;
    }

    .ql-toolbar .ql-snow {
      background-color: #ffffff;
    }

    .dicom-item {
      border: 1px solid #fff;
      color: #fff;
      cursor: pointer;
      text-align: center;
    }

    .dicom-item:hover {
      background-color: rgba(242, 61, 93, .5);
    }

    .cast {
      font-size: 18px;
    }

    a.logo-top {
      margin-top: -20px;
    }

    .col.s4.m2.l2.bg-primario.dicom-item {
      border-radius: 10px;
      margin-left: 10px;
    }

    p.escalas {
      margin-left: 15px;
    }

    span.check-escalas {
      margin-top: 22px;
    }

    button.btn-clear {
      margin: 0px;
      border: 0px;
      padding: 4px;
    }

    /***
      Estilo para la pre visualización 
    */
    .ql-align-right {
      text-align: right;
    }

    .ql-align-center {
      text-align: center;
    }

    .ql-align-left {
      text-align: left;
    }

    .panel-clear {
      padding: 0px;
      margin: 0px;
    }

    strong {
      font-size: 14px;
    }

    .firma {
      text-decoration: overline;
      font-size: 18px;
    }

    a.logo-top {
      margin: 0px;
      padding: 0px;
    }

    img#logo {
      margin-top: -24px;
      position: absolute;
    }

    .giro-90 {
      transform: rotate(90deg);
    }

    .giro-180 {
      transform: rotate(180deg);
    }

    .giro-270 {
      transform: rotate(270deg);
    }

    .giro-360 {
      transform: rotate(360deg);
    }

    div#linkDicoms {
      margin-top: 1rem;
    }

    .iconos {
      color: rgb(241, 90, 0) !important;
    }

    .gris {
      background-color: #CCD4DA;
    }

    .icono {
      margin-top: 6px;
    }

    .iconoazul {
      color: #032F48
    }

    #page-length-option_length {
      display: none;
    }

    /* div#dicomImage>canvas {
      width: 100% !important;
    } */

    .card-alert.card.cyan {
      background: rgb(3, 47, 72) !important;
      color: #fff !important;
    }

    button.btn.primario.mt-2.shepherd-button {
      padding-top: 0px;
    }

    footer {
      width: 100%;
      position: fixed;
      clear: both;
      margin-top: 0px;
      padding-top: 0px;
      overflow: visible;
      top: 95%;
    }

    /* Estilos botones de descargar dicom */
    .leng-icono {
      font-size: 20px;
    }

    .col.s4.m2.l2.bg-primario.dicom-item.descarga {
      background-color: rgb(250, 100, 0) !important;
    }


    /** inicio data table */

    :after,
    :before {
      box-sizing: border-box;
    }

    a {
      color: #337ab7;
      text-decoration: none;
    }

    i {
      margin-bottom: 4px;
    }

     .btn {
      /* display: inline-block; */
      /* font-size: 14px; */
      /* font-weight: 400; */
      /* line-height: 1.42857143; */
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      cursor: pointer;
      user-select: none;
      /* background-image: none; */
      /* border: 1px solid transparent; */
      /* border-radius: 4px; */
    } 

    p.sube-item {
      margin-top: -10px !important;
    }

    .btn-app {
      color: white;
      box-shadow: none;
      border-radius: 3px;
      position: relative;
      padding: 10px 15px;
      margin: 0;
      min-width: 60px;
      max-width: 80px;
      text-align: center;
      border: 1px solid #ddd;
      background-color: #f4f4f4;
      font-size: 12px;
      transition: all .2s;
      background-color: steelblue !important;
    }

    .btn-app>.fa,
    .btn-app>.glyphicon,
    .btn-app>.ion {
      font-size: 30px;
      display: block;
    }

    /* .btn-app:hover {
      border-color: #aaa;
      transform: scale(1.1);
    } */

    .pdf {
      background-color: #dc2f2f !important;
    }

    .excel {
      background-color: #3ca23c !important;
    }

    .csv {
      background-color: #e86c3a !important;
    }

    .imprimir {
      background-color: #8766b1 !important;
    }

    /*
Esto es opcional pero sirve para que todos los botones de exportacion se distribuyan de manera equitativa usando flexbox
.flexcontent {
    display: flex;
    justify-content: space-around;
}
*/

    .selectTable {
      height: 40px;
      float: right;
    }

    div.dataTables_wrapper div.dataTables_filter {
      text-align: left;
      margin-top: 15px;
    }

    .btn-secondary {
      color: #fff;
      background-color: #4682b4;
      border-color: #4682b4;
    }

    .btn-secondary:hover {
      color: #fff;
      background-color: #315f86;
      border-color: #545b62;
    }

    .titulo-tabla {
      color: #606263;
      text-align: center;
      margin-top: 15px;
      margin-bottom: 15px;
      font-weight: bold;
    }

    .inline {
      display: inline-block;
      padding: 0;
    }




    /******* */
  </style>

  </style>
</head>

<!-- END: Head-->