
<?php if (!defined('BASEPATH')) exit('No se permite el acceso directo al script');

class Acceso {
    
  /**
   * valida
   *
   * Esta funcion busca que exista en la coleccion de accesos el modulo principal al que vamos a ingresar
   * 
   * 
   * @param  array $privilegios array con todos los accesos
   * @param  String $modulo nombre del modulo principal
   * @param  bolean $regresa false = no manda al home
   * @return bool
   */
  public function Es_valido($privilegios,$modulo,$regresa=true){
    foreach ($privilegios as $key => $privilegio) {
      if($privilegio->modulo==$modulo){
        return true;
      }
    }
    if ($regresa) {
      redirect(base_url('Home'), 'refresh');
    } else {
      return false;
    }      
  }
  
  /**
   * array_data
   *
   * @param  Array $privilegios arreglo con los privilegios
   * @param  Int $modulo nombre del modulo
   * @return Array
   */
  public function crud($privilegios,$modulo){
    $data=[];
    foreach ($privilegios as $key => $privilegio) {
      if ($privilegio->modulo==$modulo) {
        array_push($data,$privilegio->crud);
      }
    }
    return $data;
  }

}
