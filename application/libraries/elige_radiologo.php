<?php if (!defined('BASEPATH')) exit('No se permite el acceso directo al script');

class elige_radiologo
{

  /**
   * medico
   * 
   * esta función recibe dos arreglos para proceder a elegir a un 
   * medico radiologo lo que va a interpretar
   *
   * @param  Array $lista_radiologo arreglo de todos los médicos radiologos
   * @param  Array $lista_Count lista con las cantidades de los médicos radiologo
   * @return void
   */
  public function medico($lista_radiologo, $lista_Count){

    $bandera = false;

    // este arreglo llevala los totale de dicom en general solo controlara el min y max 
    $MaxMin = [];
    foreach ($lista_radiologo as $key => $radiologo) {
      foreach ($lista_Count as $key => $item) {
        if ($item->id_personal == $radiologo->id_personal) {
          array_push($MaxMin, intval($item->tomas));
          array_push($radiologos, [
            'num_tomas' => intval($item->tomas),
            'id' => $item->id_personal,
            'nombre' => $item->radiologo
          ]);
          $bandera = true;
        }
      }
      if ($bandera == false) {
        array_push($MaxMin, 0);
        array_push($radiologos, [
          'num_tomas' => 0,
          'id' => $radiologo->id_personal,
          'nombre' => $radiologo->radiologo
        ]);
      }
      $bandera = false;
    }

    // asta este punto se tiene arreglo con el radiologo mas el numero de sus interpretaciones echas
    $toma_minima = min($MaxMin);
    $toma_maxima = max($MaxMin);
    foreach ($radiologos as $key => $radio) {
      var_dump($radio);
      if ($radio['num_tomas'] == $toma_minima) {
        $respuesta=[
          'nombre'=>$radio['nombre'],
          'id'=>$radio['id']
        ];
        return $respuesta;
      }
    }
    echo "<h1> $toma_maxima , $toma_minima</h1>";
  }
}
