
$(document).ready(function () {
  tabla_valida = $("#tabla_valida").DataTable({
    responsive: !0,
    language: {
      decimal: "",
      emptyTable: "No hay información",
      info: "",
      infoEmpty: "",
      infoFiltered: "(Filtrado de _MAX_ total entradas)",
      infoPostFix: "",
      thousands: ",",
      lengthMenu: "Mostrar _MENU_ entradas",
      loadingRecords: "Cargando...",
      processing: "Procesando...",
      search: "Buscar:",
      zeroRecords: "Sin resultados encontrados",
      paginate: {
        first: "Primero",
        last: "Ultimo",
        next: "Siguiente",
        previous: "Anterior",
      },
    },
  });
  
});


function handle_valida_imagenes(filtro) {
  if (filtro) {
    let dato=document.getElementById('fecha').value;    
    let  [dia,mes,years]=dato.split('/');   
    $.ajax({
      url: '../Estudio/datos_valida',
      data: {filtro,dia,mes,years},
      type: 'POST',
      dataType: 'json',
      success: (json) => {      
        console.log(json);
        tabla_valida.clear();
        tabla_valida.draw();
        json.map(
          ({
            fecha,id_estudios_sass,nombre,placas,placas_servidor,sass
          }) => {
            placas=parseInt(placas);
            placas_servidor=parseInt(placas_servidor);
            let status='';
            if (placas>=placas_servidor) {
              status='Correcto';
            }else{
              status='Error Faltan imágenes';
            }
            tabla_valida.row
              .add([
                fecha,sass,nombre,id_estudios_sass,placas,placas_servidor,status
              ])
              .draw(false);
          }
        );       
      },
      error: (err) => {
        swal('Advertencia', `Ocurrió algún error al momento de actualizar perfil`, 'warning');      
      }
    }); 
  }else{
    $.ajax({
      url: '../Estudio/datos_valida',
      data: {filtro},
      type: 'POST',
      dataType: 'json',
      success: (json) => {      
        console.log(json);
        tabla_valida.clear();
        tabla_valida.draw();
        json.map(
          ({
            fecha,id_estudios_sass,nombre,placas,placas_servidor,sass
          }) => {
            placas=parseInt(placas);
            placas_servidor=parseInt(placas_servidor);
            let status='';
            if (placas>=placas_servidor) {
              status='Correcto';
            }else{
              status='Error Faltan imágenes';
            }
            tabla_valida.row
              .add([
                fecha,sass,nombre,id_estudios_sass,placas,placas_servidor,status
              ])
              .draw(false);
          }
        ); 
      },
      error: (err) => {
        swal('Advertencia', `Ocurrió algún error al momento de actualizar perfil`, 'warning');      
      }
    });
  }

}

/**
 * Esta funcion crea un nuevo estudio
 */
function crear_estudio() {
  swal("Escribir nuevo estudio", {
      content: "input",
    })
    .then((value) => {
      if (value === "") {
        respuesta(null, 'No escribiste ningún estudio.');
      } else {
        let estudio = value;
        $.ajax({
          url: 'Estudio/Save',
          data: {
            estudio
          },
          type: 'POST',
          dataType: 'json',
          success: function (json) {
            console.log(json);
            respuesta(json);
            location.reload();
          }
        });
      }
    })
}

function Editarestudio(id,nombre,id_sass,honorario,placas){
  $('#modalEditar_estudio').modal('open');
  //alert(nombre)

  document.getElementById('nombre_estudio_card').innerHTML=nombre;

  document.getElementById('id_estudio').value=id;
  document.getElementById('nombre').value=nombre;
  document.getElementById('nim_sass').value=id_sass;
  document.getElementById('honorario').value=honorario;
  document.getElementById('placas').value=placas;

  document.getElementById('nombre').focus();
  document.getElementById('nim_sass').focus();
  document.getElementById('honorario').focus();
  document.getElementById('placas').focus();
  //Editarestudio(id,)
}



$("#EditaEstudio").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: 'Estudio/edita',
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      setTimeout(() => {
        window.location.reload();
      }, 1500);
      //console.log(json)
      
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de actualizar perfil`, 'warning');      
    }
  });
});
