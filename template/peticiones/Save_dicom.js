/**
 * Esta funcion guarda una dicom y regresa la imagen del dicom
 * @param {evento} event 
 */
function Save_dicom(event,url) {
  event.preventDefault();  
  status_progreso('cargando',url);
  var ImgDicom = document.getElementById('dicom');
  var file = ImgDicom.files[0];
  var data = new FormData();
  data.append('dicom', file);
  
  let paciente=document.getElementById('paciente_id').value;
  data.append('paciente_id',paciente);

  var url = 'Toma_Muestra/Save';
  document.getElementById('dicom').value=null;
  document.getElementById('btn_dicom').disabled=true;

  $.ajax({
    url: url,
    type: 'POST',
    contentType: false,
    data: data,
    processData: false,
    cache: false,
    success: (msg) => {
      let datos=JSON.parse(msg);
      $('#Modal_dicom').modal('close'); 
      swal("Satisfecho...!", "Imagen dicom guardada", "success");
      status_progreso('fin',url);
      //desabilita boton 
      document.getElementById('btn_dicom').disabled=false;
      
      console.log(msg);   
      let id=document.getElementById('id_estudio').value;
      $(`#id_lista_${id}`).append(`<li class="collection-item">
                                      ${datos.name_dicom}
                                      <input type="hidden" name="dicom_${id}[]" value="${datos.name_dicom}" /> 
                                    </li>`);
      console.log(id,'valor del id')
      //limpia formulario
      document.getElementById("form_save_dicom").reset();
    },
    error: (err) => {
      console.log(err);
    }
  });  
}
function status_progreso(status,url='') {
  if (status=='cargando') {
    document.getElementById('panel_upload').innerHTML=`
    <img src="${url}template/img/barra2.gif" id="barra-progreso" alt="progres bar">
    `;  
  }else{
    if (status=='fin') {
      document.getElementById('panel_upload').innerHTML=``;  
    }
  }  
}
/**
 * Esta funcion pasa el id del estudio al input hidden 
 * @param {*} id 
 */
function Pasa_id(id) {
  document.getElementById('id_estudio').value=id;
}
$("#form_save_dicom").submit(function (event) {  
  event.preventDefault();
  let base_url=document.getElementById('base_url').value;
  status_progreso('cargando',base_url);
  let obj=JSON.parse(document.getElementById('peticion').value);
  console.log(obj,'save dicom');
  let {id_paciente}=obj;
  let idsass=document.getElementById('sass').value;
  idsass  = idsass.replace(/\//i, "_");


  console.warn({id_paciente,idsass});
  document.getElementById('id_paciente').value=id_paciente;
  document.getElementById('idsass').value=idsass;
  
  $.ajax({
    url: '../Toma_Muestra/Guarda_dicom',    
    data: new FormData(this),    
    cache: false,    
    contentType: false,    
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: function (json) {
      console.log(json);
      
      $('#Modal_dicom').modal('close');
      swal("Satisfecho...!", "Imagen dicom guardada", "success");
      
      
      status_progreso('fin',base_url);
      //desabilita boton 
      document.getElementById('btn_dicom').disabled=false;
      
      let {name_dicom}=json;
      let id=document.getElementById('id_estudio').value;
      $(`#id_lista_${id}`).append(`<li class="collection-item">
                                      ${name_dicom}
                                      <input type="hidden" name="dicom_${id}[]" value="${name_dicom}" /> 
                                    </li>`);
      console.log(id,'valor del id');
      //limpia formulario 
      document.getElementById("form_save_dicom").reset();      
    }
  });
});

function status_progreso(status,url='') {
  if (status=='cargando') {
    document.getElementById('panel_upload').innerHTML=`
    <img src="${url}template/img/barra2.gif" id="barra-progreso" alt="progres bar">
    `;  
  }else{
    if (status=='fin') {
      document.getElementById('panel_upload').innerHTML=``;  
    }
  }  
}