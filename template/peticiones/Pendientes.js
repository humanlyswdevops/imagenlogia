$(document).ready(function () {
  let idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados",
    sEmptyTable: "Ningun dato disponible en esta tabla",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Último",
      sNext: "Siguiente",
      sPrevious: "Anterior",
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending:
        ": Activar para ordenar la columna de manera descendente",
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        1: "1 fila copiada al portapapeles",
      },

      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo",
      },
    },
  };

  tablaPendientes = $("#tabla_pendientes").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: true,
    info: true,
    autoWidth: true,
    language: idioma,
    lengthMenu: [
      [10, 20, 20, -1],
      [10, 20, 30, "Mostrar Todo"],
    ],
    dom: 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
    buttons: {
      dom: {
        container: {
          tag: "div",
          className: "flexcontent",
        },
        buttonLiner: {
          tag: null,
        },
      },
      buttons: [
        {
          extend: "copyHtml5",
          text: '<p class="sube-item">Copiar</p>',
          title: "Documento Imagenologia",
          titleAttr: "Copiar",
          className: "btn btn-app export barras",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "pdfHtml5",
          text: '<p class="sube-item" >PDF</p> ',
          title: "Documento Imagenologia",
          titleAttr: "PDF",
          className: "btn btn-app export pdf",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
          customize: function (doc) {
            doc.styles.title = {
              color: "#4E73DF",
              fontSize: "30",
              alignment: "center",
            };
            (doc.styles["td:nth-child(2)"] = {
              width: "150px",
              "max-width": "100px",
            }),
              (doc.styles.tableHeader = {
                fillColor: "#4c8aa0",
                color: "white",
                alignment: "center",
              }),
              (doc.content[1].margin = [100, 0, 100, 0]);
          },
        },

        {
          extend: "excelHtml5",
          text: '<p class="sube-item">Excel</p> ',
          title: "Documento Imagenologia",
          titleAttr: "Excel",
          className: "btn btn-app export excel",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "csvHtml5",
          text: '<p class="sube-item" >CSV</p> ',
          title: "Documento Imagenologia",
          titleAttr: "CSV",
          className: "btn btn-app export csv",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "print",
          text: '<p class="sube-item"> Imprimir </p> ',
          title: "Documento Imagenologia",
          titleAttr: "Imprimir",
          className: "btn btn-app export imprimir",
          exportOptions: {
            columns: ":visible",
          },
        },
        // {
        //   extend: "pageLength",
        //   titleAttr: "Registros a mostrar",
        //   className: "selectTable",
        // },
      ],
    },
  });
});

function handleGet_pendientes(id_medico) {
  let fecha_uno = document.querySelector("#fecha_uno").value;
  let fecha_fin = document.querySelector("#fecha_fin").value;

  
  let fecha1=fecha_uno.split('/');
  let fecha2=fecha_fin.split('/');

  let [dia_ini,mes_ini,anio_ini]=fecha1;
  let [dia_fin,mes_fin,anio_fin]=fecha2;
  // console.log({dia_ini,mes_ini,anio_ini},{dia_fin,mes_fin,anio_fin})

  let formato_inicio=`${anio_ini}-${mes_ini}-${dia_ini}`;
  let formato_fin= `${anio_fin}-${mes_fin}-${dia_fin}`;
  
  if (fecha_uno != "" && fecha_fin != "") {    
    
    

    $.ajax({
      type: 'POST',       
      url: "Muestras_Pendientes/Earring_byDate",
      dataType: 'json',
      data: { id_medico, formato_inicio, formato_fin },    
      beforeSend:()=>{
        $('#load').modal('open');
      } ,
      success: function (data) {
        $('#load').modal('close');

        tablaPendientes.clear();
        tablaPendientes.draw();
        let ruta =document.querySelector('#ruta_pendientes').value;
        data.map(({estatus,fecha,id_personal,id_toma_muestra,medico,nim_sass,paciente,udn},key)=>{
          
          let button =`
            <form action="${ruta}" method="POST">
              <input type="hidden" name="id_toma_muestra" id="id_toma_muestra${key}" value="${id_toma_muestra}" />

              <button type="submit" class="btn primario">
                <span class="material-icons">verified</span>
              </button>
            </form>`;

          tablaPendientes.row
            .add([
              nim_sass,
              medico,
              paciente,
              fecha,
              estatus,
              udn,
              button
            ])
            .draw(false);            
        })
        $('#cargando').html(``);
      },
    });
    
  }
}


