//24/04/2021
$("#btn_asignar").hide();
/**
 * evento del select cuando selecciona un paciente
 */
$("#paciente").on("change", function () {
  let name_paciente = $("#paciente option:selected").text();
  name_paciente = name_paciente.toUpperCase();
  swal({
    title: "Seleccionar Paciente",
    text: `Quiere seleccionar al paciente ${name_paciente}`,
    icon: "warning",
    dangerMode: true,
    buttons: {
      cancel: "No",
      delete: "Si",
    },
  }).then(function (willDelete) {
    if (willDelete) {
      document.getElementById("paciente_text").innerText = name_paciente;
      // deja ver el template donde se ve el contenido de todo el expediente
      document.getElementById("template").className = "col s10";
      // oculta el modulo de registro o selección de usuario
      $("#panel_cliente").hide();
      //habilitamos el select para que pueda agregar estudios
      document.getElementById("estudio").disabled = false;
      document.getElementById("paciente_id").value =
        document.getElementById("paciente").value;
    }
  });
});

/**
 * Esto solo captura el evento del checked para mandar el alert
 */
$("#is_asignar").change(function () {
  let alerta = document.getElementById("notificacion");
  if ($(this).is(":checked")) {
    alerta.innerHTML = `
      <div class="card-alert card red" style="width: 25%;">
        <div class="card-content white-text">
          <p>Alerta : Estos estudios no se asignaran a ningún medico</p>
        </div>        
      </div>
    `;
  } else {
    alerta.innerHTML = "";
  }
});

/**
 * evento de select que selecciona los estudios
 */
$("#estudio").on("change", function () {
  let texEstudio = $("#estudio option:selected").text();
  nuevo_estudio(this.value, texEstudio);
});
/**
 * esta funcion elimina un estudio del DOM
 */
function EliminaEstudio(id) {
  swal({
    title: "Eliminar estudio",
    text: "¿En verdad quieres eliminar el estudio?",
    icon: "warning",
    dangerMode: true,
    buttons: {
      cancel: "No",
      delete: "Si, Eliminalo",
    },
  }).then(function (willDelete) {
    if (willDelete) {
      document.getElementById(id).innerHTML = "";
      swal("Estudio eliminado", {
        icon: "success",
      });
    }
  });
}

/**
 * Este método va agregando los estudios para que pueda elegir una dicom
 * @param {int} id id del estudio como esta en la BD
 * @param {Strin} estudio estudio en texto ejempo 'rayos x'
 */
function nuevo_estudio(id, estudio) {
  //document.getElementById('btn_save').disabled = false;
  let bd_id = id;
  id = "estudio" + id;
  $("#panel").append(`
                      <div id="${id}" class="container">
                        <div class="card-alert card bg-primario">
                          <div class="card-content white-text">
                            <p>
                              Estudio: ${estudio}
                              <input type="hidden" name="estudios_identificador[]" value="estudio-${bd_id}" />
                            </p>
                          </div>                          
                        </div>
                        <div class="content">
                          <a onclick="Pasa_id(${bd_id})" class="waves-effect waves-light btn primario modal-trigger " href="#Modal_dicom">
                            Agregar dicom <span class="material-icons">cloud_upload</span>
                          </a>
                          <!-- Save_dicom.php llena lista -->
                          <ul class="collection" id="id_lista_${bd_id}">                            
                          </ul>                          
                        </div>
                      </div>
              `);
}

/**
 * ***************************************************************************************************
 * este método va a consumir la api que trae todos los estudios de sass
 *
 * @param {Strin} ruta esta es la ruta de la api donde se van a traer los estudios de sass
 * @param {String} metodo estolo siempre va para que la api sepa que es estudios de imagenologia
 *****************************************************************************************************/
function busca_sass(
  ruta = "https://asesores.ac-labs.com.mx/sass/index.php",
  metodo = "imagenologia"
) {
  let id_sass = document.getElementById("sass").value;
  $.ajax({
    url: ruta,
    data: { metodo, id_sass },
    type: "POST",
    dataType: "json",
    success: function (json) {
      document.getElementById("peticion").value = JSON.stringify(json);

      let { apellidos, nombre, examenes } = json;

      //let examenes=json.examenes;
      let cuenta_estudios = 0;
      //document.getElementById('btn_busca').disabled=true;//deshabilitamos el botón
      console.log(examenes, "Examenes");

      if (
        document.getElementById("paciente") &&
        document.getElementById("nim_sass")
      ) {
        console.log(json, "---*************************************-----");

        const { id_paciente } = json;
        console.error(id_paciente);
        document.getElementById("paciente").value = id_paciente;
        document.getElementById("nim_sass").value =
          document.getElementById("sass").value;
      }

      $("#panel").html("");
      //$('#panel_tarjeta').html('');
      $("#alerta").append("");
      $("#cards").html("");

      let template_ultrasonidos = "";
      let cuenta_ultrasonidos = 0;
      examenes.map((estudio) => {
        let {
          estudios: { id_examen, nombre },
        } = estudio;

        //para que no muestre los estudios si son ULTRASONIDO
        nombre = nombre.toUpperCase();
        let Is_ultrasonido = nombre.indexOf("ULTRASONIDO");
        if (Is_ultrasonido !== 0) {
          nuevo_estudio(id_examen, nombre);
          let tarjeta = crearCard(nombre);
          $("#cards").append(tarjeta);
          cuenta_estudios += 1;
        } else {
          cuenta_ultrasonidos += 1;
        }
      });

      if (cuenta_estudios >= 1) {
        $("#btn_asignar").show();
      }

      //valida que hay ultrasonidos para llenar el mensaje que se va a mostrar
      if (cuenta_ultrasonidos) {
        template_ultrasonidos = `
          <strong class="text-warning center">
            Lo Ultrasonidos no son contabilizados, ULtrasonidos encontrados: ${cuenta_ultrasonidos}
          </strong>  
          `;
      }

      let alerta = `
                <p >
                  <img src="https://img.icons8.com/cotton/35/000000/check-male--v1.png"/>
                  <strong id="name_paciente_sass">${nombre} ${apellidos} </strong> 
                </p>
                <div class="card-alert card gradient-45deg-light-blue-cyan">
                  <div class="card-content white-text">
                    <p class="center">
                      <i class="material-icons">cloud</i> Total de estudios : ${cuenta_estudios}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <i class="material-icons">fiber_pin</i> nim : ${
                        document.getElementById("sass").value
                      }                      
                    </p>     
                      ${template_ultrasonidos}                             
                  </div>
                </div>
              `;
      $("#alerta").html(alerta);
    },
  });
}
/**
 * esta función crea la tarjeta del estudio
 *
 * @param {String} estudio
 * @param {Float} precio
 */
function crearCard(estudio = "", precio = 0) {
  let template_costo = "";
  if (precio != 0) {
    template_costo = `
        <p>Total</p>
    <p class="green-text  mt-3">
      $${precio}.00
    </p>`;
  }

  let template = `
              <div class="col s12 m6 l3 card-width">
                <div class="card border-radius-6">
                  <div class="card-content center-align">
                    <i class="material-icons amber-text small-ico-bg mb-5">check</i>
                    <h5 style="font-size:19px" class="m-0">
                      <b>
                        ${estudio}
                      </b>
                    </h5>
                    ${template_costo}
                  </div>
                </div>
              </div>                
              `;
  return template;
}
/**
 * esta función guarda la toma que traemos de sass
 */
function guardarToma() {
  swal({
    title: "¿ En verdad quieres seleccionar este estudio ?",
    text: "No podrás cancelar ",
    icon: "warning",
    buttons: true,
    dangerMode: true,
    buttons: {
      cancel: "No",
      catch: {
        text: "Si",
        value: "catch",
      },
    },
  }).then((willDelete) => {
    if (willDelete) {
      //nim_sass

      //console.log('creando form Data')
      // let Form=new FormData(document.getElementById('formulario_toma_muestras'))
      const json = JSON.parse(document.getElementById("peticion").value);
      let sass = document.getElementById("sass").value;

      $.ajax({
        async: false,
        type: "POST",
        data: { nim_sass: sass },
        url: `Toma_Muestra/Validate_studys`,
        dataType: "json",
        success: function (data) {
          let conjunto = [];
          if (data.length) {
            //ya esta registrado en el sistema
            console.log(data, "*aaaaaaaaaaaaaaaa *");

            let { nombre, apellidos: app, id_paciente: curp, examenes } = json;
            if (examenes.length > data.length) {
              let estudiosConjunto = [];
              console.log(examenes);

              //############################################################
              //#   Inicio de validación de estudios nuevo
              //############################################################
              examenes.forEach((element) => {
                console.log(element);
                let {
                  estudios: { id_examen, nombre },
                } = element;
                //console.log(element)

                let existe = 0;
                let id_toma_muestra = 0;
                data.map((item) => {
                  id_toma_muestra = item.id_toma_muestra;
                  if (item.id_estudios_sass == id_examen) {
                    existe = 1;
                  }
                });
                if (!existe) {
                  let obj = {
                    id_examen,
                    nombre,
                    id_toma_muestra,
                  };
                  estudiosConjunto.push(obj);
                }

                /*data.map(
                  ({
                    id_estudios_sass,                    
                    id_toma_muestra
                  }) => {                    
                    if (id_examen != id_estudios_sass) {                                         
                      console.log(`${id_examen} != ${id_estudios_sass}`);
                      let obj =  {
                        id_examen, 
                        nombre,
                        id_toma_muestra
                      };   
                      let existe=0;
                      estudiosConjunto.map((elemento)=>{
                        
                        if (elemento.id_examen==id_examen) {
                          existe=1;
                        }
                      })
                      if (existe==0) {
                        estudiosConjunto.push(obj);  
                      }
                                              
                      
                    }
                  }
                );*/
              });
              //############################################################
              //#   Fin de validación de estudios nuevo
              //############################################################

              //Sacamos el nombre de los estudios nuevo
              let nombres_estudio = "";
              estudiosConjunto.map(({ nombre }) => {
                nombres_estudio += nombre + ",";
              });
              console.log(estudiosConjunto);

              estudios = JSON.stringify(estudiosConjunto);

              swal({
                title: "¡ Alerta ! Este nim ya se encuentra registrado",
                text: `Nuevo estudio Encontrado, Quieres agregar este estudio : ${nombres_estudio}`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
                buttons: {
                  cancel: "No",
                  catch: {
                    text: "Si",
                    value: "catch",
                  },
                },
              }).then((willDelete) => {
                if (willDelete) {
                  if (sass.length === 14) {
                    $.ajax({
                      url: "Toma_Muestra/Add_studys",
                      data: {
                        estudios,
                      },
                      type: "POST",
                      dataType: "json",
                      success: function (json) {
                        respuesta(json);
                        if ((json.status = "success")) {
                          setTimeout(() => {
                            location.reload();
                          }, 2000);
                        }
                      },
                    });
                  } else {
                    swal({
                      title: "Alerta",
                      text: "Nim del sass no valido debe tener 14 digitos",
                      icon: "error",
                    });
                  }
                }
              });
            } else {
              swal(
                "Error",
                "Este nim ya esta registrado y no tiene mas estudios",
                "error"
              );
            }
          } else {
            //No esta registrado en el sistema
            let { nombre, apellidos: app, id_paciente: curp, examenes } = json;
            let estudios = [];

            examenes.forEach((element) => {
              let {
                estudios: { id_examen, nombre },
              } = element;

              let obj = {
                nombre: nombre.trim(),
                id: id_examen,
              };
              estudios.push(obj);
            });
            estudios = JSON.stringify(estudios);
            let comentario = document.querySelector("#datos_clinicos").value;

            let is_asignated = "si";
            let isChecked = document.getElementById("is_asignar").checked;
            if (isChecked) {
              is_asignated = "no";
            } else {
              is_asignated = "si";
            }

            if (sass.length === 14) {
              $.ajax({
                url: "Toma_Muestra/Guarda_automatico",
                data: {
                  nombre,
                  app,
                  curp,
                  estudios,
                  sass,
                  is_asignated,
                  comentario,
                },
                type: "POST",
                dataType: "json",
                success: function (json) {
                  console.log(json);
                  respuesta(json);
                  if ((json.status = "success")) {
                    setTimeout(() => {
                      location.reload();
                    }, 2000);
                  }
                },
              });
            } else {
              swal({
                title: "Alerta",
                text: "Nim del sass no valido debe tener 14 digitos",
                icon: "error",
              });
            }
          }
        },
        error: function (error) {
          console.log(error);
        },
      });
    }
  });
}

/////////////////////////////////////////////////////
$("#formulario_toma_muestras").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: "../Toma_Muestra/Save",
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: "POST",
    dataType: "json",
    success: function (json) {
      console.log(json);
      respuesta(json);
      if ((json.status = "success")) {
        setTimeout(() => {
          location.reload();
        }, 2000);
      }
    },
    error: (err) => {
      console.log(err);
    },
  });
});

function respuesta(json, advertencia = "") {
  if (advertencia == "") {
    if (json.status == "success") {
      swal("Satisfecho.!", `${json.msg}`, "success");
    } else {
      swal("error.!", `${json.msg}`, "error");
    }
  } else {
    swal("Advertencia", `${advertencia}`, "warning");
  }
}

$("#sass")
  .on("keyup", function () {
    let nim = document.querySelector("#sass").value;
    if (nim.length == 15) {
      let nuevo_nim = "";
      for (let index = 0; index < nim.length; index++) {
        if (index <= 12) {
          if (index == 9) {
            nuevo_nim += "/";
          }
          nuevo_nim += nim[index];
        }
      }
      document.querySelector("#sass").value = nuevo_nim;
      busca_sass();
    } else {
      if (nim.length === 14) {
        busca_sass();
      }
    }
  })
  .keyup();

/**
 * Esta función sirve para eliminar el nim de la toma de muestra
 */
function borra_nim() {
  document.querySelector("#sass").value = "";
  document.getElementById("sass").focus();
}
