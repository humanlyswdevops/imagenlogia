
function detalle_privilegio(id_privilegio,acceso) {  
    $.ajax({    
      url : 'Permisos/get_detalleAcceso',    
      data : { id_privilegio },
      type : 'POST',
      dataType : 'json',
      success : function(json) {
        console.log(json);      
        let template='';
        json.map((Permiso)=>{
          let btn='';
          let {id_crud,id_modulo,id_privilegios,modulo,nombre,privilegio}=Permiso;
          if(acceso=='ELIMINAR'){
            btn=`<td>
                  <button class="btn secundario" onclick="eliminarAcceso(${id_crud},${id_modulo},${id_privilegios},'${acceso}')">
                    Eliminar
                  </button>
                </td>`;
          }          
          template+= `<tr>
                        <td>${privilegio}</td>
                        <td>${modulo}</td>
                        <td>${nombre}</td>
                        ${btn}
                      </tr>`;
        });
        document.getElementById('body_tabla_privilegio').innerHTML=template;

        
      },
      error : function(xhr, status) {
        alert('Disculpe, existió un problema');
      }
  });
}

function eliminarAcceso(id_crud,id_modulo,id_privilegios,acceso) {
  $.ajax({    
    url : 'Permisos/dropAcceso',    
    data : { id_crud, id_modulo,id_privilegios},
    type : 'POST',
    dataType : 'json',
    success : function(json) {
      detalle_privilegio(id_privilegios,acceso);
    },
    error : function(xhr, status) {
      alert('Disculpe, existió un problema');
    } 
  });
}

function Nuevo_acceso(event,acceso) {
  event.preventDefault()
  let operacion=document.getElementById('operacion').value;
  let modulo=document.getElementById('modulo').value;
  let privilegio=document.getElementById('privilegio').value;
  if(operacion!=''&&modulo!=''&& privilegio!=''){
    $.ajax({    
      url : 'Permisos/insertAcceso',    
      data : { operacion,modulo,privilegio },
      type : 'POST',
      dataType : 'json',
      success : function(json) {
        console.log(json);              
        respuesta(json);
        $('#privilegioM').modal('close');
        detalle_privilegio(privilegio,acceso);
      },
      error : function(xhr, status) {
        alert('Disculpe, existió un problema');
      }
    });
  }else{
    respuesta(null,'Falta seleccionar una opciones');
  }  
}

function respuesta(json,advertencia='') {
  if (advertencia=='') {
    if(json.status=='success'){    
      swal("Satisfecho.!",`${json.msg}`, "success")
    }else{
      swal("error.!", `${json.msg}`, "error")
    }  
  }else{
    swal('Advertencia',`${advertencia}`,'warning')
  }
  
}
 
function nuevo_privilegios(){
  swal("Escribir nuevo privilegio", {
    content: "input",
    })
    .then((value) => {      
      if (value === "") {        
        respuesta(null,'No escribiste ningún privilegio.');
      }
      let privilegio=value;
      $.ajax({    
        url : 'Permisos/insertPrivilegio',    
        data : { privilegio},
        type : 'POST',
        dataType : 'json',
        success : function(json) {
          console.log(json);              
          respuesta(json);                    
          location.reload();
        }        
      });
    })
}