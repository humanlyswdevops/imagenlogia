/**
 * evento del select cuando selecciona un paciente
 */
$('#medico_select').on('change', function () {
  let medico = $('#medico_select option:selected').text();
  medico = medico.toUpperCase();
  let medico_id = this.value;
  //validamos que exista en el dom si no existe no creamos en el else
  if (document.getElementById(`item_${medico_id}`)) {
    swal({
      title: "Alerta !",
      text: "Este medico ya esta seleccionado.",
      icon: "warning",
    })
  } else {
    $("#lista_radiologos").append(`
    <li class="collection-item" id="item_${medico_id}">
      ${medico}
      <input type="hidden" name="medico[]" value="${medico_id}" /> 
      <button style="width: 21px;height: 21px;" class="btn-floating mb-1 waves-effect waves-light right" onclick="remueve_radiologo('item_${medico_id}')">
        <i style="display: block;margin-top: -8px;font-size: 15px;" class="material-icons">clear</i>
      </button>      
    </li>        
    `);
  }
})

function remueve_radiologo(id) {
  // El método lanza la excepción correspondiente al (caso 2)
  let padre = document.getElementById("lista_radiologos");
  let hijo = document.getElementById(id);
  padre.removeChild(hijo)
}



$("#frm-radiologo").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: 'Asignar_Medico/Asignar',
    /* En data enviamos el contenido del formulario enviado como parametro */
    data: new FormData(this),
    /* Forzamos a que el navegador no almacene en cache las paginas solicitadas */
    cache: false,
    /* Indicamos que no establezca ningun encabezado de tipo de contenido */
    contentType: false,
    /* Indidcamos que no se procesen los datos para poder utilizar el DOM */
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: function (json) {

      console.log(json, 'asignacion');
      if (json.length === 0) {
        respuesta({
          'status': 'error',
          'msg': 'Ocurrió algún error'
        });
      } else {
        respuesta(json);
      }
      setTimeout(() => {
        window.location.reload();
      }, 2000);
    }
  });
});

$("#frm_radiologo_asignatodo").submit(function (event) {
  event.preventDefault();

  let radiologo = $("#medico_select2 option:selected").text();
  let medico_select2 = document.getElementById('medico_select2').value;
  swal({
    title: "Alerta",
    text: `En verdad quieres asignarle todas la tomas al medico: ${radiologo}`,
    icon: 'warning',
    dangerMode: true,
    buttons: {
      cancel: 'No',
      delete: 'Si'
    }
  }).then(function (willDelete) {
    if (willDelete) {
      $.ajax({
        url: 'Asignar_Medico/Un_medico',
        data: {
          medico_select2
        },
        type: 'POST',
        dataType: 'json',
        success: function (json) {
          console.log(json);
          respuesta(json);
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        }
      });
    }
  });



});



function Asigna_id_deToma(id) {
  document.getElementById('id_toma_muestra').value = id;
}