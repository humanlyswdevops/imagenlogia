$(document).ready(function () {
  var idioma = {
    sProcessing: "Procesando...",
    sLengthMenu: "Mostrar _MENU_ registros",
    sZeroRecords: "No se encontraron resultados",
    sEmptyTable: "Ningun dato disponible en esta tabla",
    sInfo:
      "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
    sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
    sInfoPostFix: "",
    sSearch: "Buscar:",
    sUrl: "",
    sInfoThousands: ",",
    sLoadingRecords: "Cargando...",
    oPaginate: {
      sFirst: "Primero",
      sLast: "Último",
      sNext: "Siguiente",
      sPrevious: "Anterior",
    },
    oAria: {
      sSortAscending: ": Activar para ordenar la columna de manera ascendente",
      sSortDescending:
        ": Activar para ordenar la columna de manera descendente",
    },
    buttons: {
      copyTitle: "Informacion copiada",
      copyKeys: "Use your keyboard or menu to select the copy command",
      copySuccess: {
        _: "%d filas copiadas al portapapeles",
        1: "1 fila copiada al portapapeles",
      },

      pageLength: {
        _: "Mostrar %d filas",
        "-1": "Mostrar Todo",
      },
    },
  };

  tabla = $("#tabla_pago").DataTable({
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: true,
    info: true,
    autoWidth: true,
    language: idioma,
    lengthMenu: [
      [10, 20, 20, -1],
      [10, 20, 30, "Mostrar Todo"],
    ],
    dom: 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
    buttons: {
      dom: {
        container: {
          tag: "div",
          className: "flexcontent",
        },
        buttonLiner: {
          tag: null,
        },
      },
      buttons: [
        {
          extend: "copyHtml5",
          text: '<p class="sube-item">Copiar</p>',
          title: "Documento Imagenologia",
          titleAttr: "Copiar",
          className: "btn btn-app export barras",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "pdfHtml5",
          text: '<p class="sube-item" >PDF</p> ',
          title: "Documento Imagenologia",
          titleAttr: "PDF",
          className: "btn btn-app export pdf",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
          customize: function (doc) {
            doc.styles.title = {
              color: "#4E73DF",
              fontSize: "30",
              alignment: "center",
            };
            (doc.styles["td:nth-child(2)"] = {
              width: "150px",
              "max-width": "100px",
            }),
              (doc.styles.tableHeader = {
                fillColor: "#4c8aa0",
                color: "white",
                alignment: "center",
              }),
              (doc.content[1].margin = [100, 0, 100, 0]);
          },
        },

        {
          extend: "excelHtml5",
          text: '<p class="sube-item">Excel</p> ',
          title: "Documento Imagenologia",
          titleAttr: "Excel",
          className: "btn btn-app export excel",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "csvHtml5",
          text: '<p class="sube-item" >CSV</p> ',
          title: "Documento Imagenologia",
          titleAttr: "CSV",
          className: "btn btn-app export csv",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "print",
          text: '<p class="sube-item"> Imprimir </p> ',
          title: "Documento Imagenologia",
          titleAttr: "Imprimir",
          className: "btn btn-app export imprimir",
          exportOptions: {
            columns: ":visible",
          },
        },
        // {
        //   extend: "pageLength",
        //   titleAttr: "Registros a mostrar",
        //   className: "selectTable",
        // },
      ],
    },
  });
  tabla_Consentrados = $("#tabla_Consentrados").DataTable({
    responsive: !0,
    paging: true,
    lengthChange: true,
    searching: true,
    ordering: true,
    info: true,
    autoWidth: true,
    language: idioma,
    lengthMenu: [
      [10, 20, 20, -1],
      [10, 20, 30, "Mostrar Todo"],
    ],
    dom: 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
    buttons: {
      dom: {
        container: {
          tag: "div",
          className: "flexcontent",
        },
        buttonLiner: {
          tag: null,
        },
      },
      buttons: [
        {
          extend: "copyHtml5",
          text: '<p class="sube-item">Copiar</p>',
          title: "Documento Imagenologia",
          titleAttr: "Copiar",
          className: "btn btn-app export barras",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "pdfHtml5",
          text: '<p class="sube-item" >PDF</p> ',
          title: "Documento Imagenologia",
          titleAttr: "PDF",
          className: "btn btn-app export pdf",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
          customize: function (doc) {
            doc.styles.title = {
              color: "#4E73DF",
              fontSize: "30",
              alignment: "center",
            };
            (doc.styles["td:nth-child(2)"] = {
              width: "150px",
              "max-width": "100px",
            }),
              (doc.styles.tableHeader = {
                fillColor: "#4c8aa0",
                color: "white",
                alignment: "center",
              }),
              (doc.content[1].margin = [100, 0, 100, 0]);
          },
        },

        {
          extend: "excelHtml5",
          text: '<p class="sube-item">Excel</p> ',
          title: "Documento Imagenologia",
          titleAttr: "Excel",
          className: "btn btn-app export excel",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "csvHtml5",
          text: '<p class="sube-item" >CSV</p> ',
          title: "Documento Imagenologia",
          titleAttr: "CSV",
          className: "btn btn-app export csv",
          exportOptions: {
            modifier: {
              search: "none",
            },
          },
        },
        {
          extend: "print",
          text: '<p class="sube-item"> Imprimir </p> ',
          title: "Documento Imagenologia",
          titleAttr: "Imprimir",
          className: "btn btn-app export imprimir",
          exportOptions: {
            columns: ":visible",
          },
        },
        // {
        //   extend: "pageLength",
        //   titleAttr: "Registros a mostrar",
        //   className: "selectTable",
        // },
      ],
    },
  });
});

function carga_grafica() {
  var ctx = document.getElementById("myChart").getContext("2d");
  let conjunto = document.getElementById("JSgrafica").value;
  conjunto = JSON.parse(conjunto);
  console.table(conjunto);
  conjunto.sort(function (a, b) {
    return convertirFecha(a.fecha) - convertirFecha(b.fecha);
  });
  console.table(conjunto);
  let fechas = [];

  let ntomas = [];
  conjunto.map((toma) => {
    //console.log(toma)
    let { tomas, fecha } = toma;
    fechas.push(fecha);
    ntomas.push(tomas);
  });
  let chart = new Chart(ctx, {
    type: "line",
    data: {
      labels: fechas,
      datasets: [
        {
          label: "Interpretaciones",
          backgroundColor: "rgb(3,47,72)",
          borderColor: "rgb(241,90,0)",
          data: ntomas,
        },
      ],
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              stepSize: 1,
            },
          },
        ],
      },
    },
  });
}

function convertirFecha(fechaString) {
  var fechaSp = fechaString.split("/");
  var anio = new Date().getFullYear();
  if (fechaSp.length == 3) {
    anio = fechaSp[2];
  }
  var mes = fechaSp[1] - 1;
  var dia = fechaSp[0];
  return new Date(anio, mes, dia);
}

$("#form_trae_pagos").submit(function (event) {
  event.preventDefault();
  let date_uno = document.getElementById("date_uno").value;
  let date_fin = document.getElementById("date_fin").value;
  let [dia_ini, mes_ini, year_ini] = date_uno.split("/");
  let [dia_fin, mes_fin, year_fin] = date_fin.split("/");
  $.ajax({
    url: "Administracion/get_pagos/",
    data: {
      dia_fin,
      mes_fin,
      year_fin,
      dia_ini,
      mes_ini,
      year_ini,
    },
    type: "POST",
    dataType: "json",
    success: function (json) {
      //console.log(json);
      tabla.clear();
      tabla.draw();
      json.map(
        ({
          radiologo,
          pago_toma,
          pago_interpretaciones,
          tomas_totales,
          totales_interpretaciones,
          udn,
        }) => {
          tabla.row
            .add([
              radiologo,
              tomas_totales,
              totales_interpretaciones,
              pago_toma,
              pago_interpretaciones,
              udn,
            ])
            .draw(false);
        }
      );
    },
  });
});

function handleGetConcentrado(id_radiologo) {
  let fecha_uno = document.getElementById("fecha_uno").value;
  let fecha_fin = document.getElementById("fecha_fin").value;
  let [dia_ini, mes_ini, year_ini] = fecha_uno.split("/");
  let [dia_fin, mes_fin, year_fin] = fecha_fin.split("/");

  fecha_uno = `${year_ini}-${mes_ini}-${dia_ini}`;
  fecha_fin = `${year_fin}-${mes_fin}-${dia_fin}`;
  
  if (
    mes_ini !== undefined &&
    year_ini !== undefined &&
    mes_fin !== undefined &&
    year_fin !== undefined
  ) {
    var jsonEstudios = []; //
    ////////////////////////////////////////
    $.ajax({
      url: "Estudio/Todos",
      type: "GET",
      dataType: "json",
      success: function (json) {
        jsonEstudios = json;
        //console.table(jsonEstudios);
        $.ajax({
          url: "Administracion/consentrado/",
          data: {
            id_radiologo,
            dia_fin,
            mes_fin,
            year_fin,
            dia_ini,
            mes_ini,
            year_ini,
            fecha_uno,
            fecha_fin,
          },
          type: "POST",
          dataType: "json",
          beforeSend: function () {
            $("#load").modal("open");
          },
          success: function (json) {
            var Conjunto_tecnicos = [];
            $.ajax({
              async: false,
              type: "GET",
              url: `https://asesores.htds-humanly.com/datoSuc`,
              dataType: "json",
              success: function (data) {
                //console.log(data,'* peticion *');
                Conjunto_tecnicos = data;
                
              },
              error: function ( error) {
                console.log(error);
              },
            });
            //console.log(Conjunto_tecnicos,'Data set init');

            let pendientes = null;
            $.ajax({
              async: false,
              type: "POST",
              url: `Administracion/consentrado_SinInterpretar`,
              data: {
                id_radiologo,
                fecha_uno,
                fecha_fin,
              },
              dataType: "json",
              success: function (data) {
                //console.log(data,'this is a data');
                pendientes = data;
              },
              error: function (request, status, error) {
                console.log(jQuery.parseJSON(request.responseText).Message);
              },
            });

            //console.log(pendientes, "Lista de pendientes");

            tabla_Consentrados.clear();
            tabla_Consentrados.draw();
            
            let arregloGrafica=[];

            let Sumatotal = 0;

            let total_placas_interpretadas=0
            json.map(
              ({
                created,
                estudio,
                id_personal,
                nim,
                nim_sass,
                placas_subidas: placas,
                radiologo,
                honorario,
                total,
                id_estudio,
                id_htds_tecnico,
              }) => {
                let found = jsonEstudios.find(
                  (estudio) => estudio.id_estudio === id_estudio
                );
                placasRegla = parseInt(found?.placas);
                //console.table(found.placas);

                placas = parseInt(placas);

                let cuenta_placas = 0;
                let calculo = 0;
                total_placas_interpretadas+=placas;
                if (placas > placasRegla) {
                  cuenta_placas = placasRegla;
                  calculo = 1 * parseFloat(honorario);
                } else {
                  if (placas === placasRegla) {
                    cuenta_placas = placasRegla;
                    calculo = 1 * parseFloat(honorario);
                  } else {
                    if (placas < placasRegla) {
                      cuenta_placas = placas;
                      cuenta_placas = `${cuenta_placas} / Incompleto`;

                      let precioXplaca = honorario / found.placas;
                      calculo = precioXplaca * placas;
                      if (cuenta_placas < 0) {
                        cuenta_placas = 0;
                      }
                    }
                  }
                }                
                // console.log(Conjunto_tecnicos,'primer busqueda');
                let conjuntox = find_und(Conjunto_tecnicos, id_htds_tecnico);

                if (conjuntox[0]?.udn.udn_name != undefined && conjuntox[0]?.udn.udn_razonSocial != undefined) {
                  let encontrados = arregloGrafica.find(
                    (filial) => filial.udn === conjuntox[0]?.udn.udn_name
                  );
                  if (!encontrados?.udn) {
                    arregloGrafica.push({
                      udn:conjuntox[0]?.udn.udn_name,
                      udn_razonSocial:conjuntox[0]?.udn.udn_razonSocial,
                      total:0,
                      total_interpretados:0,
                      total_no_interpretados:0,
                    });
                    arregloGrafica.map(({udn},key)=>{
                      if (udn == conjuntox[0]?.udn.udn_name) {
                        arregloGrafica[key].total++;  
                        arregloGrafica[key].total_interpretados++;  
                      }
                    })
                  }else{
                    arregloGrafica.map(({udn},key)=>{
                      if (udn == conjuntox[0]?.udn.udn_name) {
                        arregloGrafica[key].total++;  
                        arregloGrafica[key].total_interpretados++;  
                      }
                    })
                  }
                }

                if (conjuntox[0]?.udn.udn_name == undefined && conjuntox[0]?.udn.udn_razonSocial == undefined) {
                  console.log(id_htds_tecnico,Conjunto_tecnicos);
                  // console.log(conjuntox[0]?.persona.nombre==undefined ,conjuntox[0]?.udn.udn_name , conjuntox[0]?.udn.udn_razonSocial);
                }

                //console.log(conjuntox[0]?.persona.nombre==undefined ,conjuntox[0]?.udn.udn_name , conjuntox[0]?.udn.udn_razonSocial);
                tabla_Consentrados.row
                  .add([
                    created,
                    //radiologo,
                    estudio,
                    nim_sass,
                    placas,
                    placasRegla,
                    `$ ${honorario} .00`,
                    `$ ${calculo}.00`,
                    //`Realizo : ${persona?.nombre} ,Puesto: ${persona?.puesto}, Razón social ${persona?.razonSoc}` ,
                    `${conjuntox[0]?.persona.nombre}`,
                    `${conjuntox[0]?.udn.udn_name}`,
                    `${conjuntox[0]?.udn.udn_razonSocial}`,
                    'Realizadas'
                    //'razonSoc'
                  ])
                  .draw(false);

                Sumatotal += parseFloat(calculo);
                //Sumatotal += parseFloat(total);
              }
            );
            document.getElementById("total").textContent = `$ ${Sumatotal}.00`;

            //REcorrido del json para el que no esta interpretado

            let total_placas_no_interpretadas=0
            pendientes.map(
              ({
                created,
                estudio,
                id_personal,
                nim,
                nim_sass,
                placas_subidas: placas,
                radiologo,
                honorario,
                total,
                id_estudio,
                id_htds_tecnico,
              }) => {
                let found = jsonEstudios.find(
                  (estudio) => estudio.id_estudio === id_estudio
                );
                placasRegla = parseInt(found?.placas);
                //console.table(found.placas);
                placas = parseInt(placas);

                let cuenta_placas = 0;
                let calculo = 0;
                //conteo de las placas
                total_placas_no_interpretadas+=placas;
                if (placas > placasRegla) {
                  cuenta_placas = placasRegla;
                  calculo = 1 * parseFloat(honorario);
                } else {
                  if (placas === placasRegla) {
                    cuenta_placas = placasRegla;
                    calculo = 1 * parseFloat(honorario);                    
                  } else {
                    if (placas < placasRegla) {
                      cuenta_placas = placas;
                      cuenta_placas = `${cuenta_placas} / Incompleto`;

                      let precioXplaca = honorario / found.placas;
                      calculo = precioXplaca * placas;
                      if (cuenta_placas < 0) {
                        cuenta_placas = 0;
                      }
                    }
                  }
                }                
                //console.log(Conjunto_tecnicos,'segundo busqueda');
                let conjuntox = find_und(Conjunto_tecnicos, id_htds_tecnico);                                        
                
                if (conjuntox[0]?.udn.udn_name != undefined && conjuntox[0]?.udn.udn_razonSocial != undefined) {
                  
                  let encontrados = arregloGrafica.find(
                    (filial) => filial.udn === conjuntox[0]?.udn.udn_name
                  );

                  if (!encontrados?.udn) {
                    arregloGrafica.push({
                      total:0,
                      total_interpretados:0,
                      total_no_interpretados:0,
                      udn:conjuntox[0]?.udn.udn_name,
                      udn_razonSocial:conjuntox[0]?.udn.udn_razonSocial
                    });
                    arregloGrafica.map(({udn},key)=>{
                      if (udn == conjuntox[0]?.udn.udn_name) {
                        arregloGrafica[key].total++; 
                        arregloGrafica[key].total_no_interpretados++;   
                      }
                    })
                  }else{
                    arregloGrafica.map(({udn},key)=>{
                      if (udn == conjuntox[0]?.udn.udn_name) {
                        arregloGrafica[key].total++;  
                        arregloGrafica[key].total_no_interpretados++;   
                      }
                    })
                  }
                }
                
                
                tabla_Consentrados.row
                  .add([
                    created,                    
                    estudio,
                    nim_sass,
                    placas,
                    placasRegla,
                    `$ ${honorario} .00`,
                    `$ ${0} .00`,
                    `${conjuntox[0]?.persona.nombre}`,
                    `${conjuntox[0]?.udn.udn_name}`,
                    `${conjuntox[0]?.udn.udn_razonSocial}`,
                    'No Realizadas'                    
                  ])
                  .draw(false);
              }
            );

            //console.table(arregloGrafica,'-----------');

            //placas_no_interpretadas
            //placas_interpretadas

            //mandamos las graficas
            //estudios_no_realizados;
           // estudios_realizados;
            Grafica_dona(json.length, pendientes.length);
            Cards_indicadoras(json.length,total_placas_interpretadas,pendientes.length,total_placas_no_interpretadas);
            generaChart(arregloGrafica);
            
          },
          complete: function (xhr, status) {
            $("#load").modal("close");
          },
        });
      },
    });
    ///////////////////////////////////////
  } else {
    Swal.fire("Alerta !", "Debes seleccionar un rango de fechas", "warning");
  }
}

async function fetchudn(id_htds_tecnico) {
  var requestOptions = {
    method: "GET",
    redirect: "follow",
  };
  const response = await fetch(
    `https://asesores.htds-humanly.com/datosEmp/${id_htds_tecnico}`,
    requestOptions
  );
  const udn = await response.json();

  return udn;
}

function find_und(json = [], id_empleado='') {
  //console.log(id_empleado,json);
  id_empleado = id_empleado.toUpperCase();

  //console.log(json,id_empleado,'metodo Find');
  let encontrado = 0;
  let respuesta = [];

  json.map(({ empleados, id, nombre: nombre2, razonSoc: razonSoc2 }) => {
    if (!encontrado) {
      empleados.map(
        ({
          aMaterno,
          aPaterno,
          idEmpleado,
          idPuesto,
          idSuc,
          nombre,
          puesto,
          razonSoc
        }) => {        
          idEmpleado = idEmpleado.toUpperCase();
          if (idEmpleado == id_empleado) {
            //console.log(`${idEmpleado} == ${id_empleado}`);
            respuesta.push({
              udn: {
                udn_name: nombre2,
                udn_razonSocial: razonSoc2,
              },
              persona: {
                nombre: `${nombre} ${aPaterno} ${aMaterno}`,
                razonSoc,
                puesto,
              },
            });
            encontrado = 1;
            return;
          }
        }
      );
    }
  });
 // console.table(respuesta);
  return respuesta;
  //console.log(respuesta);
}

function pruebasudn() {
  $.ajax({
    async: false,
    type: "get",
    url: `https://asesores.htds-humanly.com/datoSuc`,
    dataType: "json",
    success: function (data) {
      find_und(data, "L204");
    },
    error: function (request, status, error) {
      console.log(jQuery.parseJSON(request.responseText).Message);
    },
  });
}

function Grafica_dona(interpretadas, no_interpretadas) {
  var chart = c3.generate({
    bindto: "#chart",
    data: {
      columns: [
        ["interpretadas", interpretadas],
        ["no_interpretadas", no_interpretadas],
      ],
      names: {
        interpretadas: "Interpretados",
        no_interpretadas: "No Interpretados",
      },
      type: "donut",
      onclick: function (d, i) {
        console.log("onclick", d, i);
      },
    },
    donut: {
      title: "interpretaciones",
    },
  });
}

function Cards_indicadoras(estudios_realizados,placas_interpretadas,estudios_no_realizados,placas_no_interpretadas) {
  document.getElementById("panel_cards").innerHTML = `
                <div class="col s12 m6 l6 xl3">
                  <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                    <div class="padding-4">
                      <div class="row">
                        <div class="col s7 m7">
                          <i class="material-icons background-round mt-5">check_circle</i>
                          <p>Estudios Interpretados</p>
                        </div>
                        <div class="col s5 m5 right-align">
                          <!-- aqui vamos a mandar los totales de estudios realizados  -->
                          <h5 class="mb-0 white-text" id="estudios_realizados">${estudios_realizados}</h5>
                          <p class="no-margin">Totales Placas</p>
                          <!-- aqui vamos a mandar totales de placas interpretadas -->
                          <p id="placas_interpretadas">${placas_interpretadas}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              
                <div class="col s12 m6 l6 xl3">
                  <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                    <div class="padding-4">
                      <div class="row">
                        <div class="col s7 m7">
                          <i class="material-icons background-round mt-5">cancel</i>
                          <p>Estudios No interpretados</p>
                        </div>
                        <div class="col s5 m5 right-align">
                          <!-- aqui vamos a mandar los totales de estudios no realizados  -->
                          <h5 class="mb-0 white-text" id="estudios_no_realizados">${estudios_no_realizados}</h5>

                          <p class="no-margin">Totales Placas</p>
                          <!-- aqui vamos a mandar totales de placas no interpretadas -->
                          <p id="placas_no_interpretadas">${placas_no_interpretadas} </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
  
  `;
}




function generaChart(json=[]) {

  let etiqueta=['x'];
  let total_estudios =["total_estudios"];
  let interpretadas=["interpretadas"];
  let no_interpretadas =["no_interpretadas"];
  json.map(({total,total_interpretados,total_no_interpretados,udn})=>{
    etiqueta.push(udn)
    total_estudios.push(total);
    interpretadas.push(total_interpretados);
    no_interpretadas.push(total_no_interpretados);
  })
  let conjunto_datos = [        
    etiqueta,
    total_estudios,
    interpretadas,
    no_interpretadas
  ];

  var chart = c3.generate({
    bindto: "#chart2",
    data: {
      x: 'x',
      columns: conjunto_datos,
      names: {

      },      
      type: "bar",
      types: {
        data3: "spline",
        interpretadas: "line",
        no_interpretadas: "area",
      },
      groups: [        
      ],
      names: {
        'no_interpretadas':'No Interpretadas',
        'total_estudios':'Total Estudios'
      }
    },
    zoom: {
      enabled: true
    },
    /*subchart: {  //ESto funciona para poner miniatura 
      show: true
    },*/
    axis: {
      x: {
        label: {
          text: 'Unidades de Negocio',
          position: 'outer-center'
        },
        type: 'category' // this needed to load string x value
      },
      y: {
        label: {
          text: 'Numero de estudios',
          position: 'outer-center'
        },
      }
    },
  });





}
