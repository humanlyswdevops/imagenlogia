
function Nuevo_paciente(event) {
  event.preventDefault();
  let nombre = document.getElementById('nombre').value;
  let apellido_p = document.getElementById('apellido_p').value;
  let apellido_m = document.getElementById('apellido_m').value;
  let curp = document.getElementById('curp').value;
  let id_laboratorio = document.getElementById('id_laboratorio').value;
  
  if (requiered(nombre,'Nombre')&&requiered(apellido_p,'Apellido paterno')&&requiered(apellido_m,'Apellido materno')&&requiered(curp,'CURP')) {
    $.ajax({    
      url : 'Paciente/Save',    
      data : {nombre,apellido_m,apellido_p,curp,id_laboratorio},
      type : 'POST',
      dataType : 'json',
      success : function(json) {
        let obj =json.data;         
        respuesta(json);
        $('#Modal_cliente').modal('close');        
        let {id,nombre}=obj;      
        console.log(obj)  
        let option =`<option value="${id}">${nombre}</option>`;
        console.log({id,nombre});                      
        // validacion que verifica si exite en DOM 
        if(document.getElementById('paciente_text')){
          document.getElementById('paciente_text').innerText=nombre;
          document.getElementById('paciente_id').value=id;
          // deja ver el template donde se ve el contenido de todo el expediente
          document.getElementById('template').className='col s10';
          // oculta el modulo de registro o seleccion de usuario
          $('#panel_cliente').hide();          
          //habilitamos el select para que pueda agregar estudios
          document.getElementById('estudio').disabled=false;
        }
      },
      error : function(xhr, status) {
        alert('Disculpe, existió un problema');
      }
    });
  }
}