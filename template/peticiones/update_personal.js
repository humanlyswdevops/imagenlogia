function editar_usuario(id_personal) {
  $.ajax({
    url: '../Usuarios/Get',
    type: 'POST',
    data: {
      id_personal
    },
    dataType: 'json',
    success: (json) => {
      $('#modal_edita_usser').modal('open');
      let {
        cedulas,
        personal
      } = json;
      //console.log(cedulas);
      //console.log(personal);
      let {
        nombre,
        apellido_paterno,
        apellido_materno,
        id_empleado,
        id_personal,
        privilegio,
        cobro_estudio,
        cobro_interpretacion
      } = personal;
console.log(personal,'people')
      document.getElementById('nombre').value = nombre;
      document.getElementById('nombre').focus();
      document.getElementById('apellido_p').value = apellido_paterno;
      document.getElementById('apellido_p').focus();
      document.getElementById('apellido_m').value = apellido_materno;
      document.getElementById('apellido_m').focus();
      document.getElementById('id_empleado').value = id_empleado;
      document.getElementById('id_empleado').focus();
      document.getElementById('id_personal').value = id_personal;
      // document.getElementById('text_cedula').focus();
      document.getElementById('privilegio').value = privilegio;
      document.getElementById('cobro_estudio').value=cobro_estudio;
      document.getElementById('cobro_estudio').focus();
      document.getElementById('cobro_interpretacion').value=cobro_interpretacion;
      document.getElementById('cobro_interpretacion').focus();
        
     
      document.getElementById('lista_cedulas').innerHTML = '';
      cedulas.map((item) => {
        //console.log(item);
        let {
          nombre,
          id_cedulas
        } = item;

        $("#lista_cedulas").append(`
            <li class="collection-item" id="item_${id_cedulas}">
              ${nombre}
              <input type="hidden" name="cedula[]" value="${id_cedulas}_${nombre}" /> 

              <button style="width: 21px;height: 21px;" class="btn-floating mb-1 waves-effect waves-light right" onclick="EliminaCedula(event,'lista_cedulas','item_${id_cedulas}',${id_cedulas})">
                <i style="display: block;margin-top: -8px;font-size: 15px;" class="material-icons">clear</i>
              </button>      
            </li>        
          `);
      });
    },
    error: (err) => {
      //console.log(err);
    }
  });
}

function id_firma_pesonal(id) {
  document.getElementById('id_user_firma').value=id;
  $('#modal_sube_firma').modal('open');
}

$('#firma').on('change', (event)=>{
  event.preventDefault();
  let oData = new FormData(document.getElementById('firma_frm'));  
  oData.append('id',document.getElementById('id_user_firma').value);
  
  $.ajax({
    url: '../Usuarios/Firma',
    data:oData ,
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      document.getElementById('firma').value=null;

      
      setTimeout('document.location.reload()',1000);
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de actualizar firma`, 'warning');      
      document.getElementById('curp').value=null;
      console.log(err)
    }
  });
});

function EliminaCedula(event, lista, id, id_cedula) {
  event.preventDefault();
  swal({
    title: "Alerta",
    text: `¿En verdad quieres borrar este numero de cedula?`,
    icon: 'warning',
    dangerMode: true,
    buttons: {
      cancel: 'No',
      delete: 'Si'
    }
  }).then(function (willDelete) {
    if (willDelete) {
      $.ajax({
        url: '../Usuarios/Elimina_Cedula',
        type: 'POST',
        data: {
          id_cedula
        },
        dataType: 'json',
        success: (json) => {
          console.log(json)
          let padre = document.getElementById(lista);
          let hijo = document.getElementById(id);
          padre.removeChild(hijo);
          respuesta(json);
        },
        error: (err) => {
          console.log(err)
        }
      });
    }
  });
}

function nueva_cedula(event) {
  event.preventDefault()
  let id_personal = document.getElementById('id_personal').value;
  let cedula = document.getElementById('text_cedula').value;
  if (cedula != '' && cedula != null) {
    $.ajax({
      url: '../Usuarios/Nueva_Cedula',
      type: 'POST',
      data: {
        cedula,
        id_personal
      },
      dataType: 'json',
      success: (json) => {
        //console.log(json)
        respuesta(json);
        try {
          let {
            data: {
              id,
              nombre
            }
          } = json;

          $("#lista_cedulas").append(`
              <li class="collection-item" id="item_${id}">
                ${nombre}
                <input type="hidden" name="cedula[]" value="${id}_${nombre}" /> 
                <button style="width: 21px;height: 21px;" class="btn-floating mb-1 waves-effect waves-light right" onclick="EliminaCedula(event,'lista_cedulas','item_${id}',${id})">
                  <i style="display: block;margin-top: -8px;font-size: 15px;" class="material-icons">clear</i>
                </button>      
              </li>        
            `);
        } catch (error) {
          //console.log(error)
        }

      },
      error: (err) => {
        console.log(err)
      }
    });
  }else{
    swal('Advertencia', `Debes de escribir un numero de cedula `, 'warning');
  }
}

function detalle_personal(event,url) {
  event.preventDefault()
  let id_personal = document.getElementById('id_personal').value;
  let rutadetalle=`${url}/${id_personal}`;
  // alert(rutadetalle);
  location.href=rutadetalle;
}

$("#frm_update_usuario").submit(function (event) {
  event.preventDefault();

  $.ajax({
    url: '../Usuarios/Update',
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {
      console.log(json);
      respuesta(json);
      setTimeout(() => {        
        window.location.reload();  
      }, 1500);
    },
    error: (err) => {
      swal('Advertencia', `El usuario ya existe o la contraseña intente con otra`, 'warning');
      //console.log(err);

    }
  });
});



function get_Cedulas(id_personal) {
  $.ajax({
    url: '../Usuarios/Get',
    type: 'POST',
    data: {
      id_personal
    },
    dataType: 'json',
    success: (json) => {
      $('#modal_edita_usser').modal('open');
      let {
        cedulas        
      } = json;                    
      document.getElementById('lista_cedulas').innerHTML = '';
      cedulas.map((item) => {
        //console.log(item);
        let {
          nombre,
          id_cedulas
        } = item;

        $("#lista_cedulas").append(`
            <li class="collection-item" id="item_${id_cedulas}">
              ${nombre}
              <input type="hidden" name="cedula[]" value="${id_cedulas}_${nombre}" /> 

              <button style="width: 21px;height: 21px;" class="btn-floating mb-1 waves-effect waves-light right" onclick="EliminaCedula(event,'lista_cedulas','item_${id_cedulas}',${id_cedulas})">
                <i style="display: block;margin-top: -8px;font-size: 15px;" class="material-icons">clear</i>
              </button>      
            </li>        
          `);
      });
    },
    error: (err) => {
      //console.log(err);
    }
  });
}