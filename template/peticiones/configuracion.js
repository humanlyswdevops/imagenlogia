var tableTool;
var snow;
function habilita_editor(clase_OR_id = '.editor') {
  console.log('habilitando editor ...')
  
  var toolbarOptions = [
    // ['image'],
    ["bold", "italic", "underline",'arial'],
    [{
      'header': 1
    },{
      'header': 2
    }],
    [{
      'list': 'ordered'
    }, {
      'list': 'bullet'
    }],
    [{
      'indent': '-1'
    }, {
      'indent': '+1'
    }],
    [{
      'direction': 'rtl'
    }],
    [{
      'size': ['small', false, 'large', 'huge']
    }],
    [{
      'header': [1, 2, 3, 4, 5, 6, false]
    }],
    [{
      'color': []
    }, {
      'background': []
    }],
    [{
      'font': []
    }],
    [{
      'align': []
    }],
    
    //[{'table':true}] 
  ];

  var FontStyle = Quill.import('attributors/style/font');
  FontStyle.whitelist =[false, 'Arial, Helvetica, sans-serif', 'Georgia, serif', 'Impact, Charcoal, sans-serif', 'Tahoma, Geneva, sans-serif', 'Times New Roman, Times, serif, -webkit-standard', 'Verdana, Geneva, sans-serif'];
  Quill.register(FontStyle, true);



  Quill.register({
    'modules/tableUI': quillTableUI.default
  }, true);      

  //ve que no duplique el editor
  var quill = $(clase_OR_id + ' .ql-editor');
  if (quill.length == 0) {

    snow = new Quill(`${clase_OR_id}`, {
      theme: 'snow',
      modules: {
        toolbar: toolbarOptions,
        table: true,
        tableUI: true
      }
    }); 


    //agrega a las herramientas la tabla
    tableTool = snow.getModule('table');  
    let template=`
    <samp class="ql-formats" id="item-tabla" style="margin-top: 4px;">
      <svg onclick="creaTabla()" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" id="tabla"
        width="20" height="20"
        viewBox="0 0 171 171"
        style=" fill:#000000;"><defs><linearGradient x1="74.05013" y1="20.56988" x2="96.94988" y2="150.43013" gradientUnits="userSpaceOnUse" id="color-1_j1EvONaCTNHF_gr1"><stop offset="0" stop-color="#ffffff"></stop><stop offset="0.293" stop-color="#ffffff"></stop><stop offset="0.566" stop-color="#ffffff"></stop><stop offset="0.832" stop-color="#ffffff"></stop><stop offset="1" stop-color="#ffffff"></stop></linearGradient><linearGradient x1="72.81394" y1="13.55888" x2="98.18606" y2="157.44113" gradientUnits="userSpaceOnUse" id="color-2_j1EvONaCTNHF_gr2"><stop offset="0" stop-color="#f15a00"></stop><stop offset="1" stop-color="#e67e22"></stop></linearGradient></defs><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g><path d="M142.50713,142.39669l-113.90381,0.11044l-0.11044,-113.90381l113.90025,-0.11044z" fill="url(#color-1_j1EvONaCTNHF_gr1)"></path><path d="M149.51456,28.48575c-0.00356,-3.91163 -3.20625,-7.11075 -7.11788,-7.11075c-0.00356,0 -0.00356,0 -0.00713,0l-113.90381,0.11044c-3.91519,0.00356 -7.11431,3.20981 -7.11075,7.125l0.11044,113.90381c0.00356,3.91162 3.20625,7.11075 7.11788,7.11075c0.00356,0 0.00356,0 0.00713,0l113.90381,-0.11044c3.91519,-0.00356 7.11431,-3.20981 7.11075,-7.125zM71.25,99.75v-28.5h28.5v28.5zM99.75,110.4375v28.5h-28.5v-28.5zM60.5625,99.75h-28.5v-28.5h28.5zM110.4375,71.25h28.5v28.5h-28.5zM138.9375,60.5625h-28.5v-28.5h28.5zM99.75,32.0625v28.5h-28.5v-28.5zM60.5625,32.0625v28.5h-28.5v-28.5zM32.0625,110.4375h28.5v28.5h-28.5zM110.4375,138.9375v-28.5h28.5v28.5z" fill="url(#color-2_j1EvONaCTNHF_gr2)"></path></g></g>
      </svg>
    </samp>
    `;    
    $('.ql-toolbar').append(template)
  }
}

function habilita_editor_existente(editor,id){
  habilita_editor(editor);
  document.getElementById(`${id}`).focus();
  //console.log(id)

  $(`#${id}`).addClass('active')
}
/*


`
<div class="s12">
  <div class="input-field col s6">
    <input id="title_interpretacion" name="title_interpretacion" type="text" class="validate">
    <label for="title_interpretacion">Titulo del machote</label>
  </div>
</div>
`*/

function creaTabla(){  
  $('#modal').modal('open');
}
if (document.getElementById('insert-table')) {
  document.querySelector('#insert-table').addEventListener('click', function() {    
    let filas= document.getElementById('filas').value;
    let columnas= document.getElementById('columnas').value;
    if (filas!='' && columnas!='' ) {
      filas=parseInt(filas);
      columnas=parseInt(columnas);
      if (filas!=0 || columnas!=0) {
        snow.focus()
        tableTool.insertTable(columnas,filas);
        $('#modal').modal('close');
        document.getElementById('filas').value = '';
        document.getElementById('columnas').value = ''
      }    
    }
  });
}





function Nueva_plantillas() {
  document.getElementById('title_machote').innerHTML=`
    <div class="s12">
      <div class="input-field col s6">
        <input id="title_interpretacion" name="title_interpretacion" type="text" class="validate" autocomplete="off">
        <label for="title_interpretacion">Titulo del machote</label>
      </div>
    </div>
  `;

  document.getElementById('plantilla').innerHTML= `
  
    <div class="s12">
      <!-- En esta parte va el editor -->
      <div id="new_body" class="editor">

      </div>
      <button class="btn primario center mt-2 ml-2 mb-2" onclick="Nueva_pre_interpretacion()">
        Guardar
      </button>
    </div>`;
  main_editor();
}
function main_editor() {
  habilita_editor();
}

function traer_interpretaciones() {
  $.ajax({
    url: 'Configuracion/Get_interpretacionesBody',
    type: 'POST',
    dataType: 'json',
    success: function (json) {
      console.log(json);
      json.map((interpretacion, key) => {
        // console.log(interpretacion);
        let {
          contenido,
          estatus,
          id_body_interpretacion,
          id_personal,
          title
        } = interpretacion;
        templatePre_interpretaciones(key,contenido,estatus,id_body_interpretacion,id_personal,title);         
      });
    },
    error: function (xhr, status) {
      alert('Disculpe, existió un problema');
    }
  });
}

function templatePre_interpretaciones(key = 0, body = '', estatus='0',id_body_interpretacion,personal_id,title) {
  let color = '';
  if (estatus === '0') {
    color = 'rgb(229,55,63)';

  } else {
    color = 'rgb(0,204,106)';

  }
  let template = `
    <li >
    <div class="collapsible-header iconos" onclick="habilita_editor_existente('#editor${key+1}','label-${id_body_interpretacion}')">
      <i class="material-icons ">content_paste</i>
      ${key+1}
      ${title}
      
      <span class="badge " title="no activo">
        fiber_manual_record </i>
      </span>
    </div>
    <div class="collapsible-body">

      <div class="s12">
        <div class="input-field col s12">
          <input id="title_interpretacion-${id_body_interpretacion}" value="${title}" name="title_interpretacion-${id_body_interpretacion}" type="text" class="validate">
          <label id="label-${id_body_interpretacion}" for="title_interpretacion-${id_body_interpretacion}">Titulo del machote</label>
        </div>
      </div>  

      <br><br><br>
      <div class="container" style="z-index:88;">             

        <div id="editor${key+1}" class="col s12">
          ${body}
        </div>

        <button class="btn primario mt-2" onclick="modificar_pre_interpretacion(${id_body_interpretacion},${personal_id},'editor${key+1}')" >
          Modificar
        </button>  

        <button class="btn primario mt-2" onclick="activarBody(${id_body_interpretacion},${personal_id})" >
          activa
        </button>
      </div>        
    </div>
    </li>`;
  $("#lista_interpretacion_pre").append(template);

}

function modificar_pre_interpretacion (id,personal,editor) {
  
  let edi=editor;  
  ed = document.getElementById(`${edi}`).children;
  let HtmlEditor = ed[0].innerHTML;
  
  let titulo = document.getElementById(`title_interpretacion-${id}`).value;

  $.ajax({
    url: 'Configuracion/Modificar_pre_interpetacion',
    type: 'POST',
    data:{
      id,personal,HtmlEditor,titulo
    },
    dataType: 'json',
    success: function (json) {
      console.log(json);
      respuesta(json);
      setTimeout(() => {
        location.reload();                
      }, 1000);
    },
    error: function (xhr, status) {
      alert('Disculpe, existió un problema');
    }
  });
}

function activarBody(id,personal) {
  $.ajax({
    url: 'Configuracion/Activar',
    type: 'POST',
    data:{
      id,personal
    },
    dataType: 'json',
    success: function (json) {
      console.log(json);
      respuesta(json);
      setTimeout(() => {
        location.reload();                
      }, 1000);
    },
    error: function (xhr, status) {
      alert('Disculpe, existió un problema');
    }
  });
}

function Nueva_pre_interpretacion() {
  ed = document.getElementById(`new_body`).children;

  let HtmlEditor = snow.getSemanticHTML()
  let title = document.getElementById('title_interpretacion').value;

  if (title!='') {
    $.ajax({
      url: 'Configuracion/Nueva',
      type: 'POST',    
      data:{HtmlEditor,title},
      dataType: 'json',
      success: function (json) {
        console.log(json);
        respuesta(json);
        setTimeout(() => {
          location.reload();
        }, 1000);
      },
      error: function (xhr, status) {
        alert('Disculpe, existió un problema');
      }
    });
  }else{
    alert('Error no puso titulo del machote ')
  }

  
}