if (document.getElementById('lienzo_dicom')) { 
  $('#lienzo_dicom').hide();
}
function traer_interpretacion(id, ruta, rutaBase, name = "") {
  $('#lienzo_dicom').hide();
  $('#consulta').show();
  $.ajax({
    url: ruta,
    data: {
      id,
    },
    type: "POST",
    dataType: "json",
    success: (json) => {
      console.log(json);
      let { texto } = json;

      if (texto.length <= 300) {
        //console.log(texto);
        let ruta = `${rutaBase}uploads/interpretacionpdf/${name}`;
        let template = `
        <embed src="${ruta}#toolbar=0" type="application/pdf" width="100%" height="500px" />
        `;
        document.getElementById("consulta").innerHTML = template;
      } else {
        document.getElementById("consulta").innerHTML = texto;
      }
    },
    error: (err) => {
      console.log(err);
    },
  });
}

function handle_viewDicom(ruta) {  
  $('#lienzo_dicom').show();
  $('#consulta').hide();
  let nueva_ruta =`https://imagenologia.dynpbx.mx/Imagenologia2${ruta}`;  
  let cadenaCorregida = nueva_ruta.substring(0, nueva_ruta.length - 1);  
  Ver_dicom(cadenaCorregida);
}

function ActualizaInterpretacion(id) {
  let bandera = document.querySelector("#bandera").value;

  var data = new FormData();
  data.append("id", id);
  data.append("bandera", bandera);

  if (bandera === "Isfile") {
    let documento = document.getElementById("interpretacionf");
    data.append("interpretacionf", documento.files[0]);
  } else if (bandera === "Istexto") {
    let contenido = "";
    contenido +=
      document.getElementById("encabezado").innerHTML +
      '<div id="salto"></div>';
    let sacaHijo = document.getElementById("consulta1").children;

    let body = snow.getSemanticHTML();
    contenido += body;
    contenido += document.getElementById("pie").innerHTML;
    data.append("contenido", contenido);
  }

  $.ajax({
    url: "../../Interpretacion/UpdateInterpretacion",
    data: data,
    processData: false,
    contentType: false,
    type: "POST",
    dataType: "json",
    success: (json) => {
      console.log(json);
      respuesta(json);
    },
    error: (err) => {
      console.log(err);
    },
  });
}

function descargar_interpretacion(id, ruta) {
  $.ajax({
    url: ruta,
    data: {
      id,
    },
    type: "POST",
    dataType: "json",
    success: (json) => {
      console.log(json);
    },
    error: (err) => {
      console.log(err);
    },
  });
}

// esto manda el correo al paciente
$("#frm_manda_acceso").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: "../../Correo",
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: "POST",
    dataType: "json",
    success: function (json) {
      // console.log(json);
      // respuesta(json);
    },
    error: (err) => {
      console.log(err);
      swal("Satisfecho", "Correo mandado!", "success");

      // if (err.responseText===''||err.responseText==='ok') {
      //   $('#modal12').modal('close');
      //   swal("Satisfecho", "Correo mandado!", "success");
      // }else{
      //   $('#modal12').modal('close');
      //   swal("Error", "No se pudo mandar el correo", "error");
      // }
    },
  });
});

function IniThabilitar() {
  habilita_editor("#interpretacion");
}
