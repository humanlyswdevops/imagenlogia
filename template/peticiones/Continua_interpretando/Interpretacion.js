function existen_datos(ruta) {
  let id_contenido = document.getElementById("id_contenido").value;

  //alert(id_contenido);
  $.ajax({
    type: "POST",
    url: `${ruta}/get_interpretacion`,
    data: { id_contenido },
    dataType: 'json',
    success: function (data) {
      console.log(data,'------------------------------------------------------------')
      
      $('.ql-editor').html(data[0].interpretacion);
      let interpretacion = data[0].interpretacion;
      
      
      var [head,body] = interpretacion.split('<div id="salto">');
      
      


 
      let cuerpo = body.replace(/<br\/>/g, "");

      let data_set = cuerpo.split('Numero de cedula profesional:');

      console.log(data_set);                

      $('#encabezado').html(head);
      $('.ql-editor').html(data_set[0]);


      
      
      /*
      let head = data[0].interpretacion;
      let data =head.split('<div >');
      console.log(data)
  */    
    },
  });
}

$(document).on("click", ".carousel-item.active", function () {
  $("#modal-galeria").modal("open");
  let img = $(this).find("img").attr("src");
  $("#imagen_panel").html(
    `<img width="100%" src="${img}" alt="imagen tomada de la interpretacion"></img>`
  );
});
// $(".active").on("click",function(){alert(0)})
// $(".carousel-item .active").on("click",()=>{alert(0)})

var tableTool;
var snow;
var Is_fullDiv = false;
function habilita_editor(clase_OR_id = ".editor") {
  var toolbarOptions = [
    // ['image'],
    ["bold", "italic", "underline"],
    [
      {
        header: 1,
      },
      {
        header: 2,
      },
    ],
    [
      {
        list: "ordered",
      },
      {
        list: "bullet",
      },
    ],
    [
      {
        indent: "-1",
      },
      {
        indent: "+1",
      },
    ],
    [
      {
        direction: "rtl",
      },
    ],
    [
      {
        size: ["small", false, "large", "huge"],
      },
    ],
    [
      {
        header: [1, 2, 3, 4, 5, 6, false],
      },
    ],
    [
      {
        color: [],
      },
      {
        background: [],
      },
    ],
    [
      {
        font: [],
      },
    ],
    [
      {
        align: [],
      },
    ],

    //[{'table':true}]
  ];

  Quill.register(
    {
      "modules/tableUI": quillTableUI.default,
    },
    true
  );

  //ve que no duplique el editor
  var quill = $(clase_OR_id + " .ql-editor");
  if (quill.length == 0) {
    snow = new Quill(`${clase_OR_id}`, {
      theme: "snow",
      modules: {
        toolbar: toolbarOptions,
        table: true,
        tableUI: true,
      },
    });

    //agrega a las herramientas la tabla
    tableTool = snow.getModule("table");
    let template = `
    <samp class="ql-formats" id="item-tabla" style="margin-top: 4px;">
      <svg onclick="creaTabla()" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" id="tabla"
        width="20" height="20"
        viewBox="0 0 171 171"
        style=" fill:#000000;"><defs><linearGradient x1="74.05013" y1="20.56988" x2="96.94988" y2="150.43013" gradientUnits="userSpaceOnUse" id="color-1_j1EvONaCTNHF_gr1"><stop offset="0" stop-color="#ffffff"></stop><stop offset="0.293" stop-color="#ffffff"></stop><stop offset="0.566" stop-color="#ffffff"></stop><stop offset="0.832" stop-color="#ffffff"></stop><stop offset="1" stop-color="#ffffff"></stop></linearGradient><linearGradient x1="72.81394" y1="13.55888" x2="98.18606" y2="157.44113" gradientUnits="userSpaceOnUse" id="color-2_j1EvONaCTNHF_gr2"><stop offset="0" stop-color="#f15a00"></stop><stop offset="1" stop-color="#e67e22"></stop></linearGradient></defs><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,171.98813v-171.98813h171.98813v171.98813z" fill="none"></path><g><path d="M142.50713,142.39669l-113.90381,0.11044l-0.11044,-113.90381l113.90025,-0.11044z" fill="url(#color-1_j1EvONaCTNHF_gr1)"></path><path d="M149.51456,28.48575c-0.00356,-3.91163 -3.20625,-7.11075 -7.11788,-7.11075c-0.00356,0 -0.00356,0 -0.00713,0l-113.90381,0.11044c-3.91519,0.00356 -7.11431,3.20981 -7.11075,7.125l0.11044,113.90381c0.00356,3.91162 3.20625,7.11075 7.11788,7.11075c0.00356,0 0.00356,0 0.00713,0l113.90381,-0.11044c3.91519,-0.00356 7.11431,-3.20981 7.11075,-7.125zM71.25,99.75v-28.5h28.5v28.5zM99.75,110.4375v28.5h-28.5v-28.5zM60.5625,99.75h-28.5v-28.5h28.5zM110.4375,71.25h28.5v28.5h-28.5zM138.9375,60.5625h-28.5v-28.5h28.5zM99.75,32.0625v28.5h-28.5v-28.5zM60.5625,32.0625v28.5h-28.5v-28.5zM32.0625,110.4375h28.5v28.5h-28.5zM110.4375,138.9375v-28.5h28.5v28.5z" fill="url(#color-2_j1EvONaCTNHF_gr2)"></path></g></g>
      </svg>
    </samp>
    `;
    $(".ql-toolbar").append(template);
  }
}
function creaTabla() {
  $("#modal").modal("open");
}
document.querySelector("#insert-table").addEventListener("click", function () {
  let filas = document.getElementById("filas").value;
  let columnas = document.getElementById("columnas").value;
  if (filas != "" && columnas != "") {
    filas = parseInt(filas);
    columnas = parseInt(columnas);
    if (filas != 0 || columnas != 0) {
      snow.focus();
      tableTool.insertTable(columnas, filas);
      $("#modal").modal("close");
      document.getElementById("filas").value = "";
      document.getElementById("columnas").value = "";
    }
  }
});

function expandir() {
  Is_fullDiv = !Is_fullDiv;
  if (Is_fullDiv) {
    document.getElementById("hoja").className = "card full";
  } else {
    document.getElementById("hoja").className = "card";
  }
}

function descargarImagen(ruta) {
  let canva = document.getElementById("dicomImage");
  canva = canva.children;
  let imagen = canva[0].toDataURL();

  let toma = $("#toma").val();
  let id_contenido = $("#id_contenido").val();

  var formData = new FormData();

  formData.append("imagen", imagen);
  formData.append("toma", toma);
  formData.append("id_contenido", id_contenido);
  $.ajax({
    type: "POST",
    url: ruta,
    data: formData,
    contentType: "multipart/form-data",
    dataType: "json",
    cache: false,
    contentType: false,
    processData: false,
    success: function ({ status, msg }) {
      let id = document.querySelector("#id_contenido").value;
      let rutaSube = document.querySelector("#ruta_sabeImg").value;
      $.ajax({
        type: "POST",
        url: rutaSube,
        data: { id },
        dataType: "json",
        success: function (data) {
          $("#carousel").html("");
          let template = ``;
          data.map((item) => {
            let { imagen } = item;

            document.querySelector("#carousel").innerHTML = `
              
                <img src="${imagen}">
              
            `;
          });
          console.log();

          $("#carousel").html(template);
        },
      });
    },
  });
}

function abre_editor() {
  $("#modal1").modal("open");
}

function crearHead($estudio, id) {
  let template = `
  <br>
  <div class="card-alert card cyan">
    <div class="card-content white-text">
      <span class="card-title white-text darken-1">
        Estudios : ${$estudio}
      </span>
    </div>
    <div class="card-action cyan darken-1">        
      <!--<button class="btn secundario" onclick="add_newEditor(editor-${id})">
        Agregar otra interpretación
      </button>-->
      <br/>
    </div>    
  </div>
  <div class="card " id="hoja-${id}">
  </div>
  `;
  $("#views").append(template);
}

function creaCuerpo_interpretacion(id) {
  let template = `
        <p class="escalas">
          <label>
            <input type="checkbox" id="toggleVOILUT${id}" />
            <span class="check-escalas">Escala de grises</span>
          </label>
        </p>
        <!-- no quitar !!!! -->
        <input type="checkbox" id="toggleModalityLUT${id}" />
        <br>

        <div class="row">
          <div class="col s12 m12 l6 xl6">
            <!-- aquí va el div donde se va a pintar la dicom  -->
            <div style="width:100%;height:650px;position:relative;color: white;display:inline-block;border-style:solid;border-color:black;" oncontextmenu="return false" class='disable-selection noIbar' unselectable='on' onselectstart='return false;' onmousedown='return false;'>
              <div id="dicomImage${id}" style="width:100%;height:600px;top:0px;left:0px; position:absolute">
              </div>
            </div>
            <!-- aquí va el listado de las dicoms  -->
            <div class="row" id="linkDicoms-${id}">              
            </div>
          </div>

          <!-- aquí va el editor de texto  -->
          <div class="col s12 m12 l6 xl6 center" id="editor-${id}")>
          </div>  
        </div>  
        `;
  $(`#hoja-${id}`).append(template);
  $(`#editor-${id}`).append(` <div>
                                <div class="editor${id}">                                 
                                </div> 
                              </div>
                              <br><br>
                              `);

  habilita_editor(`.editor${id}`);
}

function crea_listado_dicom(id = 0, contador = 0, ruta = "ruta_no_encontrada") {
  let template = `
              <div onclick="Ver_dicom('${ruta}',${id})" class="col s4 m2 l2 bg-primario dicom-item">
                <span class="material-icons cast">cast</span> 
                ${contador}
              </div>
              `;
  $(`#linkDicoms`).append(template);
}

function crea_listado_dicom_descarga(
  id = 0,
  contador = 0,
  ruta = "ruta_no_encontrada"
) {
  let template = `
                  <div onclick="descargra_img_dicom('${ruta}')" class="col s4 m2 l2 bg-primario dicom-item descarga">
                    <span class="material-icons leng-icono">
                      cloud_download
                    </span>
                    ${contador}
                  </div>
                  `;

  $(`#linkDicoms_descarga`).append(template);
}

function descargra_img_dicom(src) {
  let a = document.createElement("a");
  a.download = true;
  a.target = "_blank";
  a.href = src;

  a.click();
}

function add_newEditor() {
  TotalEditores++;
  $(`#panel_editores`).append(` <br><br>
                        <div class="input-field col s12">
                          <input id="titulo-${TotalEditores}" name="titulo-${TotalEditores}" type="text" class="validate">
                          <label for="titulo-${TotalEditores}">Titulo de la interpretación.</label>
                        </div>
                        <div class="editor" id="editor-${TotalEditores}">
                        </div>
                              `);
  habilita_editor(`#editor-${TotalEditores}`);
}

function prospone() {
  let interpretacionDocument = document.getElementById("interpretacion").value;
}


function terminar_interpretacion(seguimiento = 0, ruta = "") {
  let interpretacionDocument = document.getElementById("interpretacion").value;
  let Is_interpretacion_pdf = false;
  var data = new FormData();
  if (interpretacionDocument !== "") {
    Is_interpretacion_pdf = true;
  }

  let id_toma_muestra = document.getElementById("toma").value;

  let obj = [];
  for (let i = 1; i <= TotalEditores; i++) {
    let titulo = document.getElementById(`titulo-${i}`).value;

    let HtmlEditor = snow.getSemanticHTML();

    let head = document.getElementById("encabezado").innerHTML;
    let pie = document.getElementById("pie").innerHTML;
    let documento = head + "<br/><br/><br/>" + HtmlEditor + pie;
    obj.push({
      title: titulo,
      interpretacion: documento,
    });
  }
  console.log(obj);

  let interpretacion = JSON.stringify(obj);
  let id_contenido = document.getElementById("id_contenido").value;
  let bandera = 0;

  /** status
   *  Con esto sabemos el status de la toma
   * 2 finalizado
   * 3 no se concluyo
   * La validacion se hacer si el objeto no tiene ya elementos es 3 o si tiene mas de uno es 3
   */

  let status = 0;
  if (JsonEstudios.length > 0) {
    bandera = 1;
    status = 3;
  } else {
    status = 2;
  }

  data.append("interpretacion", interpretacion);
  data.append("id_contenido", id_contenido);
  data.append("bandera", bandera);
  data.append("JsonEstudios", JsonEstudios);
  data.append("JsonEstudios_Str", JsonEstudios_Str);
  data.append("id_toma_muestra", id_toma_muestra);
  data.append("status", status);
  if (Is_interpretacion_pdf) {
    let interpretacion = document.getElementById("interpretacion");
    console.log(interpretacion.files[0]);

    data.append("interpretacion", interpretacion.files[0]);
  }

  //console.log(data)

  /***
   * 
   * {
      interpretacion,
      id_contenido,
      bandera,
      JsonEstudios,
      JsonEstudios_Str,
      id_toma_muestra,
      status
    }
   * 
   */
  $.ajax({
    url: `${ruta}/Save`,
    data: data,
    type: "POST",
    processData: false, // tell jQuery not to process the data
    contentType: false,
    dataType: "json",
    success: (json) => {
      console.log(json);
      let { msg, status, url, log } = json;
      console.log(log, "******************************");
      swal({
        text: `${msg}`,
        icon: `${status}`,
        dangerMode: true,
      }).then((willDelete) => {
        window.location.href = url;
      });
    },
    error: (err) => {
      console.log(err);
    },
  });
}

function formato_fecha(fecha) {
  fecha.trim();
  let date = "";
  let mes = "";
  let dia = "";
  for (let i = 0; i < fecha.length; i++) {
    let element = fecha[i];
    if (i >= 0 && i <= 3) {
      date += element;
    } else {
      if (i > 3 && i <= 5) {
        mes += element;
      } else {
        dia += element;
      }
    }
  }
  fecha = `${dia}/${mes}/${date}`;
  return fecha;
}

function Get_sexo(item) {
  if (item == "M") {
    return "Masculino";
  } else {
    return "Femenino";
  }
}

function Get_nombre(nombre) {
  let conjunto = nombre.split("^");
  return conjunto[0] + " " + conjunto[1];
}

function Formato_interpretacion() {
  let fecha = moment(Date.now()).format("D/MMMM/YYYY");
  $(".documento_fecha").text(fecha);
}

function gira_dicom(giro) {
  switch (giro) {
    case 90:
      document.getElementById("dicomImage").className = "giro-90";
      break;
    case 180:
      document.getElementById("dicomImage").className = "giro-180";
      break;
    case 270:
      document.getElementById("dicomImage").className = "giro-270";
      break;
    case 360:
      document.getElementById("dicomImage").className = "giro-360";
      break;
  }
}

function openFullscreen() {
  let elem = document.getElementById("full");
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
    oculta_botones();
  } else if (elem.webkitRequestFullscreen) {
    /* Safari */
    elem.webkitRequestFullscreen();
    oculta_botones();
  } else if (elem.msRequestFullscreen) {
    /* IE11 */
    elem.msRequestFullscreen();
    oculta_botones();
  }
}

document.addEventListener("keydown", function (event) {
  if (event.keyCode === 27) {
    oculta_botones();
    oculta_botones();
  }
});

function oculta_botones(ocultar = "no") {
  if (ocultar == "no") {
    document.getElementById("noventaGrados").className = "btn ml-1 mr-1";
    document.getElementById("docientos_setenta").className = "btn ml-1 mr-1";
  } else {
    if ("si") {
      document.getElementById("noventaGrados").className = "d-none";
      document.getElementById("docientos_setenta").className = "d-none";
    }
  }
}

function tutorial() {
  $("#modal").modal("open");
}

// esto manda el correo al paciente
$("#frm_manda_acceso").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: "Correo",
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: "POST",
    dataType: "json",
    success: function (json) {
      respuesta(json);
    },
  });
});

$(document).ready(function () {
  n();
  var t = new Shepherd.Tour({
    defaultStepOptions: {
      cancelIcon: {
        enabled: !0,
      },
      classes: "tour-container",
      scrollTo: {
        behavior: "smooth",
        block: "center",
      },
    },
  });

  function n() {
    window.resizeEvt,
      576 < $(window).width()
        ? $("#tour").on("click", function () {
            clearTimeout(window.resizeEvt), t.start();
          })
        : $("#tour").on("click", function () {
            clearTimeout(window.resizeEvt),
              t.cancel(),
              (window.resizeEvt = setTimeout(function () {
                alert("Tour only works for large screens!");
              }, 250));
          });
  }
  t.addStep({
    title: "Escalas grises",
    text: "Al seleccionar pude aumentar o disminuir la intensidad de la imagen, dando Click sobre la imagen mas subir o bajar el cursor",
    attachTo: {
      element: ".check-escalas",
      on: "left",
    },
    buttons: [
      {
        action: function () {
          return this.cancel();
        },
        classes: "btn primario mt-2",
        text: "Salir",
      },
      {
        action: function () {
          return this.next();
        },
        classes: "btn primario mt-2",
        text: "Siguiente",
      },
    ],
    id: "welcome",
  }),
    t.addStep({
      title: "Rotar imagen",
      text: "al dar Click puedes rotar la imagen 90°",
      attachTo: {
        element: "#noventaGrados",
        on: "top",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Rotar imagen",
      text: "al dar Click puedes rotar la imagen 180°",
      attachTo: {
        element: "#ciento_ochenta",
        on: "top",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Rotar imagen",
      text: "al dar Click puedes rotar la imagen 270°",
      attachTo: {
        element: "#docientos_setenta",
        on: "top",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Rotar imagen",
      text: "al dar Click puedes abrir la imagen dicom en pantalla completa, para salir de este modo Presionar ESC",
      attachTo: {
        element: "#trecientos",
        on: "top",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Rotar imagen",
      text: "al dar Click puedes rotar la imagen 360°",
      attachTo: {
        element: "#full_pantalla",
        on: "top",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Zoom a imagen dicom",
      text: "Hacer Scroll sobre la imagen para aumentar o disminuir",
      attachTo: {
        element: "#dicomImage",
        on: "left",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Siguiente",
        },
      ],
    }),
    t.addStep({
      title: "Tutorial concluido",
      text: "el tutorial finalizo",
      attachTo: {
        element: ".fixed-action-btn .btn-floating",
        on: "left",
      },
      buttons: [
        {
          action: function () {
            return this.cancel();
          },
          classes: "btn primario mt-2",
          text: "Salir",
        },
        {
          action: function () {
            return this.back();
          },
          classes: "btn primario mt-2",
          text: "Anterior",
        },
        {
          action: function () {
            return this.next();
          },
          classes: "btn primario mt-2",
          text: "Finalizar",
        },
      ],
      modalOverlayOpeningPadding: "10",
    }),
    $(window).resize(n);
});

function prosponer_interpretacion(ruta) {
  ////////////////////////////////////////////////////////////////////////////////////////////
  let id_toma_muestra = document.getElementById("toma").value;
  let id_contenido = document.getElementById("id_contenido").value;
  let bandera = 0;

  /** status
   *  Con esto sabemos el status de la toma
   * 2 finalizado
   * 3 no se concluyo
   * La validacion se hacer si el objeto no tiene ya elementos es 3 o si tiene mas de uno es 3
   */

  let status = 0;
  if (JsonEstudios.length > 0) {
    bandera = 1;
    status = 3;
  } else {
    status = 2;
  }
  var data = new FormData();
  data.append("interpretacion", interpretacion);
  data.append("id_contenido", id_contenido);
  data.append("bandera", bandera);
  //data.append("JsonEstudios", JsonEstudios);
  data.append("JsonEstudios_Str", JsonEstudios_Str);
  data.append("id_toma_muestra", id_toma_muestra);
  data.append("status", status);

  //console.log(data)

  /***
   * 
   * {
      interpretacion,
      id_contenido,
      bandera,
      JsonEstudios,
      JsonEstudios_Str,
      id_toma_muestra,
      status
    }
   * 
   */

  $.ajax({
    url: `${ruta}/Postpone`,
    data: data,
    type: "POST",
    processData: false, // tell jQuery not to process the data
    contentType: false,
    // dataType: "json",
    success: (json) => {
      console.log(json);
      /*let { msg, status, url, log } = json;      
      swal({
        text: `${msg}`,
        icon: `${status}`,
        dangerMode: true,
      }).then((willDelete) => {
        window.location.href = url;
      });*/
    },
    error: (err) => {
      console.log(err);
    },
  });
  /////////////////////////////////////////////////////////////////////////////////////////////
}
