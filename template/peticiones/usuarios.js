
$( "#frm_usuarios" ).submit(function( event ) {
  event.preventDefault();
  let nombre = document.getElementById('nombre').value;
	let apellido_p = document.getElementById('apellido_p').value;
	let apellido_m = document.getElementById('apellido_m').value;	
	let id_empleado = document.getElementById('id_empleado').value;
	let udn = document.getElementById('udn').value;
	//hacemos la validacion solo de la informacion del empleado
	if (requiered(nombre,'Nombre')&&requiered(apellido_p,'Apellido paterno')&&requiered(apellido_m,'Apellido materno')&&requiered(id_empleado,'Id empleado')&&requiered(udn,'UDN')){
		let usser = document.getElementById('usser').value;
		let pass = document.getElementById('pass').value;
		let privilegio = document.getElementById('privilegio').value;
		//hacemos la validacion de informacion de usuario
		if (requiered(usser,'Usuario')&&requiered(pass,'Contraseña')&&requiered(privilegio,'Privilegio')) {			
      $.ajax({
        url: 'Usuarios/Save',         
        data: new FormData(this),        
        cache: false,    
        contentType: false,    
        processData: false,
        type: 'POST',    
        dataType : 'json',
        success:  (json)=> {
          console.log(json)          
          respuesta(json);              
          setTimeout(() => {
            location.reload();         
          }, 2500);
        },
        error:(err)=>{
          swal('Advertencia',`El usuario ya existe o la contraseña intente con otra`,'warning');
          console.log(err);
          
        }
      });
		}
  }
});

function nuevo_usuario() {	
  var formname = document.getElementById('frm_usuarios');             
  var FormData = new FormData(formname);
                  
  document.getElementById('boton').disabled=true;
	let nombre = document.getElementById('nombre').value;
	let apellido_p = document.getElementById('apellido_p').value;
	let apellido_m = document.getElementById('apellido_m').value;
	let cedula = document.getElementById('cedula').value;
	let id_empleado = document.getElementById('id_empleado').value;
	let udn = document.getElementById('udn').value;
	//hacemos la validacion solo de la informacion del empleado
	if (requiered(nombre,'Nombre')&&requiered(apellido_p,'Apellido paterno')&&requiered(apellido_m,'Apellido materno')&&requiered(cedula,'No. cedula')&&requiered(id_empleado,'Id empleado')&&requiered(udn,'UDN')){
		let usser = document.getElementById('usser').value;
		let pass = document.getElementById('pass').value;
		let privilegio = document.getElementById('privilegio').value;
		//hacemos la validacion de informacion de usuario
		if (requiered(usser,'Usuario')&&requiered(pass,'Contraseña')&&requiered(privilegio,'Privilegio')) {
			$.ajax({    
        url : 'Usuarios/Save',    
        data : {
					nombre,apellido_p,apellido_m,cedula,id_empleado,udn,usser,pass,privilegio 
				},
        type : 'POST',
        dataType : 'json',
        success : function(json) {          
          console.log(json);              
              
            document.getElementById('nombre').value='';
            document.getElementById('apellido_p').value='';
            document.getElementById('apellido_m').value='';
            document.getElementById('cedula').value='';
            document.getElementById('id_empleado').value='';
            document.getElementById('udn').value='';                      
            document.getElementById('usser').value='';
            document.getElementById('pass').value='';
            document.getElementById('privilegio').value='';        
            document.getElementById('boton').disabled=false;
          }        
      });
      
      
		}
  }   
}


function requiered(item,campo) {
	if(item===''){
		swal('Advertencia',`El campo ${campo} no puede estar vacio`,'warning');
		return false;
	}else{
		return true;
	}	
}

function pruebaa(){
  var formData = new FormData($("#frmNuevoGrupo")[0]);
  $.ajax({    
    url : 'Toma_Muestra/Save',
    data : {
      formData
    },
    type : 'POST',
    dataType : 'json',
    success : function(json) {          
      console.log(json);               
    }        
  });
}

function quitar_Hijo(lista,id) {  
  let padre = document.getElementById(lista);
  let hijo = document.getElementById(id);
  padre.removeChild(hijo)
}

function nuevaCedula(event) {
  event.preventDefault()
  swal("Escribir cedula", {
    content: "input",
  })
  .then((value) => {
    if (value!='') {

      if (document.getElementById(`item_${value}`)) {
        swal("Atención", "Este numero de cedula ya existe", "warning");         
      }else{
        $("#lista_cedulas").append(`
        <li class="collection-item" id="item_${value}">
          ${value}
          <input type="hidden" name="cedula[]" value="${value}" /> 

          <button style="width: 21px;height: 21px;" class="btn-floating mb-1 waves-effect waves-light right" onclick="quitar_Hijo('lista_cedulas','item_${value}')">
            <i style="display: block;margin-top: -8px;font-size: 15px;" class="material-icons">clear</i>
          </button>      
        </li>        
      `);
      }      
    }else{      
      swal("Atención", "No escribiste", "warning"); 
    }
    
  });
}

/*


*/

