$("#actualiza_perfil").submit(function (event) {
  event.preventDefault();
  $.ajax({
    url: 'Perfil/Update',
    data: new FormData(this),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de actualizar perfil`, 'warning');      
    }
  });
});

$('#acta_nacimiento').on('change', (event)=>{
  event.preventDefault();
  $.ajax({
    url: 'Perfil/Update_acta_nacimiento',
    data: new FormData(document.getElementById('acta_de_nacimiento')),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);      
      document.getElementById('acta_nacimiento').value=null;
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de actualizar acta de nacimiento`, 'warning');
      document.getElementById('acta_nacimiento').value=null;   
      console.log(err);
    }
  });
});

$('#curp').on('change', (event)=>{
  event.preventDefault();
  $.ajax({
    url: 'Perfil/Update_curp',
    data: new FormData(document.getElementById('curp_form')),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      document.getElementById('curp').value=null;
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de actualizar curp`, 'warning');      
      document.getElementById('curp').value=null;
      console.log(err)
    }
  });
});

$('#cedu').on('change', (event)=>{
  event.preventDefault();
  $.ajax({
    url: 'Perfil/Cedula',
    data: new FormData(document.getElementById('formulario')),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      document.getElementById('cedu').value=null;
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de agregar cedula`, 'warning');      
      document.getElementById('cedu').value=null;      
    }
  });
});

$('#cv').on('change', (event)=>{
  event.preventDefault();
  $.ajax({
    url: 'Perfil/Cv',
    data: new FormData(document.getElementById('cv_form')),
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {      
      respuesta(json);
      document.getElementById('cedu').value=null;
    },
    error: (err) => {
      swal('Advertencia', `Ocurrió algún error al momento de agregar cedula`, 'warning');      
      document.getElementById('cedu').value=null;      
    }
  });
});

function traerUsuario(ruta) {  
  let rutacedula=ruta;
  $.ajax({
    url: 'Perfil/Get',
    cache: false,
    contentType: false,
    processData: false,
    type: 'POST',
    dataType: 'json',
    success: (json) => {         
      let {
        personal: {
          apellido_materno,
          apellido_paterno,
          foto,
          nombre,curp,acta_nacimiento,cv
        }
      } = json;      
      document.getElementById('nombre').value=nombre;    
      document.getElementById('nombre').focus();  
      document.getElementById('apellido_paterno').value=apellido_paterno;
      document.getElementById('apellido_paterno').focus();
      document.getElementById('apellido_materno').value=apellido_materno;          
      document.getElementById('apellido_materno').focus();
      if (foto!='') {
        document.getElementById("ver_foto").src=`${ruta}/uploads/fotos_perfil/${foto}`;
      }      
      let ruta2=`${ruta}/uploads/documentos`; 
      if (acta_nacimiento!=''&&acta_nacimiento!=null) {
        CrearTarjetaDocumento('Acta de nacimiento',`${ruta2}/actas_nacimiento/${acta_nacimiento}`);  
      }
      if (curp!=''&&curp!=null) {
        CrearTarjetaDocumento('Curp',`${ruta2}/curps/${curp}`);  
      }
      if (cv!=''&&cv!=null) {
        CrearTarjetaDocumento('CV',`${ruta2}/cv/${cv}`);  
      }                        
      $.ajax({
        url: 'Perfil/GetCelulas',
        type: 'POST',
        dataType: 'json',
        success: (json) => {  
          console.log(json);   
          let ruta3=`${rutacedula}/uploads/documentos/cedulas`; 
          json.map((item,key)=>{
            let {nombre}= item;
            cedulas_card(`Célula ${key+1}`,`${rutacedula}uploads/documentos/cedulas/${nombre}`);            
          })          
        },
        error: (err) => {
          swal('Advertencia', `El usuario ya existe o la contraseña intente con otra`, 'warning');          
        }
      });      
    },
    error: (err) => {
      swal('Advertencia', `El usuario ya existe o la contraseña intente con otra`, 'warning');      
    }
  });
}

function CrearTarjetaDocumento(descripcion,ruta='') {
  
  let tarjeta=` <div class="col s6 m4  ">
                  <div class="card ">
                    <div class="card-image">                                                
                      <a class="btn-floating halfway-fab waves-effect waves-light primario" onclick="abrir_modal('${ruta}')"> 
                        <i class="material-icons">remove_red_eye</i>
                      </a>
                    </div>
                    <div class="card-content">
                      <p>
                        ${descripcion}
                      </p>
                    </div>
                  </div>
                </div>`;
  $('#tarjeta').append(tarjeta);
}

function abrir_modal(ruta) {
  console.log(ruta)
  $('#modal').modal('open');
  let frame=`
      <iframe src="${ruta}" frameborder="0" allowfullscreen="true" 
                                            webkitallowfullscreen="true" 
                                            mozallowfullscreen="true" 
                                            style="top: 0px; left: 0px; 
                                            width: 100%; height: 100%; 
                                            position: absolute;">
      </iframe>`
  $('#panel_documentos').append(frame);
}

function cedulas_card(descripcion,ruta) {
  // console.log(ruta);
  let tarjeta=` <div class="col s6 m4  ">
                  <div class="card ">
                    <div class="card-image">                                                
                      <a class="btn-floating halfway-fab waves-effect waves-light primario" onclick="abrir_modal('${ruta}')"> 
                        <i class="material-icons">remove_red_eye</i>
                      </a>
                    </div>
                    <div class="card-content">
                      <p>
                        ${descripcion}
                      </p>
                    </div>
                  </div>
                </div>`;
  $('#cedulas_card').append(tarjeta);  
}

function F5() {
  location.reload();
}