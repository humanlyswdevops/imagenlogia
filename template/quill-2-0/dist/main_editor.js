Quill.register({
  'modules/tableUI': quillTableUI.default
}, true)


var snow = new Quill('#snow-container', {
  theme: 'snow',
  modules: {
    table: true,
    tableUI: true
  }
});

const table = snow.getModule('table');

document.querySelector('#insert-table').addEventListener('click', function() {
  table.insertTable(9, 9);
});